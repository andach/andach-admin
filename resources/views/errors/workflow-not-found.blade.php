@extends('adminlte::page')

@section('title', 'Error - Workflow not Found')

@section('content')
<div class="info-box">
    <span class="info-box-icon bg-danger"><i class="fas fa-exclamation-triangle"></i></span>

    <div class="info-box-content">
        <span class="info-box-title">Error - Workflow Not Found</span>
    </div>
</div>

<div class="card card-danger">
    <div class="card-body">
        This error means that no workflow has been set up for the system you are trying to access. 
        This can be managed only by an administrator - if you can see a "Workflow" menu option on the left, you have permission to set this up. 
    </div>
    <div class="card-footer">This error has not been logged.</div>
</div>
@endsection