@extends('adminlte::page')

@section('title', 'All My Complaints')

@section('content_header')
    <h1>My Complaints</h1>
@stop

@section('content')
    <div class="card card-dark">
        <div class="card-header">All My Complaints</div>
        <div class="card-body">
            @if (count($complaints))
                <table id="myTable" style="width: 100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Status</th>
                            <th>Open?</th>
                            <th>No. Comments</th>
                            <th>Assigned To</th>
                            <th>Created At</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($complaints as $complaint)
                        <tr>
                            <td>{{ $complaint->id }}</td>
                            <td>
                                <a href="{{ route('complaint.edit', $complaint->id) }}">
                                    {{ $complaint->name }}
                                </a>
                            </td>
                            <td>{{ $complaint->current_status }}</td>
                            <td>{!! $complaint->openTickOrCross !!}</td>
                            <td>{{ $complaint->number_of_comments }}</td>
                            <td>{{ $complaint->assigned_to_name }}</td>
                            <td>{{ $complaint->created_at }}</td>
                        </td>
                    @endforeach
                    </tbody>
                </table>
            @else
                <p>There are no results.</p>
            @endif
        </div>
    </div>
@endsection
