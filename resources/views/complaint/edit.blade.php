@extends('adminlte::page')

@section('plugins.Select2', true)

@section('title', 'Edit Complaint')

@section('content_header')
    <h1>Edit Complaint</h1>
@stop

@section('content')
    {{ Form::model($complaint, ['route' => ['complaint.edit-post', $complaint->id], 'method' => 'post', 'files' => true]) }}
        <div class="row">
            <div class="col-3">
                @include('component.customer.card', ['customer' => $complaint->customer])
            </div>
            <div class="col-9">
                @include('component.workflight.editcomments', ['workflight' => $complaint->workflight, 'workflowable' => $complaint])
            </div>
        </div>

        <!-- accordion card -->

        <p>
            <button class="form-control btn" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                Edit Main Complaint Details
            </button>
        </p>

        <div class="collapse" id="collapseExample">
            <div class="row">
                <div class="col-12">
                    <div class="card card-dark">
                        <div class="card-header">Main Complaint Details</div>
                        <div class="card-body">
                            {{ Form::bsText('Title', 'name') }}
                            {{ Form::bsSelect('Customer', 'customer_id', $customers, null, ['placeholder' => '']) }}
                            {{ Form::bsSelect('Company', 'company_id', $companies, null, ['disabled' => 'disabled']) }}
                            {{ Form::bsSelect('Location', 'location_id', $locations) }}

                            @if (count($udfs))
                                @foreach ($udfs as $udf)
                                    {!! $udf !!}
                                @endforeach
                            @endif

                            {{ Form::bsSelect('Assign to User', 'user_id', $users, $complaint->workflight->assigned_to_user_id, ['placeholder' => '&nbsp;', 'helpBlock' => 'Pick the user you want to assign this complaint to (optional). Only users who can see the complaints section will be available. Assigning a complaint to a user will not stop others being able to see it.']) }}
                            {{ Form::bsTextarea('Description', 'description') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{ Form::bsSubmit('Update Complaint') }}
    {{ Form::close() }}
@endsection

@section('javascript')
    <script>
        $(document).ready(function() {
            $('select[name="customer_id"]').select2();
            $('select[name="user_id"]').select2();
        });
    </script>
@endsection