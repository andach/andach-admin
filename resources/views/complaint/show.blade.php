@extends('adminlte::page')

@section('title', 'Show Complaint')

@section('content_header')
    <h1>Complaint #{{ $complaint->id }} - {{ $complaint->name }}</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-header">Main Complaint Details</div>
        <div class="card-body">
            <div class="row">
                <div class="col-3">State</div>
                <div class="col-9">{{ $complaint->workflight->state->name }}</div>
                <div class="col-3">Customer</div>
                <div class="col-9">{{ $complaint->customer_name }}</div>
                <div class="col-3">Company</div>
                <div class="col-9">{{ $complaint->company->name }}</div>
                <div class="col-3">Location</div>
                <div class="col-9">{{ $complaint->location_name }}</div>

                @foreach ($udfs as $displayName => $value)
                    <div class="col-3">{{ $displayName }}</div>
                    <div class="col-9">{!! nl2br(e($value)) !!}</div>
                @endforeach

                <div class="col-12">{!! nl2br(e($complaint->description)) !!}
            </div>
        </div>
    </div>

    @include('component.workflight.showcomments', ['workflight' => $complaint->workflight])
@endsection
