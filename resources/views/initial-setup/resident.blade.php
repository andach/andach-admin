@extends('adminlte::page')

@section('plugins.Select2', true)

@section('title', 'Initial Setup - Residents')

@section('content_header')
    <h1>Initial Setup - Residents</h1>
@stop

@section('content')
    {{ Form::open(['route' => 'initial-setup.resident-post', 'method' => 'post']) }}
    <div class="row">
        <div class="col-12">
            <div class="card card-dark">
                <div class="card-header">Residents Setup</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-3">Room Number</div>
                        <div class="col-3">Parent Location</div>
                        <div class="col-3">Resident Name</div>
                        <div class="col-3">Gender</div>
                        @foreach ($rooms as $room)
                            <div class="col-3">{{ $room->name }}</div>
                            <div class="col-3">{{ $room->parent->name }}</div>
                            <div class="col-3">{{ Form::text('name['.$room->id.']', null, ['class' => 'form-control']) }}</div>
                            <div class="col-3">{{ Form::select('gender['.$room->id.']', $genders, null, ['class' => 'form-control']) }}</div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{ Form::bsSubmit('Enter Resident Data') }}

    {{ Form::close() }}
@endsection