@extends('adminlte::page')

@section('plugins.Select2', true)

@section('title', 'Initial Setup - Locations')

@section('content_header')
    <h1>Initial Setup - Locations</h1>
@stop

@section('content')
    {{ Form::open(['route' => 'initial-setup.location-post', 'method' => 'post']) }}
    <div class="row">
        <div class="col-12">
            <div class="card card-dark">
                <div class="card-header">Locations Setup</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-2"><input type="radio" name="setupType" value="numbers" id="setupType" class="form-control" /></div>
                        <div class="col-10">Select if your home just has rooms ordered by number with no gaps in them at all, starting at Room 1.</div>
                        
                        <div class="col-2"><input type="radio" name="setupType" value="wings" id="setupType" class="form-control" /></div>
                        <div class="col-10">Select if your home has named wings / corridors in which there are different rentable rooms.</div>

                        <div class="col-2"><input type="radio" name="setupType" value="manual" id="setupType" class="form-control" /></div>
                        <div class="col-10">Select if your home does not have wings or corridors, but has named rooms, gaps in numbers, or rooms that are not numeric like 7A or 12B.</div>
                    </div>
                </div>
                <div class="card-footer">Nothing showing up below? Change the radio button to another option, then change it back again.</div>
            </div>
        </div>

        <div class="col-12 showhide numbers" style="display:none">
            <div class="card card-dark">
                <div class="card-header">Setup Rooms with Numbers</div>
                <div class="card-body">
                    {{ Form::bsNumber('Number of Rooms', 'number_of_rooms') }}
                    {{ Form::bsSubmit('Create Rooms') }}
                </div>
            </div>
        </div>

        <div class="col-12 showhide wings" style="display:none">
            <div class="card card-dark">
                <div class="card-header">Setup Rooms with Wings or Corridors</div>
                <div class="card-body">
                    {{ Form::bsTextarea('Enter the names of your wings and rooms, separated by commas, one room per line', 'wing_names') }}
                    {{ Form::bsSubmit('Create Rooms and Wings / Corridors') }}
                </div>
                <div class="card-footer">
                    <p>For example, you might enter:</p>

                    <p>Red Wing,Room 1<br />
                    Red Wing,Room 2<br />
                    Red Wing,Room 3<br />
                    Blue Wing,Room 4<br />
                    Blue Wing,Room 4a<br />
                    Blue Wing,Room 5</p>
                </div>
            </div>
        </div>

        <div class="col-12 showhide manual" style="display:none">
            <div class="card card-dark">
                <div class="card-header">Setup Rooms Manually</div>
                <div class="card-body">
                    {{ Form::bsTextarea('Enter the names of your rooms, one per line', 'manual_names') }}
                    {{ Form::bsSubmit('Create Rooms and Wings / Corridors') }}
                </div>
                <div class="card-footer">
                    <p>For example, you might enter:</p>

                    <p>Room 1<br />
                    Room 2<br />
                    Room 3<br />
                    Room 4<br />
                    Room 4a<br />
                    Room 5</p>
                </div>
            </div>
        </div>
    </div>

    {{ Form::close() }}
@endsection

@section('js')
<script>
$(document).ready(function() {
    $('input[type="radio"]').click(function() {
        var inputValue = $(this).attr("value");
        var targetBox = $("." + inputValue);
        $(".showhide").not(targetBox).hide();
        $(targetBox).show();
    });

    $('#newRoom').click(function() {
        alert(this);
    });

    
    $('#newWing').click(function() {
        var newcontent = document.createElement('div');

        var element = document.getElementById("wingsDiv");
        string = "<div class='col-5'><input type='text' name='wing[]' class='form-control' /></div><div class='col-7'><input type='text' name='room[]' class='form-control' /></div>";
        element.innerHTML = element.innerHTML + string;
    });
});

{{-- function start() {
    var myspan = document.getElementById("myspan");
    myspan.onclick = function() { alert ("hi"); };

    var mydiv = document.getElementById("mydiv");
    var newcontent = document.createElement('div');
    newcontent.innerHTML = "bar";

    while (newcontent.firstChild) {
        mydiv.appendChild(newcontent.firstChild);
    }
} --}}
</script>
@endsection