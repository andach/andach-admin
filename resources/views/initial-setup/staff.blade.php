@extends('adminlte::page')

@section('plugins.Select2', true)

@section('title', 'Initial Setup - Staff')

@section('content_header')
    <h1>Initial Setup - Staff</h1>
@stop

@section('content')
    <div class="info-box">
        <span class="info-box-icon bg-info"><i class="fas fa-info-circle"></i></span>

        <div class="info-box-content">
            <span class="info-box-title">Creating your Staff Members</span>
            <span class="info-box-text">This page contains enough space to enter 60 initial members of staff, enough for the majority of care homes. If you have more than this, more
                users can be entered later. Any staff member with an email will be able to login, and their initial password will be set to "password" - which will have to be changed
                immediately on their first login. Any rows without a name entered in them will be ignored. </span>
        </div>
    </div>

    {{ Form::open(['route' => 'initial-setup.staff-post', 'method' => 'post']) }}
    <div class="row">
        <div class="col-12">
            <div class="card card-dark">
                <div class="card-header">Staff Setup</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-3">Name</div>
                        <div class="col-3">Email</div>
                        <div class="col-3">Mobile Number</div>
                        <div class="col-3">Gender</div>
                        @for ($i = 0; $i < 60; $i++)
                            <div class="col-3">{{ Form::text('name['.$i.']', null, ['class' => 'form-control']) }}</div>
                            <div class="col-3">{{ Form::text('email['.$i.']', null, ['class' => 'form-control']) }}</div>
                            <div class="col-3">{{ Form::text('mobile_number['.$i.']', null, ['class' => 'form-control']) }}</div>
                            <div class="col-3">{{ Form::select('gender['.$i.']', $genders, null, ['class' => 'form-control']) }}</div>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{ Form::bsSubmit('Enter Resident Data') }}

    {{ Form::close() }}
@endsection