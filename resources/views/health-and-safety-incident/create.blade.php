@extends('adminlte::page')

@section('plugins.Select2', true)

@section('title', 'Create Health and Safety Incident')

@section('content_header')
    <h1>Create Health and Safety Incident</h1>
@stop

@section('content')
    {{ Form::open(['route' => 'health-and-safety-incident.create-post', 'method' => 'post']) }}
    <div class="card card-dark">
        <div class="card-header">Main Health and Safety Incident Details</div>
        <div class="card-body">
            {{ Form::bsText('Title', 'name') }}
            {{ Form::bsSelect('Company', 'company_id', $companies) }}
            {{ Form::bsSelect('Location', 'location_id', $locations) }}

            @if (count($udfs))
                @foreach ($udfs as $udf)
                    {!! $udf !!}
                @endforeach
            @endif

            {{ Form::bsTextarea('Description', 'description') }}
            {{ Form::bsSelect('Assign to User', 'user_id', $users, null, ['placeholder' => '', 'helpBlock' => 'Pick the user you want to assign this complaint to (optional). Only users who can see the complaints section will be available. Assigning a complaint to a user will not stop others being able to see it.']) }}
            @if ($instantlyClose)
                {{ Form::bsCheckbox('Instantly Close?', 'instantly_close', 1, false, ['helpBlock' => 'Tick this to instantly mark this complaint as resolved as no further action is required.'])}}
            @endif
        </div>
    </div>

    <div class="card card-dark">
        <div class="card-header">Categorisation</div>
        <div class="card-body">
            {{ Form::bsSelect('Kind of Accident', 'enum_kind_of_accident', $enumKind, null, ['placeholder' => '']) }}
            {{ Form::bsSelect('Main Factor', 'enum_main_factor', $enumFactor, null, ['placeholder' => '']) }}
            {{ Form::bsSelect('Work Process', 'enum_work_process', $enumProcess, null, ['placeholder' => '']) }}
        </div>
        <div class="card-footer">These categories align with those used by the Health and Safety Executive.</div>
    </div>

    {{ Form::bsSubmit('Create Complaint') }}
    {{ Form::close() }}
@endsection

@section('javascript')
    <script>
        $(document).ready(function() {
            $('select[name="customer_id"]').select2();
            $('select[name="user_id"]').select2();
        });
    </script>
@endsection
