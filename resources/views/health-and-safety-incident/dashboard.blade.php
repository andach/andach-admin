@extends('adminlte::page')

@section('plugins.Datatables', true)
@section('plugins.Chartjs', true)

@section('title', 'Health and Safety Incidents Dashboard')

@section('content_header')
    <h1>Health and Safety Incidents Dashboard</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-4"><canvas id="open_complaints_by_age" width="400" height="400"></canvas></div>
        <div class="col-4"><canvas id="open_complaints_by_customer" width="400" height="400"></canvas></div>
        <div class="col-4"><canvas id="all_complaints_by_month" width="400" height="400"></canvas></div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $('#myTable').DataTable();
        });

        @foreach ($charts as $chart)
            {!! $chart->javascript() !!}
        @endforeach
    </script>
@endsection
