@extends('adminlte::page')

@section('plugins.Select2', true)

@section('title', 'Edit Incident')

@section('content_header')
    <h1>Edit Incident</h1>
@stop

@section('content')
    {{ Form::model($incident, ['route' => ['health-and-safety-incident.edit-post', $incident->id], 'method' => 'post', 'files' => true]) }}
        <div class="row">
            <div class="col-12">
                @include('component.workflight.editcomments', ['workflight' => $incident->workflight, 'workflowable' => $incident])
            </div>
        </div>

        <!-- accordion card -->

        <p>
            <button class="form-control btn" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                Edit Main Incident Details
            </button>
        </p>

        <div class="collapse" id="collapseExample">
            <div class="row">
                <div class="col-12">
                    <div class="card card-dark">
                        <div class="card-header">Main Incident Details</div>
                        <div class="card-body">
                            {{ Form::bsText('Title', 'name') }}
                            {{ Form::bsSelect('Company', 'company_id', $companies, null, ['disabled' => 'disabled']) }}
                            {{ Form::bsSelect('Location', 'location_id', $locations) }}

                            @if (count($udfs))
                                @foreach ($udfs as $udf)
                                    {!! $udf !!}
                                @endforeach
                            @endif

                            {{ Form::bsSelect('Assign to User', 'user_id', $users, $incident->workflight->assigned_to_user_id, ['placeholder' => '&nbsp;', 'helpBlock' => 'Pick the user you want to assign this incident to (optional). Only users who can see the incidents section will be available. Assigning a incident to a user will not stop others being able to see it.']) }}
                            {{ Form::bsTextarea('Description', 'description') }}
                            
                            {{ Form::bsSelect('Kind of Accident', 'enum_kind_of_accident', $enumKind, null, ['placeholder' => '']) }}
                            {{ Form::bsSelect('Main Factor', 'enum_main_factor', $enumFactor, null, ['placeholder' => '']) }}
                            {{ Form::bsSelect('Work Process', 'enum_work_process', $enumProcess, null, ['placeholder' => '']) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card card-dark">
            <div class="card-header">Injuries Caused by this Incident</div>
            <div class="card-body">
                <div class="row">
                    @if (count($incident->injuries))
                        <div class="col-3">Name</div>
                        <div class="col-3">Where Injured?</div>
                        <div class="col-4">Off Work Notes</div>
                        <div class="col-1">Edit</div>
                        <div class="col-1">Delete?</div>

                        @foreach ($incident->injuries as $injury)
                            <div class="col-3">{{ $injury->name }}</div>
                            <div class="col-3">{{ $injury->where_injured }}</div>
                            <div class="col-4">{{ $injury->off_work_notes }}</div>
                            <div class="col-1"><a href="{{ route('health-and-safety-incident.edit-injury', [$incident->id, $injury->id]) }}">Edit</a></div>
                            <div class="col-1">XXX</div>
                        @endforeach
                    @else
                        <div class="col-12">No injuries logged.</div> 
                    @endif

                    <div class="col-12"><a href="{{ route('health-and-safety-incident.create-injury', $incident->id) }}">Log an injury</a></div>
                </div>
            </div>
        </div>


        {{ Form::bsSubmit('Update Incident') }}
    {{ Form::close() }}
@endsection

@section('javascript')
    <script>
        $(document).ready(function() {
            $('select[name="customer_id"]').select2();
            $('select[name="user_id"]').select2();
        });
    </script>
@endsection