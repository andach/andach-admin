@extends('adminlte::page')

@section('plugins.Datatables', true)
@section('plugins.Chartjs', true)

@section('title', 'All Health and Safety Incidents')

@section('content_header')
    <h1>All Health and Safety Incidents</h1>
@stop

@section('content')
    <div class="card card-dark">
        <div class="card-header">Search</div>
        <div class="card-body">
            {{ Form::open(['route' => 'health-and-safety-incident.index', 'method' => 'get']) }}
                {{ Form::bsText('ID', 'id', $defaults['id']) }}
                {{ Form::bsText('Title', 'name', $defaults['name']) }}
                {{ Form::bsText('Description', 'description', $defaults['description']) }}
                {{ Form::bsSelect('Open/Closed', 'open_closed', ['open' => 'Open', 'closed' => 'Closed'], $defaults['open_closed'], ['placeholder' => '']) }}
                {{ Form::bsSelect('Specific State(s)', 'states_ids[]', $states, $defaults['states_ids'], ['placeholder' => '', 'multiple' => 'multiple']) }}
                {{ Form::bsSelect('Assigned To User(s)', 'users[]', $users, $defaults['users'], ['placeholder' => '', 'multiple' => 'multiple']) }}
                {{ Form::bsCheckbox('View Comment Summary?', 'comments', 1, $defaults['comments']) }}
                {{ Form::submit('Search Health and Safety Incidents') }}
            {{ Form::close() }}
        </div>
    </div>

    <div class="card card-dark">
        <div class="card-header">Results</div>
        <div class="card-body">
            @if (count($incidents))
                <table id="myTable" style="width: 100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Status</th>
                            <th>Open?</th>
                            <th>No. Comments</th>
                            <th>Assigned To</th>
                            <th>Created At</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($incidents as $incident)
                        <tr>
                            <td>{{ $incident->id }}</td>
                            <td>
                                <a href="{{ route('health-and-safety-incident.edit', $incident->id) }}">
                                    {{ $incident->name }}
                                </a>
                            </td>
                            <td>{{ $incident->current_status }}</td>
                            <td>{!! $incident->openTickOrCross !!}</td>
                            <td>{{ $incident->number_of_comments }}</td>
                            <td>{{ $incident->assigned_to_name }}</td>
                            <td>{{ $incident->created_at }}</td>
                        </tr>

                        @if ($defaults['comments'])
                            @foreach ($incident->workflight->comments as $comment)
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="4">{{ $comment->description }}</td>
                                    <td>{{ $comment->user->name }}</td>
                                    <td>{{ $comment->created_at }}</td>
                                </tr>
                            @endforeach
                        @endif
                    @endforeach
                    </tbody>
                </table>
            @else
                <p>There are no results.</p>
            @endif
        </div>
        <div class="card-footer">There are {{ count($incidents) }} results.</div>
    </div>
@endsection