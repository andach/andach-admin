@extends('adminlte::page')

@section('title', 'All My Health and Safety Incidents')

@section('content_header')
    <h1>My Health and Safety Incidents</h1>
@stop

@section('content')
    <div class="card card-dark">
        <div class="card-header">All My Health and Safety Incidents</div>
        <div class="card-body">
            @if (count($incidents))
                <table id="myTable" style="width: 100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Status</th>
                            <th>Open?</th>
                            <th>No. Comments</th>
                            <th>Assigned To</th>
                            <th>Created At</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($incidents as $incident)
                        <tr>
                            <td>{{ $incident->id }}</td>
                            <td>
                                <a href="{{ route('health-and-safety-incident.edit', $incident->id) }}">
                                    {{ $incident->name }}
                                </a>
                            </td>
                            <td>{{ $incident->current_status }}</td>
                            <td>{!! $incident->openTickOrCross !!}</td>
                            <td>{{ $incident->number_of_comments }}</td>
                            <td>{{ $incident->assigned_to_name }}</td>
                            <td>{{ $incident->created_at }}</td>
                        </td>
                    @endforeach
                    </tbody>
                </table>
            @else
                <p>There are no results.</p>
            @endif
        </div>
    </div>
@endsection
