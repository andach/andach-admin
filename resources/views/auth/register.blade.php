@extends('adminlte::page')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <!--
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        {{ Form::bsText('Name', 'name') }}
                        {{ Form::bsText('Email Address', 'email') }}
                        {{ Form::bsPassword('Password', 'password') }}
                        {{ Form::bsPassword('Confirm Password', 'password_confirmation') }}
                        {{ Form::bsSubmit('Register', 'btn-success') }}
                    </form>
                    -->
                    Registration is not available. An administrator will have to register you. 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
