@extends('adminlte::page')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        {{ Form::bsText('Email Address', 'email') }}
                        {{ Form::bsPassword('Password', 'password') }}
                        {{ Form::bsCheckbox('Remember Me?', 'remember') }}
                        {{ Form::bsSubmit('Login', 'btn-success') }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
