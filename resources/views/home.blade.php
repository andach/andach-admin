@extends('adminlte::page')

@section('title', 'Andach Admin')

@section('content_header')
    <h1>Andach Admin</h1>
@stop

@section('content')
    <div class="row">
        @foreach ($boxes as $box)
            <div class="col-lg-3 col-6">
                @include('component.small-box', ['box' => $box])
            </div>
        @endforeach
    </div>
@endsection
