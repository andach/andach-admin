@extends('adminlte::page')

@section('title', 'New Appointment')

@section('content_header')
    <h1>New Appointment</h1>
@stop

@section('content')
    {{ Form::open(['route' => 'appointment.create-post', 'method' => 'post']) }}
        <div class="row">
            <div class="col-12">
                <div class="card card-dark">
                    <div class="card-header">Employee</div>
                    <div class="card-body">
                        {{ Form::bsSelect('User', 'user_id', $users) }}
                    </div>
                    <div class="card-footer">
                        You should create the user first. Please <a href="{{ route('user.create') }}">click here to create a new user</a> then come back to this page to appoint them. 
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6">
                <div class="card card-dark">
                    <div class="card-header">Position (if it exists already)</div>
                    <div class="card-body">
                        {{ Form::bsSelect('Position', 'position_id', $positions, null, ['placeholder' => '']) }}
                    </div>
                </div>
            </div>
        
            <div class="col-12 col-md-6">
                <div class="card card-dark">
                    <div class="card-header">Or Create a New Position</div>
                    <div class="card-body">
                        {{ Form::bsSelect('Company', 'company_id', $companies) }}
                        {{ Form::bsSelect('Job', 'job_id', $jobs) }}
                        {{ Form::bsSelect('Location', 'location_id', $locations) }}
                        {{ Form::bsSelect('Team', 'team_id', $teams) }}
                        <br />
                        {{ Form::bsText('Auth Hours per Week', 'hours_per_week') }}
                        {{ Form::bsSelect('Cost Code', 'cost_code_id', $costCodes) }}
                    </div>
                </div>
            </div>

            <div class="col-12">
                <div class="card card-dark">
                    <div class="card-header">Contract Details</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                {{ Form::bsSelect('Template', 'contact_template_id', $templates) }}
                                {{ Form::bsSelect('Working Time Pattern', 'working_time_pattern_id', $wtps) }}
                                {{ Form::bsDate('From Date', 'from_date') }}
                                {{ Form::bsDate('To Date', 'to_date') }}
                            </div>
                            <div class="col-12 col-md-6">
                                {{ Form::hidden('is_part_time', 0) }}
                                {{ Form::hidden('is_zero_hours', 0) }}
                                {{ Form::hidden('is_positive_pay', 0) }}
                                {{ Form::bsCheckbox('Is Part Time', 'is_part_time', 1) }}
                                {{ Form::bsCheckbox('Is Zero Hours', 'is_zero_hours', 1) }}
                                {{ Form::bsCheckbox('Is Positively Paid', 'is_positive_pay', 1) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12">
                <div class="card card-dark">
                    <div class="card-header">Pay and Salary Details</div>
                    <div class="card-body">
                        {{ Form::bsText('Pay per Hour', 'pay_per_hour') }}
                        {{ Form::bsText('Actual Salary', 'actual_salary') }}
                        {{ Form::bsText('FTE Salary', 'fte_salary') }}
                    </div>
                </div>
            </div>
        </div>

        {{ Form::bsSubmit('Add New Appointment') }}
    {{ Form::close() }}
@endsection
