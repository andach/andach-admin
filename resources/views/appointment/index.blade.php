@extends('adminlte::page')

@section('title', 'All Appointments')

@section('content_header')
    <h1>All Appointments</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-3">User</div>
        <div class="col-3">Position</div>
        <div class="col-3">Dates</div>
        <div class="col-2">Pay</div>
        <div class="col-1">Edit</div>
        @foreach ($appointments as $appointment)
            <div class="col-3"><a href="{{ route('user.show', $appointment->user->id) }}">{{ $appointment->user->name }}</a></div>
            <div class="col-3"><a href="{{ route('position.show', $appointment->position->id) }}">{{ $appointment->position->name }}</a></div>
            <div class="col-3">{{ $appointment->from_date }} to {{ $appointment->to_date }}</div>
            <div class="col-2">{!! $appointment->pay_formatted !!}</div>
            <div class="col-1"><a href="{{ route('appointment.edit', $appointment->id) }}">Edit</a></div>
        @endforeach
    </div>
@endsection
