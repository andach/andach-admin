@extends('adminlte::page')

@section('title', 'My Appointments')

@section('content_header')
    <h1>My Appointments</h1>
@stop

@section('content')
        <div class="card card-dark">
            <div class="card-header">My Appointments</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-5">Job</div>
                    <div class="col-3">Team</div>
                    <div class="col-2">From</div>
                    <div class="col-2">To</div>

                    @foreach ($appointments as $appointment)    
                        <div class="col-5">
                            <a href="{{ route('appointment.mine', $appointment->id) }}">
                                {{ $appointment->position->job->name }}
                            </a>
                        </div>
                        <div class="col-3">{{ $appointment->position->team->name }}</div>
                        <div class="col-2">{{ $appointment->from_date }}</div>
                        <div class="col-2">{{ $appointment->to_date }}</div>
                    @endforeach
                </div>
            </div>
        </div>

        {{ Form::bsSubmit('Update Job') }}
    {{ Form::close() }}
@endsection
