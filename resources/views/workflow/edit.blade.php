@extends('adminlte::page')

@section('title', 'Edit Workflow')

@section('content_header')
<h1>Edit Workflow</h1>
@stop

@section('content')
    {{ Form::model($workflow, ['route' => ['workflow.edit-post', $workflow->id], 'method' => 'post']) }}
        <div class="card card-dark">
            <div class="card-header">Workflow Details</div>
            <div class="card-body">
                {{ Form::bsText('Name', 'name') }}
                {{ Form::bsText('Model Name', 'model_name', null, ['disabled' => 'disabled']) }}
                {{ Form::bsNumber('Min Location Level', 'min_location_level', null, ['helpBlock' => 'Min location Level']) }}
                {{ Form::bsNumber('Max Location Level', 'max_location_level', null, ['helpBlock' => 'Min location Level']) }}
            </div>
        </div>

        <div class="card card-dark">
            <div class="card-header">Current States</div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <th>Default?</th>
                        <th>State for Instantly Closing?</th>
                        <th>Delete?</th>
                        <th>Name</th>
                    </thead>
                    <tbody>
                        @foreach ($workflow->states as $state)
                            <tr>
                                <td>{{ Form::radio('default_state_id', $state->id, $state->is_default, ['class' => 'form-control']) }}</td>
                                <td>{{ Form::radio('default_instant_close_state_id', $state->id, $state->default_instant_close_status, ['class' => 'form-control']) }}</td>
                                <td>{{ Form::checkbox('delete_states[]', $state->id, null, ['class' => 'form-control']) }}</td>
                                <td>{{ $state->name }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="card card-dark">
            <div class="card-header">New State</div>
            <div class="card-body">
                {{ Form::bsText('New State', 'new_state') }}
                {{ Form::hidden('new_state_is_closed', 0) }}
                {{ Form::bsCheckboxReverse('Does this state represent the item being closed/complete/final?', 'new_state_is_closed', 1) }}
                {{ Form::bsCheckboxReverse('Tick this box if the new state is to be the default. This will override the radio button above.', 'new_state_is_default', 1) }}
            </div>
        </div>

        <div class="card card-dark">
            <div class="card-header">Transitions</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-1">Delete?</div>
                    <div class="col-5">Name</div>
                    <div class="col-3">From State</div>
                    <div class="col-3">To State</div>
                    @foreach ($workflow->transitions as $transition)
                        <div class="col-1">{{ Form::checkbox('delete_transitions[]', $transition->id, null, ['class' => 'form-control']) }}</div>
                        <div class="col-5">{{ Form::text('transition_name['.$transition->id.']', $transition->name, ['class' => 'form-control']) }}</div>
                        <div class="col-3">{{ Form::select('from_state['.$transition->id.']', $workflow->states()->pluck('name', 'id'), $transition->workflow_state_from_id, ['class' => 'form-control']) }}</div>
                        <div class="col-3">{{ Form::select('to_state['.$transition->id.']', $workflow->states()->pluck('name', 'id'), $transition->workflow_state_to_id, ['class' => 'form-control']) }}</div>
                    @endforeach
                    <div class="col-12"><hr /></div>
                </div>

                <p>Add a new Transition</p>
                {{ Form::bsText('Name', 'new_transition_name') }}
                {{ Form::bsSelect('From State', 'new_transition_state_from_id', $workflow->states()->pluck('name', 'id')) }}
                {{ Form::bsSelect('To State', 'new_transition_state_to_id', $workflow->states()->pluck('name', 'id')) }}
            </div>
            <div class="card-footer">A <em>transition</em> is an allowed movement from state to state.</div>
        </div>

        {{ Form::bsSubmit('Update Workflow') }}
    {{ Form::close() }}
@endsection
