@extends('adminlte::page')

@section('title', 'Create Workflow')

@section('content_header')
    <h1>Create Workflow</h1>
@stop

@section('content')
    {{ Form::open(['route' => 'workflow.create-post', 'method' => 'post']) }}
        <div class="card card-dark">
            <div class="card-header">Workflow Details</div>
            <div class="card-body">
                {{ Form::bsText('Name', 'name') }}
                {{ Form::bsText('Model Name', 'model_name') }}
                {{ Form::bsNumber('Min Location Level', 'min_location_level', null, ['helpBlock' => 'Min location Level']) }}
                {{ Form::bsNumber('Max Location Level', 'max_location_level', null, ['helpBlock' => 'Min location Level']) }}
            </div>
        </div>
    {{ Form::bsSubmit('Create Workflow') }}
    {{ Form::close() }}
@endsection
