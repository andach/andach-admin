@extends('adminlte::page')

@section('title', 'All Workflows')

@section('content_header')
    <h1>All Workflows</h1>
@stop

@section('content')
    <div class="card card-dark">
        <div class="card-header">Workflow Details</div>
        <div class="card-body">
            @foreach ($workflows as $workflow)
                <p><a href="{{ route('workflow.edit', $workflow->id) }}">{{ $workflow->name }}</a></p>
            @endforeach  
        </div>
    </div>
@endsection
