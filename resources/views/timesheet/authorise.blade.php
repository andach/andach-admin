@extends('adminlte::page')

@section('title', 'Authorise Timesheets')

@section('content_header')
    <h1>Authorise Timesheets</h1>
@stop

@section('content')
    {{ Form::open(['route' => 'timesheet.authorise-post', 'method' => 'post']) }}
        @foreach ($timesheets as $timesheet)
        <div class="card card-dark">
            <div class="card-header">{{ $timesheet->appointment->user->name}}, {{ $timesheet->job->name }}</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-3">Date</div>
                    <div class="col-9">{{ $timesheet->date_of_timesheet }}</div>
                    <div class="col-3">Description</div>
                    <div class="col-9">{{ $timesheet->description }}</div>
                    <div class="col-3">Time</div>
                    <div class="col-9">{{ $timesheet->mins_logged }}</div>

                    <div class="col-12"><hr /></div>

                    <div class="col-3">Approve</div>
                    <div class="col-9">{{ Form::radio('approve_reject['.$timesheet->id.']', 1, true) }}</div>

                    <div class="col-3">Reject</div>
                    <div class="col-9">{{ Form::radio('approve_reject['.$timesheet->id.']', 0) }}</div>
                </div>
            </div>
        </div>
        @endforeach 

        {{ Form::bsSubmit('Authorise Timesheets') }}
    {{ Form::close() }}
@endsection
