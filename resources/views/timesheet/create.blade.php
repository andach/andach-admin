@extends('adminlte::page')

@section('title', 'Create Timesheet')

@section('content_header')
    <h1>Create Timesheet</h1>
@stop

@section('content')
    @include('component.appointment.pick', ['redirectRoute' => 'timesheet.create'])

    {{ Form::open(['route' => 'timesheet.create-post', 'method' => 'post']) }}
        <div class="card card-dark">
            <div class="card-header">Current Entered Timesheets for {{ $date }}</div>
            <div class="card-body">
                @if ($todayTimesheets->count())
                    <div class="row">
                        <div class="col-3">Category</div>
                        <div class="col-2">Time Logged</div>
                        <div class="col-1">Approved?</div>
                        <div class="col-6">Description of Work</div>

                        @foreach ($todayTimesheets as $timesheet)
                            <div class="col-3">{{ $timesheet->category->name }}</div>
                            <div class="col-2">{{ time_string_from_mins($timesheet->mins_logged) }}</div>
                            <div class="col-1">{!! $timesheet->approved_tick_cross !!}</div>
                            <div class="col-6">{{ $timesheet->description }}</div>
                        @endforeach

                        <div class="col-3">&nbsp;</div>
                        <div class="col-2">{{ $totalTimeLogged }}</div>
                        <div class="col-7">&nbsp;</div>
                    </div>
                @else
                    <div class="card-empty-body">You have not entered any time for {{ $date }} yet.</div>
                @endif
            </div>
        </div>

        <div class="card card-dark">
            <div class="card-header">Timesheet Details</div>
            <div class="card-body">
                {{ Form::bsSelect('Category', 'category_id', $categories) }}
                {{ Form::bsDate('Date', 'date_of_timesheet', $date) }}
                {{ Form::bsText('Hours', 'time_hours') }}
                {{ Form::bsText('Minutes', 'time_mins') }}
                {{ Form::bsTextarea('Description of Work Performed', 'description') }}
            </div>
        </div>

        {{ Form::bsSubmit('Create Timesheet') }}
    {{ Form::close() }}
@endsection
