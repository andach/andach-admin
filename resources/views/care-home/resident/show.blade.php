@extends('adminlte::page')

@section('title', 'Show Resident')

@section('content_header')
    <h1>{{ $resident->user->name }} - 
        @if ($rental)
            {{ $rental->rentedThing->name }}
        @else
            Former Resident
        @endif</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12 col-sm-6 col-md-4">
            <div class="card card-dark">
                <div class="card-header">Resident Details</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12"><img src="{{ $resident->user->picture_url }}" style="border-radius: 50%; max-width: 100%; height: auto;" alt="Resident Image" class="img-fluid"></div>
                        <div class="col-12">
                            {{ $resident->user->name }}
                            @if ($resident->user->preferred_name)
                                ({{ $resident->user->preferred_name }})
                            @endif
                        </div>
                        <div class="col-12">&nbsp;</div>
                        <div class="col-1"><i class="fas fa-at"></i></div><div class="col-11">{{ $resident->user->email }}</div>
                        <div class="col-1"><i class="fas fa-home"></i></div><div class="col-11">{!! $resident->user->address !!}</div>
                        <div class="col-1"><i class="fas fa-phone"></i></div><div class="col-11">{{ $resident->user->phone_number }}</div>
                        <div class="col-1"><i class="fas fa-mobile-alt"></i></div><div class="col-11">{{ $resident->user->mobile_number }}</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-sm-6 col-md-8">
            <div class="card card-dark">
                <div class="card-header">Critical Information</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            @if ($resident->is_challenging)
                                <h3><i class="fas fa-exclamation-triangle"></i> Challenging Behaviour</h3>

                                <p>{!! nl2br($resident->challenging_details) !!}</p>
                            @endif

                            @if ($resident->is_safeguarding)
                                <h3><i class="fas fa-exclamation-triangle"></i> Safeguarding Risks</h3>

                                <p>{!! nl2br($resident->safeguarding_details) !!}</p>
                            @endif

                            @if ($resident->allergies)
                                <h3><i class="fas fa-exclamation-triangle"></i> Allergies</h3>

                                <p>{!! nl2br($resident->allergies) !!}</p>
                            @endif

                            <div class="row">
                                <div class="col-12 col-sm-4">
                                    <h3>GP Details</h3>
                                    <p>{!! nl2br($resident->gp_details) !!}</p>
                                </div>
                                <div class="col-12 col-sm-4">
                                    <h3>Next of Kin</h3>
                                    <p>{!! nl2br($resident->next_of_kin) !!}</p>
                                </div>
                                <div class="col-12 col-sm-4">
                                    <h3>Power of Attorney</h3>
                                    <p>{!! nl2br($resident->power_of_attorney) !!}</p>
                                </div>

                                <div class="col-12">
                                    @if ($eolWishes)
                                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#eolModal">
                                            View End of Life Wishes
                                        </button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12">
            <div class="card card-dark">
                <div class="card-header">Other Risks and Hazards</div>
                <div class="card-body">
                    @if (!$eolWishes)
                        <div class="alert alert-danger">There is no information for end of life wishes for this resident. Please <a href="{{ route('care-home.resident.edit', [$resident->id, 'end-of-life']) }}">ensure these are recorded</a> as soon as possible.</div>
                    @endif

                    @if (!$fallAssess)
                        <div class="alert alert-danger">No Falls Assessments have been completed. Please ensure that a qualified member of staff <a href="{{ route('care-home.resident.edit', [$resident->id, 'falls']) }}">completes a fall assessment</a> as soon as possible.</div>
                    @else
                        <h3>Falls</h3>
                        @if ($fallAssess->number_of_risk_factors == 0)
                            <div class="alert alert-success">On the most recent falls risk assessment (completed on {{ $fallAssess->date_created }}), no risk factors were identified.
                        @else
                            <p>{{ $fallAssess->number_of_risk_factors }} risk factors have been identified in the falls risk. 
                        @endif

                        <p>For more information, <a href="{{ route('care-home.resident.falls-assessment.view', $fallAssess->id) }}">view the full risk assessment</a>.</p>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="eolModal" tabindex="-1" role="dialog" aria-labelledby="eolModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="eolModalLabel">End of Life Wishes</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @if (!$eolWishes)

                    @else 

                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection