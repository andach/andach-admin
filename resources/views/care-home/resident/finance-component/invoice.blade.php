{{ Form::open(['route' => ['care-home.resident.finance-post', $resident->id], 'method' => 'post']) }}
    {{ Form::hidden('action', 'invoice') }}

    <div class="card card-dark">
        <div class="card-header">Invoices</div>
        <div class="card-body">
            <div class="row">
                {{-- @if (count($invoices))
                    <div class="col-3">Date</div>
                    <div class="col-3">Customer</div>
                    <div class="col-3">Amount</div>
                    <div class="col-3">Paid?</div>
                @else
                    <div class="col-12">This resident does not have any invoices yet.</div>
                @endif --}}
            </div>
        </div>
    </div>

{{ Form::close() }}