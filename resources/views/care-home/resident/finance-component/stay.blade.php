{{ Form::open(['route' => ['care-home.resident.finance-post', $resident->id], 'method' => 'post']) }}
    {{ Form::hidden('action', 'stay') }}

    <div class="row">
        <div class="col-4">
            @include('component.resident.card', ['resident' => $resident])
        </div>

        <div class="col-8">
            <div class="card card-dark">
                <div class="card-header">Current Charges</div>
                <div class="card-body">
                    <div class="row">
                        @if ($currentCharges)
                            @foreach ($currentCharges as $resp)
                                <div class="col-8">{{ $resp->user->name }}</div>
                                <div class="col-4">{!! $resp->formatted_price !!}</div>
                            @endforeach
                        @else
                            <div class="col-12">This resident does not have any billing information - presumably they are a new resident and this has not yet been set up. </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12">
            <div class="card card-dark">
                <div class="card-header">Update or Set Billing Information</div>
                <div class="card-body">
                    {{ Form::bsSelect('Room', 'rented_thing_id', $rooms, null, ['placeholder' => '']) }}
                    {{ Form::bsDate('Effective Date', 'from_date') }}
                    {{ Form::bsNumber('Charge Per Week', 'charge_per_week') }}

                    <div class="row">
                        <div class="col-12">Enter who is responsible for the charges. Note that you will have to have created a user for anyone the system is going to bill. </div>
                        
                        @for ($count = 0; $count < 10; $count++)
                            <div class="col-6">{{ Form::select('responsible_user_id['.$count.']', $users, null, ['class' => 'form-control', 'placeholder' => '', 'style' => 'width: 100%']) }}</div>
                            <div class="col-6">{{ Form::number('cost_for_user_id['.$count.']', null, ['class' => 'form-control']) }}</div>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{ Form::bsSubmit('Update Finance Information') }}

{{ Form::close() }}