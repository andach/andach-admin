@extends('adminlte::page')

@section('title', 'All Residents (Including Historic)')

@section('content_header')
    <h1>All Residents (Including Historic)</h1>
@stop

@section('content')
    <div class="card card-dark">
        <div class="card-header">All Residents (Including Historic</div>
        <div class="card-body">
            <div class="row">
                <div class="col-12">Person</div>
                @foreach ($residents as $resident)
                    <div class="col-6"><a href="{{ route('care-home.resident.edit', $resident->id) }}">{{ $resident->user->name }}</a></div>
                    <div class="col-6"><a href="{{ route('care-home.resident.finance', $resident->id) }}">{{ $resident->user->name }}</a></div>
                @endforeach
            </div>
        </div>
    </div>
@endsection