@extends('adminlte::page')

@section('title', 'Falls Assessment')

@section('content_header')
    <h1>Falls Assessment</h1>
@stop

@section('content')
    <p>This is an assessment of <a href="{{ route('care-home.resident.show', $assessment->resident->id) }}">{{ $assessment->resident->user->name }}</a> made on {{ $assessment->created_at }} by {{ $assessment->user->name }}.</p>

    <div class="card card-dark">
        <div class="card-header">Assessment</div>
        <div class="card-body">
            <div class="row">
                @foreach ($quesWithCat as $category => $questions)
                    <div class="col-12"><b>{{ $category }}</b></div>
                    
                    @foreach ($questions as $key => $text)
                        <div class="col-2">
                            @if ($answers[$key])
                                <span style="color:#72b01d"><i class="fas fa-check"></i></span>
                            @else
                                <span style="color:#bf0603"><i class="fas fa-times"></i></span>
                            @endif
                        </div>
                        <div class="col-10">{{ $text }}</div>

                        @if ($answers[$key.'_comments'])
                            <div class="col-1">&nbsp;</div>
                            <div class="col-1">Notes</div>
                            <div class="col-10"><i>{{ $answers[$key.'_comments'] }}</i></div>
                        @endif
                    @endforeach

                    <div class="col-12">&nbsp;</div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
