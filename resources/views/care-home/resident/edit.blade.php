@extends('adminlte::page')

@section('title', 'Edit Resident')

@section('content_header')
    <h1>Edit Resident</h1>
@stop

@section('content')
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link @if ($page ==='general') active @endif" id="general-tab" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-selected="true">General</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if ($page ==='catheter') active @endif" id="catheter-tab" id="catheter-tab" data-toggle="tab" href="#catheter" role="tab" aria-controls="catheter" aria-selected="false">Catheter</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if ($page ==='endOfLife') active @endif" id="end-of-life-tab" id="end-of-life-tab" data-toggle="tab" href="#end-of-life" role="tab" aria-controls="end-of-life" aria-selected="false">End of Life</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if ($page ==='falls') active @endif" id="falls-tab" id="falls-tab" data-toggle="tab" href="#falls" role="tab" aria-controls="falls" aria-selected="false">Falls</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if ($page ==='foodlog') active @endif" id="foodlog-tab" id="foodlog-tab" data-toggle="tab" href="#foodlog" role="tab" aria-controls="foodlog" aria-selected="false">Food Log</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if ($page ==='hospital') active @endif" id="hospital-tab" id="hospital-tab" data-toggle="tab" href="#hospital" role="tab" aria-controls="hospital" aria-selected="false">Hospital</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if ($page ==='illness') active @endif" id="illness-tab" id="illness-tab" data-toggle="tab" href="#illness" role="tab" aria-controls="illness" aria-selected="false">Illness</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if ($page ==='malnutrition') active @endif" id="malnutrition-tab" id="malnutrition-tab" data-toggle="tab" href="#malnutrition" role="tab" aria-controls="malnutrition" aria-selected="false">Nutrition</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if ($page ==='medical') active @endif" id="medical-tab" id="medical-tab" data-toggle="tab" href="#medical" role="tab" aria-controls="medical" aria-selected="false">Medical</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if ($page ==='skin') active @endif" id="skin-tab" id="skin-tab" data-toggle="tab" href="#skin" role="tab" aria-controls="skin" aria-selected="false">Skin</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if ($page ==='weightlog') active @endif" id="weightlog-tab" id="weightlog-tab" data-toggle="tab" href="#weightlog" role="tab" aria-controls="weightlog" aria-selected="false">Weight Log</a>
        </li>
    </ul>

    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade @if ($page ==='general') show active @endif" id="general" role="tabpanel" aria-labelledby="general-tab">@include('care-home.resident.edit-component.general')</div>
        <div class="tab-pane fade @if ($page ==='catheter') show active @endif" id="catheter" role="tabpanel" aria-labelledby="catheter-tab">@include('care-home.resident.edit-component.catheter')</div>
        <div class="tab-pane fade @if ($page ==='endOfLife') show active @endif" id="end-of-life" role="tabpanel" aria-labelledby="end-of-life-tab">@include('care-home.resident.edit-component.end-of-life')</div>
        <div class="tab-pane fade @if ($page ==='falls') show active @endif" id="falls" role="tabpanel" aria-labelledby="falls-tab">@include('care-home.resident.edit-component.falls')</div>
        <div class="tab-pane fade @if ($page ==='foodlog') show active @endif" id="foodlog" role="tabpanel" aria-labelledby="foodlog-tab">@include('care-home.resident.edit-component.foodlog')</div>
        <div class="tab-pane fade @if ($page ==='hospital') show active @endif" id="hospital" role="tabpanel" aria-labelledby="hospital-tab">@include('care-home.resident.edit-component.hospital')</div>
        <div class="tab-pane fade @if ($page ==='illness') show active @endif" id="illness" role="tabpanel" aria-labelledby="illness-tab">@include('care-home.resident.edit-component.illness')</div>
        <div class="tab-pane fade @if ($page ==='malnutrition') show active @endif" id="malnutrition" role="tabpanel" aria-labelledby="malnutrition-tab">@include('care-home.resident.edit-component.malnutrition')</div>
        <div class="tab-pane fade @if ($page ==='medical') show active @endif" id="medical" role="tabpanel" aria-labelledby="medical-tab">@include('care-home.resident.edit-component.medical')</div>
        <div class="tab-pane fade @if ($page ==='skin') show active @endif" id="skin" role="tabpanel" aria-labelledby="skin-tab">@include('care-home.resident.edit-component.skin')</div>
        <div class="tab-pane fade @if ($page ==='weightlog') show active @endif" id="weightlog" role="tabpanel" aria-labelledby="weightlog-tab">@include('care-home.resident.edit-component.weightlog')</div>
    </div>
@endsection

@section('js')
<script>
    @foreach ($fallQuestions as $category => $questions)
        @foreach ($questions as $key => $question)
            document.getElementById("{{ $key }}").addEventListener("click", function() 
            {
                var div = document.getElementById("{{ $key }}_commentsdiv");

                if (div.style.display === "none") {
                    div.style.display = "block";
                } else {
                    div.style.display = "none";
                }
            });
        @endforeach
    @endforeach
</script>
@endsection