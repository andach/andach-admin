@extends('adminlte::page')

@section('title', 'Current Residents')

@section('content_header')
    <h1>Current Residents</h1>
@stop

@section('content')
    <div class="card card-dark">
        <div class="card-header">Current Residents</div>
        <div class="card-body">
            <div class="row">
                <div class="col-3">Room</div>
                <div class="col-9">
                    <div class="row">
                        <div class="col-3">Person</div>
                        <div class="col-3">Start Date</div>
                        <div class="col-3">End Date</div>
                        <div class="col-3">Care Record</div>
                    </div>
                </div>
                @foreach ($locations as $location)
                    <div class="col-3">{{ $location->name }}</div>
                    <div class="col-9">
                        <div class="row">
                            @foreach ($rentals[$location->id] as $rental)
                                <div class="col-3"><a href="{{ route('care-home.resident.show', $rental->user->resident->id) }}">{{ $rental->user->name ?? '' }}</a></div>
                                <div class="col-3">{{ $rental->from_date ?? '' }}</div>
                                <div class="col-3">{{ $rental->to_date ?? '' }}</div>
                                <div class="col-3">
                                    <a href="{{ route('care-home.resident.edit', $rental->user->resident->id) }}">Edit Care Record</a>    
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection