@extends('adminlte::page')

@section('title', 'Create Resident')

@section('content_header')
    <h1>Create Resident</h1>
@stop

@section('content')
    {{ Form::open(['route' => 'care-home.resident.create-post', 'method' => 'post']) }}
        <div class="row">
            <div class="col-md-12 col-lg-6">
                <div class="card card-dark">
                    <div class="card-header">User Details</div>
                    <div class="card-body">
                        {{ Form::bsText('Name', 'name', null, ['helpBlock' => 'This is the full name of the user, "Mr. John Smith", for example.']) }}
                        {{ Form::bsText('Preferred Name', 'preferred_name', null, ['helpBlock' => 'This is the name the person preferrs to be addressed by, which could be "Mr. Smith", "John", "Johnny", etc.']) }}
                        {{ Form::bsText('First Name', 'first_name') }}
                        {{ Form::bsText('Middle Names', 'middle_names') }}
                        {{ Form::bsText('Last Name', 'last_name') }}
                        {{ Form::bsFile('Picture (optional)', 'picture_path') }}
                        {{ Form::hidden('is_customer', 1) }}
                        {{ Form::bsSelect('Ethnic Origin', 'ethnic_origin_id', $ethnicOrigins, null, ['placeholder' => '']) }}
                        {{ Form::bsSelect('Religion', 'religion_id', $religions, null, ['placeholder' => '']) }}
                        {{ Form::bsSelect('Sexual Orientation', 'sexual_orientation_id', $sexualOrientations, null, ['placeholder' => '']) }}
                    </div>
                </div>
            </div>

            <div class="col-md-12 col-lg-6">
                <div class="card card-dark">
                    <div class="card-header">Address and Contact Information</div>
                    <div class="card-body">
                        {{ Form::bsText('Address Line 1', 'address_1') }}
                        {{ Form::bsText('Address Line 2', 'address_2') }}
                        {{ Form::bsText('Address Line 3', 'address_3') }}
                        {{ Form::bsText('Address Line 4', 'address_4') }}
                        {{ Form::bsSelect('County', 'county_id', $counties, null, ['placeholder' => '']) }}
                        {{ Form::bsText('Phone Number', 'phone_number') }}
                        {{ Form::bsText('Mobile Number', 'mobile_number') }}
                    </div>
                </div>
            </div>

            <div class="col-12">
                <div class="card card-dark">
                    <div class="card-header">Update or Set Billing Information</div>
                    <div class="card-body">
                        {{ Form::bsSelect('Room', 'rented_thing_id', $rooms, null, ['placeholder' => '']) }}
                        {{ Form::bsDate('Effective Date', 'from_date') }}
                    </div>
                </div>
            </div>
        </div>

        {{ Form::bsSubmit('Create User') }}
    {{ Form::close() }}
@endsection
