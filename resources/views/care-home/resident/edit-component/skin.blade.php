{{ Form::open(['route' => ['care-home.resident.edit-post', $resident->id], 'method' => 'post']) }}
    {{ Form::hidden('action', 'skin') }}

    <div class="card card-dark">
        <div class="card-header">Update Skin Risk Assessment</div>
        <div class="card-body">
            <div class="row">
                <div class="col-4"><b>Sensory Perception</b></div>
                <div class="col-2 text-center">{{ Form::radio('braden_sensory_perception', 1) }}<br />Completely Limited</div>
                <div class="col-2 text-center">{{ Form::radio('braden_sensory_perception', 2) }}<br />Very Limited</div>
                <div class="col-2 text-center">{{ Form::radio('braden_sensory_perception', 3) }}<br />Slightly Limited</div>
                <div class="col-2 text-center">{{ Form::radio('braden_sensory_perception', 4) }}<br />No Impairment</div>
                
                <div class="col-4"><b>Moisture</b></div>
                <div class="col-2 text-center">{{ Form::radio('braden_moisture', 1) }}<br />Completely Moist</div>
                <div class="col-2 text-center">{{ Form::radio('braden_moisture', 2) }}<br />Very Moist</div>
                <div class="col-2 text-center">{{ Form::radio('braden_moisture', 3) }}<br />Slightly Moist</div>
                <div class="col-2 text-center">{{ Form::radio('braden_moisture', 4) }}<br />Rarely Moist</div>
                
                <div class="col-4"><b>Activity</b></div>
                <div class="col-2 text-center">{{ Form::radio('braden_activity', 1) }}<br />Completely Limited</div>
                <div class="col-2 text-center">{{ Form::radio('braden_activity', 2) }}<br />Very Limited</div>
                <div class="col-2 text-center">{{ Form::radio('braden_activity', 3) }}<br />Slightly Limited</div>
                <div class="col-2 text-center">{{ Form::radio('braden_activity', 4) }}<br />No Impairment</div>
                
                <div class="col-4"><b>Mobility</b></div>
                <div class="col-2 text-center">{{ Form::radio('braden_mobility', 1) }}<br />Completely Immobile</div>
                <div class="col-2 text-center">{{ Form::radio('braden_mobility', 2) }}<br />Very Limited</div>
                <div class="col-2 text-center">{{ Form::radio('braden_mobility', 3) }}<br />Slightly Limited</div>
                <div class="col-2 text-center">{{ Form::radio('braden_mobility', 4) }}<br />No Limitations</div>
                
                <div class="col-4"><b>Nutrition</b></div>
                <div class="col-2 text-center">{{ Form::radio('braden_nutrition', 1) }}<br />Very Poor</div>
                <div class="col-2 text-center">{{ Form::radio('braden_nutrition', 2) }}<br />Probably Inadequate</div>
                <div class="col-2 text-center">{{ Form::radio('braden_nutrition', 3) }}<br />Adequate</div>
                <div class="col-2 text-center">{{ Form::radio('braden_nutrition', 4) }}<br />Excellent</div>
                
                <div class="col-4"><b>Friction</b></div>
                <div class="col-2 text-center">{{ Form::radio('braden_friction', 1) }}<br />Problem</div>
                <div class="col-2 text-center">{{ Form::radio('braden_friction', 2) }}<br />Potential Problem</div>
                <div class="col-2 text-center">{{ Form::radio('braden_friction', 3) }}<br />No Apparant Problem</div>
                <div class="col-2 text-center">&nbsp;</div>
            </div>

            {{ Form::bsTextarea('General Observations', 'general_observations') }}
            {{ Form::bsCheckbox('Broken Skin?', 'is_broken_skin', 1) }}
            {{ Form::bsCheckbox('Is Infection Present?', 'is_infection_present', 1) }}
            {{ Form::bsText('What is infection being treated with?', 'infection_treated_with') }}
        </div>
    </div>

    <div class="card card-dark">
        <div class="card-header">Previous Assessments</div>
        <div class="card-body">
            <div class="row">
                <div class="col-2">Date</div>
                <div class="col-4">Braden Assessment</div>
                <div class="col-1">Broken Skin?</div>
                <div class="col-1">Infection?</div>
                <div class="col-4">Infection Treated With</div>

                @foreach ($resident->skinAssessments as $skin)
                    <div class="col-2">{{ $skin->date_of_assessment }}</div>
                    <div class="col-4">@include('care-home.resident.edit-component.skin-braden', $skin)</div>
                    <div class="col-1">{{ $skin->broken_skin_formatted }}</div>
                    <div class="col-1">{{ $skin->infection_present_formatted }}</div>
                    <div class="col-4">{{ $skin->infection_treated_with }}</div>

                    @if ($skin->general_observations)
                        <div class="col-12"><i>General observations: {{ $skin->general_observations }}</i></div>
                        <div class="col-12">&nbsp;</div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
    {{ Form::bsSubmit('Update Skin Assessment')}}
{{ Form::close() }}