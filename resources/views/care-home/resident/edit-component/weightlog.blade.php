{{ Form::open(['route' => ['care-home.resident.edit-post', $resident->id], 'method' => 'post']) }}
    {{ Form::hidden('action', 'weightlog') }}

    <div class="card card-dark">
        <div class="card-header">Update Weight Log</div>
        <div class="card-body">
            <div class="row">
                <div class="col-3"><b>Weight</b></div>
                <div class="col-2">{{ Form::text('weight_in_kg', null, ['class' => 'form-control']) }}</div>
                <div class="col-7">kg</div>

                <div class="col-3">&nbsp;</div>
                <div class="col-2">{{ Form::text('weight_in_st', null, ['class' => 'form-control']) }}</div>
                <div class="col-2">st</div>
                <div class="col-2">{{ Form::text('weight_in_lbs', null, ['class' => 'form-control']) }}</div>
                <div class="col-2">lbs</div>
            </div>

            {{ Form::bsText('BMI', 'bmi') }}
        </div>
    </div>

    <div class="card card-dark">
        <div class="card-header">Weight Log</div>
        <div class="card-body">
            <div class="row">
                <div class="col-2">Date</div>
                <div class="col-2">Recorded By</div>
                <div class="col-2">Weight (kg)</div>
                <div class="col-2">Weight (lbs)</div>
                <div class="col-2">Weight (st)</div>
                <div class="col-2">BMI</div>

                @foreach ($resident->weightLog as $weightLog)
                    <div class="col-2">{{ $weightLog->date_of_assessment }}</div>
                    <div class="col-2">{{ $weightLog->user->name }}</div>
                    <div class="col-2">{{ $weightLog->formatted_weight_kg }}</div>
                    <div class="col-2">{{ $weightLog->formatted_weight_lbs }}</div>
                    <div class="col-2">{{ $weightLog->formatted_weight_st }}</div>
                    <div class="col-2">{{ $weightLog->bmi }}</div>
                @endforeach
            </div>
        </div>
    </div>

    {{ Form::bsSubmit('Update Weight Log')}}
{{ Form::close() }}