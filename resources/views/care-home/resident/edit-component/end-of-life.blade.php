{{ Form::model($eolWishes, ['route' => ['care-home.resident.edit-post', $resident->id], 'method' => 'post']) }}
    {{ Form::hidden('action', 'endOfLife') }}

    <div class="card card-dark">
        <div class="card-header">End of Life Wishes</div>
        <div class="card-body">
            {{ Form::bsTextarea('What are the residents wishes?', 'what') }}
            {{ Form::bsTextarea('Who should we contact?', 'who') }}
            {{ Form::bsTextarea('Where would the resident like to spend their final days?', 'where') }}
            {{ Form::bsTextarea('Special requests - faith/religion, pets, music, etc.', 'special_requests') }}
        </div>
    </div>

    {{ Form::bsSubmit('Update End of Life Wishes')}}
{{ Form::close() }}