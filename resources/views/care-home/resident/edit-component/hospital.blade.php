{{ Form::open(['route' => ['care-home.resident.edit-post', $resident->id], 'method' => 'post']) }}
    {{ Form::hidden('action', 'hospital') }}

    <div class="card card-dark">
        <div class="card-header">Record New Visit</div>
        <div class="card-body">
            {{ Form::bsDate('From', 'date_from') }}
            {{ Form::bsDate('To', 'date_to') }}
            {{ Form::bsText('Reason', 'reason') }}
            {{ Form::bsText('Outcome', 'outcome') }}
            {{ Form::bsTextarea('Any Further Risks Identified?', 'extra_risk_assessment') }}
        </div>
    </div>

    <div class="card card-dark">
        <div class="card-header">Previous Visits</div>
        <div class="card-body">
            <div class="row">
                <div class="col-2">From</div>
                <div class="col-2">To</div>
                <div class="col-4">Reason</div>
                <div class="col-3">Outcome</div>
                <div class="col-1">Edit</div>

                @foreach ($resident->hospitalAdmissions as $hosp)
                    <div class="col-2">{{ $hosp->date_from }}</div>
                    <div class="col-2">{{ $hosp->date_to }}</div>
                    <div class="col-4">{{ $hosp->reason }}</div>
                    <div class="col-3">{{ $hosp->outcome }}</div>
                    <div class="col-1">Edit</div>

                    @if ($hosp->extra_risk_assessment)
                        <div class="col-12"><i>Further risks identified: {{ $hosp->extra_risk_assessment }}</i></div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>

    {{ Form::bsSubmit('Update Hospital Visit Log')}}
{{ Form::close() }}