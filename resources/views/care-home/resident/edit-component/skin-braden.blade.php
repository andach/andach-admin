<!-- Button trigger modal -->
<button type="button" class="{{ $skin->button_class }}" data-toggle="modal" data-target="#{{ $skin->resident->id }}-{{ $skin->id }}">
  {{ $skin->button_text }}
</button>

<!-- Modal -->
<div class="modal fade" id="{{ $skin->resident->id }}-{{ $skin->id }}" tabindex="-1" role="dialog" aria-labelledby="{{ $skin->resident->id }}-{{ $skin->id }}Label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="{{ $skin->resident->id }}-{{ $skin->id }}Label">Braden Skin Assessment of {{ $skin->resident->user->name }} on {{ $skin->date_of_assessment }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Section</th>
                    <th scope="col">Score</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Sensory Perception</td>
                    <td>{{ $skin->braden_sensory_perception }}</td>
                </tr>
                <tr>
                    <td>Moisture</td>
                    <td>{{ $skin->braden_moisture }}</td>
                </tr>
                <tr>
                    <td>Activity</td>
                    <td>{{ $skin->braden_activity }}</td>
                </tr>
                <tr>
                    <td>Mobility</td>
                    <td>{{ $skin->braden_mobility }}</td>
                </tr>
                <tr>
                    <td>Nutrition</td>
                    <td>{{ $skin->braden_nutrition }}</td>
                </tr>
                <tr>
                    <td>Friction</td>
                    <td>{{ $skin->braden_friction }}</td>
                </tr>
                <tr>
                    <td>Total</td>
                    <td>{{ $skin->braden_total }}</td>
                </tr>
            </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>