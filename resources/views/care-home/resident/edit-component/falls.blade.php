{{ Form::open(['route' => ['care-home.resident.edit-post', $resident->id], 'method' => 'post']) }}
    {{ Form::hidden('action', 'falls') }}

    <div class="card card-dark">
        <div class="card-header">Previous Falls</div>
        <div class="card-body">
            @if (count($falls))
                FALLS INFORMATION HERE
            @else
                <p>This resident has no falls recorded.</p>
            @endif
        </div>
    </div>

    <div class="card card-dark">
        <div class="card-header">Make a New Assessment</div>
        <div class="card-body">
            @foreach ($fallQuestions as $category => $questions)
                <div class="card card-dark">
                    <div class="card-header">{{ $category }}</div>
                    <div class="card-body">
                        @foreach ($questions as $key => $question)
                            {{ Form::hidden($key, 0) }}
                            {{ Form::bsCheckboxReverse($question, $key, 1, $lastFallAssess[$key]) }}
                            <div id="{{ $key }}_commentsdiv" @if (!$lastFallAssess[$key]) style="display:none @endif">
                                {{ Form::bsTextarea('Comments and Mitigations', $key.'_comments', $lastFallAssess[$key.'_comments']) }}
                            </div>
                        @endforeach
                    </div>
                    {{-- <div class="card-footer">
                        <div class="info-box">
                            <span class="info-box-icon bg-info"><i class="fas fa-info-circle"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-title">Creating a User that can Login</span>
                                <span class="info-box-text">To create a user that can login, you will need to supply an email and password, and ensure that "staff" is selected at the bottom of this page. </span>
                            </div>
                        </div>
                    </div> --}}
                </div>
            @endforeach
        </div>
    </div>

    <div class="card card-dark">
        <div class="card-header">Previous Fall Risk Assessments</div>
        <div class="card-body">
            <div class="row">
                <div class="col-3">Date</div>
                <div class="col-4">Assessment By</div>
                <div class="col-3">Risk Factors Identified</div>
                <div class="col-2">Full Assessment</div>

                @foreach ($resident->fallAssessments as $fall)
                    <div class="col-3">{{ $fall->date_of_assessment }}</div>
                    <div class="col-4">{{ $fall->user->name }}</div>
                    <div class="col-3">{{ $fall->number_of_risk_factors }}</div>
                    <div class="col-2"><a href="{{ route('care-home.resident.falls-assessment.view', $fall->id) }}">View</a></div>
                @endforeach
            </div>
        </div>
    </div>

    {{ Form::bsSubmit('Update Falls Log')}}
{{ Form::close() }}