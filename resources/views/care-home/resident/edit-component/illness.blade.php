{{ Form::open(['route' => ['care-home.resident.edit-post', $resident->id], 'method' => 'post']) }}
    {{ Form::hidden('action', 'illness') }}

    <div class="card card-dark">
        <div class="card-header">Record New Illness</div>
        <div class="card-body">
            {{ Form::bsDate('From', 'date_from') }}
            {{ Form::bsDate('To', 'date_to') }}
            {{ Form::bsText('Illness', 'illness') }}
            {{ Form::bsTextarea('General Observations', 'general_observations') }}
            {{ Form::bsTextarea('Any Further Risks Identified?', 'extra_risk_assessment') }}
        </div>
    </div>

    <div class="card card-dark">
        <div class="card-header">Previous Visits</div>
        <div class="card-body">
            <div class="row">
                <div class="col-2">From</div>
                <div class="col-2">To</div>
                <div class="col-4">Illness</div>
                <div class="col-3">Observations</div>
                <div class="col-1">Edit</div>

                @foreach ($resident->illnesses as $illness)
                    <div class="col-2">{{ $illness->date_from }}</div>
                    <div class="col-2">{{ $illness->date_to }}</div>
                    <div class="col-4">{{ $illness->illness }}</div>
                    <div class="col-3">{{ $illness->general_observations }}</div>
                    <div class="col-1">Edit</div>

                    @if ($illness->extra_risk_assessment)
                        <div class="col-12"><i>Further risks identified: {{ $illness->extra_risk_assessment }}</i></div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>

    {{ Form::bsSubmit('Update Illness Log')}}
{{ Form::close() }}