{{ Form::model($resident, ['route' => ['care-home.resident.edit-post', $resident->id], 'method' => 'post']) }}
    {{ Form::hidden('action', 'general') }}

    <div class="card card-dark">
        <div class="card-header">General Resident Details</div>
        <div class="card-body">
            {{ Form::bsCheckboxReverse('Does the resident have a history of challenging behaviour?', 'is_challenging', 1) }}
            {{ Form::bsTextarea('Challenging behaviour - further details', 'challenging_details') }}
            
            {{ Form::bsCheckboxReverse('Are there any safeguarding concerns?', 'is_safeguarding', 1) }}
            {{ Form::bsTextarea('Safeguarding concerns - further details', 'safeguarding_details') }}
        </div>
    </div>

    <div class="card card-dark">
        <div class="card-header">Contact and Emergency Information</div>
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-md-6">
                    {{ Form::bsTextarea('Allergies', 'allergies') }}
                    {{ Form::bsTextarea('GP Name and Address', 'gp_details') }}
                </div>
                <div class="col-12 col-md-6">
                    {{ Form::bsTextarea('Next of Kin', 'next_of_kin') }}
                    {{ Form::bsTextarea('Power of Attorney', 'power_of_attorney') }}
                </div>
            </div>
        </div>
    </div>

    {{ Form::bsSubmit('Update Details')}}
{{ Form::close() }}