{{ Form::open(['route' => ['care-home.resident.edit-post', $resident->id], 'method' => 'post']) }}
    {{ Form::hidden('action', 'malnutrition') }}
    {{ Form::hidden('acutely_ill', 0) }}

    <div class="card card-dark">
        <div class="card-header">Update Malnutrition (MUST) Assessment</div>
        <div class="card-body">
            <div class="row">
                <div class="col-4">BMI</div>
                <div class="col-4">Weight Loss (3-6m)</div>
                <div class="col-4">Acutely Ill</div>

                <div class="col-4">
                    {{ Form::radio('must_bmi', 0) }} &gt;20<br />
                    {{ Form::radio('must_bmi', 1) }} 18.5-20<br />
                    {{ Form::radio('must_bmi', 2) }} &lt;18.5
                </div>
                <div class="col-4">
                    {{ Form::radio('must_weight_loss', 0) }} &lt;5%<br />
                    {{ Form::radio('must_weight_loss', 1) }} 5-10%<br />
                    {{ Form::radio('must_weight_loss', 2) }} &gt;10%
                </div>
                <div class="col-4">
                    {{ Form::checkbox('is_acutely_ill', 1, null, ['class' => 'form-control']) }}
                </div>
            </div>
        </div>
    </div>

    <div class="card card-dark">
        <div class="card-header">Previous Assessments</div>
        <div class="card-body">
            <div class="row">
                <div class="col-2">Date</div>
                <div class="col-2">By</div>
                <div class="col-2">BMI</div>
                <div class="col-2">Weight Loss</div>
                <div class="col-2">Acutely Ill?</div>
                <div class="col-2">Risk</div>

                @foreach ($resident->malnutritionAssessments as $malAss)
                    <div class="col-2">{{ $malAss->date_of_assessment }}</div>
                    <div class="col-2">{{ $malAss->user->name }}</div>
                    <div class="col-2">{{ $malAss->bmi_formatted }}</div>
                    <div class="col-2">{{ $malAss->weight_loss_formatted }}</div>
                    <div class="col-2">{{ $malAss->acutely_ill_formatted }}</div>
                    <div class="col-2">{{ $malAss->risk_formatted }}</div>
                @endforeach
            </div>
        </div>
    </div>

    {{ Form::bsSubmit('Update Nutrition Assessment')}}
{{ Form::close() }}