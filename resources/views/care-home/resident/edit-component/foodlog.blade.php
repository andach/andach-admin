{{ Form::open(['route' => ['care-home.resident.edit-post', $resident->id], 'method' => 'post']) }}
    {{ Form::hidden('action', 'foodlog') }}

    <div class="card card-dark">
        <div class="card-header">Log Food</div>
        <div class="card-body">
            {{ Form::bsDate('Date', 'date_eaten') }}
            {{ Form::bsSelect('Meal', 'enum_meal', $enumMeals) }}
            {{ Form::bsText('Food/Drink', 'food_and_drink') }}
            {{ Form::bsText('Fortified With?', 'fortified_with') }}
        </div>
    </div>

    <div class="card card-dark">
        <div class="card-header">Food Log (Last Seven Days)</div>
        <div class="card-body">
            <div class="row">
                @foreach ($foodWeek as $date => $meals)
                    <div class="col-12"><b>{{ $date }}</b></div>

                    <div class="col-3">Date</div>
                    <div class="col-3">Meal</div>
                    <div class="col-3">Food and Drink</div>
                    <div class="col-3">Fortified With?</div>

                    @foreach ($meals as $meal)
                        <div class="col-3">{{ $meal->date_eaten }}</div>
                        <div class="col-3">{{ $enumMeals[$meal->enum_meal] }}</div>
                        <div class="col-3">{{ $meal->food_and_drink }}</div>
                        <div class="col-3">{{ $meal->fortified_with }}</div>
                    @endforeach

                    <div class="col-12">&nbsp;</div>
                @endforeach
            </div>
        </div>
    </div>

    {{ Form::bsSubmit('Update Food Log')}}
{{ Form::close() }}