{{ Form::open(['route' => ['care-home.resident.edit-post', $resident->id], 'method' => 'post']) }}
    {{ Form::hidden('action', 'medical') }}

    <div class="card card-dark">
        <div class="card-header">Update Assessment</div>
        <div class="card-body">
            {{ Form::bsSelect('Mobility', 'enum_mobility', $enumMobility) }}
            {{ Form::bsSelect('Continence', 'enum_continence', $enumContinence) }}
            {{ Form::bsSelect('Cognition', 'enum_cognition', $enumCognition) }}
            {{ Form::bsSelect('Communication', 'enum_communication', $enumCommunication) }}
            {{ Form::bsSelect('Hearing', 'enum_hearing', $enumHearing) }}
            {{ Form::bsSelect('Sight', 'enum_sight', $enumSight) }}
            {{ Form::bsText('Blood Pressure', 'blood_pressure') }}
            {{ Form::bsText('Pulse', 'pulse') }}
            {{ Form::bsText('Oxygen Sats', 'oxygen_sats') }}
            {{ Form::bsText('Respiration Rate', 'respiration_rate') }}
            {{ Form::bsSelect('Smoker', 'enum_smoker', $enumSmoker) }}
            {{ Form::bsSelect('Alcohol', 'enum_alcohol', $enumAlcohol) }}
        </div>
    </div>

    <div class="card card-dark">
        <div class="card-header">Previous Assessments</div>
        <div class="card-body">
            <div class="row">
                <div class="col-2">Date</div>
                <div class="col-3">Made By</div>
                <div class="col-7">Assessment</div>

                @foreach ($resident->medicalAssessments as $medical)
                    <div class="col-2">{{ $medical->date_of_assessment }}</div>
                    <div class="col-3">{{ $medical->user->name }}</div>
                    <div class="col-7">{!! $medical->print_assessment !!}</div>
                @endforeach
            </div>
        </div>
    </div>

    {{ Form::bsSubmit('Update Medical Assessment')}}
{{ Form::close() }}