{{ Form::open(['route' => ['care-home.resident.edit-post', $resident->id], 'method' => 'post']) }}
    {{ Form::hidden('action', 'catheter') }}
    
    <div class="card card-dark">
        <div class="card-header">Record Catheter Change</div>
        <div class="card-body">
            {{ Form::bsDate('Date Catheter Changed', 'date_changed') }}
            {{ Form::bsSelect('Who Changed the Catheter', 'changed_by_user_id', $users, null, ['default' => Auth::id()]) }}
            {{ Form::bsText('Size', 'size') }}
            {{ Form::bsTextarea('Reasons for Change and Notes', 'reasons') }}
        </div>
    </div>

    <div class="card card-dark">
        <div class="card-header">Previous Assessments</div>
        <div class="card-body">
            <div class="row">
                <div class="col-2">Date</div>
                <div class="col-2">Changed By</div>
                <div class="col-1">Size</div>
                <div class="col-7">Reasons / Notes</div>

                @foreach ($resident->catheterChanges as $catheter)
                    <div class="col-2">{{ $catheter->date_changed }}</div>
                    <div class="col-2">{{ $catheter->changedByUser->name }}</div>
                    <div class="col-1">{{ $catheter->size }}</div>
                    <div class="col-7">{{ $catheter->reasons }}</div>
                @endforeach
            </div>
        </div>
    </div>

    {{ Form::bsSubmit('Update Catheter Log')}}
{{ Form::close() }}