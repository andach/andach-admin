@extends('adminlte::page')

@section('plugins.Select2', true)

@section('title', 'Resident Finance Details')

@section('content_header')
    <h1>Resident Finance Details</h1>
@stop

@section('content')
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link @if ($page ==='stay') active @endif" id="stay-tab" data-toggle="tab" href="#stay" role="tab" aria-controls="stay" aria-selected="true">Stays</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if ($page ==='invoice') active @endif" id="invoice-tab" id="invoice-tab" data-toggle="tab" href="#invoice" role="tab" aria-controls="invoice" aria-selected="false">Invoices</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if ($page ==='funding') active @endif" id="funding-tab" id="funding-tab" data-toggle="tab" href="#funding" role="tab" aria-controls="funding" aria-selected="false">Funding</a>
        </li>
    </ul>

    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade @if ($page ==='stay') show active @endif" id="stay" role="tabpanel" aria-labelledby="stay-tab">@include('care-home.resident.finance-component.stay')</div>
        <div class="tab-pane fade @if ($page ==='invoice') show active @endif" id="invoice" role="tabpanel" aria-labelledby="invoice-tab">@include('care-home.resident.finance-component.invoice')</div>
        <div class="tab-pane fade @if ($page ==='funding') show active @endif" id="funding" role="tabpanel" aria-labelledby="funding-tab">@include('care-home.resident.finance-component.funding')</div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $('select[name="responsible_user_id[0]"]').select2();
            $('select[name="responsible_user_id[1]"]').select2();
            $('select[name="responsible_user_id[2]"]').select2();
            $('select[name="responsible_user_id[3]"]').select2();
            $('select[name="responsible_user_id[4]"]').select2();
            $('select[name="responsible_user_id[5]"]').select2();
            $('select[name="responsible_user_id[6]"]').select2();
            $('select[name="responsible_user_id[7]"]').select2();
            $('select[name="responsible_user_id[8]"]').select2();
            $('select[name="responsible_user_id[9]"]').select2();
        });
    </script>
@endsection
