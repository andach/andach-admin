@extends('adminlte::page')

@section('plugins.Select2', true)

@section('title', 'Edit User')

@section('content_header')
    <h1>Edit User - {{ $user->name }}</h1>
@stop

@section('content')
    {{ Form::model($user, ['route' => ['user.edit-post', $user->id], 'method' => 'post', 'files' => true]) }}
        <div class="card card-dark">
            <div class="card-header">User Details</div>
            <div class="card-body">
                {{ Form::bsSelect('Company', 'company_id', $companies) }}
                {{ Form::bsText('Full Name', 'name', null, ['helpBlock' => 'This is the full name of the user, "Mr. John Smith", for example.']) }}
                {{ Form::bsText('Preferred Name', 'preferred_name', null, ['helpBlock' => 'This is the name the person preferrs to be addressed by, which could be "Mr. Smith", "John", "Johnny", etc.']) }}
                {{ Form::bsText('First Name', 'first_name') }}
                {{ Form::bsText('Middle Names', 'middle_names') }}
                {{ Form::bsText('Last Name', 'last_name') }}
                {{ Form::bsText('Email', 'email') }}
                {{ Form::bsPassword('Password', 'password', ['helpBlock' => 'Enter a new password only if you want this to be changed, otherwise leave this blank.']) }}
                {{ Form::bsPassword('Password (to confirm)', 'password_confirmation') }}
                {{ Form::bsSelect('Roles', 'roles[]', $roles->pluck('name', 'id'), null, ['multiple' => 'multiple']) }}
                {{ Form::bsFile('Replace Picture (optional)', 'picture_path') }}
                {{ Form::hidden('is_customer', 0) }}
                {{ Form::hidden('is_staff', 0) }}
                {{ Form::hidden('is_supplier', 0) }}
                {{ Form::bsCheckbox('Is Customer', 'is_customer', 1) }}
                {{ Form::bsCheckbox('Is Staff', 'is_staff', 1) }}
                {{ Form::bsCheckbox('Is Supplier', 'is_supplier', 1) }}
            </div>
        </div>

        <div class="card card-dark">
            <div class="card-header">Address and Contact Information</div>
            <div class="card-body">
                {{ Form::bsText('Address Line 1', 'address_1') }}
                {{ Form::bsText('Address Line 2', 'address_2') }}
                {{ Form::bsText('Address Line 3', 'address_3') }}
                {{ Form::bsText('Address Line 4', 'address_4') }}
                {{ Form::bsSelect('County', 'county_id', $counties, null, ['placeholder' => '']) }}
                {{ Form::bsText('Phone Number', 'phone_number') }}
                {{ Form::bsText('Mobile Number', 'mobile_number') }}
            </div>
        </div>

        <div class="card card-dark">
            <div class="card-header">GDPR Sensitive Information</div>
            <div class="card-body">
                {{ Form::bsSelect('Ethnic Origin', 'ethnic_origin_id', $ethnicOrigins, null, ['placeholder' => '']) }}
                {{ Form::bsSelect('Religion', 'religion_id', $religions, null, ['placeholder' => '']) }}
                {{ Form::bsSelect('Sexual Orientation', 'sexual_orientation_id', $sexualOrientations, null, ['placeholder' => '']) }}
            </div>
        </div>

        <div class="card card-dark">
            <div class="card-header">Link to Companies</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-1">Delete</div>
                    <div class="col-3">Company</div>
                    <div class="col-5">Model</div>
                    <div class="col-3">Permission To</div>

                    @php $count = 0; @endphp
                    @foreach ($user->companies as $company)
                        <div class="col-1">
                            {{ Form::checkbox('company_pivot_delete_id[]', $company->pivot->id, null, ['class' => 'form-control']) }}
                        </div>
                        <div class="col-3">
                            {{ Form::select('company_pivot_id['.$company->pivot->id.']', $companies, $company->id, ['class' => 'form-control']) }}
                        </div>
                        <div class="col-5">
                            {{ Form::text('company_model_name['.$company->pivot->id.']', $company->pivot->model_name, ['class' => 'form-control']) }}
                        </div>
                        <div class="col-3">
                            {{ Form::text('company_permission['.$company->pivot->id.']', $company->pivot->permission, ['class' => 'form-control']) }}
                        </div>

                        @php $count++; @endphp
                    @endforeach

                </div>

                <h2>Add Permission to Access another company</h2>
                {{ Form::bsSelect('Company', 'company_new_pivot_id', $companies) }}
                {{ Form::bsText('Model Name', 'company_new_model_name') }}
                {{ Form::bsText('Permission', 'company_new_permission') }}
            </div>
        </div>

        {{ Form::bsSubmit('Edit User') }}
    {{ Form::close() }}
@endsection

@section('javascript')
    <script>
        $(document).ready(function() {
            $('select[name="roles[]"]').select2();
        });
    </script>
@endsection