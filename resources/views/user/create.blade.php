@extends('adminlte::page')

@section('title', 'Create User')

@section('content_header')
    <h1>Create User</h1>
@stop

@section('content')
    <div class="info-box">
        <span class="info-box-icon bg-info"><i class="fas fa-info-circle"></i></span>

        <div class="info-box-content">
            <span class="info-box-title">Creating a User that can Login</span>
            <span class="info-box-text">To create a user that can login, you will need to supply an email and password, and ensure that "staff" is selected at the bottom of this page. </span>
        </div>
    </div>

    {{ Form::open(['route' => 'user.create-post', 'method' => 'post']) }}
        <div class="row">
            <div class="col-md-12 col-lg-6">
                <div class="card card-dark">
                    <div class="card-header">User Details</div>
                    <div class="card-body">
                        {{ Form::bsSelect('Company', 'company_id', $companies) }}
                        {{ Form::bsText('Name', 'name', null, ['helpBlock' => 'This is the full name of the user, "Mr. John Smith", for example.']) }}
                        {{ Form::bsText('Preferred Name', 'preferred_name', null, ['helpBlock' => 'This is the name the person preferrs to be addressed by, which could be "Mr. Smith", "John", "Johnny", etc.']) }}
                        {{ Form::bsText('First Name', 'first_name') }}
                        {{ Form::bsText('Middle Names', 'middle_names') }}
                        {{ Form::bsText('Last Name', 'last_name') }}
                        {{ Form::bsSelect('Roles', 'roles[]', $roles->pluck('name', 'id'), null, ['multiple' => 'multiple']) }}
                        {{ Form::bsFile('Picture (optional)', 'picture_path') }}
                        {{ Form::hidden('is_customer', 0) }}
                        {{ Form::hidden('is_staff', 0) }}
                        {{ Form::hidden('is_supplier', 0) }}
                    </div>
                </div>

                <div class="card card-dark">
                    <div class="card-header">GDPR Sensitive Information</div>
                    <div class="card-body">
                        {{ Form::bsSelect('Ethnic Origin', 'ethnic_origin_id', $ethnicOrigins, null, ['placeholder' => '']) }}
                        {{ Form::bsSelect('Religion', 'religion_id', $religions, null, ['placeholder' => '']) }}
                        {{ Form::bsSelect('Sexual Orientation', 'sexual_orientation_id', $sexualOrientations, null, ['placeholder' => '']) }}
                    </div>
                </div>
            </div>

            <div class="col-md-12 col-lg-6">
                <div class="card card-dark">
                    <div class="card-header">Email and Password Information</div>
                    <div class="card-body">
                        {{ Form::bsText('Email', 'email') }}
                        {{ Form::bsPassword('Password', 'password') }}
                        {{ Form::bsPassword('Password (to confirm)', 'password_confirmation') }}
                    </div>
                </div>

                <div class="card card-dark">
                    <div class="card-header">Address and Contact Information</div>
                    <div class="card-body">
                        {{ Form::bsText('Address Line 1', 'address_1') }}
                        {{ Form::bsText('Address Line 2', 'address_2') }}
                        {{ Form::bsText('Address Line 3', 'address_3') }}
                        {{ Form::bsText('Address Line 4', 'address_4') }}
                        {{ Form::bsSelect('County', 'county_id', $counties, null, ['placeholder' => '']) }}
                        {{ Form::bsText('Phone Number', 'phone_number') }}
                        {{ Form::bsText('Mobile Number', 'mobile_number') }}
                    </div>
                </div>
            </div>

            <div class="col-12">
                <div class="card card-dark">
                    <div class="card-header">User Roles</div>
                    <div class="card-body">
                        <p>Andach Admin uses the same database for customers, suppliers and staff. Currently only staff are able to login - but shortly we'll extend this system to allow customers and suppliers to log in as well. Please indicate what type of user you are creating by ticking the boxes below. </p>
                        {{ Form::bsCheckboxReverse('Customer', 'is_customer', 1) }}
                        {{ Form::bsCheckboxReverse('Staff', 'is_staff', 1) }}
                        {{ Form::bsCheckboxReverse('Supplier', 'is_supplier', 1) }}
                    </div>
                </div>
            </div>
        </div>

        {{ Form::bsSubmit('Create User') }}
    {{ Form::close() }}
@endsection
