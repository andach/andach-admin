@extends('adminlte::page')

@section('title', 'My Account')

@section('content_header')
    <h1>My Account</h1>
@stop

@section('content')

    {{ Form::model($user, ['route' => 'user.my-account', 'method' => 'post']) }}
        <div class="card card-dark">
            <div class="card-header">Your Details</div>
            <div class="card-body">
                {{ Form::bsText('Name', 'name', null, ['helpBlock' => 'This is the your full name, "Mr. John Smith", for example.']) }}
                {{ Form::bsText('Preferred Name', 'preferred_name', null, ['helpBlock' => 'This is the name the person you preferr to be addressed by, which could be "Mr. Smith", "John", "Johnny", etc.']) }}
                {{ Form::bsText('Email', 'email') }}
                {{ Form::bsPassword('Password', 'password', ['helpBlock' => 'Enter a new password only if you want this to be changed, otherwise leave this blank.']) }}
                {{ Form::bsPassword('Password (to confirm)', 'password_confirmation') }}
                {{ Form::bsFile('Replace Picture (optional)', 'picture_path') }}
            </div>
        </div>

        <div class="card card-dark">
            <div class="card-header">Address and Contact Information</div>
            <div class="card-body">
                {{ Form::bsText('Address Line 1', 'address_1') }}
                {{ Form::bsText('Address Line 2', 'address_2') }}
                {{ Form::bsText('Address Line 3', 'address_3') }}
                {{ Form::bsText('Address Line 4', 'address_4') }}
                {{ Form::bsSelect('County', 'county_id', $counties, ['placeholder' => '']) }}
                {{ Form::bsText('Phone Number', 'phone_number') }}
                {{ Form::bsText('Mobile Number', 'mobile_number') }}
            </div>
        </div>

        <div class="card card-dark">
            <div class="card-header">GDPR Sensitive Information</div>
            <div class="card-body">
                {{ Form::bsSelect('Ethnic Origin', 'ethnic_origin_id', $ethnicOrigins, null, ['placeholder' => '']) }}
                {{ Form::bsSelect('Religion', 'religion_id', $religions, null, ['placeholder' => '']) }}
                {{ Form::bsSelect('Sexual Orientation', 'sexual_orientation_id', $sexualOrientations, null, ['placeholder' => '']) }}
            </div>
        </div>

        {{ Form::bsSubmit('Save Details') }}
    {{ Form::close() }}
@endsection
