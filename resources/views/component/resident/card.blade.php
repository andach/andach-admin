@if ($resident)
    <div class="card card-dark">
        <div class="card-header">Resident: {{ $resident->user->name }}</div>
        <div class="card-body">
            <div class="square-img">
                <img class="rounded-circle img img-responsive full-width" src="{{ $resident->user->picture_url }}">
            </div>
            
            <p class="card-text">{!! $resident->user->full_contact_details !!}</p>
        </div>
    </div>
@else 
    <div class="card card-dark">
        <div class="card-header">No Resident Specified</div>
        <div class="card-body">
            <div class="square-img">
                <img class="rounded-circle img img-responsive full-width" src="/img/user.svg">
            </div>
        </div>
    </div>
@endif