@if(Auth::user()->appointments()->current()->count() > 1)
    <div class="alert alert-info">
        More than one appointment. Your current selected appointment is "{{ $selectedAppointment->position->job->name }}". 
        <a href="{{ route('session.appointment', $redirectRoute) }}">Pick a different appointment.</a>
    </div>
@endif