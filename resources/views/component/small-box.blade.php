<div class="small-box {{ $box['class'] }}">
    <div class="inner">
        <h3>{{ $box['number'] }}</h3>

        <p>{{ $box['name'] }}</p>
    </div>
    <div class="icon">
        <i class="{{ $box['icon'] }}"></i>
    </div>
    <a href="{{ $box['link'] }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
</div>
