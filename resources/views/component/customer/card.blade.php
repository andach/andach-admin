@if ($customer)
    <div class="card card-dark">
        <div class="card-header">{{ $customer->name }}</div>
        <div class="card-body">
            <div class="square-img">
                <img class="rounded-circle img img-responsive full-width" src="{{ $customer->picture_url }}">
            </div>
            
            <p class="card-text">{!! $customer->full_contact_details !!}</p>
        </div>
    </div>
@else 
    <div class="card card-dark">
        <div class="card-header">No Customer Specified</div>
        <div class="card-body">
            <div class="square-img">
                <img class="rounded-circle img img-responsive full-width" src="/img/user.svg">
            </div>
        </div>
    </div>
@endif