<tr>
    <td>
        <div class="icheck-primary">
            {{ Form::checkbox('message_ids[]', $message->id, null) }}
        </div>
    </td>
    <td class="mailbox-star"><a href="#">@if ($message->pivot_is_starred) <i class="fas fa-star text-warning"></i> @endif</a></td>
    <td class="mailbox-name">{!! $message->sender_name_with_link !!}</td>
    <td class="mailbox-subject">
        <b><a href="{{ route('message.show', $message->id) }}">{{ $message->name }}</a></b>
        <br />{{ $message->description_ellipsis }}
    </td>
    <td class="mailbox-attachment">{{ $message->attachment_picture }}</td>
    <td class="mailbox-date">{{ $message->sent_ago }}</td>
</tr>
