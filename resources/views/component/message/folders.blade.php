<div class="card">
    <div class="card-header">
        <h3 class="card-title">Folders</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
        </div>
    </div>

    <div class="card-body p-0">
        <ul class="nav nav-pills flex-column">
            @foreach ($folders as $folderArray)
                <li class="nav-item">
                    <a href="{{ route($folderArray['route']) }}" class="nav-link">
                    <i class="{{ $folderArray['icon'] }}"></i> {{ $folderArray['name'] }}
                    @if (isset($folderArray['unread']))
                        @if ($folderArray['unread'])
                            <span class="badge bg-primary float-right">{{ $folderArray['unread'] }}</span>
                        @endif
                    @endif
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
</div>
