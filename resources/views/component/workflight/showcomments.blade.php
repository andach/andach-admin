<div class="card card-dark">
    <div class="card-header">Comments (Current State: {{ $workflight->state->name }})</div>
    <div class="card-body">
        <div class="row">
            @if (count($workflight->comments))
                @foreach ($workflight->comments as $comment)
                    @include('component.workflight.comment', ['comment' => $comment])
                @endforeach
            @else
                <div class="col-12">There are no comments or attachments yet. </div>
            @endif
        </div>
    </div>
</div>
