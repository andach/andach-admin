<div class="row">
    <div class="col-12 @if (!$workflight->isClosed()) col-md-6 @endif">
        <div class="card card-dark direct-chat-primary">
            <div class="card-header">Comments (Current State: {{ $workflight->state->name }})</div>
            <div class="card-body">
                <div class="direct-chat-messages" style="height: auto; overflow: none">

                    <!-- initial description -->
                    <div class="direct-chat-msg @if (Auth::id() === $workflowable->user_id) right @endif">
                        <div class="direct-chat-infos clearfix">
                            <span class="direct-chat-name float-left">{{ $workflowable->user->name }}</span>
                            <span class="direct-chat-timestamp float-right">{{ $workflowable->created_at }}</span>
                        </div>
                        <!-- /.direct-chat-infos -->
                        <img class="direct-chat-img" src="{{ $workflowable->user->picture_url }}" alt="message user image">
                        <!-- /.direct-chat-img -->
                        <div class="direct-chat-text">
                            @if ($workflowable->description)
                                <p>{!! nl2br(e($workflowable->description)) !!}</p>
                            @endif
                        </div>
                    </div>

                    <!-- comments list -->
                    @if (count($workflight->comments))
                        @foreach ($workflight->comments as $comment)
                            @include('component.workflight.comment', ['comment' => $comment])
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>

    @if (!$workflight->isClosed())
        <div class="col-12 col-md-6">
            <div class="card card-dark">
                <div class="card-header">Add comment or attachment</div>
                <div class="card-body">
                    {{ Form::bsTextarea('Comment', 'workflight_comments') }}
                    {{ Form::bsFile('New Attachments', 'workflight_file') }}

                    <p><hr /></p>

                    @if (count($workflight->transitionsAvailable($workflowable->udfValuesArray())))
                        {{ Form::bsSelect('Change State', 'transition_id', $workflight->transitionsAvailable($workflowable->udfValuesArray())->pluck('name', 'id'), null, ['placeholder' => '']) }}
                    @else
                        <p>You do not have permission to change the state of this item.</p>
                    @endif
                </div>
            </div>
        </div>
    @endif
</div>