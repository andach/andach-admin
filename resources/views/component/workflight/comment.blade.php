<div class="direct-chat-msg @if (Auth::id() === $comment->user_id) right @endif">
    <div class="direct-chat-infos clearfix">
        <span class="direct-chat-name float-left">{{ $comment->user->name }}</span>
        <span class="direct-chat-timestamp float-right">{{ $comment->created_at }}</span>
    </div>
    <!-- /.direct-chat-infos -->
    <img class="direct-chat-img" src="{{ $comment->user->picture_url }}" alt="message user image">
    <!-- /.direct-chat-img -->
    <div class="direct-chat-text">
        @if ($comment->description)
            <p>{!! nl2br(e($comment->description)) !!}</p>
        @endif
        @if ($comment->attachment_path)
            <p>Provided an attachment: <a href="{{ asset($comment->web_path) }}" style="color:black">Link to file</a></p>
        @endif
    </div>
</div>