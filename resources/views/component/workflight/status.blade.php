<div class="card card-dark">
    <div class="card-header">Change Workflow State (Current State: {{ $workflowable->workflight->state->name }})</div>
    <div class="card-body">
        @if (count($workflowable->workflight->transitionsAvailable($workflowable->udfValuesArray())))
            {{ Form::bsSelect('Change State', 'transition_id', $workflowable->workflight->transitionsAvailable($workflowable->udfValuesArray())->pluck('name', 'id'), null, ['placeholder' => '']) }}
        @else
            <p>You do not have permission to change the state of this item.
        @endif
    </div>
</div>
