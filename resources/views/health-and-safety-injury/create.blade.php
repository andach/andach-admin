@extends('adminlte::page')

@section('plugins.Select2', true)

@section('title', 'Create Injury')

@section('content_header')
    <h1>Create Injury</h1>
@stop

@section('content')
    {{ Form::open(['route' => ['health-and-safety-incident.create-injury-post', $incident->id], 'method' => 'post']) }}
    <div class="row">
        <div class="col-6">
            <div class="card card-dark">
                <div class="card-header">Injured Party</div>
                <div class="card-body">
                    {{ Form::bsSelect('User', 'user_id', $users, null, ['placeholder' => '', 'helpBlock' => 'If applicable. If the injured party doesn\'t have a user, enter details below.']) }}

                    {{ Form::bsText('Name', 'injured_person_name') }}
                    {{ Form::bsTextarea('Additional Details', 'injured_person_details') }}
                </div>
            </div>
        </div>

        <div class="col-6">
            <div class="card card-dark">
                <div class="card-header">Injured Body Part(s)</div>
                <div class="card-body">
                    @foreach ($bodyParts as $key => $bodyPart)
                        {{ Form::bsCheckbox($bodyPart, 'body_part[]', $key) }}
                    @endforeach
                </div>
            </div>
        </div>

        <div class="col-12">
            <div class="card card-dark">
                <div class="card-header">Incapacitation and Details</div>
                <div class="card-body">
                    {{ Form::hidden('was_incapacitated', 0) }}
                    {{ Form::hidden('was_medically_treated', 0) }}
                    {{ Form::hidden('was_taken_to_hospital', 0) }}

                    {{ Form::bsCheckboxReverse('Was this person incapacitated?', 'was_incapacitated', 1) }}
                    {{ Form::bsCheckboxReverse('Was this person medically treated?', 'was_medically_treated', 1) }}
                    {{ Form::bsCheckboxReverse('Was this person taken to hospital?', 'was_taken_to_hospital', 1) }}
                    
                    <div class="col-12">If the person was a worker for you and was off work as a result of this injury, please record the dates they were sent off work, came back on light duties (if applicable) and were able to come back to work fully.</div>

                    {{ Form::bsDate('Off Work', 'incapacitated_from_date') }}
                    {{ Form::bsDate('Back on Light Duties', 'returned_to_light_duties_date') }}
                    {{ Form::bsDate('Back to Work Fully', 'returned_fully_date') }}
                </div>
            </div>
        </div>
    </div>

    {{ Form::bsSubmit('Register Injury') }}
    {{ Form::close() }}
@endsection

@section('javascript')
    <script>
        $(document).ready(function() {
            $('select[name="customer_id"]').select2();
            $('select[name="user_id"]').select2();
        });
    </script>
@endsection
