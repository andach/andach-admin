@extends('adminlte::page')

@section('title', 'Setup Locations')

@section('content_header')
    <h1>Setup Locations</h1>
@stop

@section('content')
    {{-- {{ Form::open(['route' => 'location.index-post', 'method' => 'post']) }} --}}
        <div class="card card-dark">
            <div class="card-header">All Locations</div>
            <div class="card-body">
                <table id="tree">
                    <thead>
                        <th>ID</th>
                        <th>Location</th>
                        <th>Is Rentable?</th>
                    </tr>
                    </thead>
                    <!-- Optionally define a row that serves as template, when new nodes are created: -->
                    <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <button onclick="addChild()">Add Child</button>
                <button onclick="addSibling()">Add Sibling</button>
                <button onclick="expandAll()">Expand All</button>
                <button onclick="deleteLocation()">Delete Location</button>
            </div>
        </div>

        {{-- {{ Form::bsSubmit('Update Locations') }}
    {{ Form::close() }} --}}
@endsection

@section('js')
<script>
    $(document).ready(function() {
        $("#tree").fancytree({
            extensions: ['edit', 'filter', 'table'],
            source: {
                url: "/location/json",
                cache: false
            },

            
            renderColumns: function(event, data) {
                var node = data.node,
                $tdList = $(node.tr).find(">td");

                // (index #0 is rendered by fancytree by adding the checkbox)
                $tdList.eq(1).text(node.getIndexHier());

                if (node.data.is_rentable)
                {
                    $tdList.eq(2).html("<input type='checkbox' name='like' value='" + node.key + "' checked='checked'>");
                } else {
                    $tdList.eq(2).html("<input type='checkbox' name='like' value='" + node.key + "'>");
                }
            },
            
            edit: {
                triggerStart: ["clickActive", "dblclick", "f2", "mac+enter", "shift+click"],
                beforeEdit: function(event, data){
                    // Return false to prevent edit mode
                },
                edit: function(event, data){
                    // Editor was opened (available as data.input)
                },
                beforeClose: function(event, data)
                {
                    // Return false to prevent cancel/save (data.input is available)
                    console.log(event.type, event, data);
                    if( data.originalEvent.type === "mousedown" ) 
                    {
                    // We could prevent the mouse click from generating a blur event
                    // (which would then again close the editor) and return `false` to keep
                    // the editor open:
            //                  data.originalEvent.preventDefault();
            //                  return false;
                    // Or go on with closing the editor, but discard any changes:
            //                  data.save = false;
                    }
                },
                save: function(event, data)
                {
                    // Save data.input.val() or return false to keep editor open
                    console.log("save ATTEMPTE...", this.key, data.input.val());
                    console.log("save ...", this, data);

                    var xhttp = new XMLHttpRequest();
                    if (!data.isNew) {
                        xhttp.open("GET", "/location/ajax/update/"+this.key+"/"+data.input.val(), true);
                        xhttp.send();
                    } else {
                        parentID = this.parent.key;
                        if (isNaN(parentID)) {
                            parentID = 0;
                        }

                        xhttp.open("GET", "/location/ajax/create/"+parentID+"/"+data.input.val(), true);
                        xhttp.send();
                    }

                    location.reload();
                },
                close: function(event, data) {
                    // Editor was removed
                    if(data.save) {
                        // Since we started an async request, mark the node as preliminary
                        $(data.node.span).addClass("pending");
                    }
                }
            }
        });
    });

    $("#tree").on("click", "input[name=like]", function(e){
        var node = $.ui.fancytree.getNode(e),
        $input = $(e.target);

        e.stopPropagation();  // prevent fancytree activate for this row

        if($input.is(":checked")){
            var xhttp = new XMLHttpRequest();
            xhttp.open("GET", "/location/ajax/is-rentable/"+node.key+"/1", true);
            xhttp.send();
        } else {
            var xhttp = new XMLHttpRequest();
            xhttp.open("GET", "/location/ajax/is-rentable/"+node.key+"/0", true);
            xhttp.send();
        }
    });

    function addChild() {
        var node = $.ui.fancytree.getTree("#tree").getActiveNode();
        if( !node ) {
            alert("Please select a location.");
            return;
        }
        node.editCreateNode("child", "Node title");
    }

    function addSibling() {
        var node = $.ui.fancytree.getTree("#tree").getActiveNode();
        if( !node ) {
            alert("Please select a location.");
            return;
        }
        node.editCreateNode("after", {
            title: "New location"
        });
    }

    function deleteLocation() {
        var node = $.ui.fancytree.getTree("#tree").getActiveNode();

        console.log(node);
        
        var xhttp = new XMLHttpRequest();
        xhttp.open("GET", "/location/ajax/delete/"+node.key, true);
        xhttp.send();

        node.remove();
    }

    function expandAll() {
        $.ui.fancytree.getTree("#tree").expandAll();
    }
</script>
@endsection