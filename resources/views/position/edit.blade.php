@extends('adminlte::page')

@section('title', 'Edit Position')

@section('content_header')
    <h1>Edit Position</h1>
@stop

@section('content')
    {{ Form::model($position, ['route' => ['position.edit-post', $position->id], 'method' => 'post']) }}
        <div class="card card-dark">
            <div class="card-header">Edit Position</div>
            <div class="card-body">
                    {{ Form::bsText('Auth Hours per Week', 'hours_per_week') }}
                    {{ Form::bsDate('From Date', 'from_date') }}
                    {{ Form::bsDate('To Date', 'to_date') }}
                    {{ Form::bsSelect('Company', 'company_id', $companies) }}
                    {{ Form::bsSelect('Job', 'job_id', $jobs) }}
                    {{ Form::bsSelect('Location', 'location_id', $locations) }}
                    {{ Form::bsSelect('Team', 'team_id', $teams) }}
                    {{ Form::bsSelect('Cost Code', 'cost_code_id', $costCodes) }}
            </div>
        </div>
        {{ Form::bsSubmit('Edit Position') }}
    {{ Form::close() }}
@endsection
