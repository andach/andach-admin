@extends('adminlte::page')

@section('title', 'Positions')

@section('content_header')
    <h1>All Positions</h1>
@stop

@section('info')
    A <b>position</b> is a requirement to perform a certain number of hours a week of work, in a location. For example, your business may require 20 hours a week of receptionist work, in your head office. Or you may have 105 hours of work a week for Accounts Assistants. <br /><br />
    Having a position active is a <i>total number of authorised hours</i>. The 105 hours of work a week for Accounts Assistants could be three full-time employees at 35 hours per week, or a combination of part-time employees, as long as the total hours doesn't exceed 105 hours. <br /><br />
    By using position management, you can give team leaders and managers the right to manage and hire their own staff, subject to the hours limited by the positions, and the pay scales limited by the Job. 
@endsection

@section('content')
    <div class="card card-dark">
        <div class="card-header">All Positions</div>
        <div class="card-body">
            @if ($positions->count())
                <div class="row">
                    <div class="col-1">ID</div>
                    <div class="col-2">Company / Location</div>
                    <div class="col-2">Job</div>
                    <div class="col-2">Team / Cost Code</div>
                    <div class="col-2">Dates</div>
                    <div class="col-2">Hours</div>
                    <div class="col-1">Edit?</div>

                    @foreach ($positions as $position)
                        <div class="col-1">
                            <a href="{{ route('position.show', $position->id) }}">{{ $position->id }}</a>
                        </div>
                        <div class="col-2">{{ $position->company->name }}<br />{{ $position->location->name }}</div>
                        <div class="col-2">{{ $position->job->name }}</div>
                        <div class="col-2">{{ $position->team->name }}<br />{{ $position->costCode->name }}</div>
                        <div class="col-2">{{ $position->dates }}</div>
                        <div class="col-2">{{ $position->hours }}</div>
                        <div class="col-1">
                            <a href="{{ route('position.edit', $position->id) }}">Edit</a>
                        </div>
                    @endforeach
                </div>
            @else 
                <div class="card-empty-body">There are no positions set up yet.</div>
            @endif
        </div>
    </div> 

    {{ Form::open(['route' => 'position.create-post', 'method' => 'post']) }}
        <div class="card card-dark">
            <div class="card-header">Add a New Position</div>
            <div class="card-body">
                    {{ Form::bsText('Auth Hours per Week', 'hours_per_week') }}
                    {{ Form::bsDate('From Date', 'from_date') }}
                    {{ Form::bsDate('To Date', 'to_date') }}
                    {{ Form::bsSelect('Company', 'company_id', $companies) }}
                    {{ Form::bsSelect('Job', 'job_id', $jobs, null, ['helpBlock' => '<a href="'.route('hr.jobs').'">Manage Jobs</a>.']) }}
                    {{ Form::bsSelect('Location', 'location_id', $locations, null, ['helpBlock' => '<a href="'.route('location.index').'">Manage Locations</a>.']) }}
                    {{ Form::bsSelect('Team', 'team_id', $teams, null, ['helpBlock' => '<a href="'.route('hr.teams').'">Manage Teams</a>.']) }}
                    {{ Form::bsSelect('Cost Code', 'cost_code_id', $costCodes, null, ['helpBlock' => '<a href="'.route('cost-code.index').'">Manage Cost Codes</a>.']) }}
            </div>
        </div>
        {{ Form::bsSubmit('Create Position') }}
    {{ Form::close() }}
@endsection
