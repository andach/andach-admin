@extends('adminlte::page')

@section('title', 'Create Customer')

@section('content_header')
    <h1>Create Customer</h1>
@stop

@section('content')
    
    {{ Form::open(['route' => 'customer.create-post', 'method' => 'post']) }}
        <div class="card card-dark">
            <div class="card-header">Customer Details</div>
            <div class="card-body">
                {{ Form::bsSelect('Company', 'company_id', $companies) }}
                {{ Form::bsText('Name', 'name', null, ['helpBlock' => 'This is the full name of the user, "Mr. John Smith", for example.']) }}
                {{ Form::bsText('Preferred Name', 'preferred_name', null, ['helpBlock' => 'This is the name the person preferrs to be addressed by, which could be "Mr. Smith", "John", "Johnny", etc.']) }}
                {{ Form::bsText('First Name', 'first_name') }}
                {{ Form::bsText('Middle Names', 'middle_names') }}
                {{ Form::bsText('Last Name', 'last_name') }}
                {{ Form::bsText('Email', 'email') }}
                {{ Form::bsPassword('Password', 'password') }}
                {{ Form::bsPassword('Password (to confirm)', 'password_confirmation') }}
                {{ Form::bsFile('Picture (optional)', 'picture_path') }}
            </div>
        </div>

        <div class="card card-dark">
            <div class="card-header">Address and Contact Information</div>
            <div class="card-body">
                {{ Form::bsText('Address Line 1', 'address_1') }}
                {{ Form::bsText('Address Line 2', 'address_2') }}
                {{ Form::bsText('Address Line 3', 'address_3') }}
                {{ Form::bsText('Address Line 4', 'address_4') }}
                {{ Form::bsSelect('County', 'county_id', $counties, ['placeholder' => '']) }}
                {{ Form::bsText('Phone Number', 'phone_number') }}
                {{ Form::bsText('Mobile Number', 'mobile_number') }}
            </div>
        </div>

        <div class="card card-dark">
            <div class="card-header">GDPR Sensitive Information</div>
            <div class="card-body">
                {{ Form::bsSelect('Ethnic Origin', 'ethnic_origin_id', $ethnicOrigins, null, ['placeholder' => '']) }}
                {{ Form::bsSelect('Religion', 'religion_id', $religions, null, ['placeholder' => '']) }}
                {{ Form::bsSelect('Sexual Orientation', 'sexual_orientation_id', $sexualOrientations, null, ['placeholder' => '']) }}
            </div>
        </div>

        {{ Form::bsSubmit('Create Customer') }}
    {{ Form::close() }}
@endsection
