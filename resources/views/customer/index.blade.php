@extends('adminlte::page')

@section('title', 'All Customers')

@section('content_header')
    <h1>All Customers</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-3">Name</div>
        <div class="col-3">Email</div>
        <div class="col-2">Staff / Customer / Supplier</div>
        <div class="col-3">Open Workflow Objects</div>
        <div class="col-1">Edit</div>
        @foreach ($users as $user)
            <div class="col-3">{{ $user->name }}</div>
            <div class="col-3">{{ $user->email }}</div>
            <div class="col-2">{{ $user->printStaffCustomerSupplier }}</div>
            <div class="col-3">{{ $user->printOpenWorkflowObjects }}</div>
            <div class="col-1"><a href="{{ route('user.edit', $user->id) }}">Edit</a></div>
        @endforeach
    </div>
@endsection
