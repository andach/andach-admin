@extends('adminlte::page')

@section('title', 'Show Customer')

@section('content_header')
    <h1>Show Customer - {{ $user->name }}</h1>

    <div class="card-deck">
        <div class="card">
            <div class="card-header">Customer Details</div>
            <div class="card-body">
                <p><img src="{{ $user->picture_url }}" style="border-radius: 50%;" alt="Customer Image"></p>
                <p>
                    {{ $user->name }}
                    @if ($user->preferred_name)
                        <br />($user->preferred_name)
                    @endif
                </p>
                <p>{{ $user->email }}</p>
            </div>
        </div>

        <div class="card">
            <div class="card-header">Address & Contact Details</div>
            <div class="card-body">
                <p><i class="fas fa-home"></i>{{ $user->address }}</p>
                <p><i class="fas fa-phone"></i>{{ $user->phone_number }}</p>
                <p><i class="fas fa-mobile-alt"></i>{{ $user->mobile_number }}</p>
            </div>
        </div>
    </div>

    @if ($user->tickets()->count())
        <div class="card">
            <div class="card-header">Tickets</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-1">ID</div>
                    <div class="col-9">Name</div>
                    <div class="col-2">State</div>
                    <div class="col-1">Is Closed?</div>

                    @foreach ($user->tickets as $ticket)
                        <div class="col-1">{{ $ticket->id }}</div>
                        <div class="col-9">{{ $ticket->name }}</div>
                        <div class="col-2">{{ $ticket->workflight->state->name }}</div>
                        <div class="col-1">{{ $ticket->isClosed() }}</div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
@stop

@section('content')
@endsection
