@extends('adminlte::page')

@section('title', 'Roles')

@section('content_header')
    <h1>Roles</h1>
    <p><a href="{{ route('role.create') }}">Create New Role</a></p>
@stop

@section('content')
    <div class="card card-dark">
        <div class="card-header">All Roles</div>
        <div class="card-body">
            <div class="row">
                <div class="col-2">Name</div>
                <div class="col-5">Permissions</div>
                <div class="col-5">Transitions</div>

                @foreach ($roles as $role)
                    <div class="col-2"><a href="{{ route('role.edit', $role->id) }}">{{ $role->name }}</a></div>
                    <div class="col-5">{!! implode('<br />', $role->rolePermissions()->pluck('access_to')->toArray()) !!}</div>
                    <div class="col-5">{!! implode('<br />', $role->transitions()->pluck('name')->toArray()) !!}</div>
                @endforeach
        </div>
    </div>
@endsection
