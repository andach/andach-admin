@extends('adminlte::page')

@section('title', 'Edit Role')

@section('content_header')
    <h1>Edit Role - {{ $role->name }}</h1>
@stop

@section('content')
    {{ Form::model($role, ['route' => ['role.edit-post', $role->id], 'method' => 'post']) }}
        <div class="card card-dark">
            <div class="card-header">Name</div>
            <div class="card-body">{{ Form::bsText('Name', 'name') }}</div>
        </div>

        <div class="card card-dark">
            <div class="card-header">Access Levels</div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col" class="text-center">Name</th>
                            <th scope="col" class="text-center">Read</th>
                            <th scope="col" class="text-center">Manage Mine</th>
                            <th scope="col" class="text-center">Edit</th>
                            <th scope="col" class="text-center">Edit / Update</th>
                            <th scope="col" class="text-center">Administrate</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($menuConfig as $name => $arrayOfPossibilities)
                            <tr>
                                <td scope="col">{{ $name }}</td>

                                @foreach ($menuKeys as $key)
                                    <td>
                                        @if (array_key_exists($key, $arrayOfPossibilities))
                                            {{ Form::checkbox('rolePermission['.$name.'.'.$key.']', 1, in_array($name.'.'.$key, $accessTo), ['class' => 'form-control']) }}
                                        @endif
                                    </td>
                                @endforeach
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            @foreach ($transitions as $workflowID => $transitionArray)
                <div class="col">
                    <div class="card card-dark">
                        <div class="card-header">{{ $workflows[$workflowID] }}</div>
                        <div class="card-body">
                            <div class="row">
                                @foreach ($transitionArray as $transition)
                                    <div class="col-3">{{ Form::checkbox('transitions[]', $transition->id, $role->transitions->contains($transition->id), ['class' => 'form-control']) }}</div>
                                    <div class="col-9">{{ $transition->name }}</div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        @if (count($companies) > 1)
            <div class="card card-dark">
                <div class="card-header">Please Select the Companies to apply this access to</div>
                <div class="card-body">
                    <div class="row">
                        @foreach ($companies as $company)
                            <div class="col-3">{{ Form::checkbox('companies[]', $company->id, $role->companies->contains($company->id), ['class' => 'form-control']) }}</div>
                            <div class="col-9">{{ $company->name }}</div>
                        @endforeach
                    </div>
                </div>
            </div>
        @else
            {{ Form::hidden('companies[]', 1) }}
        @endif

        {{ Form::bsSubmit('Edit Role') }}
    {{ Form::close() }}
@endsection
