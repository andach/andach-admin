@extends('adminlte::page')

@section('title', 'Show Cost Code')

@section('content_header')
    <h1>Show Cost Code</h1>
@stop

@section('content_header')
    <div class="card-dark">
        <div class="card">
            <div class="card-header">Cost Code Details</div>
            <div class="card-body">
                <p>{{ $costCode->name }}</p>
                <p>{{ $costCode->code }}</p>
                <p>{{ $costCode->parent_name }}</p>
            </div>
        </div>
    </div>
@stop

@section('content')
@endsection
