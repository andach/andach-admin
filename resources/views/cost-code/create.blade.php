@extends('adminlte::page')

@section('title', 'Create Cost Code')

@section('content_header')
    <h1>Create Cost Code</h1>
@stop

@section('content')
    
    {{ Form::open(['route' => 'cost-code.create-post', 'method' => 'post']) }}
        <div class="card card-dark">
            <div class="card-header">Cost Code Details</div>
            <div class="card-body">
                {{ Form::bsText('Name', 'name') }}
                {{ Form::bsText('Code', 'code') }}
                {{ Form::bsSelect('Parent', 'parent_id', $costCodes, null, ['placeholder' => '']) }}
            </div>
        </div>

        {{ Form::bsSubmit('Create Cost Code') }}
    {{ Form::close() }}
@endsection
