@extends('adminlte::page')

@section('title', 'All Cost Codes')

@section('content_header')
    <h1>All Cost Codes</h1>
    <p><a href="{{ route('cost-code.create') }}">Create a new Cost Code</a></p>
@stop

@section('content')
    <div class="card card-dark">
        <div class="card-header">All Cost Codes</div>
        <div class="card-body">
            @if ($costCodes->count())
                <div class="row">
                    <div class="col-4">Name</div>
                    <div class="col-2">Code</div>
                    <div class="col-4">Parent</div>
                    <div class="col-2">Edit</div>
                    @foreach ($costCodes as $costCode)
                        <div class="col-4">{{ $costCode->name }}</div>
                        <div class="col-2">{{ $costCode->code }}</div>
                        <div class="col-4">{{ $costCode->parent_name }}</div>
                        <div class="col-2"><a href="{{ route('cost-code.edit', $costCode->id) }}">Edit</a></div>
                    @endforeach
                </div>
            @else 
                <div class="card-empty-body">There are no cost codes set up yet.</div>
            @endif
        </div>
    </div>
@endsection
