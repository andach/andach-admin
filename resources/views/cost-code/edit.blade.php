@extends('adminlte::page')

@section('title', 'Edit Cost Code')

@section('content_header')
    <h1>Edit Cost Code</h1>
@stop

@section('content')
    {{ Form::model($costCode, ['route' => ['cost-code.edit-post', $costCode->id], 'method' => 'post']) }}
        <div class="card card-dark">
            <div class="card-header">Cost Code Details</div>
            <div class="card-body">
                {{ Form::bsText('Name', 'name') }}
                {{ Form::bsText('Code', 'code') }}
                {{ Form::bsSelect('Parent', 'parent_id', $costCodes, ['placeholder' => '']) }}
            </div>
        </div>

        {{ Form::bsSubmit('Edit Cost Code') }}
    {{ Form::close() }}
@endsection