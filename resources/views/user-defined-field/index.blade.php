@extends('adminlte::page')

@section('title', 'All UDFs')

@section('content_header')
    <h1>All User Defined Fields</h1>
@stop

@section('content')
    <div class="info-box">
        <span class="info-box-icon bg-info"><i class="fas fa-info-circle"></i></span>

        <div class="info-box-content">
            <span class="info-box-title">Help - User Defined Fields</span>
            <span class="info-box-text">A "User Defined Field" is some additional information that your company needs to add and capture to a ticket, complaint or health and safety incident.
                They can be a date, number, text or a list of values in a drop-down box. 
                This is all fully customisable to ensure that your company captures the information you require. </span>
        </div>
    </div>

    @if (count($udfs))
        <div class="card card-dark">
            <div class="card-header">All User Defined Fields</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-3">Name</div>
                    <div class="col-3">Display Name</div>
                    <div class="col-3">Type</div>
                    <div class="col-3">Model</div>

                    @foreach ($udfs as $udf)
                        <div class="col-3"><a href="{{ route('user-defined-field.edit', $udf->id) }}">{{ $udf->name }}</a></div>
                        <div class="col-3">{{ $udf->display_name }}</div>
                        <div class="col-3">{{ $udf->type }}</div>
                        <div class="col-3">{{ $udf->model_name }}</div>
                    @endforeach
                </div>
            </div>
            <div class="card-footer">There are {{ count($udfs) }} fields defined.</div>
        </div>
    @else
        <div class="card card-dark">
            <div class="card-header">User Defined Fields</div>
            <div class="card-body">There are no user defined fields. Do you want to <a href="{{ route('user-defined-field.create') }}">create one</a>?</div>
        </div>
    @endif
@endsection
