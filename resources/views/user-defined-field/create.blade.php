@extends('adminlte::page')

@section('title', 'Create UDF')

@section('content_header')
    <h1>Create User Defined Field</h1>
@stop

@section('content')
    {{ Form::open(['route' => 'user-defined-field.create-post', 'method' => 'post']) }}
        <div class="card card-dark">
            <div class="card-header">UDF Information</div>
            <div class="card-body">
                {{ Form::bsText('Name', 'name', null, ['helpBlock' => 'The name of this item is used in the background for the form. It must be unique, and contain letters, numbers and underscores only. It will not be displayed anywhere. Examples include "ticket_severity" and "number_of_things".']) }}
                {{ Form::bsText('Display Name', 'display_name', null, ['helpBlock' => 'This is the text that will be shown to your staff in the app. Format this appropriately as it will be shown on screen, like "Ticket Severity" or "Number of Things".']) }}
                {{ Form::bsSelect('Type', 'type', $types) }}
                {{ Form::bsSelect('Model', 'model_name', $models) }}
                {{ Form::bsText('Values', 'items', null, ['helpBlock' => 'If you have selected "list" above, give a commma-separated list of possible options. For example, if this was a \'severity\' field, you might enter "Low,Medium,High,Urgent"']) }}
                {{ Form::bsTextarea('Description', 'description') }}
            </div>
        </div>

        {{ Form::bsSubmit('Create User Defined Field') }}
    {{ Form::close() }}
@endsection
