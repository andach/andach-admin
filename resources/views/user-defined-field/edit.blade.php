@extends('adminlte::page')

@section('title', 'Edit UDF')

@section('content_header')
    <h1>Edit User Defined Field</h1>
@stop

@section('content')    
    {{ Form::model($udf, ['route' => ['user-defined-field.edit-post', $udf->id], 'method' => 'post']) }}
        <div class="card card-dark">
            <div class="card-header">UDF Information</div>
            <div class="card-body">
                {{ Form::bsText('Name', 'name', null, ['helpBlock' => 'The name of this item is used in the background for the form. It must be unique, and contain letters, numbers and underscores only. It will not be displayed anywhere. Examples include "ticket_severity" and "number_of_things".']) }}
                {{ Form::bsText('Display Name', 'display_name', null, ['helpBlock' => 'This is the text that will be shown to your staff in the app. Format this appropriately as it will be shown on screen, like "Ticket Severity" or "Number of Things".']) }}
                {{ Form::bsSelect('Type', 'type', $types, null, ['disabled' => 'disabled']) }}
                {{ Form::bsSelect('Model', 'model_name', $models, null, ['disabled' => 'disabled']) }}
                {{ Form::bsTextarea('Description', 'description') }}

                @if ($udf->type == 'list')
                    <div class="row">
                        <div class="col-12">Allowed Values</div>

                        <div class="col-2">Delete?</div>
                        <div class="col-10">Value</div>

                        @foreach ($udf->items as $item)
                            <div class="col-2">{{ Form::checkbox('delete_values[]', $item->id, null, ['class' => 'form-control']) }}</div>
                            <div class="col-10">{{ $item->name }}</div>
                        @endforeach
                    </div>

                    {{ Form::bsText('Add New Values', 'newItems', null, ['helpText' => 'Add any new values as a comma-separated list']) }}
                @endif
            </div>
        </div>
            
        {{ Form::bsSubmit('Edit User Defined Field') }}
    {{ Form::close() }}
@endsection
