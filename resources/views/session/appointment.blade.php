@extends('adminlte::page')

@section('title', 'Pick Appointment')

@section('content_header')
    <h1>Pick Appointment</h1>
@stop

@section('content')
    <div class="row">
        @foreach ($appointments as $appointment)
            <div class="col">
                {{ Form::open(['route' => 'session.appointment-post', 'method' => 'post']) }}
                    <div class="card card-dark">
                        <div class="card-header">{{ $appointment->position->job->name }}</div>
                        <div class="card-body">
                            {{ Form::hidden('appointment_id', $appointment->id) }}
                            {{ Form::hidden('redirectRoute', $redirectRoute) }}
                            {{ Form::bsSubmit('Select this Appointment') }}
                        </div>
                    </div>
                {{ Form::close() }}
            </div>
        @endforeach
    </div>
@endsection