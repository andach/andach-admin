@extends('landlord.template')

@section('content')
<section class="page-section" id="success">
    <div class="container">
        <!-- Portfolio Section Heading-->
        <div class="text-center">
            <h2 class="page-section-heading text-secondary mb-0 d-inline-block">YOUR NEW DASHBOARD</h2>
        </div>
        <!-- Icon Divider-->
        <div class="divider-custom">
            <div class="divider-custom-line"></div>
            <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
            <div class="divider-custom-line"></div>
        </div>
        <div class="row">
            <div class="col-lg-4 ml-auto">
                <p class="lead text-blue">
                    Thank you for signing up for Andach Admin. 
                    Your dashboard can be accessed at <a href="https://{{ $subdomain ?? 'example' }}.andachadmin.co.uk">https://{{ $subdomain ?? 'example' }}.andachadmin.co.uk</a>. 
                    For support, please contact <a href="mailto:info@andach.co.uk">info@andach.co.uk</a>. 
                </p>
            </div>
            <div class="col-lg-4 mr-auto">
                <p class="lead text-blue">
                    Login to your dashboard with the email address <b>{{ $email ?? 'example@example.com' }}</b> and the password you registered at sign up. 
                    Your first user is a super-admin and will be able to set up your entire dashboard. You can change this after setup. 
                </p>
            </div>
        </div>
    </div>
</section>
@endsection