@extends('landlord.template')

@section('content')
    <h2>What is "Workflow" in Andach Admin?</h2>
    <p>A "Workflow" in Andach Admin is a series of <i>states</i> and <i>transitions</i> that can be applied to a Ticket, Complaint or Health and Safety Incident.</p>
    <p>A state is a status that a Ticket (etc.) 'sits at'. Common states can be 'Open', 'Closed', 'Waiting Customer Information', etc. Certain items at certain states might only be viewable by certain groups of people. </p>
    <p>A transition is a way of moving from one status to another. </p>

    <h3>The Simplest Setup</h3>
    <p>The simplest meaningful setup is to have two states - "Open" and "Closed", and one transition, "Closing". This transition will move a Ticket from the open to closed state. </p>
    <p>You might want to allow tickets to be re-opened. In which case you'd create another transition, probably called "Reopening", moving the ticket from closed to open.</p>

    <h3>A More Complex Setup</h3>
    <p>Larger organisations may have more complex process flows. A food manufacturing business might receive complaints about their products that require response, or require skilled investigation - perhaps they made a customer sick. </p>
    <p>Here, we would set up a number of states. Open, Health and Safety Investigation, Requires Company Response, Waiting Customer Information, Closed. </p>
    <p>We would also need a number of transitions to be able to move complaints through these states. These could be:</p>
    <ul>
        <li>Closing - from Open to Closed</li>
        <li>Requires Customer Investigation - from Open to Waiting Customer Investigation</li>
        <li>Customer Information Received - from Waiting Customer Investigation to Open</li>
        <li>Requires Investigation - from Open to Health and Safety Investigation</li>
        <li>Requies Response - from Health and Safety Investigation to Requires Company Response</li>
        <li>Closing After Response - from Requires Company Response to Closed</li>
    </ul>
    <p>This example would be suitable for a company whereby a first team gathers information about the complaint (maybe writing to the customer to learn more about the problem), and then passes some of them on to a health and safety team (where necessary) who would investigate this and get back to the customer.</p>
    <p>So one workflow can cover minor complaints or queries - which would be logged to help identify patterns within the data - and serious queries which will be passed onto a second team.</p>
@endsection