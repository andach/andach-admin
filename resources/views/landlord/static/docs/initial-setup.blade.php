@extends('landlord.template')

@section('content')
    <h2>Setting Up Roles for your Organisation</h2>
    <p class="text-secondary">Andach Admin is a highly customisable piece of software, and this setup process can be complex. For any of our clients
        who use our software, we'll set this up for you, we don't recommend doing this yourself.</p>
    <p>When you first sign up for Andach Admin, you'll need to set up your permissions and roles for any users that you want to create. 
        If you're a sole trader, or you only need one person to log into the system, this can be skipped. You'll need to be logged in
        as your admin user (the first user created at signup) to get this started.</p>
    <p>On the left side of your screen, click on the "Permissions / Roles" menu item. As an example, we'll create two roles. One to 
        view and respond to tickets, and one to administrate them completely.</p>

    <h3>Creating a new Role</h3>
    <p>On your menu, click "Manage Roles", then "Create New Role" at the top. 
@endsection