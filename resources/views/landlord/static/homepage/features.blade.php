<section class="page-section portfolio" id="features">
    <div class="container">
        <!-- Portfolio Section Heading-->
        <div class="text-center">
            <h2 class="page-section-heading text-secondary mb-0 d-inline-block">FEATURES</h2>
        </div>
        <!-- Icon Divider-->
        <div class="divider-custom">
            <div class="divider-custom-line"></div>
            <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
            <div class="divider-custom-line"></div>
        </div>
        <!-- Portfolio Grid Items-->
        <div class="row justify-content-center">
            <!-- Portfolio Items-->
            @include('landlord.static.homepage.component.feature', ['number' => 0, 'title' => 'Health and Safety', 'image' => 'biohazard'])
            @include('landlord.static.homepage.component.feature', ['number' => 1, 'title' => 'Complaints', 'image' => 'complain'])
            @include('landlord.static.homepage.component.feature', ['number' => 2, 'title' => 'CRM', 'image' => 'crm'])
            @include('landlord.static.homepage.component.feature', ['number' => 3, 'title' => 'GDPR', 'image' => 'lock'])
        </div>
    </div>
</section>