<form action="{{ route('landlord.sign-up') }}" method="post">
    @csrf
    <section class="page-section bg-primary mb-0" id="sign-up">
        <div class="container">
            <!-- About Section Heading-->
            <div class="text-center">
                <h2 class="page-section-heading d-inline-block">SIGN UP</h2>
            </div>
            <!-- Icon Divider-->
            <div class="divider-custom divider-light">
                <div class="divider-custom-line"></div>
                <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                <div class="divider-custom-line"></div>
            </div>
            <!-- About Section Content-->
            <div class="row">
                <div class="col-lg-8 m-auto">
                    {{ Form::bsText('Name', 'name', null, ['helpBlock' => 'The name of the person to be the main contact for this service. The system will create them an administrator login.']) }}
                    {{ Form::bsEmail('Email', 'email', null, ['helpBlock' => 'Your main contact\'s email. This will be what you use to login to the system.']) }}
                    {{ Form::bsText('Your Company', 'company') }}
                    {{ Form::bsPassword('Password', 'password') }}
                    {{ Form::bsPassword('Password (Confirm)', 'password_confirmation') }}
                    {{ Form::bsText('Desired Subdomain', 'subdomain', null, ['helpBlock' => 'Enter the subdomain you would like. Your dashboard will be accessible at https://subdomain.andachadmin.co.uk']) }}
                    {{ Form::bsSubmit('Sign Up for Free Today!')}}
                </div>
            </div>
        </div>
    </section>
</form>