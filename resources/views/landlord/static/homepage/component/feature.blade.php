<div class="col-md-6 col-lg-4 mb-5">
    <div class="portfolio-item mx-auto" data-toggle="modal" data-target="#portfolioModal{{ $number }}">
        <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
            <div class="portfolio-item-caption-content text-center"><i class="fas fa-plus fa-3x"></i></div>
        </div>
        <img class="img-fluid" src="/landlord/portfolio/{{ $image }}.svg" alt="{{ $title }}"/>
    </div>
    <div class="justify-content-center">
        <h3 class="modal-h3">{{ $title }}</h3>
    </div>
</div>