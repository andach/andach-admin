<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Andach Admin | Documentation</title>
        <!-- Font Awesome icons (free version)-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="/landlord/css/styles.css" rel="stylesheet">
        <!-- Fonts CSS-->
        <link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Proza Libre' rel='stylesheet'>
        <link rel="stylesheet" href="/landlord/css/body.css">
    </head>
    <body id="page-top">
        <nav class="navbar navbar-expand-lg bg-secondary fixed-top" id="mainNav">
            <div class="container"><a class="navbar-brand js-scroll-trigger" href="/#page-top">ANDACH ADMIN - DOCS</a>
                <button class="navbar-toggler navbar-toggler-right font-weight-bold bg-primary rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">Menu <i class="fas fa-bars"></i></button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="{{ route('landlord.docs.initial-setup') }}">Initial Setup</a></li>
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="{{ route('landlord.docs.workflow') }}">Workflow</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <header class="masthead bg-primary">
            <div class="container d-flex flex-column">
                <!-- Masthead Heading-->
                <h1 class="masthead-heading mb-0 text-center">ANDACH ADMIN</h1>
                <p>&nbsp;</p>
                @yield('content')
            </div>
        </header>


@include('landlord.footer')