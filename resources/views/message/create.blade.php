@extends('adminlte::page')

@section('plugins.Select2', true)

@section('title', 'Create Message')

@section('content_header')
    <h1>Create Message</h1>
@stop

@section('content')
    {{ Form::open(['route' => 'message.create-post', 'method' => 'post']) }}
        <div class="row">
            <div class="col-12 col-md-3">
                @include('component.message.folders', $folders)
            </div>
            <div class="col-12 col-md-9">
                <div class="card card-dark">
                    <div class="card-header">Create Message</div>
                    <div class="card-body">
                        {{ Form::bsSelect('To', 'user_ids[]', $users, null, ['multiple' => 'multiple']) }}
                        {{ Form::bsText('Subject', 'name') }}
                        {{ Form::bsTextarea('Body of Message', 'description') }}
                        {{ Form::bsSubmit('Send Message') }}
                    </div>
                </div>
            </div>
        </div>
    {{ Form::close() }}
@endsection

@section('javascript')
    <script>
        $(document).ready(function() {
            $('select[name="user_ids[]"]').select2();
        });
    </script>
@endsection
