@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1>{{ $title }}</h1>
@stop

@section('content')
  {{ Form::open(['route' => 'message.delete', 'method' => 'post']) }}
    <div class="row">
        <div class="col-12 col-md-3">
            @include('component.message.folders', $folders)
        </div>

        <div class="col-12 col-md-9">
          <div class="card card-primary card-outline">
            <div class="card-header">
              <h3 class="card-title">{{ $title }}</h3>

              <div class="card-tools">
                <div class="input-group input-group-sm">
                  <input type="text" class="form-control" placeholder="Search Mail">
                  <div class="input-group-append">
                    <div class="btn btn-primary">
                      <i class="fas fa-search"></i>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->

            <div class="card-body p-0">
              <div class="mailbox-controls">
                <!-- Check all button -->
                <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="far fa-square"></i>
                </button>
                <div class="btn-group">
                  <button type="button" class="btn btn-default btn-sm"><i class="far fa-trash-alt"></i></button>
                  <button type="button" class="btn btn-default btn-sm"><i class="fas fa-reply"></i></button>
                  <button type="button" class="btn btn-default btn-sm"><i class="fas fa-share"></i></button>
                </div>
                <!-- /.btn-group -->
                <button type="button" class="btn btn-default btn-sm"><i class="fas fa-sync-alt"></i></button>
                <div class="float-right">
                  1-50/200
                  <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm"><i class="fas fa-chevron-left"></i></button>
                    <button type="button" class="btn btn-default btn-sm"><i class="fas fa-chevron-right"></i></button>
                  </div>
                  <!-- /.btn-group -->
                </div>
                <!-- /.float-right -->
              </div>
              <div class="table-responsive mailbox-messages">
                @if (count($messages))
                  <table class="table table-hover table-striped">
                    <tbody>
                      @foreach ($messages as $message)
                        @include('component.message.line', $message)
                      @endforeach
                    </tbody>
                  </table>
                  <!-- /.table -->
                @else
                  <p>There are no messages.</p>
                @endif
              </div>
              <!-- /.mail-box-messages -->
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
              {{ Form::submit('Delete Messages', ['class' => 'form-control col-9 col-md-3 btn btn-danger']) }}
            </div>
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
  {{ Form::close() }}
@endsection
