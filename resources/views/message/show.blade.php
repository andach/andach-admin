@extends('adminlte::page')

@section('title', 'View Message')

@section('content_header')
    <h1>View Message</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12 col-md-3">
            @include('component.message.folders', $folders)
        </div>
        <div class="col-12 col-md-9">
            <div class="card card-dark">
                <div class="card-header">View Message</div>
                <div class="card-body">
                    <div class="mailbox-read-info">
                        <h5>{{ $message->name }}</h5>
                        <h6>From: {{ $message->sender_name }}
                        <span class="mailbox-read-time float-right">{{ $message->created_at }}</span></h6>
                    </div>

                    
                    <div class="mailbox-controls with-border text-center">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Delete">
                                <i class="far fa-trash-alt"></i></button>
                            <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Reply">
                                <i class="fas fa-reply"></i></button>
                            <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Forward">
                                <i class="fas fa-share"></i></button>
                        </div>

                        <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" title="Print">
                            <i class="fas fa-print"></i></button>
                    </div>

                    <div class="mailbox-read-message">
                        {!! nl2br(e($message->description)) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
