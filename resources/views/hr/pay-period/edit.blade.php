@extends('adminlte::page')

@section('title', 'Edit Pay Period')

@section('content_header')
    <h1>Edit Pay Period</h1>
@stop

@section('content')
    {{ Form::model($payPeriod, ['route' => ['hr.pay-period-post', $payPeriod->id], 'method' => 'post']) }}
        <div class="card card-dark">
            <div class="card-header">Edit Pay Period</div>
            <div class="card-body">
                {{ Form::bsSelect('Pay Frequency', 'pay_frequency_id', $payFrequencies) }}
                {{ Form::bsSelect('Pay Year', 'pay_year_id', $payYears) }}
                {{ Form::bsText('Name', 'name') }}
                {{ Form::bsDate('From Date', 'from_date') }}
                {{ Form::bsDate('To Date', 'to_date') }}
            </div>
        </div>

        {{ Form::bsSubmit('Update Pay Period') }}
    {{ Form::close() }}
@endsection
