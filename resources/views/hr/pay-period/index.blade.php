@extends('adminlte::page')

@section('title', 'Setup Pay Periods')

@section('content_header')
    <h1>Setup Pay Periods</h1>
@stop

@section('content')
    {{ Form::open(['route' => 'hr.pay-periods-post', 'method' => 'post']) }}
        <div class="card card-dark">
            <div class="card-header">All Pay Periods</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-1">Delete?</div>
                    <div class="col-2">Frequency</div>
                    <div class="col-2">Year</div>
                    <div class="col-3">Name</div>
                    <div class="col-2">From Date</div>
                    <div class="col-2">To Date</div>

                    @foreach ($payPeriods as $payPeriod)
                        <div class="col-1">{{ Form::checkbox('delete_ids[]', $payPeriod->id, null, ['class' => 'form-control']) }}</div>
                        <div class="col-2">{{ $payPeriod->payFrequency->name }}</div>
                        <div class="col-2">{{ $payPeriod->payYear->name }}</div>
                        <div class="col-3">
                            <a href="{{ route('hr.pay-period', $payPeriod->id ) }}">{{ $payPeriod->name }}</a>
                        </div>
                        <div class="col-2">{{ $payPeriod->from_date }}</div>
                        <div class="col-2">{{ $payPeriod->to_date }}</div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="card card-dark">
            <div class="card-header">Add New Pay Period</div>
            <div class="card-body">
                {{ Form::bsSelect('Pay Frequency', 'pay_frequency_id', $payFrequencies) }}
                {{ Form::bsSelect('Pay Year', 'pay_year_id', $payYears) }}
                {{ Form::bsText('Name', 'name') }}
                {{ Form::bsDate('From Date', 'from_date') }}
                {{ Form::bsDate('To Date', 'to_date') }}
            </div>
        </div>

        {{ Form::bsSubmit('Update Jobs') }}
    {{ Form::close() }}
@endsection
