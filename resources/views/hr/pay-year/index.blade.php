@extends('adminlte::page')

@section('title', 'Setup Pay Years')

@section('content_header')
    <h1>Setup Pay Years</h1>
@stop

@section('content')
    {{ Form::open(['route' => 'hr.pay-years-post', 'method' => 'post']) }}
        <div class="card card-dark">
            <div class="card-header">All Pay Years</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-1">Delete?</div>
                    <div class="col-6">Name</div>
                    <div class="col-5">Created At</div>

                    @foreach ($payYears as $payYear)
                        <div class="col-1">{{ Form::checkbox('delete_ids[]', $payYear->id, null, ['class' => 'form-control']) }}</div>
                        <div class="col-6">{{ $payYear->name }}</div>
                        <div class="col-5">{{ $payYear->created_at }}</div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="card card-dark">
            <div class="card-header">Add New Pay Year</div>
            <div class="card-body">
                {{ Form::bsText('Name', 'name') }}
            </div>
        </div>

        {{ Form::bsSubmit('Update Jobs') }}
    {{ Form::close() }}
@endsection
