@extends('adminlte::page')

@section('title', 'Setup Jobs')

@section('content_header')
    <h1>Setup Jobs</h1>
@stop

@section('info')
    A <b>job</b> is a job title, and a set of responsibilities. The responsibilities should be the same for everyone with the same job title. A job might be a "Receptionist", "Accounts Manager", "Health and Safety Director" or similar. <br /></br >
    Once you have created a job, you will likely need to create a position, which is an authorisation for a certain number of hours to be worked at a particular job in a week - or to hire a zero hours employee.
@endsection

@section('content')
    {{ Form::open(['route' => 'hr.jobs-post', 'method' => 'post']) }}
        <div class="card card-dark">
            <div class="card-header">All Jobs</div>
            <div class="card-body">
                @if ($jobs->count())
                    <div class="row">
                        <div class="col-1">Delete?</div>
                        <div class="col-1">ID</div>
                        <div class="col-5">Job Title</div>
                        <div class="col-2">Is Director?</div>
                        <div class="col-3">FTE Hours</div>

                        @foreach ($jobs as $job)
                            <div class="col-1">{{ Form::checkbox('delete_ids[]', $job->id, null, ['class' => 'form-control']) }}</div>
                            <div class="col-1">{{ $job->id }}</div>
                            <div class="col-5"><a href="{{ route('hr.job', $job->id) }}">{{ $job->name }}</a></div>
                            <div class="col-2">{{ $job->is_director }}</div>
                            <div class="col-3">{{ $job->fte_hours }}</div>
                        @endforeach
                    </div>
                @else 
                    <div class="card-empty-body">There are no jobs set up yet.</div>
                @endif
            </div>
        </div>

        <div class="card card-dark">
            <div class="card-header">Add New Job</div>
            <div class="card-body">
                {{ Form::bsText('Job Title', 'name') }}
                {{ Form::hidden('is_director', 0) }}
                {{ Form::bsCheckbox('Is Director?', 'is_director', 1, null, ['helpBlock' => 'Tick this box if this job is a legal "company director" role.']) }}
                {{ Form::bsText('FTE Hours', 'fte_hours', null, ['helpBlock' => 'Enter the number of hours for someone to work at this job to be considered "full time".']) }}
                {{ Form::bsTextarea('Description of Work', 'description_of_work') }}
            </div>
        </div>

        {{ Form::bsSubmit('Update Jobs') }}
    {{ Form::close() }}
@endsection
