@extends('adminlte::page')

@section('title', 'Edit Job')

@section('content_header')
    <h1>Edit Job</h1>
@stop

@section('content')
    {{ Form::model($job, ['route' => ['hr.job-post', $job->id], 'method' => 'post']) }}
        <div class="card card-dark">
            <div class="card-header">Edit Job</div>
            <div class="card-body">
                {{ Form::bsText('Name', 'name') }}
                {{ Form::bsCheckbox('Is Director', 'is_director') }}
                {{ Form::bsText('FTE Hours', 'fte_hours') }}
                {{ Form::bsTextarea('Description of Work', 'description_of_work') }}
                {{ Form::bsSelect('Roles', 'roles', $roles, null, ['multiple' => 'multiple']) }}
            </div>
        </div>

        {{ Form::bsSubmit('Update Job') }}
    {{ Form::close() }}
@endsection
