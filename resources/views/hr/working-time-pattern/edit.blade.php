@extends('adminlte::page')

@section('title', 'Edit Working Time Pattern')

@section('content_header')
    <h1>Edit Working Time Pattern</h1>
@stop

@section('content')
    {{ Form::model($workingTimePattern, ['route' => ['hr.working-time-pattern-post', $workingTimePattern->id], 'method' => 'post']) }}
        <div class="card card-dark">
            <div class="card-header">Edit Working Time Pattern</div>
            <div class="card-body">
                <div class="row">
                    {{ Form::bsText('Name', 'name') }}
                    {{ Form::bsText('Mon Mins', 'mon_mins') }}
                    {{ Form::bsText('Tue Mins', 'tue_mins') }}
                    {{ Form::bsText('Wed Mins', 'wed_mins') }}
                    {{ Form::bsText('Thu Mins', 'thu_mins') }}
                    {{ Form::bsText('Fri Mins', 'fri_mins') }}
                    {{ Form::bsText('Sat Mins', 'sat_mins') }}
                    {{ Form::bsText('Sun Mins', 'sun_mins') }}
                </div>
            </div>
        </div>

        {{ Form::bsSubmit('Update Working Time Pattern') }}
    {{ Form::close() }}
@endsection
