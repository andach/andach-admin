@extends('adminlte::page')

@section('title', 'Setup Working Time Patterns')

@section('content_header')
    <h1>Setup Working Time Patterns</h1>
@stop

@section('content')
    {{ Form::open(['route' => 'hr.working-time-pattern-post', 'method' => 'post']) }}
        <div class="card card-dark">
            <div class="card-header">All Working Time Patterns</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-1">Delete?</div>
                    <div class="col-4">Name</div>
                    <div class="col-1">Mon</div>
                    <div class="col-1">Tue</div>
                    <div class="col-1">Wed</div>
                    <div class="col-1">Thu</div>
                    <div class="col-1">Fri</div>
                    <div class="col-1">Sat</div>
                    <div class="col-1">Sun</div>

                    @foreach ($workingTimePatterns as $workingTimePattern)
                        <div class="col-1">{{ Form::checkbox('delete_ids[]', $workingTimePattern->id, null, ['class' => 'form-control']) }}</div>
                        <div class="col-4">{{ $workingTimePattern->name }}</div>
                        <div class="col-1">{{ $workingTimePattern->mon_mins }}</div>
                        <div class="col-1">{{ $workingTimePattern->tue_mins }}</div>
                        <div class="col-1">{{ $workingTimePattern->wed_mins }}</div>
                        <div class="col-1">{{ $workingTimePattern->thu_mins }}</div>
                        <div class="col-1">{{ $workingTimePattern->fri_mins }}</div>
                        <div class="col-1">{{ $workingTimePattern->sat_mins }}</div>
                        <div class="col-1">{{ $workingTimePattern->sun_mins }}</div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="card card-dark">
            <div class="card-header">Add New Working Time Pattern</div>
            <div class="card-body">
                {{ Form::bsText('Name', 'name') }}
                {{ Form::bsText('Mon Mins', 'mon_mins') }}
                {{ Form::bsText('Tue Mins', 'tue_mins') }}
                {{ Form::bsText('Wed Mins', 'wed_mins') }}
                {{ Form::bsText('Thu Mins', 'thu_mins') }}
                {{ Form::bsText('Fri Mins', 'fri_mins') }}
                {{ Form::bsText('Sat Mins', 'sat_mins') }}
                {{ Form::bsText('Sun Mins', 'sun_mins') }}
            </div>
        </div>

        {{ Form::bsSubmit('Update Working Time Patterns') }}
    {{ Form::close() }}
@endsection
