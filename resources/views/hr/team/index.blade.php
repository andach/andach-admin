@extends('adminlte::page')

@section('title', 'Setup Teams')

@section('content_header')
    <h1>Setup Teams</h1>
@stop

@section('content')
    {{ Form::open(['route' => 'hr.teams-post', 'method' => 'post']) }}
        <div class="card card-dark">
            <div class="card-header">All Teams</div>
            <div class="card-body">
                @if ($teams->count())
                    <div class="row">
                        <div class="col-1">Delete?</div>
                        <div class="col-2">Name</div>
                        <div class="col-3">Parent</div>
                        <div class="col-3">Manager (Position)</div>
                        <div class="col-3">Manager (Individual)</div>

                        @foreach ($teams as $team)
                            <div class="col-1">{{ Form::checkbox('delete_ids[]', $team->id, null, ['class' => 'form-control']) }}</div>
                            <div class="col-2"><a href="{{ route('hr.team', $team->id) }}">{{ $team->name }}</a></div>
                            <div class="col-3">{{ $team->parent_name }}</div>
                            <div class="col-3">{{ $team->manager_name }}</div>
                            <div class="col-3">TODO: This</div>
                        @endforeach
                    </div>
                @else
                    <div class="card-empty-body">There are no teams set up yet.</div>
                @endif
            </div>
        </div>

        <div class="card card-dark">
            <div class="card-header">Add New Team</div>
            <div class="card-body">
                {{ Form::bsText('Name', 'name') }}
                {{ Form::bsSelect('Parent', 'parent_id', $teams->pluck('name', 'id')) }}
                {{ Form::bsSelect('Manager', 'manager_id', $managers) }}
            </div>
        </div>

        {{ Form::bsSubmit('Update Teams') }}
    {{ Form::close() }}
@endsection
