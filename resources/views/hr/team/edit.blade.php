@extends('adminlte::page')

@section('title', 'Edit Team')

@section('content_header')
    <h1>Edit Team</h1>
@stop

@section('content')
    {{ Form::model($team, ['route' => ['hr.team-post', $team->id], 'method' => 'post']) }}
        <div class="card card-dark">
            <div class="card-header">Edit Team</div>
            <div class="card-body">
                {{ Form::bsText('Name', 'name') }}
                {{ Form::bsSelect('Parent', 'parent_id', $teams) }}
                {{ Form::bsSelect('Manager', 'manager_id', $managers) }}
            </div>
        </div>

        {{ Form::bsSubmit('Update Team') }}
    {{ Form::close() }}
@endsection
