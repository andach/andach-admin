@extends('adminlte::page')

@section('title', 'Setup Companies')

@section('content_header')
    <h1>Setup Companies</h1>
@stop

@section('content')
    {{ Form::open(['route' => 'company.index-post', 'method' => 'post']) }}
        <div class="card card-dark">
            <div class="card-header">All Companies</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-1">Delete?</div>
                    <div class="col-5">Name</div>
                    <div class="col-6">Parent</div>

                    @foreach ($companies as $company)
                        <div class="col-1">{{ Form::checkbox('delete_ids[]', $company->id, null, ['class' => 'form-control']) }}</div>
                        <div class="col-5">{{ Form::text('name['.$company->id.']', $company->name, ['class' => 'form-control']) }}</div>
                        <div class="col-6">{{ Form::select('parent_id['.$company->id.']', $companies->pluck('name', 'id'), $company->parent_id, ['class' => 'form-control']) }}</div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="card card-dark">
            <div class="card-header">Add New Company</div>
            <div class="card-body">
                {{ Form::bsText('Name', 'new_name') }}
                {{ Form::bsSelect('Parent', 'new_parent_id', $companies->pluck('name', 'id'), null, ['placeholder' => '']) }}
            </div>
        </div>

        {{ Form::bsSubmit('Update Companies') }}
    {{ Form::close() }}
@endsection
