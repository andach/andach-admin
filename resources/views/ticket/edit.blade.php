@extends('adminlte::page')

@section('plugins.Select2', true)

@section('title', 'Edit Ticket')

@section('content_header')
    <h1>Edit Ticket</h1>
@stop

@section('content')
    {{ Form::model($ticket, ['route' => ['ticket.edit-post', $ticket->id], 'method' => 'post', 'files' => true]) }}
        <div class="row">
            <div class="col-3">
                @include('component.customer.card', ['customer' => $ticket->customer])
            </div>
            <div class="col-9">
                @include('component.workflight.editcomments', ['workflight' => $ticket->workflight, 'workflowable' => $ticket])
            </div>
        </div>

        <!-- accordion card -->

        <p>
            <button class="form-control btn" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                Edit Main Ticket Details
            </button>
        </p>

        <div class="collapse" id="collapseExample">
            <div class="row">
                <div class="col-12">
                    <div class="card card-dark">
                        <div class="card-header">Main Ticket Details</div>
                        <div class="card-body">
                            {{ Form::bsText('Title', 'name') }}
                            {{ Form::bsSelect('Customer', 'customer_id', $customers, null, ['placeholder' => '']) }}
                            {{ Form::bsSelect('Company', 'company_id', $companies, null, ['disabled' => 'disabled']) }}
                            {{ Form::bsSelect('Location', 'location_id', $locations) }}

                            @if (count($udfs))
                                @foreach ($udfs as $udf)
                                    {!! $udf !!}
                                @endforeach
                            @endif

                            {{ Form::bsSelect('Assign to User', 'user_id', $users, $ticket->workflight->assigned_to_user_id, ['placeholder' => '&nbsp;', 'helpBlock' => 'Pick the user you want to assign this ticket to (optional). Only users who can see the tickets section will be available. Assigning a ticket to a user will not stop others being able to see it.']) }}
                            {{ Form::bsTextarea('Description', 'description') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{ Form::bsSubmit('Update Ticket') }}
    {{ Form::close() }}
@endsection

@section('javascript')
    <script>
        $(document).ready(function() {
            $('select[name="customer_id"]').select2();
            $('select[name="user_id"]').select2();
            $('.collapse').collapse();
        });
    </script>
@endsection