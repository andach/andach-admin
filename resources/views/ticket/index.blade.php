@extends('adminlte::page')

@section('plugins.Datatables', true)
@section('plugins.Chartjs', true)

@section('title', 'All Tickets')

@section('content_header')
    <h1>All Tickets</h1>
@stop

@section('content')
    <div class="card card-dark">
        <div class="card-header">Search</div>
        <div class="card-body">
            {{ Form::open(['route' => 'ticket.index', 'method' => 'get']) }}
                {{ Form::bsText('ID', 'id', $defaults['id']) }}
                {{ Form::bsText('Title', 'name', $defaults['name']) }}
                {{ Form::bsText('Description', 'description', $defaults['description']) }}
                <!-- {{ Form::bsSelect('Open/Closed', 'open_closed', ['open' => 'Open', 'closed' => 'Closed'], $defaults['open_closed'], ['placeholder' => '']) }} -->
                {{ Form::bsSelect('Specific State(s)', 'states_ids[]', $states, $defaults['states_ids'], ['placeholder' => '', 'multiple' => 'multiple']) }}
                {{ Form::bsSelect('Assigned To User(s)', 'users[]', $users, $defaults['users'], ['placeholder' => '', 'multiple' => 'multiple']) }}
                {{ Form::bsCheckbox('View Comment Summary?', 'comments', 1, $defaults['comments']) }}
                {{ Form::submit('Search Tickets') }}
            {{ Form::close() }}
        </div>
    </div>

    <div class="card card-dark">
        <div class="card-header">Results</div>
        <div class="card-body">
            @if (count($tickets))
                <table id="myTable" style="width: 100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Status</th>
                            <th>Open?</th>
                            <th>No. Comments</th>
                            <th>Assigned To</th>
                            <th>Created At</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($tickets as $ticket)
                        <tr>
                            <td>{{ $ticket->id }}</td>
                            <td>
                                <a href="{{ route('ticket.edit', $ticket->id) }}">
                                    {{ $ticket->name }}
                                </a>
                            </td>
                            <td>{{ $ticket->current_status }}</td>
                            <td>{!! $ticket->openTickOrCross !!}</td>
                            <td>{{ $ticket->number_of_comments }}</td>
                            <td>{{ $ticket->assigned_to_name }}</td>
                            <td>{{ $ticket->created_at }}</td>
                        </tr>

                        @if ($defaults['comments'])
                            @foreach ($ticket->workflight->comments as $comment)
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="4">{{ $comment->description }}</td>
                                    <td>{{ $comment->user->name }}</td>
                                    <td>{{ $comment->created_at }}</td>
                                </tr>
                            @endforeach
                        @endif
                    @endforeach
                    </tbody>
                </table>
            @else
                <p>There are no results.</p>
            @endif
        </div>
        <div class="card-footer">There are {{ count($tickets) }} results.</div>
    </div>

    <!--
    <div class="row">
        <div class="col-4"><canvas id="open_tickets_by_age" width="400" height="400"></canvas></div>
        <div class="col-4"><canvas id="open_tickets_by_customer" width="400" height="400"></canvas></div>
        <div class="col-4"><canvas id="all_tickets_by_month" width="400" height="400"></canvas></div>
    </div>
    -->
@endsection

@section('javascript')
    {{-- <script>
        $(document).ready(function () {
            $('#myTable').DataTable();
        });

        @foreach ($charts as $chart)
            {!! $chart !!}
        @endforeach
    </script> --}}
@endsection
