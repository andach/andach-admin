@extends('adminlte::page')

@section('title', 'All My Tickets')

@section('content_header')
    <h1>My Tickets</h1>
@stop

@section('content')
    <div class="card card-dark">
        <div class="card-header">All My Tickets</div>
        <div class="card-body">
            @if (count($tickets))
                <table id="myTable" style="width: 100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Status</th>
                            <th>Open?</th>
                            <th>No. Comments</th>
                            <th>Assigned To</th>
                            <th>Created At</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($tickets as $ticket)
                        <tr>
                            <td>{{ $ticket->id }}</td>
                            <td>
                                <a href="{{ route('ticket.edit', $ticket->id) }}">
                                    {{ $ticket->name }}
                                </a>
                            </td>
                            <td>{{ $ticket->current_status }}</td>
                            <td>{!! $ticket->openTickOrCross !!}</td>
                            <td>{{ $ticket->number_of_comments }}</td>
                            <td>{{ $ticket->assigned_to_name }}</td>
                            <td>{{ $ticket->created_at }}</td>
                        </td>
                    @endforeach
                    </tbody>
                </table>
            @else
                <p>There are no results.</p>
            @endif
        </div>
    </div>
@endsection
