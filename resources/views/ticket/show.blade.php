@extends('adminlte::page')

@section('title', 'Show Ticket')

@section('content_header')
    <h1>Ticket #{{ $ticket->id }} - {{ $ticket->name }}</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-header">Main Ticket Details</div>
        <div class="card-body">
            <div class="row">
                <div class="col-3">State</div>
                <div class="col-9">{{ $ticket->workflight->state->name }}</div>
                <div class="col-3">Customer</div>
                <div class="col-9">{{ $ticket->customer_name }}</div>
                <div class="col-3">Company</div>
                <div class="col-9">{{ $ticket->company->name }}</div>
                <div class="col-3">Location</div>
                <div class="col-9">{{ $ticket->location_name }}</div>

                @foreach ($udfs as $displayName => $value)
                    <div class="col-3">{{ $displayName }}</div>
                    <div class="col-9">{!! nl2br(e($value)) !!}</div>
                @endforeach

                <div class="col-12">{!! nl2br(e($ticket->description)) !!}
            </div>
        </div>
    </div>

    @include('component.workflight.showcomments', ['workflight' => $ticket->workflight])
@endsection
