<?php

namespace App\Observers\HR;

use App\Models\HR\Position;

class PositionObserver
{
    public function saving(Position $position): void
    {
        $position->name = $position->hours_per_week.' h/w of '.$position->job->name.' for '.$position->team->name;
    }
}
