<?php

namespace App\Observers\HR;

use App\Models\HR\Appointment;

class AppointmentObserver
{
    public function saving(Appointment $appointment): void
    {
        $appointment->job_id  = $appointment->position->job_id;
        $appointment->team_id = $appointment->position->team_id;
    }
}
