<?php

namespace App\Observers\HR;

use App\Models\HR\Contract;

class ContractObserver
{
    public function creating(Contract $contract): void
    {
        $contract->full_text = '';
    }
}
