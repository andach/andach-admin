<?php

namespace App\Observers\Workflow;

use App\Mail\WorkflightComment;
use App\Models\Message\Message;
use App\Models\Workflow\Comment;
use App\Models\User;
use Auth;
use Mail;

class CommentObserver
{
    public function created(Comment $comment): void
    {
        // Check if the user making the comment is the same as the assigned to user
        if ($comment->workflight->assigned_to_user_id !== $comment->user_id)
        {
            $data = [];
            $data['user_id']     = 0;
            $data['name']        = 'A new comment has been made on "'.$comment->workflight->workflowable->name.'"';
            $data['description'] = 'A new comment has been made on something you have been assigned to.';
            $data['from_name']   = 'Messages System';

            $message = Auth::user()->messagesSent()->create($data);
            $message->toUsers()->sync([$comment->workflight->assigned_to_user_id]);

            $user = User::find($comment->workflight->assigned_to_user_id);

            if ($user)
            {
                if (filter_var($user->email, FILTER_VALIDATE_EMAIL))
                {
                    Mail::to($user)->send(new WorkflightComment($comment->workflight->workflowable));
                }
            }
        }
    }
}
