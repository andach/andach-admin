<?php

namespace App\Observers\Time;

use App\Models\Time\Timesheet;

class TimesheetObserver
{
    public function saving(Timesheet $timesheet): void
    {
        $timesheet->position_id = $timesheet->appointment->position_id;
        $timesheet->job_id      = $timesheet->appointment->position->job_id;
        $timesheet->team_id     = $timesheet->appointment->position->team_id;

        $timesheet->week_of_timesheet = strtotime('Monday this week', $timesheet->date_of_timesheet);
    }
}
