<?php

namespace App\Providers;

use App\Models\Role;
use Auth;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        \App\Models\HR\Appointment::observe(\App\Observers\HR\AppointmentObserver::class);
        \App\Models\HR\Contract::observe(\App\Observers\HR\ContractObserver::class);
        \App\Models\HR\Position::observe(\App\Observers\HR\PositionObserver::class);
        \App\Models\Time\Timesheet::observe(\App\Observers\Time\TimesheetObserver::class);
        \App\Models\Workflow\Comment::observe(\App\Observers\Workflow\CommentObserver::class);

        Schema::defaultStringLength(191);

        $events->listen(BuildingMenu::class, function (BuildingMenu $event) 
        {
            $generatedMenu = [];
            
            if (Auth::id()) 
            {
                $roles = Auth::user()->allRoles();

                $configMenu = config('menu.menu');
                // dd($configMenu);

                foreach ($configMenu as $modelName => $array)
                {
                    foreach ($array as $permissionType => $menuLine)
                    {
                        if (Auth::user()->is_super_admin)
                        {
                            if (!array_key_exists('text', $menuLine))
                            {
                                foreach ($menuLine as $line)
                                {
                                    $generatedMenu[$modelName][] = $line;
                                }
                            } else {
                                $generatedMenu[$modelName][] = $menuLine;
                            }
                        } else {
                            if (Role::canAccess($roles, $modelName.'.'.$permissionType))
                            {
                                if (!array_key_exists('text', $menuLine))
                                {
                                    foreach ($menuLine as $line)
                                    {
                                        $generatedMenu[$modelName][] = $line;
                                    }
                                } else {
                                    $generatedMenu[$modelName][] = $menuLine;
                                }
                            }
                        }
                    }
                }
            }

            // dd($roles, $generatedMenu);

            // Menu has now been generated as $generatedMenu (which is an array of items). Add a home button first, then the rest of the menu. 
            $event->menu->add([
                'text'  => 'Home',
                'route' => 'home',
                'icon'  => 'nav-icon fas fa-home',
            ]);

            foreach ($generatedMenu as $header => $submenu)
            {
                $event->menu->add([
                    'text' => config('menu.menu_headers.'.$header.'.name'),
                    'submenu' => $submenu,
                    'icon' => config('menu.menu_headers.'.$header.'.icon'),
                ]);
            }

            if (!Auth::check())
            {
                $event->menu->add([
                    'text' => 'Login',
                    'route' => 'login',
                    'icon' => 'nav-icon fas fa-sign-in-alt',
                ]);
            }
        });
    }
}
