<?php

if (!function_exists('time_string_from_mins'))
{
    /**
     * Returns a human readable file size
     *
     * @param int $bytes
     * Bytes contains the size of the bytes to convert
     *
     * @param int $decimals
     * Number of decimal places to be returned
     *
     * @return string a string in human readable format
     *
     * */
    function time_string_from_mins(int $mins): string
    {
        $hours = floor($mins / 60);
        $mins = $mins - $hours * 60;

        if ($hours)
        {
            return $hours.' hrs, '.$mins.' mins';
        }

        return $mins.' mins';
    }
}
