<?php

namespace App\Listeners;

use App\Events\WorkflightCreated;
use App\Models\Workflow\Workflow;
use Auth;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CreateWorkflight
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     */
    public function handle(WorkflightCreated $event): void
    {
        // TODO: Proper Exception Handling
        // This will give us App\Models\Ticket\Ticket or similar.
        $fullClass = get_class($event->objectCreatingWorkflow);

        $workflow = Workflow::where('model_name', $fullClass)->first();

        $data = [];
        $data['workflow_id']         = $workflow->id;
        $data['assigned_to_user_id'] = Auth::id();
        $data['state_id']            = $workflow->states()->default()->first()->id;
        $data['name']                = $fullClass;

        $workflight = $event->objectCreatingWorkflow->workflight()->create($data);

        // Now create the first audit log for this flight.
        $dataAudit = [];
        $dataAudit['assigned_to_user_id'] = Auth::id();
        $dataAudit['instigating_user_id'] = Auth::id();
        $dataAudit['state_id']            = $data['state_id'];
        $dataAudit['from_datetime']       = date('Y-m-d h:i:s');

        $workflight->audits()->create($dataAudit);
    }
}
