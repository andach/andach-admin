<?php

namespace App\Exceptions;

use Exception;

class WorkflowNotFoundException extends Exception
{
    public function render($request)
    {
        return response()->view('errors.workflow-not-found', [], 500);
    }
}
