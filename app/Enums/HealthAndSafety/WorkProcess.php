<?php

namespace App\Enums\HealthAndSafety;

use App\Enums\Enum;

class WorkProcess extends Enum
{
    private const PROD   = 'Production, manufacturing or processing';
    private const STORE  = 'Storing / warehousing';
    private const CONNEW = 'Construction - new building';
    private const CONCIV = 'Construction - civil engineering, infrastructures, roads, bridges, ports';
    private const CONREP = 'Construction - remodelling, repairing, extenring, building maintenance';
    private const DEM    = 'Demolition';
    private const AGRIC  = 'Agricultural work, forestry, horticulture, fishing, work with animals';
    private const CLEAN  = 'Cleaning, industrial or manual';
    private const WASTE  = 'Waste management, disposal, treatment';
    private const MONINS = 'Monitoring / inspection';
    private const PUBL   = 'Service or assistance to the public';
    private const OFFICE = 'Teaching, training, office work';
    private const BUYSEL = 'Commercial activity - buying, selling and associated services';
    private const REPAIR = 'Maintenance, repair';
    private const MOVE   = 'Movement, including aboard transport';
    private const SPTART = 'Sport or artistic activity';
    private const OTHER  = 'Other process not listed above';
}