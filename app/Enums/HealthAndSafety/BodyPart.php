<?php

namespace App\Enums\HealthAndSafety;

use App\Enums\Enum;

class BodyPart extends Enum
{
    private const HEAD   = 'Head';
    private const EYES   = 'Eyes';
    private const ENT    = 'Ears / Nose / Throat';
    private const MOUTH  = 'Mouth';
    private const CHEST  = 'Chest';
    private const ARMS   = 'Arms';
    private const HANDS  = 'Hands';
    private const GEN    = 'Genitals';
    private const HIPS   = 'Hips';
    private const LEGS   = 'Legs';
    private const FEET   = 'Feet';
}