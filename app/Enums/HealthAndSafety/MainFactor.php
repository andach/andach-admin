<?php

namespace App\Enums\HealthAndSafety;

use App\Enums\Enum;

class MainFactor extends Enum
{
    private const BOOM   = 'Electrical problem, explosion or fire';
    private const LEAK   = 'Overflow, leak, vaporisation or emission of liquid, solid or gaseous product';
    private const BREAK  = 'Breakage, bursting or collapse of material';
    private const CONTRL = 'Loss of control of machinery, transport or equipment';
    private const FALL   = 'Slip, stumble or fall';
    private const WLKSRP = 'Walking on a sharp object';
    private const SIT    = 'Kneeling, sitting or leaning on an object';
    private const SWEPT  = 'Being caught or carried away by something (or by momentum)';
    private const LIFT   = 'Lifting, carrying, standing up';
    private const PUSHPL = 'Pushing, pull';
    private const BEND   = 'Putting down, bending down';
    private const TWIST  = 'Twisting, turning';
    private const AGGR   = 'Shock, fright, violence, aggression';
    private const OTHER  = 'Other cause not listed above';
}