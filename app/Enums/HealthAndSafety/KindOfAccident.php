<?php

namespace App\Enums\HealthAndSafety;

use App\Enums\Enum;

class KindOfAccident extends Enum
{
    private const MACH   = 'Contact with machinery';
    private const STROBJ = 'Struck by object';
    private const STRMV  = 'Struck by moving vehicle';
    private const STRAG  = 'Struck against';
    private const LIFT   = 'Lifting and handling injuries';
    private const TRIP   = 'Slip, trip, fall same level';
    private const HEIGHT = 'Fall from height';
    private const COLLAP = 'Trapped by something collapsing';
    private const ASPHYX = 'Drowned or asphyxiated';
    private const HRMSUB = 'Exposure to harmful substance';
    private const FIRE   = 'Exposed to fire';
    private const EXPL   = 'Exposed to explosion';
    private const ELECT  = 'Contact with electricity';
    private const ANIMAL = 'Injured by an animal';
    private const ASLT   = 'Physical assault';
    private const OTHER  = 'Another kind of accident';
}