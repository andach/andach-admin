<?php

namespace App\Enums;

use App\Enums\Enum;

class Gender extends Enum
{
    private const M = 'Male';
    private const F = 'Female';
    private const O = 'Other / Non-Binary';
}