<?php

namespace App\Enums\CareHome;

use App\Enums\Enum;

class Meal extends Enum
{
    private const BREAKF = 'Breakfast';
    private const MORSNA = 'Morning Snack';
    private const LUNCH  = 'Lunch';
    private const AFTSNA = 'Afternoon Snack';
    private const DINNER = 'Dinner';
    private const EVESNA = 'Evening Snack';
}