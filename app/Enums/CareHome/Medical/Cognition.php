<?php

namespace App\Enums\CareHome\Medical;

use App\Enums\Enum;

class Cognition extends Enum
{
    private const OK     = 'No Impairment';
    private const CONFUS = 'Some Confusion';
    private const LIMIT  = 'Severely Limited Communication';
    private const NONE   = 'No Meaningful Interaction';
}