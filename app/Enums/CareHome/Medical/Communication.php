<?php

namespace App\Enums\CareHome\Medical;

use App\Enums\Enum;

class Communication extends Enum
{
    private const CLEAR  = 'Speaks and Communicates Clearly';
    private const HARD   = 'Difficult to Understand';
    private const UNABLE = 'Unable to Communicate Verbally';
}