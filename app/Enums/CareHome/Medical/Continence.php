<?php

namespace App\Enums\CareHome\Medical;

use App\Enums\Enum;

class Continence extends Enum
{
    private const CONT   = 'Continent';
    private const URINE  = 'Urinary Incontinent';
    private const FAECES = 'Faecally Incontinent';
    private const BOTH   = 'Doubly Incontinent';
}