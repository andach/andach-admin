<?php

namespace App\Enums\CareHome\Medical;

use App\Enums\Enum;

class Sight extends Enum
{
    private const OK     = 'No Impairment';
    private const MILD   = 'Mild Sight Impairment';
    private const SIGNIF = 'Significant Sight Impairment';
    private const BLIND  = 'Blind';
}