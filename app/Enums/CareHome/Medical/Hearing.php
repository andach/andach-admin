<?php

namespace App\Enums\CareHome\Medical;

use App\Enums\Enum;

class Hearing extends Enum
{
    private const OK     = 'No Impairment';
    private const MILD   = 'Mild Hearing Difficulties';
    private const SIGNIF = 'Significant Hearing Difficulties';
    private const DEAF   = 'Deaf';
}