<?php

namespace App\Enums\CareHome\Medical;

use App\Enums\Enum;

class Mobility extends Enum
{
    private const INDEP  = 'Independent';
    private const AIDS   = 'Walking Aids (Stick/Frame)';
    private const ASSIST = 'Needs Significant Assistance';
    private const CHAIR  = 'Chair and Bed Bound';
    private const BED    = 'Bedbound';
}