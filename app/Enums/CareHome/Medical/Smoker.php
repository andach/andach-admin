<?php

namespace App\Enums\CareHome\Medical;

use App\Enums\Enum;

class Smoker extends Enum
{
    private const NONSM  = 'Non-Smoker';
    private const EXSM   = 'Ex-Smoker';
    private const LIGHT  = 'Light Smoker (1-5 per day)';
    private const MEDIUM = 'Medium Smoker (6-15 per day)';
    private const HEAVY  = 'Heavy Smoker (15+ per day)';
}