<?php

namespace App\Enums\CareHome\Medical;

use App\Enums\Enum;

class Alcohol extends Enum
{
    private const NOALC  = 'Non-Drinker';
    private const LIGHT  = 'Light Drinker';
    private const MEDIUM = 'Medium Drinker';
    private const HEAVY  = 'Heavy Drinker';
}