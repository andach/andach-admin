<?php

namespace App\Enums;

use App\Enums\Enum;

class TimeUnit extends Enum
{
    private const WEEKLY  = 'Weekly';
    private const MONTHLY = 'Monthly';
}