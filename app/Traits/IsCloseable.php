<?php

namespace App\Traits;

use App\Models\Company;
use Auth;

trait IsCloseable
{
    public function getOpenTickOrCrossAttribute(): string
    {
        if ($this->is_closed)
        {
            return '<i class="fas fa-times"></i>';
        } else {
            return '<i class="fas fa-check"></i>';
        }
    }

    public static function scopeIsClosed(\Illuminate\Database\Eloquent\Builder $query): \Illuminate\Database\Eloquent\Builder
    {
        return $query->where('is_closed', 1);
    }

    public static function scopeIsOpen(\Illuminate\Database\Eloquent\Builder $query): \Illuminate\Database\Eloquent\Builder
    {
        return $query->where('is_closed', 0);
    }
}
