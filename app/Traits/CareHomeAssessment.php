<?php

namespace App\Traits;

trait CareHomeAssessment
{
    public function resident(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\CareHome\Resident');
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'completed_by_user_id');
    }
}
