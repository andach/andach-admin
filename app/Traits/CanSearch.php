<?php

namespace App\Traits;

trait CanSearch
{
    /**
     * Here, we expect $searchParams to be an array of type
     * $searchParams = [
     *     'id' => ['=', $request->id],
     *     'name' => ['LIKE', '%'.$request->name.'%'],
     *     'description' => ['LIKE', '%'.$request->description.'%'],
     * ];
     */
    public static function search(\Illuminate\Database\Eloquent\Builder $query, array $searchParams): \Illuminate\Database\Eloquent\Builder
    {
        foreach ($searchParams as $name => $searchFor)
        {
            if ($searchFor[1] !== null and $searchFor[1] !== '%%')
            {
                $query = $query->where($name, $searchFor[0], $searchFor[1]);
            }
        }

        return $query;
    }
}
