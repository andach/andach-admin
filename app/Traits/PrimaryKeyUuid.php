<?php

namespace App\Traits;

use App\Models\BaseModel;

trait PrimaryKeyUuid
{
    protected static function boot(): void
    {
        parent::boot();

        static::creating(function (BaseModel $model): void {
            $uuid = sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
            $model->setAttribute($model->getKeyName(), $uuid);
        });
    }

    public function initializePrimaryKeyUuidTrait(): void
    {
        $this->keyType      = 'string';
        $this->incrementing = false;
    }

    public function getIncrementing(): bool
    {
        return false;
    }

    public function getKeyType(): string
    {
        return 'string';
    }
}
