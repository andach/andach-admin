<?php

namespace App\Traits;

use App\Models\Company;
use Auth;

trait AccessLimitedByCompany
{
    /**
     * Method to be used when a model (such as tickets, incidents, complaints) is restricted by the link_companies_users table so that
     * certain users can only access models for certain companies. Returns a collection of companies.
     */
    public static function canAccessCompanies(string $permission): \Illuminate\Database\Eloquent\Collection
    {
        // Don't need this functionality at the moment.
        return Company::all();

        if (Auth::user()->is_super_admin)
        {
            return Company::all();
        }

        return Auth::user()->companies()
            ->wherePivot('model_name', get_class())
            ->wherePivot('permission', $permission)
            ->get();
    }

    /**
     * Returns true or false based on whether the user can access this model for this particular company.
     */
    public function canAccessCompany(string $permission): bool
    {
        return in_array($this->company_id, self::canAccessCompanies($permission)->pluck('id')->toArray());
    }

    /**
     * Basically the same as the above but gives an instant 403 if they can't access it.
     */
    public function canAccessCompanyOrDie(string $permission): void
    {
        if (!$this->canAccessCompany($permission))
        {
            abort(403, 'You do not have access to company ID '.$this->company_id.' for the resource '.get_class());
        }
    }
}
