<?php

namespace App\Traits;

use App\Exceptions\WorkflowNotFoundException;
use App\Models\Location\Location;
use App\Models\Workflow\Transition;
use App\Models\Workflow\Workflow;
use DateTime;

trait HasWorkflight
{
    public function workflight(): \Illuminate\Database\Eloquent\Relations\MorphOne
    {
        return $this->morphOne('App\Models\Workflow\Workflight', 'workflowable');
    }

    public function addComment(?string $description = '', ?\Illuminate\Http\UploadedFile $attachment = null): void
    {
        $this->workflight->addComment($description, $attachment);
    }

    public function assignWorkflightTo(?int $userID = null): bool
    {
        return $this->workflight->assignTo($userID);
    }

    /**
     * Returns whether this workflowable object can be instantly closed, if there is a single route to the status with
     * default_instant_close_status = 1 in the database.
     *
     * Note that it is best to set this up with a single transition straight to closed status.
     */
    public static function canBeInstantlyClosed(): bool
    {
        $transition = self::transitionToInstantlyClose();

        if (!$transition)
        {
            return false;
        }

        // Now all we need to do is check whether this transition is generally available. Because this is a static function,
        // the UDFs won't be set yet, so we just check based on roles.
        return $transition->isAvailableBasedOnRoles();
    }

    public function getAssignedToNameAttribute(): string
    {
        return $this->workflight->assigned_to_name;
    }

    public function getCurrentStatusAttribute(): string
    {
        return $this->workflight->current_status;
    }

    public function getDaysOpenAttribute(): int
    {
        if ($this->isClosed())
        {
            return 0;
        }

        $start = new Datetime($this->created_at);
        $end   = new DateTime();

        return $end->diff($start)->format("%a") + 1;
    }

    public function getLastCommentByNameAttribute(): name
    {
        return $this->workflight->last_comment_by_name;
    }

    public function getNumberOfCommentsAttribute(): int
    {
        return $this->workflight->number_of_comments;
    }

    // Attempts to instantly close the workflowable. Returns true on success, else false.
    public function instantlyClose(): bool
    {
        if (!self::canBeInstantlyClosed())
        {
            return false;
        }

        $transition = self::transitionToInstantlyClose();

        // TODO: The "transitionToState" function needs checking between HasWorkflight.php and Workflight.php.
        return $this->workflight->transitionToState($transition->id);
    }

    public function isClosed(): bool
    {
        return $this->workflight->isClosed();
    }

    public static function scopeClosed(\Illuminate\Database\Eloquent\Builder $query): \Illuminate\Database\Eloquent\Builder
    {
        return $query->where('is_closed', 1);
    }

    public static function scopeOpen(\Illuminate\Database\Eloquent\Builder $query): \Illuminate\Database\Eloquent\Builder
    {
        return $query->where('is_closed', 0);
    }

    public static function scopeOpenMoreThanDays(\Illuminate\Database\Eloquent\Builder $query, int $days): \Illuminate\Database\Eloquent\Builder
    {
        return $query->where('created_at', '>', date('Y-m-d h:i:s', strtotime('-'.$days.' days')));
    }

    // Returns the transition that we need to instantly close this object, or null.
    public static function transitionToInstantlyClose(): ?Transition
    {
        $workflow = self::workflow();

        $startState = $workflow->states()->where('is_default', 1)->first();
        $endState   = $workflow->states()->where('default_instant_close_status', 1)->first();

        // $startStateID should never be false. Regardless, just in case. If there is no available end state like this
        // then we can't instantly close. Return false.
        if (!$startState or !$endState)
        {
            return null;
        }

        $transition = $workflow->transitions()
            ->where('workflow_state_from_id', $startState->id)
            ->where('workflow_state_to_id', $endState->id)
            ->first();

        return $transition;
    }

    // TODO: This is inconsistent with the actual function in Workflight.php. Check this, as it's likely an error.
    public function transitionToState(?int $newStateID = null): bool
    {
        if ($newStateID)
        {
            return $this->workflight->transitionToState($newStateID);
        }

        return false;
    }

    public static function workflow(): Workflow
    {
        $workflow = Workflow::where('model_name', get_class())->first();

        if (!$workflow)
        {
            throw new WorkflowNotFoundException('Workflow not found for '.get_class());
        }

        return $workflow;
    }

    // Returns a collection of available locations that this type of object can be attached to.
    public static function workflowLocations(): \Illuminate\Database\Eloquent\Collection
    {
        $workflow = self::workflow();

        $maxLocationLevel = $workflow->max_location_level;
        $minLocationLevel = $workflow->min_location_level;

        $locations = Location::withDepth()
            ->having('depth', '>=', $minLocationLevel)
            ->having('depth', '<=', $maxLocationLevel)
            ->get();

        return $locations;
    }

    public static function workflowLocationsForForm(): array
    {
        $locations = self::workflowLocations()->toTree();

        $GLOBALS['traverseReturn'] = [];

        $traverse = function ($categories, $prefix = '-') use (&$traverse): void {
            foreach ($categories as $category) {
                // echo PHP_EOL.$prefix.' '.$category->name;
                $GLOBALS['traverseReturn'][$category->id] = $prefix.' '.$category->name;

                $traverse($category->children, $prefix.'-');
            }
        };

        $traverse($locations, '');

        // dd($GLOBALS['traverseReturn']);

        return $GLOBALS['traverseReturn'];
    }
}
