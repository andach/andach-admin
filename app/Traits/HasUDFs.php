<?php

namespace App\Traits;

use App\Models\UDF\UDF;

trait HasUDFs
{
    public static function udfs(): \Illuminate\Database\Eloquent\Collection
    {
        return UDF::where('model_name', get_class())->get();
    }

    public function udfValues(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany('App\Models\UDF\UDFValue', 'fieldable');
    }

    /**
     * Returns an array of UDFs, being Form::components.
     */
    public static function udfsForCreatePage(): array
    {
        $return = [];

        foreach (self::udfs() as $udf)
        {
            $return[] = $udf->formItem();
        }

        return $return;
    }

    /**
     * Returns an array of UDFs, being Form::components.
     */
    public function udfsForEditPage(): array
    {
        $return = [];

        foreach (self::udfs() as $udf)
        {
            $udfValue = $udf->values()
                ->where('fieldable_id', $this->id)
                ->where('fieldable_type', get_class())
                ->first();

            if ($udfValue)
            {
                $return[] = $udfValue->formItem();
            } else {
                $return[] = $udf->formItem();
            }
        }

        return $return;
    }

    /**
     * Returns an array of UDFs, being names mapped to string representations of their value.
     */
    public function udfsForShowPage(): array
    {
        $return = [];

        foreach ($this->udfValues as $udfValue)
        {
            $return[$udfValue->udf->display_name] = $udfValue->value;
        }

        return $return;
    }

    /**
     * Returns a simple array of the UDF id mapped to the current value of the UDF.
     */
    public function udfValuesArray(): array
    {
        $return = [];

        foreach (self::udfs() as $udf)
        {
            $udfValue = $this->udfValues()
                ->where('user_defined_field_id', $udf->id)
                ->first();

            $value = '';

            if ($udfValue)
            {
                $value = $udfValue->value;
            }

            $return[$udf->id] = $value;
        }

        return $return;
    }

    /**
     * A function to update all UDFs from the $request->all() item.
     */
    public function updateUDFs(array $data): void
    {
        // Loop through all possible UDFs to see if we have a value.
        foreach (self::udfs() as $udf)
        {
            $valueToUpdateTo = '';
            if (isset($data[$udf->name]))
            {
                $valueToUpdateTo = $data[$udf->name];
            }

            $value = $this->udfValues()->firstOrNew(['user_defined_field_id' => $udf->id]);

            $value->updateValue($valueToUpdateTo);
        }
    }
}
