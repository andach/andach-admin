<?php

namespace App\Traits;

trait HasFromAndToDates
{
    public function scopeCurrent(\Illuminate\Database\Eloquent\Builder $query, ?string $date = ''): \Illuminate\Database\Eloquent\Builder
    {
        if ($date)
        {
            $date = date('Y-m-d', strtotime($date));
        } else {
            $date = date('Y-m-d');
        }

        return $query
            ->where('from_date', '<=', $date)
            ->where(function ($q) use ($date): void {
                $q->where('to_date', '>=', $date)
                ->orWhereNull('to_date');
            });
    }
}
