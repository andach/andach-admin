<?php

namespace App\Traits;

use App\Models\Company;
use Auth;

trait IsInvoice
{
    protected static function booted(): void
    {
        static::saving(function ($user) {
            if ($this->is_finalised)
            {
                return false;
            }
        });
    }

    public function refreshTotals(): void
    {
        $this->total_net   = $this->lines->sum('net');
        $this->total_vat   = $this->lines->sum('vat');
        $this->total_gross = $this->lines->sum('gross');
        $this->save();
    }
}
