<?php

namespace App\Traits;

use App\Models\Chart;

trait HasCharts
{
    /**
     * Returns the full javascript required for the chart to display, suitable for direct injection into a <script> tag.
     */
    public static function getChart(string $htmlID, string $type, array $data, ?array $dataOptions = [], ?array $otherOptions = []): string
    {
        $chart = new Chart($htmlID, $type, $data, $dataOptions, $otherOptions);

        return $chart->javascript();
    }
}
