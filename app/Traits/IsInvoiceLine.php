<?php

namespace App\Traits;

use App\Models\Company;
use Auth;

trait IsInvoiceLine
{
    protected static function booted(): void
    {
        static::saved(function ($user): void {
            $this->invoice->refreshTotals();
        });

        static::saving(function ($user) {
            if ($this->invoice->is_finalised)
            {
                return false;
            }

            // Calculate VAT if required. Note that vat_percentage is not a stored value, but if passed, will be used to calculate, and provided VAT will be ignored.
            if ($this->vat_percentage)
            {
                if ($this->gross)
                {
                    $this->net = round($this->gross * $this->vat_percentage / (1 + $this->vat_percentage), 2);
                } else {
                    $this->gross = round($this->net * $this->vat_percentage, 2);
                }
            }

            // Account for net, vat and gross totals being provided.
            if ($this->net && $this->vat && $this->gross)
            {
                if ($this->net + $this->vat !== $this->gross)
                {
                    // Then the provided invoice line doesn't balance and we reject it.
                    return false;
                }
            } else {
                // At least one of net, vat or gross is missing.
                if (!$this->net)
                {
                    $this->net = $this->gross - $this->vat;
                }

                if (!$this->vat)
                {
                    $this->vat = $this->gross - $this->net;
                }

                if (!$this->gross)
                {
                    $this->gross = $this->net + $this->vat;
                }
            }
        });
    }
}
