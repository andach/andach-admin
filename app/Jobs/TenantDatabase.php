<?php

namespace App\Jobs;

use App\Services\TenantManager;
use App\Models\Tenant;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;

class TenantDatabase implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;

    protected $tenant;

    protected $tenantManager;

    public function __construct(Tenant $tenant, TenantManager $tenantManager)
    {
        $this->tenant        = $tenant;
        $this->tenantManager = $tenantManager;
    }

    public function handle(TenantManager $tenantManager): void
    {
        $database    = 'tenant_' . $this->tenant->id;
        $connection  = \DB::connection('tenant');
        $createMysql = $connection->statement('CREATE DATABASE ' . $database);

        if ($createMysql) {
            $tenantManager->setTenant($this->tenant);
            \DB::purge('tenant');
            $this->migrate();
        } else {
            $connection->statement('DROP DATABASE ' . $database);
        }
    }

    private function migrate(): void
    {
        $migrator = app('migrator');
        $migrator->setConnection('tenant');

        if (! $migrator->repositoryExists()) {
            $migrator->getRepository()->createRepository();
        }

        $migrator->run(database_path('migrations/tenants'), []);
    }
}
