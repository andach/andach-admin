<?php

namespace App\Models\HR;

use App\Models\HR\Contract;
use App\Models\Time\Timesheet;
use App\Traits\HasFromAndToDates;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Appointment extends BaseModel
{
    use HasFromAndToDates;
    use SoftDeletes;

    protected $fillable = ['position_id', 'user_id', 'from_date', 'to_date'];
    protected $table = 'appointments';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function approvedHolidays(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Time\Holiday', 'approved_by_appointment_id');
    }

    public function contracts(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\HR\Contract', 'appointment_id');
    }

    public function grossPays(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Payslip\GrossPay', 'appointment_id');
    }

    public function holidays(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Time\Holiday', 'appointment_id');
    }

    public function position(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\HR\Position', 'position_id');
    }

    public function timesheets(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Time\Timesheet', 'appointment_id');
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function currentContract(): Contract
    {
        return $this->contracts()->orderBy('from_date', 'desc')->first();
    }

    public function currentContractedHours(): float
    {
        $contract = $this->currentContract();

        if ($contract)
        {
            return $contract->hours_per_week;
        }

        return 0;
    }

    public function getPayFormattedAttribute(): string
    {
        return $this->currentContract()->pay_formatted;
    }

    public function timesheetsToAuthorise(): \Illuminate\Support\Collection
    {
        return Timesheet::whereIn('team_id', $this->position->teamsManaged->pluck('id')->toArray())
            ->needsAuthorising()
            ->get();
    }

    public function updateContract(array $array): Contract
    {
        // First check that the new from_date is after the latest from_date on existing contracts.
        $latestContract = $this->contracts()->orderBy('from_date', 'desc')->first();

        if ($latestContract)
        {
            if ($array->from_date < $latestContract->from_date)
            {
                dd('it is not possible to set a new contract that starts previous to an existing one');
            }
        }

        $this->contracts()
            ->whereNull('to_date')
            ->update([
                'to_date' => date('Y-m-d', strtotime($array['from_date'].' -1 day'))
            ]);

        return $this->contracts()->create($array);
    }
}
