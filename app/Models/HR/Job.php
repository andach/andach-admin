<?php

namespace App\Models\HR;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Job extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['name', 'description_of_work', 'is_director', 'fte_hours'];
    protected $table = 'jobs';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function contracts(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany('App\Models\HR\Contract', 'link_contracts_jobs', 'job_id', 'contract_id');
    }

    public function positions(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\HR\Position', 'job_id');
    }

    public function roles(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany('App\Models\Role', 'link_jobs_roles', 'job_id', 'role_id');
    }
}
