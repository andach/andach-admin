<?php

namespace App\Models\HR;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Team extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['name', 'parent_id', 'manager_id'];
    protected $table = 'teams';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function children(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\HR\Team', 'parent_id');
    }

    public function manager(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\HR\Position', 'manager_id');
    }

    public function parent(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\HR\Team', 'parent_id');
    }

    public function positions(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\HR\Position', 'team_id');
    }

    public function getManagerNameAttribute(): string
    {
        if ($this->manager)
        {
            return $this->manager->name;
        }

        return '';
    }

    public function getParentNameAttribute(): string
    {
        if ($this->parent)
        {
            return $this->parent->name;
        }

        return '';
    }
}
