<?php

namespace App\Models\HR;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContractTemplate extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['pay_frequency_id', 'name', 'full_text', 'days_holiday_per_year', 'days_holiday_includes_bank_holidays', 'overtime_at_multiplier', 'pension_percent_1', 'pension_percent_2', 'pension_matched_to_percent_1', 'pension_matched_to_percent_2'];
    protected $table = 'contracts_templates';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function contracts(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\HR\Contract', 'contract_template_id');
    }

    public function payFrequency(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Payslip\PayFrequency', 'pay_frequency_id');
    }
}
