<?php

namespace App\Models\HR;

use App\Traits\HasFromAndToDates;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Position extends BaseModel
{
    use HasFromAndToDates;
    use SoftDeletes;

    protected $fillable = ['company_id', 'job_id', 'location_id', 'from_date', 'to_date', 'team_id', 'cost_code_id', 'hours_per_week'];
    protected $table = 'positions';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function appointments(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\HR\Appointment', 'position_id');
    }

    public function company(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    public function costCode(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\CostCode', 'cost_code_id');
    }

    public function location(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Location\Location', 'location_id');
    }

    public function job(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\HR\Job', 'job_id');
    }

    public function team(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\HR\Team', 'team_id');
    }

    public function teamsManaged(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\HR\Team', 'manager_id');
    }

    public function currentAppointedHours(): float
    {
        $appointments = $this->appointments()->current()->get();
        $return       = 0;

        foreach ($appointments as $appointment)
        {
            $return += $appointment->currentContractedHours();
        }

        return $return;
    }

    public function getDatesAttribute(): string
    {
        if ($this->to_date)
        {
            if (time() > strtotime($this->to_date))
            {
                return $this->from_date.' to '.$this->to_date.' (Closed)';
            }
            return $this->from_date.' to '.$this->to_date;
        }

        return $this->from_date.' (no closing date)';
    }

    public function getHoursAttribute(): string
    {
        $currentHours = $this->currentAppointedHours();

        return $currentHours.' / '.$this->hours_per_week;
    }
}
