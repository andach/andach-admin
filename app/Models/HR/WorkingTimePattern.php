<?php

namespace App\Models\HR;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkingTimePattern extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['name', 'mon_mins', 'tue_mins', 'wed_mins', 'thu_mins', 'fri_mins', 'sat_mins', 'sun_mins'];
    protected $table = 'working_time_patterns';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function contracts(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\HR\Contract', 'working_time_pattern_id');
    }
}
