<?php

namespace App\Models\HR;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contract extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['appointment_id', 'contract_template_id', 'working_time_pattern_id', 'from_date', 'to_date', 'full_text', 'pay_per_hour', 'salary', 'salary_fte', 'file_path', 'is_part_time', 'is_zero_hours', 'is_positive_pay', 'hours_per_week'];
    protected $table = 'contracts';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function appointment(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\HR\Appointment', 'appointment_id');
    }

    public function jobs(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany('App\Models\HR\Job', 'link_contracts_jobs', 'contract_id', 'job_id');
    }

    public function template(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\HR\ContractTemplate', 'contract_template_id');
    }

    public function workingTimePattern(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\HR\WorkingTimePattern', 'working_time_pattern_id');
    }

    public function getPayFormattedAttribute(): string
    {
        if ($this->pay_per_hour)
        {
            return '&pound;'.number_format($this->pay_per_hour, 2).' / hour';
        }

        return '&pound;'.number_format($this->salary, 2);
    }
}
