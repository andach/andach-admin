<?php

namespace App\Models\Rental;

use App\Models\BaseModel;
use App\Traits\HasFromAndToDates;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rental extends BaseModel
{
    use HasFromAndToDates;
    use SoftDeletes;

    protected $fillable = ['location_id', 'user_id', 'from_date', 'to_date', 'rented_thing_id', 'rented_thing_type'];
    protected $table = 'rentals';
    public $timestamps = true;

    protected $dates = ['deleted_at'];

    public function chargePeriods(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Rental\ChargePeriod', 'rental_id');
    }

    public function location(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Location\Location', 'location_id');
    }

    public function rentedThing(): \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo();
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function currentCharges(): Array
    {
        return $this->chargePeriods()->current()->first()->responsibilities;
    }

    public function endAt(string $date): bool
    {
        $this->end_date = $date;
        $this->save();
        $this->chargePeriods()->current()->first()->endAt($date);
    }

    public function generateInvoices(): void
    {
        dd('need to write the generateInvoice() function in Rental.php');
    }
}
