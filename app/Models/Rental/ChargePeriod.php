<?php

namespace App\Models\Rental;

use App\Models\BaseModel;
use App\Traits\HasFromAndToDates;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChargePeriod extends BaseModel
{
    use HasFromAndToDates;
    use SoftDeletes;

    protected $fillable = ['rental_id', 'from_date', 'to_date', 'price_per_time_unit', 'enum_time_unit'];
    protected $table = 'rentals_charge_periods';
    public $timestamps = true;

    protected $dates = ['deleted_at'];

    public function rental(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Rental\Rental', 'rental_id');
    }

    public function responsibilities(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Rental\Responsibility', 'rental_charge_period_id');
    }

    public function endAt(string $date): bool
    {
        $this->end_date = $date;
        $this->save();
    }
}
