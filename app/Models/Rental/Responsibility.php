<?php

namespace App\Models\Rental;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Responsibility extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['rental_charge_period_id', 'user_id', 'price'];
    protected $table = 'rentals_responsibility';
    public $timestamps = true;

    protected $dates = ['deleted_at'];

    public function chargePeriod(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Rental\ChargePeriod', 'rental_charge_period_id');
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function getFormattedPriceAttribute(): string
    {
        return '&pound;'.number_format($this->price, 2);
    }
}
