<?php

namespace App\Models\Complaint;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Source extends BaseModel
{
    use SoftDeletes;

    protected $table = 'complaints_sources';
    public $timestamps = true;

    protected $dates = ['deleted_at'];

    public function complaints(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Complaint\Complaint');
    }
}
