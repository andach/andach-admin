<?php

namespace App\Models\Complaint;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends BaseModel
{
    use SoftDeletes;

    protected $table = 'complaints_categories';
    public $timestamps = true;

    protected $dates = ['deleted_at'];

    public function complaint(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Complaint\Complaint');
    }

    public function parent(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Complaint\Category', 'parent_id');
    }

    public function children(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Complaint\Category', 'parent_id');
    }
}
