<?php

namespace App\Models\Complaint;

use App\Traits\AccessLimitedByCompany;
use App\Traits\CanSearch;
use App\Traits\IsCloseable;
use App\Traits\HasCharts;
use App\Traits\HasWorkflight;
use App\Traits\HasUDFs;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Complaint extends BaseModel
{
    use AccessLimitedByCompany;
    use CanSearch;
    use IsCloseable;
    use HasCharts;
    use HasWorkflight;
    use HasUDFs;
    use SoftDeletes;

    protected $table = 'complaints';
    public $timestamps = true;
    protected $fillable = ['name', 'description', 'company_id', 'customer_id', 'product_id', 'supplier_id', 'location_id', 'user_id', 'response_required', 'response_sent_date'];
    protected $dates = ['deleted_at'];

    public function company(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    public function category(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Complaint\Category');
    }

    public function customer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'customer_id');
    }

    public function openedByUser(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'opened_by_user_id');
    }

    public function product(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Product\Product');
    }

    public function source(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Complaint\Source');
    }

    public function store(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Location\Location');
    }

    public function supplier(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'supplier_id');
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function workflight(): \Illuminate\Database\Eloquent\Relations\MorphOne
    {
        return $this->morphOne('App\Models\Workflow\Workflight', 'workflowable');
    }
}
