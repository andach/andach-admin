<?php

namespace App\Models;

use App\Models\BaseModel;
use App\Models\Role;
use App\Models\User;

class RolePermission extends BaseModel
{
    protected $table = 'roles_permissions';
    public $timestamps = true;
    protected $fillable = ['role_id', 'access_to'];

    public function role(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Role', 'role_id');
    }

    public static function usersWithAccessTo(array $accessToDo): \Illuminate\Database\Eloquent\Collection
    {
        // $roleIDs = Role::whereHas('rolePermissions', function($query1) use ($accessToDo) {
        //     $query1->whereIn('access_to', $accessToDo);
        // })->get();

        // dd($roleIDs);

        $return = User::whereHas('roles', function ($query) use ($accessToDo): void {
            // user_id is required here*
            $query->whereHas('rolePermissions', function ($query1) use ($accessToDo): void {
                $query1->whereIn('access_to', $accessToDo);
            });
        })
        ->orWhere('is_super_admin', 1)
        ->get();

        return $return;
    }
}
