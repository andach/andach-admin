<?php

namespace App\Models\CareHome;

use App\Models\CareHome\Assessment\Fall;
use App\Models\Rental\ChargePeriod;
use App\Models\Rental\Rental;
use App\Models\Rental\Responsibility;
use App\Models\BaseModel;
use App\Traits\PrimaryKeyUuid;
use Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class Resident extends BaseModel
{
    use PrimaryKeyUuid;
    use SoftDeletes;

    protected $table = 'care_home_residents';
    public $timestamps = true;

    protected $dates = ['deleted_at', 'from_date', 'to_date'];
    protected $fillable = ['user_id', 'is_challenging', 'challenging_details', 'is_safeguarding', 'safeguarding_details', 'allergies', 'gp_details', 'next_of_kin', 'power_of_attorney'];

    public function catheterChanges(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\CareHome\CareRecord\Catheter');
    }

    public function endOfLifeWishes(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\CareHome\CareRecord\EndOfLife');
    }

    public function fallAssessments(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\CareHome\Assessment\Fall');
    }

    public function foodLog(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\CareHome\CareRecord\FoodLog');
    }

    public function hospitalAdmissions(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\CareHome\CareRecord\HospitalAdmission');
    }

    public function illnesses(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\CareHome\CareRecord\Illness');
    }

    public function malnutritionAssessments(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\CareHome\Assessment\Malnutrition');
    }

    public function medicalAssessments(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\CareHome\Assessment\Medical');
    }

    public function skinAssessments(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\CareHome\Assessment\Skin');
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\User');
    }

    public function weightLog(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\CareHome\CareRecord\WeightLog');
    }

    // Adds the date (today) and the completed_by_user_id field to input data.
    public function addDateAndUserID(array $data): array
    {
        return $data + [
            'completed_by_user_id' => Auth::id(),
            'date_of_assessment'   => date('Y-m-d'),
        ];
    }

    public function currentEndOfLifeWishes(): ?object
    {
        return $this->endOfLifeWishes()->orderBy('id', 'desc')->first();
    }

    public function currentChargePeriod(): ?ChargePeriod
    {
        if (!$rental = $this->currentRental())
        {
            return null;
        }

        return $rental->chargePeriods()->current()->first();
    }

    public function currentCharges(): ?\Illuminate\Database\Eloquent\Collection
    {
        if (!$chargePeriod = $this->currentChargePeriod())
        {
            return null;
        }

        return $chargePeriod->responsibilities;
    }

    public function currentRental(): ?Rental
    {
        return $this->user->rentals()->current()->first();
    }

    public function lastFallAssessmentArray(): array
    {
        $lastAssessment = $this->fallAssessments()->orderBy('date_of_assessment', 'desc')->first();

        if ($lastAssessment)
        {
            return $lastAssessment->jsonAsArray();
        }

        $return = [];
        $questions = Fall::questionsWithoutCategories();
        foreach (array_keys($questions) as $key)
        {
            $return[$key]             = 0;
            $return[$key.'_comments'] = '';
        }

        return $return;
    }

    public static function scopeCurrent(\Illuminate\Database\Eloquent\Builder $query): \Illuminate\Database\Eloquent\Builder
    {
        return $query->whereHas('user', function ($query): void {
            $query->whereHas('rentals', function ($query1): void {
                $query1->where('from_date', '<=', date('Y-m-d'))->where(function ($query2): void {
                    $query2->where('to_date', '>=', date('Y-m-d'))
                        ->orWhereNull('to_date');
                });
            });
        });
    }

    public function updateBillingInformation(array $data): bool
    {
        $data['charge_per_week'] = 0;

        // If we have a non-zero charge, check that this has all been allocated across users.
        if ($data['charge_per_week'])
        {
            if (array_sum($data['cost_for_user_id']) != $data['charge_per_week'])
            {
                return false;
            }
        }

        if ($currentRental = $this->currentRental())
        {
            // If we have a current rental, then we need to end it (if price changed) or the charge period.
            $endDate = strtotime($data['from_date'].' -1 day');

            if ($currentRental->charge_per_week == $data['charge_per_week'] && $currentRental->rented_thing_id == $data['rented_thing_id'])
            {
                // Then we only have to update the current charge period.
                if ($currentChargePeriod = $currentRental->currentChargePeriod())
                {
                    $currentChargePeriod->endAt($endDate);
                }

                $newChargePeriod = $currentRental->chargePeriods()->create([
                    'from_date'           => $data['from_date'],
                    'price_per_time_unit' => $data['charge_per_week'],
                    'enum_time_unit'      => 'WEEKLY',
                ]);

                $newChargePeriod->save();
            } else {
                // Note that this also ends the current charge period.
                $currentRental->endAt($endDate);

                $newRental = $this->user->rentals()->create([
                    'rented_thing_id'   => $data['rented_thing_id'],
                    'rented_thing_type' => 'App\Models\Location\Location',
                    'from_date'         => $data['from_date'],
                ]);

                $newRental->save();

                $newChargePeriod = $newRental->chargePeriods()->create([
                    'from_date'           => $data['from_date'],
                    'price_per_time_unit' => $data['charge_per_week'],
                    'enum_time_unit'      => 'WEEKLY',
                ]);

                $newChargePeriod->save();
            }
        } else {
            // This user does not have any rental data associated with them. Just enter new data.

            $newRental = $this->user->rentals()->create([
                'rented_thing_id'   => $data['rented_thing_id'],
                'rented_thing_type' => 'App\Models\Location\Location',
                'from_date'         => $data['from_date'],
            ]);

            $newRental->save();

            $newChargePeriod = $newRental->chargePeriods()->create([
                'from_date'           => $data['from_date'],
                'price_per_time_unit' => $data['charge_per_week'],
                'enum_time_unit'      => 'WEEKLY',
            ]);
        }

        if (isset($data['responsible_user_id']))
        {
            foreach ($data['responsible_user_id'] as $id => $userID)
            {
                if ($userID && $data['cost_for_user_id'][$id])
                {
                    $newChargePeriod->responsibilities()->create([
                        'user_id' => $userID,
                        'price'   => $data['cost_for_user_id'][$id],
                    ]);
                }
            }
        }

        return true;
    }

    public function updateCatheter(array $data): bool
    {
        $data = $this->addDateAndUserID($data);

        return true && $this->catheterChanges()->create($data);
    }

    public function updateMedical(array $data): bool
    {
        $data = $this->addDateAndUserID($data);

        return true && $this->medicalAssessments()->create($data);
    }

    public function updateEndOfLife(array $data): bool
    {
        $data = $this->addDateAndUserID($data);

        return true && $this->endOfLifeWishes()->create($data);
    }

    public function updateFalls(array $data): bool
    {
        $cleanData            = [];
        $categorisedQuestions = Fall::questions();

        foreach ($categorisedQuestions as $questions)
        {
            foreach (array_keys($questions) as $key)
            {
                $cleanData[$key]             = $data[$key];
                $cleanData[$key.'_comments'] = $data[$key.'_comments'];
            }
        }

        $allData = $this->addDateAndUserID(['json_response' => json_encode($cleanData)]);

        return true && $this->fallAssessments()->create($allData);
    }

    public function updateFoodLog(array $data): bool
    {
        $data = $this->addDateAndUserID($data);

        return true && $this->foodLog()->create($data);
    }

    public function updateGeneral(array $data): bool
    {
        return true && $this->update($data);
    }

    public function updateHospital(array $data): bool
    {
        $data = $this->addDateAndUserID($data);

        return true && $this->hospitalAdmissions()->create($data);
    }

    public function updateIllness(array $data): bool
    {
        $data = $this->addDateAndUserID($data);

        return true && $this->illnesses()->create($data);
    }

    public function updateMalnutrition(array $data): bool
    {
        $data = $this->addDateAndUserID($data);

        return true && $this->malnutritionAssessments()->create($data);
    }

    public function updateSkin(array $data): bool
    {
        $data = $this->addDateAndUserID($data);

        $data['braden_total'] = $data['braden_sensory_perception']
            + $data['braden_moisture']
            + $data['braden_activity']
            + $data['braden_mobility']
            + $data['braden_nutrition']
            + $data['braden_friction'];

        return true && $this->skinAssessments()->create($data);
    }

    public function updateWeightlog(array $data): bool
    {
        $data = $this->addDateAndUserID($data);

        if (isset($data['weight_in_kg']))
        {
            $data['weight_in_lbs'] = $data['weight_in_kg'] * 2.20462;
        } else {
            if (isset($data['weight_in_st']))
            {
                $data['weight_in_lbs'] = $data['weight_in_lbs'] + $data['weight_in_st'] * 14;
            }

            $data['weight_in_kg'] = $data['weight_in_lbs'] / 2.20462;
        }

        return true && $this->weightLog()->create($data);
    }
}
