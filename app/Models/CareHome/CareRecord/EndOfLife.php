<?php

namespace App\Models\CareHome\CareRecord;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class EndOfLife extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['resident_id', 'date_of_assessment', 'completed_by_user_id', 'what', 'who', 'where', 'special_requests'];
    protected $table = 'care_home_records_end_of_life_wishes';
    public $timestamps = true;

    protected $dates = ['deleted_at'];
}
