<?php

namespace App\Models\CareHome\CareRecord;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Catheter extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['resident_id', 'date_of_assessment', 'completed_by_user_id', 'date_changed', 'reasons', 'size', 'changed_by_user_id'];
    protected $table = 'care_home_records_catheters';
    public $timestamps = true;

    protected $dates = ['deleted_at'];

    public function changedByUser(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'changed_by_user_id');
    }
}
