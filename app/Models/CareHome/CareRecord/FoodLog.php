<?php

namespace App\Models\CareHome\CareRecord;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class FoodLog extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['resident_id', 'completed_by_user_id', 'date_eaten', 'enum_meal', 'food_and_drink', 'fortified_with'];
    protected $table = 'care_home_records_food_log';
    public $timestamps = true;

    protected $dates = ['deleted_at'];
}
