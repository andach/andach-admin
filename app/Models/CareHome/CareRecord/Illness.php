<?php

namespace App\Models\CareHome\CareRecord;

use App\Models\BaseModel;
use App\Traits\CareHomeAssessment;
use Illuminate\Database\Eloquent\SoftDeletes;

class Illness extends BaseModel
{
    use CareHomeAssessment;
    use SoftDeletes;

    protected $fillable = ['resident_id', 'date_of_assessment', 'completed_by_user_id', 'date_from', 'date_to', 'illness', 'general_observations', 'extra_risk_assessment'];
    protected $table = 'care_home_records_illnesses';
    public $timestamps = true;

    protected $dates = ['deleted_at'];
}
