<?php

namespace App\Models\CareHome\CareRecord;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class HospitalAdmission extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['resident_id', 'date_of_assessment', 'completed_by_user_id', 'date_from', 'date_to', 'reason', 'outcome', 'extra_risk_assessment'];
    protected $table = 'care_home_records_hospital_admissions';
    public $timestamps = true;

    protected $dates = ['deleted_at'];
}
