<?php

namespace App\Models\CareHome\CareRecord;

use App\Models\BaseModel;
use App\Traits\CareHomeAssessment;
use Illuminate\Database\Eloquent\SoftDeletes;

class WeightLog extends BaseModel
{
    use CareHomeAssessment;
    use SoftDeletes;

    protected $fillable = ['resident_id', 'date_of_assessment', 'completed_by_user_id', 'weight_in_kg', 'weight_in_lbs', 'bmi'];
    protected $table = 'care_home_records_weight_log';
    public $timestamps = true;

    protected $dates = ['deleted_at'];

    public function getFormattedWeightKgAttribute(): string
    {
        return number_format($this->weight_in_kg, 1).' kg';
    }

    public function getFormattedWeightLbsAttribute(): string
    {
        return number_format($this->weight_in_lbs, 0).' lbs';
    }

    public function getFormattedWeightStAttribute(): string
    {
        $st  = floor($this->weight_in_lbs / 14);
        $lbs = $this->weight_in_lbs - $st * 14;

        return number_format($st, 0).' st, '.number_format($lbs, 0).' lbs';
    }
}
