<?php

namespace App\Models\CareHome\Assessment;

use App\Models\BaseModel;
use App\Traits\CareHomeAssessment;
use Illuminate\Database\Eloquent\SoftDeletes;

class Malnutrition extends BaseModel
{
    use CareHomeAssessment;
    use SoftDeletes;

    protected $fillable = ['resident_id', 'date_of_assessment', 'completed_by_user_id', 'bmi', 'weight_loss', 'is_acutely_ill', 'additional_comments'];
    protected $table = 'care_home_records_malnutrition_assessments';
    public $timestamps = true;

    protected $dates = ['deleted_at'];

    public function getAcutelyIllFormattedAttribute(): string
    {
        if ($this->is_acutely_ill)
        {
            return 'Y';
        }

        return 'N';
    }

    public function getBmiFormattedAttribute(): string
    {
        $return = [
            0 => '> 20',
            1 => '18.5 - 20',
            2 => '< 18.5',
        ];

        return $return[$this->must_bmi];
    }

    public function getRiskFormattedAttribute(): string
    {
        if ($this->riskScore() === 0)
        {
            return 'Low';
        } elseif ($this->riskScore() === 1) {
            return 'Medium';
        }

        return 'High';
    }

    public function getWeightLossFormattedAttribute(): string
    {
        $return = [
            0 => '< 5%',
            1 => '5% - 10%',
            2 => '> 10%',
        ];

        return $return[$this->must_weight_loss];
    }

    public function riskScore(): int
    {
        return $this->must_bmi + $this->must_weight_loss + $this->is_acutely_ill * 2;
    }
}
