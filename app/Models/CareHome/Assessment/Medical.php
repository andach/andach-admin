<?php

namespace App\Models\CareHome\Assessment;

use App\Enums\CareHome\Medical\Alcohol;
use App\Enums\CareHome\Medical\Cognition;
use App\Enums\CareHome\Medical\Communication;
use App\Enums\CareHome\Medical\Continence;
use App\Enums\CareHome\Medical\Hearing;
use App\Enums\CareHome\Medical\Mobility;
use App\Enums\CareHome\Medical\Sight;
use App\Enums\CareHome\Medical\Smoker;
use App\Models\BaseModel;
use App\Traits\CareHomeAssessment;
use Illuminate\Database\Eloquent\SoftDeletes;

class Medical extends BaseModel
{
    use CareHomeAssessment;
    use SoftDeletes;

    protected $fillable = ['resident_id', 'date_of_assessment', 'completed_by_user_id', 'enum_mobility', 'enum_continence', 'enum_cognition', 'enum_communication', 'enum_hearing', 'enum_sight', 'blood_pressure', 'pulse', 'oxygen_sats', 'respiration_rate', 'enum_smoker', 'enum_alcohol'];
    protected $table = 'care_home_records_medical';
    public $timestamps = true;

    protected $dates = ['deleted_at'];

    public function getPrintAssessmentAttribute(): string
    {
        return '<div class="row">
            <div class="col-5">Mobility</div>
            <div class="col-7">'.Mobility::toArray()[$this->enum_mobility].'</div>
            
            <div class="col-5">Continence</div>
            <div class="col-7">'.Continence::toArray()[$this->enum_continence].'</div>
            
            <div class="col-5">Cognition</div>
            <div class="col-7">'.Cognition::toArray()[$this->enum_cognition].'</div>
            
            <div class="col-5">Communication</div>
            <div class="col-7">'.Communication::toArray()[$this->enum_communication].'</div>
            
            <div class="col-5">Hearing</div>
            <div class="col-7">'.Hearing::toArray()[$this->enum_hearing].'</div>
            
            <div class="col-5">Sight</div>
            <div class="col-7">'.Sight::toArray()[$this->enum_sight].'</div>
            
            <div class="col-5">Blood Pressure</div>
            <div class="col-7">'.$this->blood_pressure.'</div>
            
            <div class="col-5">Pulse</div>
            <div class="col-7">'.$this->pulse.'</div>
            
            <div class="col-5">Oxygen Sats</div>
            <div class="col-7">'.$this->oxygen_sats.'</div>
            
            <div class="col-5">Respiration Rate</div>
            <div class="col-7">'.$this->respiration_rate.'</div>
            
            <div class="col-5">Smoker</div>
            <div class="col-7">'.Smoker::toArray()[$this->enum_smoker].'</div>
            
            <div class="col-5">Alcohol</div>
            <div class="col-7">'.Alcohol::toArray()[$this->enum_alcohol].'</div>
        
        </div>';
    }
}
