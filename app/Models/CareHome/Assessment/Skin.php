<?php

namespace App\Models\CareHome\Assessment;

use App\Models\BaseModel;
use App\Traits\CareHomeAssessment;
use Illuminate\Database\Eloquent\SoftDeletes;

class Skin extends BaseModel
{
    use CareHomeAssessment;
    use SoftDeletes;

    protected $fillable = ['resident_id', 'date_of_assessment', 'completed_by_user_id', 'braden_sensory_perception', 'braden_moisture', 'braden_activity', 'braden_mobility', 'braden_nutrition', 'braden_friction', 'braden_total', 'general_observations', 'is_broken_skin', 'is_infection_present', 'infection_treated_with'];
    protected $table = 'care_home_records_skin_integrity_assessments';
    public $timestamps = true;

    protected $dates = ['deleted_at'];

    public function getButtonClassAttribute(): string
    {
        if ($this->braden_total <= 12)
        {
            return 'btn btn-danger';
        } elseif ($this->braden_total <= 14) {
            return 'btn btn-warning';
        }

        return 'btn btn-success';
    }

    public function getButtonTextAttribute(): string
    {
        if ($this->braden_total <= 12)
        {
            return 'High Risk';
        } elseif ($this->braden_total <= 14) {
            return 'Medium Risk';
        }

        return 'Low Risk';
    }

    public function getBrokenSkinFormattedAttribute(): string
    {
        if ($this->is_broken_skin)
        {
            return 'Y';
        }

        return 'N';
    }

    public function getInfectionPresentFormattedAttribute(): string
    {
        if ($this->is_infection_present)
        {
            return 'Y';
        }

        return 'N';
    }
}
