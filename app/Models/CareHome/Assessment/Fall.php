<?php

namespace App\Models\CareHome\Assessment;

use App\Models\BaseModel;
use App\Traits\CareHomeAssessment;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fall extends BaseModel
{
    use CareHomeAssessment;
    use SoftDeletes;

    protected $fillable = ['resident_id', 'date_of_assessment', 'completed_by_user_id', 'json_response'];
    protected $table = 'care_home_records_falls_risk_assessments';
    public $timestamps = true;

    protected $dates = ['deleted_at'];

    public function getNumberOfRiskFactorsAttribute(): int
    {
        $questions = self::questionsWithoutCategories();
        $score     = 0;
        $answers   = $this->jsonAsArray();

        foreach (array_keys($questions) as $key)
        {
            if ($answers[$key])
            {
                $score++;
            }
        }

        return $score;
    }

    public function jsonAsArray(): array
    {
        return json_decode($this->json_response, true);
    }

    public static function questions(): array
    {
        return [
            'History and Health' => [
                'previous_falls' => 'Has the resident had more than one fall in the last 12 months?',
                'osteoporosis' => 'Does the resident have, or are they at risk of osteoporosis?',
                'confused' => 'Is the resident confused, disorientated, restless or agitated?',
                'judgement' => 'Does the resident have reduced judgement and/or are they uncorporative with staff frequently?',
                'continence' => 'Does the resident have continence issues that could increase risk of falls?',
            ],
            'Balance and Mobility' => [
                'unsteady' => 'Is the resident unsteady on their feet or unsafe walking?',
                'transfers' => 'Does the resident have difficulty with getting on and off the toilet, bed, chair or similar?',
                'dizziness' => 'Does the resident suffer from dizziness, light-headedness when standing, fainting or palpitations?',
            ],
            'Medication' => [
                'four_or_more' => 'Is the patient on four or more different types of medication?',
                'risky_medicines' => 'Is the patient taking sedatives, anti-depressents, anti-Parkinson medication, diuretics, anti-psychotics, anti-coagulants or anti-hypertensives?',
                'recent_change' => 'Has there been a recent change in medication that might affect falls?',
            ],
            'Nutrition' => [
                'lost_weight' => 'Has the patient lost weight recently and/or do they have little appetite?',
                'daylight' => 'Does the patient spend little time outside in the sun?',
            ],
            'Senses' => [
                'bad_eyesight' => 'Does the resident have bad eyesight?',
                'bad_hearing' => 'Does the resident have bad hearing?',
            ],
            'Clothing' => [
                'feet_issues' => 'Does the resident have corns, ingrown nails, bunions, loss of sense, or similar foot problems?',
                'bad_shoes' => 'Does the resident wear poorly fitting shoes, or those that are difficult to walk in?',
            ],
            'Other' => [
                'out_of_bed' => 'Does the resident get out of bed regularly overnight?',
                'new_resident' => 'Is the resident new, and if so, are they well oriented to their new environment?',
                'other' => 'Are there any other factors that might increase the risk of falls?',
            ],
        ];
    }

    public static function questionsWithoutCategories(): array
    {
        $questionsWithCategories    = self::questions();
        $questionsWithoutCategories = [];

        foreach ($questionsWithCategories as $question)
        {
            foreach ($question as $key => $text)
            {
                $questionsWithoutCategories[$key] = $text;
            }
        }

        return $questionsWithoutCategories;
    }
}
