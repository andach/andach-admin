<?php

namespace App\Models\Payslip;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class PayFrequency extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['name'];
    protected $table = 'pay_frequency';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function contractTemplates(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\HR\ContractTemplate', 'pay_frequency_id');
    }

    public function payPeriod(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Payslip\PayPeriod', 'pay_frequency_id');
    }
}
