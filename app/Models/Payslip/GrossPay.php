<?php

namespace App\Models\Payslip;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class GrossPay extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['appointment_id', 'pay_period_id', 'gross_pay'];
    protected $table = 'gross_pay';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function appointment(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\HR\Appointment', 'appointment_id');
    }

    public function payPeriod(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Payslip\PayPeriod', 'pay_period_id');
    }

    public function payslip(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Payslip\Payslip', 'payslip_id');
    }
}
