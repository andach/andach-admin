<?php

namespace App\Models\Payslip;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class PayYear extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['name'];
    protected $table = 'pay_years';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function payPeriods(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Payslip\PayPeriod', 'pay_year_id');
    }
}
