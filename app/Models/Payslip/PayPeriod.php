<?php

namespace App\Models\Payslip;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class PayPeriod extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['pay_frequency_id', 'pay_year_id', 'name', 'from_date', 'to_date'];
    protected $table = 'pay_periods';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function grossPays(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Payslip\GrossPay', 'pay_period_id');
    }

    public function payFrequency(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Payslip\PayFrequency', 'pay_frequency_id');
    }

    public function payYear(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Payslip\PayYear', 'pay_year_id');
    }
}
