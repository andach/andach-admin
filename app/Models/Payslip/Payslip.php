<?php

namespace App\Models\Payslip;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payslip extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [];
    protected $table = 'payslips';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function grossPays(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Payslip\GrossPay', 'payslip_id');
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
