<?php

namespace App\Models\Workflow;

use Auth;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transition extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['name', 'workflow_state_from_id', 'workflow_state_to_id'];
    protected $table = 'workflows_transitions';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function roles(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany('App\Models\Role', 'link_roles_workflows_transitions', 'workflow_transition_id', 'role_id');
    }

    public function stateFrom(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Workflow\State', 'workflow_state_from_id');
    }

    public function stateTo(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Workflow\State', 'workflow_state_to_id');
    }

    public function udf(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany('App\Models\UDF\UDF', 'link_udf_values_workflows_transitions', 'workflow_transition_id', 'udf_id')
            ->withPivot('allow_or_deny', 'operator', 'operator_value');
    }

    public function workflow(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Workflow\Workflow', 'workflow_id');
    }

    /**
     * Returns true or false based on whether this is available based off UDFs.
     * Requires a simple array of UDF ids mapped to their current values.
     */
    public function isAvailable(array $udfValues): bool
    {
        $return = true;

        if (!$this->isAvailableBasedOnRoles())
        {
            return false;
        }

        // Now check through each UDF to see whether they limit this transition.
        foreach ($this->udf as $udf)
        {
            $udfValue = $udfValues[$udf->id];

            if ($udf->pivot->allow_or_deny)
            {
                // Then this is an ALLOW rule. Deny if false.
                dd('do not current support allow rules yet');
            } else {
                // Then this is a DENY rule. Deny if true.
                switch ($udf->pivot->operator)
                {
                    case '>':
                        $return = $udfValue > $udf->pivot->operator_value;
                        break;
                    case '>=':
                        $return = $udfValue >= $udf->pivot->operator_value;
                        break;
                    case '=':
                        $return = $udfValue == $udf->pivot->operator_value;
                        break;
                    case '<':
                        $return = $udfValue < $udf->pivot->operator_value;
                        break;
                    case '<=':
                        $return = $udfValue <= $udf->pivot->operator_value;
                        break;
                }
            }
        }

        return $return;
    }

    /**
     * Shows whether this transition is avilable based ONLY on the roles that the currently logged in user has
     * and this setup, ignoring the values of any UDFs.
     */
    public function isAvailableBasedOnRoles(): bool
    {
        if (!Auth::user()->is_super_admin)
        {
            // First check the roles with the current user's roles.
            $userRoles = Auth::user()->allRoles()->pluck('id')->toArray();
            $thisRoles = $this->roles()->pluck('roles.id')->toArray();

            if (!array_intersect($userRoles, $thisRoles))
            {
                return false;
            }
        }

        return true;
    }
}
