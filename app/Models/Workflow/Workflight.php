<?php

namespace App\Models\Workflow;

use App\Models\Workflow\Transition;
use Auth;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Workflight extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['workflow_id', 'assigned_to_user_id', 'state_id', 'name'];
    protected $table = 'workflights';
    public $timestamps = true;

    protected $dates = ['deleted_at'];

    public function assignedToUser(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'assigned_to_user_id');
    }

    public function audits(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Workflow\WorkflightAudit', 'workflight_id');
    }

    public function comments(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Workflow\Comment', 'workflight_id');
    }

    public function state(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Workflow\State', 'state_id');
    }

    public function workflow(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Workflow\Workflow', 'workflow_id');
    }

    public function workflowable(): \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo();
    }

    public function addComment(?string $description = '', ?\Illuminate\Http\UploadedFile $attachment = null): bool
    {
        $data = [];

        if ($description != '' || $attachment !== null) {
            if ($attachment !== null) {
                $data['attachment_path'] = $attachment->store('workflights', 'public');
            }
            $data['description'] = $description ?? '';
            $data['user_id']     = Auth::id();

            $this->comments()->create($data);

            return true;
        }

        return false;
    }

    /**
     * Assigns the workflight to a given user. If $permissionToCheck is set, then the script will check that the
     * user has permission to do whatever is specified, and fail otherwise.
     */
    public function assignTo(?int $userID = null): bool
    {
        if (!$userID)
        {
            $userID = 0;
        }

        $this->assigned_to_user_id = $userID;
        $this->save();

        return true;
    }

    public function getAssignedToNameAttribute(): string
    {
        if ($this->assignedToUser)
        {
            return $this->assignedToUser->name;
        }

        return '';
    }

    public function getCurrentStatusAttribute(): string
    {
        return $this->state->name;
    }

    public function getLastCommentByNameAttribute(): string
    {
        if ($this->comments()->count()) {
            return $this->comments()->orderBy('created_at', 'desc')->first()->user->name;
        }

        return '';
    }

    public function getNumberOfCommentsAttribute(): string
    {
        return $this->comments()->count();
    }

    public function isClosed(): string
    {
        if ($this->state->is_closed) {
            return true;
        }

        return false;
    }

    /**
     * Returns a collection of states that the current user can move to.
     */
    public function transitionsAvailable(array $udfValues): \Illuminate\Database\Eloquent\Collection
    {
        // TODO: Limit these down by role.
        $allTransitions = $this->workflow->transitions()->where('workflow_state_from_id', $this->state_id)->get();

        $ids = [];

        foreach ($allTransitions as $transition)
        {
            if ($transition->isAvailable($udfValues))
            {
                $ids[] = $transition->id;
            }
        }

        return Transition::whereIn('id', $ids)->get();
    }

    /**
     * Transitions the workflow to the new state.
     */
    public function transitionToState(int $transitionID): bool
    {
        $transition = Transition::find($transitionID);

        if ($transition->workflow_state_from_id != $this->state_id)
        {
            return false;
        }

        $data = [];
        $data['assigned_to_user_id'] = $this->assigned_to_user_id;
        $data['instigating_user_id'] = Auth::id();
        $data['state_id']            = $transition->workflow_state_to_id;
        $data['from_datetime']       = date('Y-m-d h:i:s');

        $this->audits()
            ->whereNull('to_datetime')
            ->update(['to_datetime' => date('Y-m-d h:i:s')]);

        $this->audits()->create($data);

        $this->state_id = $transition->workflow_state_to_id;
        $this->save();

        // Now we need to check if the state we transitioned to is closed, and update the workflowable as appropriate.
        $this->workflowable->is_closed = State::find($this->state_id)->is_closed;
        $this->workflowable->save();

        return true;
    }
}
