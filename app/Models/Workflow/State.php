<?php

namespace App\Models\Workflow;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class State extends BaseModel
{
    use SoftDeletes;

    protected $table = 'workflows_states';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function transitionsFrom(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Workflow\Transition', 'workflow_state_from_id');
    }

    public function transitionsTo(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Workflow\Transition', 'workflow_state_to_id');
    }

    public function workflow(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Workflow\Workflow', 'workflow_id');
    }

    public function scopeDefault(\Illuminate\Database\Eloquent\Builder $query): \Illuminate\Database\Eloquent\Builder
    {
        return $query->where('is_default', 1);
    }
}
