<?php

namespace App\Models\Workflow;

use App\Models\BaseModel;
use App\Services\TenantManager;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['attachment_path', 'user_id', 'description'];
    protected $table = 'comments';
    public $timestamps = true;

    protected $dates = ['deleted_at'];

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function workflight(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Workflow\Workflight', 'workflight_id');
    }

    public function getWebPathAttribute(): string
    {
        return '/storage/tenant_'.TENANT_ID.'/'.$this->attachment_path;
    }
}
