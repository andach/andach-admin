<?php

namespace App\Models\Workflow;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkflightAudit extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['assigned_to_user_id', 'instigating_user_id', 'state_id', 'from_datetime', 'to_datetime'];
    protected $table = 'workflights_audit';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function assignedUser(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'assigned_to_user_id');
    }

    public function instigatingUser(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'instigating_user_id');
    }

    public function state(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Workflow\State', 'state_id');
    }

    public function workflight(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Workflow\Workflight', 'workflight_id');
    }
}
