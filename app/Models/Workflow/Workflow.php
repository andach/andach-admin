<?php

namespace App\Models\Workflow;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Workflow extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['name', 'model_name', 'max_location_level', 'min_location_level'];
    protected $table = 'workflows';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function company(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    public function states(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Workflow\State', 'workflow_id');
    }

    public function transitions(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Workflow\Transition', 'workflow_id');
    }

    public function workflights(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Workflow\Workflight', 'workflow_id');
    }

    public function setDefaultCloseState(?int $stateID = null): void
    {
        if (!$stateID)
        {
            return;
        }

        $this->states()->update(['default_instant_close_status' => 0]);
        $this->states()->where('id', $stateID)->update(['default_instant_close_status' => 1]);
    }

    public function setDefaultState(?int $stateID = null): void
    {
        if (!$stateID)
        {
            return;
        }

        $this->states()->update(['is_default' => 0]);
        $this->states()->where('id', $stateID)->update(['is_default' => 1]);
    }
}
