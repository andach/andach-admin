<?php

namespace App\Models\UDF;

use Form;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class UDF extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['name', 'display_name', 'type', 'model_name', 'description'];
    protected $table = 'user_defined_fields';
    public $timestamps = true;

    protected $dates = ['deleted_at'];

    public function items(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\UDF\UDFItem', 'user_defined_field_id');
    }

    public function values(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\UDF\UDFValue', 'user_defined_field_id');
    }

    /**
     * Return the form object for use in a blade template.
     */
    public function formItem(?string $value = null): string
    {
        switch ($this->type)
        {
            case 'date':
                return Form::bsDate($this->display_name, $this->name, $value);
            break;

            case 'integer':
                return Form::bsNumber($this->display_name, $this->name, $value);
            break;

            case 'float':
            case 'string':
                return Form::bsText($this->display_name, $this->name, $value);
            break;

            case 'text':
                return Form::bsTextarea($this->display_name, $this->name, $value);
            break;

            case 'list':
                return Form::bsSelect($this->display_name, $this->name, $this->formSelectOptions(), $value, ['placeholder' => '']);
            break;
        }

        return '';
    }

    /**
     * If this is a 'list' item, returns an array of possible options. Else returns an empty array.
     */
    public function formSelectOptions(): array
    {
        return $this->items()->pluck('name', 'id')->toArray();
    }
}
