<?php

namespace App\Models\UDF;

use Form;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class UDFValue extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['user_defined_field_id'];
    protected $table = 'user_defined_fields_values';
    public $timestamps = true;

    protected $dates = ['deleted_at'];

    public function fieldable(): \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo();
    }

    public function item(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\UDF\UDFItem', 'user_defined_field_item_id');
    }

    public function udf(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\UDF\UDF', 'user_defined_field_id');
    }

    /**
     * Return the form object for use in a blade template.
     */
    public function formItem(): string
    {
        switch ($this->udf->type)
        {
            case 'date':
                return Form::bsDate($this->udf->display_name, $this->udf->name, $this->value_date, ['placeholder' => 'Enter a date in the form YYYY-MM-DD, and time, if needed, in hh:mm:ss format.']);
            break;

            case 'integer':
                return Form::bsNumber($this->udf->display_name, $this->udf->name, $this->value_integer);
            break;

            case 'string':
                return Form::bsText($this->udf->display_name, $this->udf->name, $this->value_string);
            break;

            case 'float':
                return Form::bsText($this->udf->display_name, $this->udf->name, $this->value_float);
            break;

            case 'text':
                return Form::bsTextarea($this->udf->display_name, $this->udf->name, $this->value_text);
            break;

            case 'list':
                return Form::bsSelect($this->udf->display_name, $this->udf->name, $this->udf->formSelectOptions(), $this->user_defined_field_item_id);
            break;
        }

        return '';
    }

    public function getValueAttribute(): string
    {
        switch ($this->udf->type)
        {
            case 'date':
                return $this->value_date ?? '';
                break;

            case 'float':
                return $this->value_float ?? '';
                break;

            case 'integer':
                return $this->value_integer ?? '';
                break;

            case 'list':
                if ($this->item)
                {
                    return $this->item->name;
                }

                return '';
                break;

            case 'string':
                return $this->value_string ?? '';
                break;

            case 'text':
                return $this->value_text ?? '';
                break;
        }

        return '';
    }

    public function updateValue(string $newValue): void
    {
        if (!$newValue)
        {
            $newValue = null;
        }

        switch ($this->udf->type)
        {
            case 'date':
                $this->value_date = $newValue;
                break;

            case 'float':
                $this->value_float = $newValue;
                break;

            case 'integer':
                $this->value_integer = $newValue;
                break;

            case 'list':
                $this->user_defined_field_item_id = $newValue;
                break;

            case 'string':
                $this->value_string = $newValue;
                break;

            case 'text':
                $this->value_text = $newValue;
                break;
        }

        $this->save();
    }
}
