<?php

namespace App\Models\UDF;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class UDFItem extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['user_defined_field_id', 'name'];
    protected $table = 'user_defined_fields_items';
    public $timestamps = true;

    protected $dates = ['deleted_at'];

    public function udf(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\UDF\UDF', 'user_defined_field_id');
    }

    public function values(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\UDF\UDFValue', 'user_defined_field_item_id');
    }
}
