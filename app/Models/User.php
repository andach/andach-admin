<?php

namespace App\Models;

use App\Models\Ticket;
use Hash;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->connection = 'tenant';

        if (app()->environment() === 'testing') {
            $this->connection = config('testing_tenant');
        }
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'first_name', 'middle_names', 'last_name', 'preferred_name', 'email', 'company_id', 'is_staff', 'is_customer', 'is_supplier', 'address_1', 'address_2', 'address_3', 'address_4', 'county_id', 'postcode', 'mobile_number', 'phone_number', 'enum_gender', 'ethnic_origin_id', 'religion_id', 'sexual_orientation_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function appointments(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\HR\Appointment', 'user_id');
    }

    public function company(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    public function companyUsers(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\CompanyUser', 'user_id');
    }

    public function comments(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Workflow\Comment', 'user_id');
    }

    public function companies(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany('App\Models\Company', 'link_companies_users', 'user_id', 'company_id')->withPivot('id', 'model_name', 'permission');
    }

    public function county(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Reference\County', 'county_id');
    }

    public function ethnicOrigin(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Reference\EthnicOrigin', 'ethnic_origin_id');
    }

    public function healthAndSafetyIncidents(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\HealthAndSafety\Incident', 'user_id');
    }

    public function healthAndSafetyInjuries(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\HealthAndSafety\Injury', 'user_id');
    }

    public function messagesReceived(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany('App\Models\Message\Message', 'link_messages_users', 'user_id', 'message_id')
            ->withPivot('is_read', 'is_deleted', 'is_starred');
    }

    public function messagesSent(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Message\Message', 'user_id');
    }

    public function openedComplaints(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Complaint\Complaint', 'opened_by_user_id');
    }

    public function ownedLocations(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Location\Location', 'owned_by_user_id');
    }

    public function religion(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Reference\Religion', 'religion_id');
    }

    public function resident(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne('App\Models\CareHome\Resident');
    }

    public function roles(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany('App\Models\Role', 'link_roles_users', 'user_id', 'role_id');
    }

    public function sexualOrientation(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Reference\SexualOrientation', 'sexual_orientation_id');
    }

    public function rentals(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Rental\Rental', 'user_id');
    }

    /**
     * Gives all the tickets the user owns as a customer.
     */
    public function tickets(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Ticket\Ticket', 'customer_id');
    }

    /**
     * Gives all the tickets the user has opened.
     */
    public function ticketsOpened(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Ticket\Ticket', 'user_id');
    }

    /**
     * Returns all roles this user can access, either directly or through their job/position/appointment. Returns a collection of roles.
     */
    public function allRoles(): \Illuminate\Support\Collection
    {
        $roleIDs = $this->roles()->pluck('roles.id')->toArray();

        $appointments = $this->appointments()->current()->get();
        foreach ($appointments as $appointment)
        {
            $roleIDs = array_merge($roleIDs, $appointment->position->job->roles->pluck('id')->toArray());
        }

        return Role::whereIn('id', $roleIDs)->with('rolePermissions')->get();
    }

    public function getAddressAttribute(): string
    {
        $data = [
            $this->address_1,
            $this->address_2,
            $this->address_3,
            $this->address_4,
            $this->county_string,
            $this->postcode,
        ];

        $return = [];

        foreach ($data as $line)
        {
            if ($line)
            {
                $return[] = $line;
            }
        }

        return implode('<br />', $return);
    }

    public function getCountyStringAttribute(): string
    {
        if ($this->county)
        {
            return $this->county->name;
        }

        return '';
    }

    public function getEthnicOriginAttribute(): string
    {
        if ($this->ethnicOrigin)
        {
            return $this->ethnicOrigin->name;
        }

        return '';
    }

    public function getFullContactDetailsAttribute(): string
    {
        $data = [
            $this->address_1,
            $this->address_2,
            $this->address_3,
            $this->address_4,
            $this->county_string,
            $this->postcode,
            $this->email,
            $this->phone_number,
            $this->mobile_number,
        ];

        $arr = [];

        foreach ($data as $line)
        {
            if ($line)
            {
                $arr[] = $line;
            }
        }

        return implode('<br />', $arr);
    }

    public function getPictureUrlAttribute(): string
    {
        if ($this->picture_path)
        {
            return '/storage/tenant_'.TENANT_ID.'/'.$this->picture_path;
        }

        return '/img/user.svg';
    }

    public function getPrintOpenWorkflowObjectsAttribute(): string
    {
        return 'FUNCTIONALITY TO BE WRITTEN';
    }

    public function getPrintStaffCustomerSupplierAttribute(): string
    {
        $return = [];

        if ($this->is_staff)
        {
            $return[] = 'Staff';
        }

        if ($this->is_customer)
        {
            $return[] = 'Customer';
        }

        if ($this->is_supplier)
        {
            $return[] = 'Supplier';
        }

        return implode(' / ', $return);
    }

    public function getReligionAttribute(): string
    {
        if ($this->religion)
        {
            return $this->religion->name;
        }

        return '';
    }

    public function getSexualOrientationAttribute(): string
    {
        if ($this->sexualOrientation)
        {
            return $this->sexualOrientation->name;
        }

        return '';
    }

    public static function scopeIsCustomer(\Illuminate\Database\Eloquent\Builder $query): \Illuminate\Database\Eloquent\Builder
    {
        return $query->where('is_customer', 1);
    }

    public static function scopeIsStaff(\Illuminate\Database\Eloquent\Builder $query): \Illuminate\Database\Eloquent\Builder
    {
        return $query->where('is_staff', 1);
    }

    public static function scopeIsSupplier(\Illuminate\Database\Eloquent\Builder $query): \Illuminate\Database\Eloquent\Builder
    {
        return $query->where('is_supplier', 1);
    }

    public function updatePassword(?string $newPassword = null): void
    {
        if ($newPassword)
        {
            $this->password = Hash::make($newPassword);
            $this->save();
        }
    }

    public function updatePicture(?\Illuminate\Http\UploadedFile $attachment = null): void
    {
        if (!$attachment)
        {
            return;
        }

        $this->picture_path = $attachment->store('users', 'public');
        $this->save();
    }
}
