<?php

namespace App\Models\Time;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shift extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['appointment_id', 'job_id', 'position_id', 'team_id', 'start_datetime', 'end_datetime', 'minutes_worked', 'minutes_break'];
    protected $table = 'shifts';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function appointment(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\HR\Appointment', 'appointment_id');
    }

    public function category(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Time\Category', 'category_id');
    }

    public function job(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\HR\Job', 'job_id');
    }

    public function position(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\HR\Position', 'position_id');
    }

    public function team(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\HR\Team', 'team_id');
    }

    public function timesheets(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Time\Timesheet', 'shift_id');
    }
}
