<?php

namespace App\Models\Time;

use App\Traits\ApprovedByAppointment;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Holiday extends BaseModel
{
    use ApprovedByAppointment;
    use SoftDeletes;

    protected $fillable = ['appointment_id', 'from_datetime', 'to_datetime', 'days_off', 'hours_off', 'is_approved', 'is_rejected', 'approved_by_appointment_id', 'approved_datetime'];
    protected $table = 'holidays';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function appointment(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\HR\Appointment', 'appointment_id');
    }
}
