<?php

namespace App\Models\Time;

use App\Traits\ApprovedByAppointment;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Timesheet extends BaseModel
{
    use ApprovedByAppointment;
    use SoftDeletes;

    protected $fillable = ['appointment_id', 'absence_id', 'category_id', 'project_task_id', 'process_id', 'mins_logged', 'date_of_timesheet',
        'description'];
    protected $table = 'timesheets';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function absence(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Time\Absence', 'absence_id');
    }

    public function appointment(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\HR\Appointment', 'appointment_id');
    }

    public function category(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Time\Category', 'category_id');
    }

    public function job(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\HR\Job', 'job_id');
    }

    public function payslip(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Payslip\Payslip', 'payslip_id');
    }

    public function position(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\HR\Position', 'position_id');
    }

    public function team(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\HR\Team', 'team_id');
    }

    /**
     * Approves or rejects this timesheet based on whether the supplied boolean is true or false.
     */
    public function approveOrReject(bool $boo, int $appointmentID): void
    {
        $this->approved_by_appointment_id = $appointmentID;
        $this->approved_datetime          = date('Y-m-d h:i:s');

        if ($boo)
        {
            $this->is_approved = 1;
        } else {
            $this->is_rejected = 1;
        }

        $this->save();
    }

    public function getApprovedTickCrossAttribute(): string
    {
        if ($this->is_approved)
        {
            return '<img src="/img/tick.svg" height="32px" />';
        } elseif ($this->is_rejected) {
            return '<img src="/img/cross.svg" height="32px" />';
        }

        return '<img src="/img/hourglass.svg" height="32px" />';
    }

    public static function scopeNeedsAuthorising(\Illuminate\Database\Eloquent\Builder $query): \Illuminate\Database\Eloquent\Builder
    {
        return $query->where('is_approved', 0)
            ->where('is_rejected', 0)
            ->whereHas('job', function (\Illuminate\Database\Eloquent\Builder $query): void {
                $query->where('is_positively_paid', '1');
            });
    }
}
