<?php

namespace App\Models\Time;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Absence extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['name'];
    protected $table = 'timesheets_absence';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function timesheets(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Time\Timesheet', 'absence_id');
    }
}
