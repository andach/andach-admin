<?php

namespace App\Models\Reference;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class SexualOrientation extends BaseModel
{
    use SoftDeletes;

    protected $table = 'sexual_orientations';
    public $timestamps = true;

    protected $dates = ['deleted_at'];

    public function users(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\User', 'sexual_orientation_id');
    }
}
