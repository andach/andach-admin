<?php

namespace App\Models\Reference;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class EthnicOrigin extends BaseModel
{
    use SoftDeletes;

    protected $table = 'ethnic_origins';
    public $timestamps = true;

    protected $dates = ['deleted_at'];

    public function users(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\User', 'ethnic_origin_id');
    }
}
