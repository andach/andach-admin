<?php

namespace App\Models\Reference;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class County extends BaseModel
{
    use SoftDeletes;

    protected $table = 'counties';
    public $timestamps = true;

    protected $dates = ['deleted_at'];

    public function users(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\User', 'county_id');
    }
}
