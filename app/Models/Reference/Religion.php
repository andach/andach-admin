<?php

namespace App\Models\Reference;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Religion extends BaseModel
{
    use SoftDeletes;

    protected $table = 'religions';
    public $timestamps = true;

    protected $dates = ['deleted_at'];

    public function users(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\User', 'religion_id');
    }
}
