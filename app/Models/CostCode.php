<?php

namespace App\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class CostCode extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['name', 'code', 'parent_id'];
    protected $table = 'cost_codes';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function children(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\CostCode', 'parent_id');
    }

    public function parent(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\CostCode', 'parent_id');
    }

    public function positions(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\HR\Position', 'cost_code_id');
    }

    public function getParentNameAttribute(): string
    {
        if ($this->parent)
        {
            return $this->parent->name;
        }

        return '';
    }
}
