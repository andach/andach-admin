<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chart extends Model
{
    public $name;
    public $type;
    public $labels;
    public $datasets;
    public $dataOptions;
    public $options;
    public $colorScheme;

    public function __construct(string $name, string $type = 'bar', array $labels = [], array $datasets = [], array $dataOptions = [], array $options = [])
    {
        $this->name        = $name;
        $this->type        = $type;
        $this->labels      = $labels;
        $this->datasets    = $datasets;
        $this->dataOptions = $dataOptions;
        $this->options     = $options;
    }

    public function addDatasetByColumn(\Illuminate\Database\Eloquent\Collection $collection, string $title, string $columnName): void
    {
        $data = array_count_values($collection->pluck($columnName)->toArray());

        $dataset = [];
        $dataset['label'] = $title;
        $dataset['data']  = array_values($data);

        $this->labels = array_keys($data);

        $this->addManualDataset($dataset);
    }

    public function addDatasetByDates(\Illuminate\Database\Eloquent\Collection $collection, string $title, string $displayBy): void
    {
        $data = array_count_values($collection->pluck('created_at')
        ->transform(function ($item) {
            return date($displayBy, strtotime($item));
        })
        ->toArray());

        $dataset = [];
        $dataset['label'] = $title;
        $dataset['data']  = array_values($data);

        $this->labels = array_keys($data);

        $this->addManualDataset($dataset);
    }

    public function addManualDataset(array $dataset): void
    {
        $this->datasets[] = $dataset;
    }

    private function backgroundColors(array $dataset): array
    {
        $return = [];

        $countDataset     = count($dataset);
        $countColorScheme = count($this->colorScheme);

        if ($this->colorScheme)
        {
            for ($i = 0; $i < $countDataset; $i++)
            {
                $arrayIndex = $i % $countColorScheme;

                $return[] = 'rgba('.$this->colorScheme[$arrayIndex].', .8)';
            }

            // dd($return);

            return $return;
        }

        return array_fill(0, $countDataset, 'rgba(1, 83, 121, .8)');
    }

    private function borderColors(array $dataset): array
    {
        $return = [];

        $countDataset     = count($dataset);
        $countColorScheme = count($this->colorScheme);

        if ($this->colorScheme)
        {
            for ($i = 0; $i < $countDataset; $i++)
            {
                $arrayIndex = $i % $countColorScheme;

                $return[] = 'rgba('.$this->colorScheme[$arrayIndex].', 1)';
            }



            return $return;
        }

        return array_fill(0, $countDataset, 'rgba(1, 83, 121, 1)');
    }

    public function javascript(): string
    {
        // dd($this);

        // Generate a random variable name for this chart, a 20-character string of letters.
        $varName = substr(str_shuffle('abcdefghijklmnopqrstuvwxyz'), 1, 20);

        $return = "var ".$varName." = document.getElementById('".$this->name."').getContext('2d');
            var chart = new Chart(".$varName.", {
                // The type of chart we want to create
                type: '".$this->type."',

                // The data for our dataset
                data: ".self::javascriptData().",

                // Configuration options go here
                options: ".self::javascriptOptions()."
            });";

        return $return;
    }

    public function javascriptData(): string
    {
        $datasets = $this->javascriptDatasets();

        $obj = [
            'labels' => $this->labels,
            'datasets' => $datasets,
        ];

        return json_encode($obj);
    }

    public function javascriptDatasets(): array
    {
        $return    = [];
        $addToEach = [];

        foreach ($this->datasets as $dataset)
        {
            if ('bar' == $this->type)
            {
                $addToEach = [
                    'backgroundColor' => $this->backgroundColors($dataset['data']),
                    'borderColor' => $this->borderColors($dataset['data']),
                    'borderWidth' => 2,
                ];
            }

            $return[] = array_merge([
                'label' => $dataset['label'] ?? 'TEST LABEL',
                'data'  => $dataset['data'],
            ], $addToEach);
        }

        return $return;
    }

    public function javascriptOptions(): string
    {
        return json_encode($this->options);
    }

    public function setColorScheme(string $colorScheme): void
    {
        switch ($colorScheme)
        {
            case 'andach':
                $this->colorScheme = [
                    '1, 83, 121',
                    '114, 176, 29',
                    '246, 81, 29',
                    '191, 6, 3',
                    '52, 58, 64',
                ];
                break;

            case 'blackwhite':
                $this->colorScheme = [
                    '255, 255, 255',
                    '0, 0, 0'
                ];
                break;

            case 'rainbow':
                $this->colorScheme = [
                    '255, 0, 0',
                    '255, 127, 0',
                    '255, 255, 0',
                    '0, 255, 0',
                    '0, 0, 255',
                    '46, 43, 95',
                    '139, 0, 255',
                ];
                break;
        }
    }
}
