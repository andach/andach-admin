<?php

namespace App\Models;

use App\Models\BaseModel;

class CompanyUser extends BaseModel
{
    protected $fillable = ['user_id', 'company_id', 'model_name', 'permission'];
    protected $table = 'link_companies_users';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function company(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
