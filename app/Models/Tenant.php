<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Tenant extends Model
{
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->connection = 'mysql';

        if (app()->environment() === 'testing') {
            $this->connection = config('testing_tenant');
        }
    }

    public function createTenantDatabase(Request $request): void
    {
        \Artisan::call('tenants:create '.$this->id);
        \Artisan::call('tenants:migrate');
        \DB::insert('insert into tenant_'.$this->id.'.companies (name, parent_id) values (?, ?)', [
            $request->company,
            0,
        ]);
        \DB::insert('insert into tenant_'.$this->id.'.locations (name, _lft, _rgt) values (?, ?, ?)', [
            $request->company,
            1,
            2,
        ]);
        \DB::insert('insert into tenant_'.$this->id.'.users (company_id, name, email, email_verified_at, password, is_staff, is_super_admin) values (?, ?, ?, ?, ?, ?, ?)', [
            1,
            $request->name,
            $request->email,
            date('Y-m-d h:i:s'),
            \Hash::make($request->password),
            1,
            1,
        ]);

        // Create three default workflows.
        \DB::insert('insert into tenant_'.$this->id.'.workflows (name, model_name, min_location_level, max_location_level) values (?, ?, ?, ?)', [
            'Complaints',
            'App\Models\Complaint\Complaint',
            0,
            1,
        ]);
        \DB::insert('insert into tenant_'.$this->id.'.workflows (name, model_name, min_location_level, max_location_level) values (?, ?, ?, ?)', [
            'Tickets',
            'App\Models\Ticket\Ticket',
            0,
            1,
        ]);
        \DB::insert('insert into tenant_'.$this->id.'.workflows (name, model_name, min_location_level, max_location_level) values (?, ?, ?, ?)', [
            'Health and Safety Investigation',
            'App\Models\HealthAndSafety\Incident',
            0,
            1,
        ]);

        // Create the necessary workflow states
        \DB::insert('insert into tenant_'.$this->id.'.workflows_states (workflow_id, name, is_default, is_closed, default_instant_close_status) values (?, ?, ?, ?, ?)', [
            1, 'Opened', 1, 0, 0,
        ]);
        \DB::insert('insert into tenant_'.$this->id.'.workflows_states (workflow_id, name, is_default, is_closed, default_instant_close_status) values (?, ?, ?, ?, ?)', [
            1, 'Closed', 0, 1, 1,
        ]);
        \DB::insert('insert into tenant_'.$this->id.'.workflows_states (workflow_id, name, is_default, is_closed, default_instant_close_status) values (?, ?, ?, ?, ?)', [
            2, 'Opened', 1, 0, 0,
        ]);
        \DB::insert('insert into tenant_'.$this->id.'.workflows_states (workflow_id, name, is_default, is_closed, default_instant_close_status) values (?, ?, ?, ?, ?)', [
            2, 'Closed', 0, 1, 1,
        ]);
        \DB::insert('insert into tenant_'.$this->id.'.workflows_states (workflow_id, name, is_default, is_closed, default_instant_close_status) values (?, ?, ?, ?, ?)', [
            3, 'Opened', 1, 0, 0,
        ]);
        \DB::insert('insert into tenant_'.$this->id.'.workflows_states (workflow_id, name, is_default, is_closed, default_instant_close_status) values (?, ?, ?, ?, ?)', [
            3, 'Closed', 0, 1, 1,
        ]);
        \DB::insert('insert into tenant_'.$this->id.'.workflows_states (workflow_id, name, is_default, is_closed, default_instant_close_status) values (?, ?, ?, ?, ?)', [
            3, 'Regulator Report Required', 0, 0, 0,
        ]);

        // Create the workflow transitions
        \DB::insert('insert into tenant_'.$this->id.'.workflows_transitions (workflow_id, name, workflow_state_from_id, workflow_state_to_id) values (?, ?, ?, ?)', [
            1, 'Close Complaint', 1, 2,
        ]);
        \DB::insert('insert into tenant_'.$this->id.'.workflows_transitions (workflow_id, name, workflow_state_from_id, workflow_state_to_id) values (?, ?, ?, ?)', [
            1, 'Re-Open Complaint', 2, 1,
        ]);
        \DB::insert('insert into tenant_'.$this->id.'.workflows_transitions (workflow_id, name, workflow_state_from_id, workflow_state_to_id) values (?, ?, ?, ?)', [
            2, 'Close Ticket', 3, 4,
        ]);
        \DB::insert('insert into tenant_'.$this->id.'.workflows_transitions (workflow_id, name, workflow_state_from_id, workflow_state_to_id) values (?, ?, ?, ?)', [
            2, 'Re-Open Ticket', 4, 3,
        ]);
        \DB::insert('insert into tenant_'.$this->id.'.workflows_transitions (workflow_id, name, workflow_state_from_id, workflow_state_to_id) values (?, ?, ?, ?)', [
            3, 'Close Incident', 5, 6,
        ]);
        \DB::insert('insert into tenant_'.$this->id.'.workflows_transitions (workflow_id, name, workflow_state_from_id, workflow_state_to_id) values (?, ?, ?, ?)', [
            3, 'Re-Open Incident', 6, 5,
        ]);
        \DB::insert('insert into tenant_'.$this->id.'.workflows_transitions (workflow_id, name, workflow_state_from_id, workflow_state_to_id) values (?, ?, ?, ?)', [
            3, 'Incident Requires Regulator Report', 5, 7,
        ]);
        \DB::insert('insert into tenant_'.$this->id.'.workflows_transitions (workflow_id, name, workflow_state_from_id, workflow_state_to_id) values (?, ?, ?, ?)', [
            3, 'Regulator Report Completed (Close)', 7, 6,
        ]);
        \DB::insert('insert into tenant_'.$this->id.'.workflows_transitions (workflow_id, name, workflow_state_from_id, workflow_state_to_id) values (?, ?, ?, ?)', [
            3, 'Regulator Report Unnecessary (Open)', 7, 5,
        ]);
        \DB::insert('insert into tenant_'.$this->id.'.workflows_transitions (workflow_id, name, workflow_state_from_id, workflow_state_to_id) values (?, ?, ?, ?)', [
            3, 'Regulator Report Unnecessary (Close)', 7, 6,
        ]);
    }

    public function route(string $name, array $parameters = []): string
    {
        $host = $this->domain ?? $this->subdomain;
        return 'https://' . $host . app('url')->route($name, $parameters, false);
    }
}
