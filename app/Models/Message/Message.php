<?php

namespace App\Models\Message;

use App\Traits\PrimaryKeyUuid;
use Auth;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends BaseModel
{
    use PrimaryKeyUuid;
    use SoftDeletes;

    protected $fillable = ['user_id', 'name', 'description'];

    public function attachments(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Message\Attachment', 'message_id');
    }

    public function fromUser(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function toUsers(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany('App\Models\User', 'link_messages_users', 'message_id', 'user_id')
            ->withPivot('is_read', 'is_deleted', 'is_starred');
    }

    /**
     * Returns the $args['folders'] array suitable for passing directly to
     * /resources/views/component/message/folders.blade.php
     */
    public static function folders(): array
    {
        return [
            [
                'name'   => 'Inbox',
                'icon'   => 'fas fa-inbox',
                'route'  => 'message.inbox',
                'unread' => Auth::user()
                    ->messagesReceived()
                    ->wherePivot('is_read', 0)
                    ->wherePivot('is_deleted', 0)
                    ->count(),
            ],
            [
                'name'   => 'Outbox',
                'icon'   => 'far fa-envelope',
                'route'  => 'message.outbox',
            ],
            [
                'name'   => 'Bin',
                'icon'   => 'far fa-trash-alt',
                'route'  => 'message.bin',
                'unread' => Auth::user()
                    ->messagesReceived()
                    ->wherePivot('is_read', 0)
                    ->wherePivot('is_deleted', 1)
                    ->count(),
            ],
        ];
    }

    public function getDescriptionEllipsisAttribute(): string
    {
        return strlen($this->description) > 50 ? substr($this->description,0,50)."..." : $this->description;
    }

    public function getSenderNameAttribute(): string
    {
        if ($this->fromUser)
        {
            return $this->fromUser->name;
        }

        return $this->from_name;
    }

    public function getSenderNameWithLinkAttribute(): string
    {
        if ($this->fromUser)
        {
            return '<a href="'.route('message.create', $this->user_id).'">'.$this->fromUser->name.'</a>';
        }

        return $this->from_name;
    }

    public function getSentAgoAttribute(): string
    {
        if ($this->created_at)
        {
            return $this->created_at->diffForHumans();
        }

        return '';
    }

    public function isReadForUser(?int $userID = null): bool
    {
        if (!$userID)
        {
            $userID = Auth::id();
        }

        $userLink = $this->toUsers()->where('users.id', $userID)->first();
        return true && $userLink->pivot->is_read;
    }

    public function markAsRead(?int $userID = null): void
    {
        if (!$userID)
        {
            $userID = Auth::id();
        }

        $this->toUsers()->updateExistingPivot($userID, ['is_read' => 1]);
    }

    public function markAsUnread(?int $userID = null): void
    {
        if (!$userID)
        {
            $userID = Auth::id();
        }

        $this->toUsers()->updateExistingPivot($userID, ['is_read' => 0]);
    }
}
