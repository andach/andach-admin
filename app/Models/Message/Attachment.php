<?php

namespace App\Models\Message;

use App\Traits\PrimaryKeyUuid;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attachment extends BaseModel
{
    use PrimaryKeyUuid;
    use SoftDeletes;

    protected $fillable = ['message_id', 'name', 'path'];

    public function message(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Message\Message', 'message_id');
    }
}
