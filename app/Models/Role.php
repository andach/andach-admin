<?php

namespace App\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends BaseModel
{
    use SoftDeletes;

    protected $table = 'roles';
    public $timestamps = true;
    protected $fillable = ['name'];
    protected $dates = ['deleted_at'];

    public function companies(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany('App\Models\Company', 'link_companies_roles', 'role_id', 'company_id');
    }

    public function rolePermissions(): \Illuminate\Database\Eloquent\Relations\hasMany
    {
        return $this->hasMany('App\Models\RolePermission', 'role_id');
    }

    public function transitions(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany('App\Models\Workflow\Transition', 'link_roles_workflows_transitions', 'role_id', 'workflow_transition_id');
    }

    public function users(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany('App\Models\User', 'link_roles_users', 'role_id', 'user_id');
    }

    public static function canAccess(\Illuminate\Database\Eloquent\Collection $roleCollection, string $accessTo): bool
    {
        foreach ($roleCollection as $role)
        {
            if (in_array($accessTo, $role->rolePermissions->pluck('access_to')->toArray()))
            {
                return true;
            }
        }

        return false;
    }

    // Updates the rolePermissions associated with this role based on an array like ['complaint.read', 'user.edit', etc.]
    public function updateRolePermissions(array $array): void
    {
        if (!$array)
        {
            $array = [];
        }

        $existingPermissions = $this->rolePermissions;

        $deleteIDs = [];
        foreach ($existingPermissions as $exPerm)
        {
            if (!in_array($exPerm->access_to, $array))
            {
                $deleteIDs = $exPerm->id;
            }
        }
        $this->rolePermissions->each->delete($deleteIDs);

        foreach ($array as $newPermission)
        {
            $this->rolePermissions()->firstOrCreate(['access_to' => $newPermission]);
        }
    }
}
