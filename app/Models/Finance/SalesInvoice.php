<?php

namespace App\Models\Finance;

use App\Models\BaseModel;
use App\Traits\IsInvoice;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalesInvoice extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['name', 'user_id', 'invoice_date', 'supply_date', 'due_date', 'terms_of_payment', 'total_net', 'total_vat', 'total_gross'];
    protected $table = 'sales_invoices';
    public $timestamps = true;

    protected $dates = ['deleted_at'];

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
