<?php

namespace App\Models\Finance;

use App\Models\BaseModel;
use App\Traits\IsInvoiceLine;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalesInvoiceLine extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['description', 'thing_being_invoiced_id', 'thing_being_invoiced_type', 'net', 'vat', 'gross'];
    protected $table = 'sales_invoices_lines';
    public $timestamps = true;

    protected $dates = ['deleted_at'];

    public function thingBeingInvoiced(): \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo();
    }
}
