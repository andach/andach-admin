<?php

namespace App\Models\Location;

use App\Models\Location\LocationHierarchy;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kalnoy\Nestedset\NodeTrait;

class Location extends BaseModel
{
    use NodeTrait;
    use SoftDeletes;

    protected $table = 'locations';
    public $timestamps = true;
    protected $fillable = ['name', 'parent_id', 'level', 'is_rentable'];
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be visible in the JSON export.
     *
     * @var array
     */
    protected $visible = ['id', 'name', 'children', 'is_rentable'];

    public function complaints(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Complaint\Complaint', 'location_id');
    }

    public function healthAndSafetyIncidents(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\HealthAndSafety\Incident', 'location_id');
    }

    public function rentals(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany('App\Models\Rental\Rental', 'rented_thing');
    }

    public function tickets(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Ticket\Ticket', 'location_id');
    }

    public function users(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany('App\Models\User', 'link_locations_users', 'location_id', 'user_id')->withPivot('model_name');
    }

    // Returns the name of this location with the current renter (or one of the current renters), and the end date, if it exists.
    public function getNameWithRenterAttribute(): string
    {
        $rental = $this->rentals()->current()->first();
        $append = '';

        if ($rental)
        {
            $append .= ' ('.$rental->user->name;

            if ($rental->to_date)
            {
                $append .= ' ends: '.$rental->to_date;
            } else {
                $append .= ' with no end date';
            }

            $append .= ')';
        }

        return $this->name.$append;
    }

    public function scopeRentable(\Illuminate\Database\Eloquent\Builder $query): \Illuminate\Database\Eloquent\Builder
    {
        return $query->where('is_rentable', 1);
    }
}
