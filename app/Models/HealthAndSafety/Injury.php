<?php

namespace App\Models\HealthAndSafety;

use App\Enums\HealthAndSafety\BodyPart;
use App\Models\BaseModel;
use DateTime;
use Illuminate\Database\Eloquent\SoftDeletes;

class Injury extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['enum_body_parts', 'incident_id', 'user_id', 'injured_person_name', 'injured_person_details', 'incapacitated_from_date', 'returned_to_light_duties_date', 'returned_fully_date'];
    protected $table = 'health_and_safety_injuries';
    public $timestamps = true;

    protected $dates = ['deleted_at'];

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function incident(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\HealthAndSafety\Incident', 'incident_id');
    }

    public function bodyParts(): array
    {
        return explode(',', $this->enum_body_parts);
    }

    public function bodyPartsReadable(): array
    {
        $return = [];

        $bodyParts = explode(',', $this->enum_body_parts);

        foreach ($bodyParts as $key)
        {
            $return[] = BodyPart::$key();
        }

        return $return;
    }

    public function daysOffWork(): int
    {
        $start = new DateTime();
        $end   = new DateTime();

        if ($this->incapacitated_from_date)
        {
            $start = new DateTime($this->incapacitated_from_date);

            if ($this->returned_fully_date)
            {
                $end = new DateTime($this->returned_fully_date);
            }
        }

        return $end->diff($start)->format("%a");
    }

    public function daysOnLightDuties(): int
    {
        $start = new DateTime();
        $end   = new DateTime();

        if ($this->returned_to_light_duties_date)
        {
            $start = new DateTime($this->returned_to_light_duties_date);

            if ($this->returned_fully_date)
            {
                $end = new DateTime($this->returned_fully_date);
            }
        }

        return $end->diff($start)->format("%a");
    }

    public function getNameAttribute(): string
    {
        if ($this->user)
        {
            return $this->user->name;
        }

        return $this->injured_person_name ?? '';
    }

    public function getOffWorkNotesAttribute(): string
    {
        $daysOffWork = $this->daysOffWork();
        $daysOnLight = $this->daysOnLightDuties();
        $return      = '';

        if ($daysOffWork or $daysOnLight)
        {
            $return .= 'Off Work: '.$daysOffWork.' day(s)';

            if ($daysOnLight)
            {
                $return .= 'Of which '.$daysOnLight.' day(s) on light duties';
            }
        }

        return $return;
    }

    public function getWhereInjuredAttribute(): string
    {
        return implode(', ', $this->bodyPartsReadable());
    }
}
