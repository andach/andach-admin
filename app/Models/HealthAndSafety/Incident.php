<?php

namespace App\Models\HealthAndSafety;

use App\Traits\AccessLimitedByCompany;
use App\Traits\CanSearch;
use App\Traits\IsCloseable;
use App\Traits\HasCharts;
use App\Traits\HasWorkflight;
use App\Traits\HasUDFs;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Incident extends BaseModel
{
    use AccessLimitedByCompany;
    use CanSearch;
    use IsCloseable;
    use HasCharts;
    use HasWorkflight;
    use HasUDFs;
    use SoftDeletes;

    protected $fillable = ['name', 'company_id', 'location_id', 'user_id', 'description', 'enum_kind_of_accident', 'enum_work_process', 'enum_main_factor', 'is_closed', 'is_riddor_reportable', 'riddor_report_date', 'riddor_report_reference'];
    protected $table = 'health_and_safety_incidents';
    public $timestamps = true;

    protected $dates = ['deleted_at'];

    public function injuries(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\HealthAndSafety\Injury', 'incident_id');
    }

    public function location(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Location\Location', 'location_id');
    }

    public function workflight(): \Illuminate\Database\Eloquent\Relations\MorphOne
    {
        return $this->morphOne('App\Models\Workflow\Workflight', 'workflowable');
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
