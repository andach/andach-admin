<?php

namespace App\Models\Ticket;

use App\Traits\AccessLimitedByCompany;
use App\Traits\CanSearch;
use App\Traits\IsCloseable;
use App\Traits\HasCharts;
use App\Traits\HasWorkflight;
use App\Traits\HasUDFs;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends BaseModel
{
    use AccessLimitedByCompany;
    use CanSearch;
    use IsCloseable;
    use HasCharts;
    use HasWorkflight;
    use HasUDFs;
    use SoftDeletes;

    protected $fillable = ['name', 'description', 'company_id', 'customer_id', 'location_id', 'user_id'];
    protected $table = 'tickets';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    public function company(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    public function customer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'customer_id');
    }

    public function location(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Location\Location', 'location_id');
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function getCustomerNameAttribute(): string
    {
        if ($this->customer)
        {
            return $this->customer->name;
        }

        return '';
    }

    public function getLocationNameAttribute(): string
    {
        if ($this->location)
        {
            return $this->location->name;
        }

        return '';
    }
}
