<?php

namespace App\Services;

use App\Models\Tenant;

class TenantManager
{
    /*
     * @var null|App\Models\Tenant
     */
    private $tenant;

    public function setTenant(?Tenant $tenant): \App\Services\TenantManager
    {
        $this->tenant = $tenant;
        return $this;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function loadTenant(string $identifier): bool
    {
        if ('testing' === env('APP_ENV'))
        {
            $tenant = new Tenant();
            $tenant->subdomain = 'localhost';
            $this->setTenant($tenant);
            return true;
        }

        $tenant = Tenant::query()->where('subdomain' ? 'subdomain' : 'domain', '=', $identifier)->first();

        if ($tenant)
        {
            $this->setTenant($tenant);
            return true;
        }

        dd('no tenant has been set');

        return false;
    }
}
