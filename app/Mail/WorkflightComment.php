<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WorkflightComment extends Mailable
{
    use Queueable, SerializesModels;

    public $workflowable;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(object $workflowable)
    {
        $this->workflowable = $workflowable;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('example@example.com')
            ->view('email.workflow.comment');
    }
}
