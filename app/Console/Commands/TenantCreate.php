<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\TenantDatabase;
use App\Services\TenantManager;
use App\Models\Tenant;

class TenantCreate extends Command
{

    protected $signature = 'tenants:create {id}';

    protected $description = 'Create tenant databases';

    protected $tenantManager;

    protected $migrator;

    public function __construct(TenantManager $tenantManager)
    {
        parent::__construct();

        $this->tenantManager = $tenantManager;
        $this->migrator = app('migrator');
    }

    public function handle(): void
    {
        $tenant = Tenant::find($this->argument('id'));

        TenantDatabase::dispatch($tenant, app(TenantManager::class));
    }
}
