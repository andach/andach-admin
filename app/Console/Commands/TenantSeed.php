<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\TenantManager;
use App\Models\Tenant;

class TenantSeed extends Command
{

    protected $signature = 'tenants:seed {id}';

    protected $description = 'Seed tenant databases';

    protected $tenantManager;

    protected $migrator;

    public function __construct(TenantManager $tenantManager)
    {
        parent::__construct();

        $this->tenantManager = $tenantManager;
        $this->migrator = app('migrator');
    }

    public function handle(): void
    {
        if ($this->argument('id'))
        {
            $tenants = Tenant::where('id', $this->argument('id'))->get();
        }

        foreach ($tenants as $tenant) {
            $this->tenantManager->setTenant($tenant);
            \DB::purge('tenant');
            $this->seed();
        }
    }

    private function seed(): void
    {
        $this->prepareDatabase();

        \Artisan::call('db:seed');
    }

    protected function prepareDatabase(): void
    {
        $this->migrator->setConnection('tenant');
    }
}
