<?php

namespace App\Http\Controllers;

use App\Models\Workflow\State;
use App\Models\Workflow\Transition;
use App\Models\Workflow\Workflow;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\View\View;

class WorkflowController extends Controller
{
    public function create(): View
    {
        $args = [];

        return view('workflow.create', $args);
    }

    public function createPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'name'               => 'required',
            'model_name'         => 'required',
            'min_location_level' => 'required',
            'max_location_level' => 'required',
        ]);

        $workflow = Workflow::create($request->all());
        $workflow->save();

        session()->flash('success', 'The Workflow has been created.');

        return redirect()->route('workflow.edit', $workflow->id);
    }

    public function edit(int $id): View
    {
        $args = [];
        $args['workflow']  = Workflow::find($id);

        return view('workflow.edit', $args);
    }

    public function editPost(Request $request, int $id): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'name'                         => 'required',
            'min_location_level'           => 'required',
            'max_location_level'           => 'required',
            'new_transition_state_from_id' => 'required_with:new_transition_name',
            'new_transition_state_to_id'   => 'required_with:new_transition_name',
        ]);

        $workflow = Workflow::find($id);
        $workflow->name               = $request->name;
        $workflow->min_location_level = $request->min_location_level;
        $workflow->max_location_level = $request->max_location_level;
        $workflow->save();

        // Deal with all the changes to the states.
        if (isset($request->delete_states))
        {
            if (count($request->delete_states)) {
                State::destroy($request->delete_states);
            }
        }

        $workflow->setDefaultState($request->default_state_id);
        $workflow->setDefaultCloseState($request->default_instant_close_state_id);

        if ($request->new_state) {
            $state = new State();
            $state->workflow_id = $workflow->id;
            $state->name        = $request->new_state;
            $state->is_closed   = $request->new_state_is_closed ?? 0;
            $state->save();

            if ($request->new_state_is_default) {
                $workflow->setDefaultState($state->id);
            }
        }

        // Now deal with any changes to the transitions. Do the editing first, then the deleting. It's irrelevant which order they happen in,
        // and doing them this way just reduces the change of errors.
        if ($request->transition_name)
        {
            foreach ($request->transition_name as $transitionID => $name) {
                $transition = Transition::find($transitionID);
                $transition->name                   = $name;
                $transition->workflow_state_from_id = $request->from_state[$transitionID];
                $transition->workflow_state_to_id   = $request->to_state[$transitionID];
                $transition->save();
            }
        }

        Transition::destroy($request->delete_transitions);

        // Finally deal with any new transitions
        if ($request->new_transition_name)
        {
            $workflow->transitions()->create([
                'name'                   => $request->new_transition_name,
                'workflow_state_from_id' => $request->new_transition_state_from_id,
                'workflow_state_to_id'   => $request->new_transition_state_to_id,
            ]);
        }

        session()->flash('success', 'The Workflow has been edited.');

        return redirect()->route('workflow.edit', $workflow->id);
    }

    public function index(): View
    {
        $args = [];
        $args['workflows'] = Workflow::all();

        return view('workflow.index', $args);
    }

    public function show(int $id): View
    {
        $args = [];
        $args['workflow'] = Workflow::find($id);

        return view('workflow.show', $args);
    }
}
