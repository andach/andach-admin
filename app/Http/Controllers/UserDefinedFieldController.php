<?php

namespace App\Http\Controllers;

use App\Models\UDF\UDF;
use App\Models\UDF\UDFItem;
use Illuminate\Http\Request;
use Illuminate\View\View;

class UserDefinedFieldController extends Controller
{
    protected $models = [
        'App\Models\Complaint\Complaint' => 'Complaint',
        'App\Models\HealthAndSafety\Incident' => 'Health and Safety',
        'App\Models\Ticket\Ticket' => 'Ticket'
    ];
    protected $types  = [
        'list' => 'List',
        'string' => 'Short String of Text',
        'date' => 'Date',
        'integer' => 'Integer (Whole Number)',
        'float' => 'Fractional Number or Percentage',
        'text' => 'Full Text',
    ];

    public function create(): View
    {
        $args = [];
        $args['models'] = $this->models;
        $args['types']  = $this->types;

        return view('user-defined-field.create', $args);
    }

    public function createPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'name' => 'required',
            'display_name' => 'required',
            'type' => 'required',
            'model_name' => 'required',
        ]);

        $udf = UDF::create($request->all());

        if ($request->type == 'list') {
            $listItems = explode(',', $request->items);

            foreach ($listItems as $item) {
                $udf->items()->create(['name' => trim($item)]);
            }
        }

        session()->flash('success', 'The User Defined Field has been added.');

        return redirect()->route('user-defined-field.edit', $udf->id);
    }

    public function edit(int $id): View
    {
        $args = [];
        $args['udf']    = UDF::find($id);
        $args['models'] = $this->models;
        $args['types']  = $this->types;

        return view('user-defined-field.edit', $args);
    }

    public function editPost(Request $request, int $id): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'name' => 'required',
            'display_name' => 'required',
        ]);

        $data = [];
        $data['name']         = $request->name;
        $data['display_name'] = $request->display_name;

        $udf = UDF::find($id);
        $udf->update($data);

        if ($request->delete_values)
        {
            foreach ($request->delete_values as $valueID) {
                UDFItem::destroy($valueID);
            }
        }

        $listItems = explode(',', $request->newItems);

        foreach ($listItems as $item)
        {
            if (trim($item))
            {
                $udf->items()->create(['name' => trim($item)]);
            }
        }

        session()->flash('success', 'The User Defined Field has been edited.');

        return redirect()->route('user-defined-field.edit', $udf->id);
    }

    public function index(): View
    {
        $args = [];
        $args['udfs']   = UDF::all();

        return view('user-defined-field.index', $args);
    }
}
