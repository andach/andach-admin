<?php

namespace App\Http\Controllers;

use App\Events\WorkflightCreated;
use App\Models\Ticket\Ticket;
use App\Models\Workflow\Workflight;
use App\Models\Company;
use App\Models\RolePermission;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\View\View;

class TicketController extends Controller
{
    public function create(): View
    {
        // No need to do any sort of authentication check here. The only relevant check is the role can access ticket.create.
        $args = [];
        $args['customers']      = User::isCustomer()->get()->pluck('name', 'id');
        $args['companies']      = Ticket::canAccessCompanies('create')->pluck('name', 'id');
        $args['locations']      = Ticket::workflowLocationsForForm();
        $args['users']          = RolePermission::usersWithAccessTo(['ticket.read', 'ticket.mine', 'ticket.edit', 'ticket.admin'])->pluck('name', 'id');
        $args['udfs']           = Ticket::udfsForCreatePage();
        $args['instantlyClose'] = Ticket::canBeInstantlyClosed();

        return view('ticket.create', $args);
    }

    public function createPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'company_id' => 'required',
        ]);

        $data = $request->all();
        $data['user_id'] = Auth::id();

        $ticket = Ticket::create($data);
        $ticket->updateUDFs($data);
        event(new WorkflightCreated($ticket));

        // We need to check if tickets can be closed instantly, and if so, check whether the user has instantly closed it.
        // And if so, then close it.
        if (isset($request->instantly_close))
        {
            $ticket->instantlyClose();
            session()->flash('success', 'The ticket was instantly created and closed.');
            return redirect()->route('ticket.index');
        }

        session()->flash('success', 'The Ticket was created');

        $ticket->assignWorkflightTo($request->user_id);

        return redirect()->route('ticket.index');
    }

    public function edit(int $id): View
    {
        $args = [];
        $args['ticket'] = Ticket::find($id);

        // Need to check that the person can access this ticket (i.e. that they can see tickets for this company).
        $args['ticket']->canAccessCompanyOrDie('edit');

        $args['customers'] = User::isCustomer()->get()->pluck('name', 'id');
        $args['companies'] = Ticket::canAccessCompanies('edit')->pluck('name', 'id');
        $args['locations'] = Ticket::workflowLocationsForForm();
        $args['users']     = RolePermission::usersWithAccessTo(['ticket.read', 'ticket.mine', 'ticket.edit', 'ticket.admin'])->pluck('name', 'id');
        $args['udfs']      = $args['ticket']->udfsForEditPage();

        return view('ticket.edit', $args);
    }

    public function editPost(Request $request, int $id): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);

        $ticket = Ticket::find($id);
        $ticket->canAccessCompanyOrDie('edit');

        $ticket->update([
            'name' => $request->name,
            'description' => $request->description,
            'customer_id' => $request->customer_id,
            'location_id' => $request->location_id,
        ]);
        $ticket->updateUDFs($request->all());
        $ticket->transitionToState($request->transition_id);

        $ticket->addComment($request->workflight_comments, $request->workflight_file);

        session()->flash('success', 'The Ticket as been updated');

        $ticket->assignWorkflightTo($request->user_id);

        return redirect()->route('ticket.edit', $ticket->id);
    }

    public function index(Request $request): View
    {
        $defaults = [];
        $defaults['id']          = $request->id;
        $defaults['name']        = $request->name;
        $defaults['description'] = $request->description;
        $defaults['open_closed'] = $request->open_closed;
        $defaults['states_ids']  = $request->states_ids;
        $defaults['users']       = $request->users;
        $defaults['comments']    = $request->comments;

        // Handle the search logic
        $tickets = Ticket::whereIn('company_id', Ticket::canAccessCompanies('show')->pluck('id')->toArray());

        $search = [
            'id'          => ['=', $request->id],
            'name'        => ['LIKE', '%'.$request->name.'%'],
            'description' => ['LIKE', '%'.$request->description.'%'],
        ];
        $tickets = Ticket::search($tickets, $search);

        if ('open' === $request->open_closed)
        {
            $tickets = $tickets->where('is_closed', 0);
        }

        if ('closed' === $request->open_closed)
        {
            $tickets = $tickets->where('is_closed', 1);
        }

        if ($request->states_ids)
        {
            if ($request->states_ids[0] !== null)
            {
                $ticketIDs = Workflight::whereIn('state_id', $request->states_ids)
                    ->where('workflowable_type', 'App\Models\Ticket\Ticket')
                    ->pluck('workflowable_id')
                    ->toArray();
                $tickets = $tickets->whereIn('id', $ticketIDs);
            }
        }

        if ($request->users)
        {
            if ($request->users[0] !== null)
            {
                $ticketIDs = Workflight::whereIn('assigned_to_user_id', $request->users)
                    ->where('workflowable_type', 'App\Models\Ticket\Ticket')
                    ->pluck('workflowable_id')
                    ->toArray();
                $tickets = $tickets->whereIn('id', $ticketIDs);
            }
        }

        $workflow = Ticket::workflow();

        $args = [];
        $args['defaults'] = $defaults;
        $args['tickets']  = $tickets->get();
        $args['states']   = $workflow->states()->pluck('name', 'id');
        $args['users']    = User::pluck('name', 'id');

        // $chartData1 = array_count_values(Ticket::isOpen()
        //     ->pluck('created_at')
        //     ->transform(function ($item) {
        //         return date('M-Y', strtotime($item));
        //     })
        //     ->toArray());

        // $chartData2 = array_count_values(Ticket::with('customer')
        //     ->isOpen()
        //     ->whereNotNull('customer_id')
        //     ->get()
        //     ->pluck('customer.name')
        //     ->toArray());

        // $chartData3 = array_count_values(Ticket::get()
        //     ->pluck('created_at')
        //     ->transform(function ($item) {
        //         return date('M-Y', strtotime($item));
        //     })
        //     ->toArray());

        // $args['charts']   = [
        //     Ticket::getChart('open_tickets_by_age', 'bar', $chartData1),
        //     Ticket::getChart('open_tickets_by_customer', 'pie', $chartData2),
        //     Ticket::getChart('all_tickets_by_month', 'bar', $chartData3),
        // ];

        return view('ticket.index', $args);
    }

    public function mine(): View
    {
        $args = [];
        $args['tickets'] = Ticket::whereHas('workflight', function ($query): void {
            $query->where('assigned_to_user_id', Auth::id());
        })->get();

        return view('ticket.mine', $args);
    }

    public function show(int $id): View
    {
        $args = [];
        $args['ticket'] = Ticket::find($id);
        $args['udfs']   = $args['ticket']->udfsForShowPage();

        $args['ticket']->canAccessCompanyOrDie('show');

        return view('ticket.show', $args);
    }
}
