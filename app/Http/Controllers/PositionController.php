<?php

namespace App\Http\Controllers;

use App\Models\HR\Job;
use App\Models\HR\Position;
use App\Models\HR\Team;
use App\Models\Location\Location;
use App\Models\Company;
use App\Models\CostCode;
use Illuminate\Http\Request;
use Illuminate\View\View;

class PositionController extends Controller
{
    public function createPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'hours_per_week' => 'required',
            'from_date' => 'required',
        ]);

        Position::create($request->all());

        session()->flash('success', 'The Position has been created');

        return redirect()->route('position.index');
    }

    public function edit(int $id): View
    {
        $args = [];
        $args['position']  = Position::find($id);
        $args['companies'] = Company::pluck('name', 'id');
        $args['jobs']      = Job::pluck('name', 'id');
        $args['locations'] = Location::pluck('name', 'id');
        $args['teams']     = Team::pluck('name', 'id');
        $args['costCodes'] = CostCode::pluck('name', 'id');

        return view('position.edit', $args);
    }

    public function editPost(Request $request, int $id): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'hours_per_week' => 'required',
            'from_date' => 'required',
        ]);

        Position::find($id)->update($request->all());

        session()->flash('success', 'The Position has been updated');

        return redirect()->route('position.edit', $id);
    }

    public function index(): View
    {
        $args = [];
        $args['positions'] = Position::all();
        $args['companies'] = Company::pluck('name', 'id');
        $args['jobs']      = Job::pluck('name', 'id');
        $args['locations'] = Location::pluck('name', 'id');
        $args['teams']     = Team::pluck('name', 'id');
        $args['costCodes'] = CostCode::pluck('name', 'id');

        return view('position.index', $args);
    }
}
