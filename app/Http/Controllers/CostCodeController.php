<?php

namespace App\Http\Controllers;

use App\Models\CostCode;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CostCodeController extends Controller
{
    public function create(): View
    {
        $args = [];
        $args['costCodes'] = CostCode::pluck('name', 'id');

        return view('cost-code.create', $args);
    }

    public function createPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'name' => 'required',
        ]);

        CostCode::create($request->all());

        session()->flash('success', 'The Cost Code has been created');

        return redirect()->route('cost-code.index');
    }

    public function edit(int $id): View
    {
        $args = [];
        $args['costCodes'] = CostCode::pluck('name', 'id');
        $args['costCode']  = CostCode::find($id);

        return view('cost-code.edit', $args);
    }

    public function editPost(Request $request, int $id): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'name' => 'required',
        ]);

        CostCode::find($id)->update($request->all());

        session()->flash('success', 'The Cost Code has been updated');

        return redirect()->route('cost-code.edit', $id);
    }

    public function index(): View
    {
        $args = [];
        $args['costCodes'] = CostCode::all();

        return view('cost-code.index', $args);
    }

    public function show(int $id): View
    {
        $args = [];
        $args['costCode'] = CostCode::find($id);

        return view('cost-code.show', $args);
    }
}
