<?php

namespace App\Http\Controllers;

use App\Models\Reference\County;
use App\Models\Reference\EthnicOrigin;
use App\Models\Reference\Religion;
use App\Models\Reference\SexualOrientation;
use App\Models\Company;
use App\Models\Role;
use App\Models\User;
use Auth;
use Hash;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CustomerController extends Controller
{
    public function create(): View
    {
        $args = [];
        $args['companies']           = Company::all()->pluck('name', 'id');
        $args['counties']            = County::all()->pluck('name', 'id');
        $args['ethnicOrigins']       = EthnicOrigin::all()->pluck('name', 'id');
        $args['religions']           = Religion::all()->pluck('name', 'id');
        $args['sexualOrientations']  = SexualOrientation::all()->pluck('name', 'id');

        return view('customer.create', $args);
    }

    public function createPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'company_id' => 'required',
            'name' => 'required',
            'password' => 'confirmed',
            'picture_path' => 'nullable|image',
        ]);

        $data = $request->all();
        $data['is_customer'] = 1;
        $data['is_staff']    = 0;
        $data['is_supplier'] = 0;

        $user = User::create($data);
        $user->password = Hash::make($request->password);
        $user->save();

        $user->updatePicture($request->picture_path);

        session()->flash('success', 'The Customer has been created');

        return redirect()->route('customer.create');
    }

    public function edit(int $id): View
    {
        $args = [];
        $args['user']                = User::find($id);

        if (!$args['user']->is_customer)
        {
            abort(403, 'This is not a customer');
        }

        $args['companies']           = Company::all()->pluck('name', 'id');
        $args['counties']            = County::all()->pluck('name', 'id');
        $args['ethnicOrigins']       = EthnicOrigin::all()->pluck('name', 'id');
        $args['religions']           = Religion::all()->pluck('name', 'id');
        $args['sexualOrientations']  = SexualOrientation::all()->pluck('name', 'id');

        return view('customer.edit', $args);
    }

    public function editPost(Request $request, int $id): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'company_id' => 'required',
            'name' => 'required',
            'password' => 'nullable|confirmed',
            'picture_path' => 'nullable|image',
        ]);

        $user = User::find($id);

        if (!$user->is_customer)
        {
            abort(403, 'This is not a customer');
        }

        $data = $request->all();
        $user->update($data);
        $user->updatePassword($request->password);
        $user->save();

        $user->updatePicture($request->picture_path);

        session()->flash('success', 'The Customer has been edited');

        return redirect()->route('customer.edit', $user->id);
    }

    public function index(): View
    {
        $args = [];
        $args['users'] = User::isCustomer()->get();

        return view('customer.index', $args);
    }

    public function show(int $id): View
    {
        $args = [];
        $args['user'] = User::find($id);

        if (!$args['user']->is_customer)
        {
            abort(403, 'This is not a customer');
        }

        return view('customer.show', $args);
    }
}
