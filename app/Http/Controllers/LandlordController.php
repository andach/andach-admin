<?php

namespace App\Http\Controllers;

use App\Models\Tenant;
use Illuminate\Http\Request;
use Illuminate\View\View;

class LandlordController extends Controller
{
    public function signUp(Request $request): \Illuminate\Http\RedirectResponse
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'company' => 'required',
            'password' => 'required|confirmed',
            'subdomain' => 'required|unique:tenants,subdomain|alpha_num',
        ]);

        if ($validator->fails())
        {
            return redirect('/#sign-up')
                ->withErrors($validator)
                ->withInput();
        }

        $tenant = new Tenant();
        $tenant->subdomain = $request->subdomain;
        $tenant->save();
        $tenant->createTenantDatabase($request);

        return redirect('/success?email='.$request->email.'&subdomain='.$request->subdomain);
    }

    public function success(Request $request): View
    {
        $args = [];
        $args['email']     = $request->email;
        $args['subdomain'] = $request->subdomain;

        return view('landlord.dynamic.success', $args);
    }
}
