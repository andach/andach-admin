<?php

namespace App\Http\Controllers;

use App\Models\HR\Appointment;
use Auth;
use Illuminate\Http\Request;
use Illuminate\View\View;

class SessionController extends Controller
{
    public function appointment(?string $redirectRoute = ''): View
    {
        $args = [];
        $appointments = Auth::user()->appointments()->current()->get();
        $args['appointments']  = $appointments;
        $args['redirectRoute'] = $redirectRoute;

        return view('session.appointment', $args);
    }

    public function appointmentCheck(?string $redirectRoute = ''): \Illuminate\Http\RedirectResponse
    {
        $appointments = Auth::user()->appointments()->current()->get();

        if (1 == $appointments->count())
        {
            session()->put('appointment_id', $appointments->first()->id);

            return redirect()->route('home');
        }

        return redirect()->route('session.appointment', $redirectRoute);
    }

    public function appointmentPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        $appointment = Appointment::find($request->appointment_id);

        if (Auth::id() == $appointment->user_id)
        {
            session()->put('appointment_id', $request->appointment_id);

            session()->flash('success', 'The appointment has been selected');
        }

        return redirect()->route($request->redirectRoute);
    }
}
