<?php

namespace App\Http\Controllers;

use App\Models\Ticket\Ticket;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\View\View;

class HomeController extends Controller
{
    public function home(): View
    {
        $boxes = [];

        $boxes[] = [
            'class'  => 'bg-info',
            'number' => Ticket::count(),
            'name'   => 'Number of Tickets',
            'icon'   => '',
            'link'   => route('ticket.index'),
        ];

        $boxes[] = [
            'class'  => 'bg-success',
            'number' => Ticket::whereHas('workflight', function ($query): void {
                $query->where('assigned_to_user_id', Auth::id());
            })->open()->count(),
            'name'   => 'Open Tickets Assigned to You',
            'icon'   => '',
            'link'   => route('ticket.mine'),
        ];

        $boxes[] = [
            'class'  => 'bg-warning',
            'number' => User::count(),
            'name'   => 'Total Users',
            'icon'   => '',
            'link'   => route('user.index'),
        ];

        $boxes[] = [
            'class'  => 'bg-danger',
            'number' => Ticket::openMoreThanDays(7)->count(),
            'name'   => 'Tickets Open for More than 7 Days',
            'icon'   => '',
            'link'   => route('ticket.index'),
        ];

        $args = [];
        $args['boxes'] = $boxes;

        return view('home', $args);
    }
}
