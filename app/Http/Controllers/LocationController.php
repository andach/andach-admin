<?php

namespace App\Http\Controllers;

use App\Models\Location\Location;
use Auth;
use Illuminate\Http\Request;
use Illuminate\View\View;

class LocationController extends Controller
{
    public function ajaxCreate(int $parentID, string $title): string
    {
        if (!Auth::user()->is_super_admin)
        {
            dd('no permission');
        }

        // Have to put this here just in case there's ever a desynchronisation on the _lft and _rgt columns.
        Location::fixTree();

        $location = new Location();
        $location->name = $title;
        $location->parent_id = $parentID;
        $location->save();

        Location::fixTree();

        return 'yep that worked';
    }

    public function ajaxDelete(int $id): string
    {
        if (!Auth::user()->is_super_admin)
        {
            dd('no permission');
        }

        // Have to put this here just in case there's ever a desynchronisation on the _lft and _rgt columns.
        Location::fixTree();

        $location = Location::find($id);
        $location->delete();

        Location::fixTree();

        return 'yep that worked';
    }

    public function ajaxIsRentable(int $id, int $newValue): string
    {
        if (!Auth::user()->is_super_admin)
        {
            dd('no permission');
        }

        // Have to put this here just in case there's ever a desynchronisation on the _lft and _rgt columns.
        Location::fixTree();

        $location = Location::find($id);
        $location->is_rentable = $newValue;
        $location->save();

        Location::fixTree();

        return 'yep that worked';
    }

    public function ajaxUpdate(int $id, string $title): string
    {
        if (!Auth::user()->is_super_admin)
        {
            dd('no permission');
        }

        // Have to put this here just in case there's ever a desynchronisation on the _lft and _rgt columns.
        Location::fixTree();

        $location = Location::find($id);
        $location->name = $title;
        $location->save();

        Location::fixTree();

        return 'yep that worked';
    }

    public function index(): View
    {
        $args = [];
        $args['locations'] = Location::all();

        return view('location.index', $args);
    }

    public function indexPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        // This statement accounts for if we don't have any locations yet.
        if ($request->name)
        {
            foreach ($request->name as $id => $name)
            {
                $location = Location::find($id);
                $location->name = $name;
                $location->save();
            }
            session()->flash('success', 'The locations have been updated.');
        }

        if ($request->delete_ids)
        {
            if (Auth::user()->is_super_admin)
            {
                Location::whereIn('id', $request->delete_ids)->delete();
            } else {
                session()->flash('error', 'You do not have permission to delete a location. Please contact support to do so.');
            }
        }

        if ($request->new_name)
        {
            $location = new Location();
            $location->name = $request->new_name;
            $location->parent_id = $request->new_parent_id;
            $location->save();

            session()->flash('success', 'The locations have been updated and new location created.');
        }

        return redirect()->route('location.index');
    }

    public function json(): \Illuminate\Http\JsonResponse
    {
        $tree = Location::get()->toTree();

        // Some day I'll work out how to do things properly.
        $json = $tree->toJson();
        $json = str_replace('"id"', '"key"', $json);
        $json = str_replace('"name"', '"title"', $json);
        $json = json_decode($json, true);

        return response()->json($json);
    }
}
