<?php

namespace App\Http\Controllers;

use App\Enums\Gender;
use App\Models\CareHome\Resident;
use App\Models\Location\Location;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\View\View;

class InitialSetupController extends Controller
{
    public function location(): View
    {
        $args = [];

        return view('initial-setup.location', $args);
    }

    public function locationPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        switch ($request->setupType)
        {
            case 'manual':
                $request->validate([
                    'manual_names' => 'required',
                ]);

                $array = explode('<br />', nl2br($request->manual_names));
                foreach ($array as $roomName)
                {
                    if ($roomName)
                    {
                        Location::fixTree();

                        $location = new Location();
                        $location->name = $roomName;
                        $location->parent_id = 1;
                        $location->is_rentable = 1;
                        $location->save();

                        Location::fixTree();
                    }
                }

                session()->flash('success', 'Your rooms have been set up.');
                break;

            case 'numbers':
                $request->validate([
                    'number_of_rooms' => 'required|numeric',
                ]);

                for ($i = 1; $i <= $request->number_of_rooms; $i++)
                {
                    Location::fixTree();

                    $location = new Location();
                    $location->name = 'Room '.$i;
                    $location->parent_id = 1;
                    $location->is_rentable = 1;
                    $location->save();

                    Location::fixTree();
                }

                session()->flash('success', $request->number_of_rooms.' rooms have been set up.');
                break;

            case 'wings':
                $request->validate([
                    'wing_names' => 'required',
                ]);

                $array = explode('<br />', nl2br($request->wing_names));
                $array2        = [];
                $wings         = [];
                $rooms         = [];
                $wingsAndRooms = [];

                foreach ($array as $wingAndRoom)
                {
                    $array2 = explode(',', $wingAndRoom);
                    $wings[] = trim($array2[0]);
                    $rooms[] = trim($array2[1]);
                }

                foreach ($wings as $id => $wingName)
                {
                    $wingsAndRooms[$wingName][] = $rooms[$id];
                }

                foreach ($wingsAndRooms as $wingName => $rooms)
                {
                    Location::fixTree();

                    $wing = new Location();
                    $wing->name =$wingName;
                    $wing->parent_id = 1;
                    $wing->save();

                    foreach ($rooms as $roomName)
                    {
                        $room = new Location();
                        $room->name =$roomName;
                        $room->parent_id = $wing->id;
                        $room->is_rentable = 1;
                        $room->save();
                    }

                    Location::fixTree();
                }

                session()->flash('success', 'Your wings/corridors and rooms have been set up.');
                break;
        }

        return redirect()->route('initial-setup');
    }

    public function resident(): View
    {
        $args = [];
        $args['rooms']   = Location::rentable()->get();
        $args['genders'] = Gender::toArray();

        return view('initial-setup.resident', $args);
    }

    public function residentPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        foreach ($request->name as $id => $name)
        {
            if ($name)
            {
                $gender = $request->gender[$id];

                $user = User::create([
                    'company_id' => 1,
                    'name' => $name,
                    'enum_gender' => $gender,
                    'is_customer' => 1,
                ]);
                $resident = $user->resident()->create();
                $resident->save();
            }
        }

        session()->flash('success', 'Your residents have been initially set up.');

        return redirect()->route('initial-setup');
    }

    public function staff(): View
    {
        $args = [];
        $args['genders'] = Gender::toArray();

        return view('initial-setup.staff', $args);
    }

    public function staffPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        foreach ($request->name as $id => $name)
        {
            if ($name)
            {
                $gender = $request->gender[$id] ?? '';
                $phone_number = $request->phone_number[$id] ?? '';
                $email = $request->email[$id] ?? '';

                $user = User::create([
                    'company_id' => 1,
                    'name' => $name,
                    'enum_gender' => $gender,
                    'is_staff' => 1,
                    'phone_number' => $phone_number,
                    'email' => $email,
                ]);
                if ($email)
                {
                    $user->updatePassword('password');
                }
            }
        }

        session()->flash('success', 'Your staff members have been initially set up.');

        return redirect()->route('initial-setup');
    }

    public function start(): \Illuminate\Http\RedirectResponse
    {
        if (Location::count() == 1)
        {
            return redirect()->route('initial-setup.location');
        }

        if (Resident::count() == 0)
        {
            return redirect()->route('initial-setup.resident');
        }

        if (User::isStaff()->count() == 1)
        {
            return redirect()->route('initial-setup.staff');
        }
    }
}
