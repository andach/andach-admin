<?php

namespace App\Http\Controllers;

use App\Models\HR\Appointment;
use App\Models\HR\Contract;
use App\Models\HR\ContractTemplate;
use App\Models\HR\Job;
use App\Models\HR\Position;
use App\Models\HR\Team;
use App\Models\HR\WorkingTimePattern;
use App\Models\Location\Location;
use App\Models\Payslip\PayFrequency;
use App\Models\Payslip\PayPeriod;
use App\Models\Payslip\PayYear;
use App\Models\Company;
use App\Models\CostCode;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\View\View;

class AppointmentController extends Controller
{
    public function create(): View
    {
        $args = [];
        $args['users']     = User::where('is_staff', 1)->pluck('name', 'id');
        $args['companies'] = Company::pluck('name', 'id');
        $args['jobs']      = Job::pluck('name', 'id');
        $args['locations'] = Location::pluck('name', 'id');
        $args['teams']     = Team::pluck('name', 'id');
        $args['positions'] = Position::pluck('name', 'id');
        $args['costCodes'] = CostCode::pluck('name', 'id');
        $args['templates'] = ContractTemplate::pluck('name', 'id');
        $args['wtps']      = WorkingTimePattern::pluck('name', 'id');

        return view('appointment.create', $args);
    }

    public function createPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'user_id' => 'required',
            'position_id' => 'required_without:hours_per_week',
            'hours_per_week' => 'required_without:position_id',
            'from_date' => 'required',

            'pay_per_hour' => 'required_without_all:actual_salary,fte_salary',
            'actual_salary' => 'required_without_all:pay_per_hour,fte_salary',
            'fte_salary' => 'required_without_all:pay_per_hour,actual_salary',
        ]);

        if (!$request->position_id)
        {
            $position = Position::create([
                'company_id'     => $request->company_id,
                'job_id'         => $request->job_id,
                'location_id'    => $request->location_id,
                'team_id'        => $request->team_id,
                'hours_per_week' => $request->hours_per_week,
                'cost_code_id'   => $request->cost_code_id,
            ]);
        } else {
            $position = Position::find($request->position_id);
        }

        $appointment = $position->appointments()->create([
            'user_id'   => $request->user_id,
            'from_date' => $request->from_date,
            'to_date'   => $request->to_date,
        ]);

        $appointment->updateContract([
            'appointment_id' => $appointment->id,
            'contract_template_id' => $request->contract_template_id,
            'working_time_pattern_id' => $request->working_time_pattern_id,
            'from_date' => $request->from_date,
            'to_date'   => $request->to_date,
            'is_part_time'   => $request->is_part_time,
            'is_zero_hours'   => $request->is_zero_hours,
            'is_positive_pay'   => $request->is_positive_pay,
        ]);

        return redirect()->route('appointment.index');
    }

    public function index(): View
    {
        $args = [];
        $args['appointments'] = Appointment::all();

        return view('appointment.index', $args);
    }

    public function mine(int $id): View
    {
        $args = [];
        $args['appointment'] = Appointment::where('user_id', Auth::id())
            ->where('id', $id)
            ->get();

        return view('appointment.my', $args);
    }

    public function mineAll(): View
    {
        $args = [];
        $args['appointments'] = Appointment::where('user_id', Auth::id())->get();

        return view('appointment.my-all', $args);
    }
}
