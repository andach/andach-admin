<?php

namespace App\Http\Controllers;

use App\Models\HR\Appointment;
use App\Models\HR\ContractTemplate;
use App\Models\HR\Job;
use App\Models\HR\Position;
use App\Models\HR\Team;
use App\Models\HR\WorkingTimePattern;
use App\Models\Location\Location;
use App\Models\Payslip\PayFrequency;
use App\Models\Payslip\PayPeriod;
use App\Models\Payslip\PayYear;
use App\Models\Company;
use App\Models\CostCode;
use App\Models\Role;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\View\View;

class HRController extends Controller
{
    public function job(int $id): View
    {
        $args = [];
        $args['job']   = Job::find($id);
        $args['roles'] = Role::pluck('name', 'id');

        return view('hr.job.edit', $args);
    }

    public function jobPost(Request $request, int $id): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'name' => 'required',
            'fte_hours' => 'required_with',
        ]);

        $job = Job::find($id);
        $job->update($request->all());
        $job->roles()->sync($request->roles);

        session()->flash('success', 'The Job has been edited.');

        return redirect()->route('hr.job', $job->id);
    }

    public function jobs(): View
    {
        $args = [];
        $args['jobs'] = Job::all();

        return view('hr.job.index', $args);
    }

    public function jobsPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'fte_hours' => 'required_with:name',
            'description_of_work' => 'required_with:name',
        ]);

        Job::destroy($request->delete_ids);

        if ($request->name)
        {
            Job::create($request->all());
        }

        session()->flash('success', 'The Jobs have been updated.');

        return redirect()->route('hr.jobs');
    }

    public function payPeriod(int $id): View
    {
        $args = [];
        $args['payPeriod']      = PayPeriod::find($id);
        $args['payFrequencies'] = PayFrequency::pluck('name', 'id');
        $args['payYears']       = PayYear::pluck('name', 'id');

        return view('hr.pay-period.edit', $args);
    }

    public function payPeriodPost(Request $request, int $id): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'pay_frequency_id' => 'required',
            'pay_year_id' => 'required',
            'name' => 'required',
            'from_date' => 'required',
            'to_date' => 'required',
        ]);

        $payPeriod = PayPeriod::find($id);
        $payPeriod->update($request->all());

        session()->flash('success', 'The Pay Period has been edited.');

        return redirect()->route('hr.pay-period', $payPeriod->id);
    }

    public function payPeriods(): View
    {
        $args = [];
        $args['payPeriods']     = PayPeriod::all();
        $args['payFrequencies'] = PayFrequency::pluck('name', 'id');
        $args['payYears']       = PayYear::pluck('name', 'id');

        return view('hr.pay-period.index', $args);
    }

    public function payPeriodsPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'pay_frequency_id' => 'required_with:name',
            'pay_year_id' => 'required_with:name',
            'from_date' => 'required_with:name',
            'to_date' => 'required_with:name',
        ]);

        PayPeriod::destroy($request->delete_ids);

        if ($request->name)
        {
            PayPeriod::create($request->all());
        }

        session()->flash('success', 'The Pay Periods have been updated.');

        return redirect()->route('hr.pay-periods');
    }

    public function payYears(): View
    {
        $args = [];
        $args['payYears'] = PayYear::all();

        return view('hr.pay-year.index', $args);
    }

    public function payYearsPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        PayYear::destroy($request->delete_ids);

        if ($request->name)
        {
            PayYear::create($request->all());
        }

        session()->flash('success', 'The Pay Years have been updated.');

        return redirect()->route('hr.pay-years');
    }

    public function team(int $id): View
    {
        $args = [];
        $args['team']     = Team::find($id);
        $args['teams']    = Team::pluck('name', 'id');
        $args['managers'] = Position::pluck('name', 'id');

        return view('hr.team.edit', $args);
    }

    public function teamPost(Request $request, int $id): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'name' => 'required',
            'manager_id' => 'required',
        ]);

        $team = Team::find($id);
        $team->update($request->all());

        session()->flash('success', 'The Team has been edited.');

        return redirect()->route('hr.team', $team->id);
    }

    public function teams(): View
    {
        $args = [];
        $args['teams']    = Team::all();
        $args['managers'] = Position::pluck('name', 'id');

        return view('hr.team.index', $args);
    }

    public function teamsPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        Team::destroy($request->delete_ids);

        if ($request->name)
        {
            Team::create($request->all());
        }

        session()->flash('success', 'The Teams have been updated.');

        return redirect()->route('hr.teams');
    }

    public function workingTimePattern(int $id): View
    {
        $args = [];
        $args['workingTimePattern'] = WorkingTimePattern::find($id);

        return view('hr.working-time-pattern.edit', $args);
    }

    public function workingTimePatternPost(Request $request, int $id): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'name' => 'required',
        ]);

        $workingTimePattern = WorkingTimePattern::find($id);
        $workingTimePattern->update($request->all());

        session()->flash('success', 'The Working Time Pattern has been edited.');

        return redirect()->route('hr.working-time-pattern', $workingTimePattern->id);
    }

    public function workingTimePatterns(): View
    {
        $args = [];
        $args['workingTimePatterns'] = WorkingTimePattern::all();

        return view('hr.working-time-pattern.index', $args);
    }

    public function workingTimePatternsPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        WorkingTimePattern::destroy($request->delete_ids);

        if ($request->name)
        {
            WorkingTimePattern::create($request->all());
        }

        session()->flash('success', 'The Working Time Patterns have been updated.');

        return redirect()->route('hr.working-time-patterns');
    }
}
