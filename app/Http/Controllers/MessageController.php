<?php

namespace App\Http\Controllers;

use App\Models\Message\Message;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\View\View;

class MessageController extends Controller
{
    public function bin(): View
    {
        $args = [];
        $args['messages'] = Auth::user()->messagesReceived()->wherePivot('is_deleted', 1)->get();
        $args['folders']  = Message::folders();
        $args['title']    = 'Bin';

        return view('message.inbox', $args);
    }

    public function create(): View
    {
        $args = [];
        $args['folders'] = Message::folders();
        $args['users']   = User::isStaff()->pluck('name', 'id')->toArray();

        return view('message.create', $args);
    }

    public function createPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'user_ids' => 'required',
        ]);

        $message = Auth::user()->messagesSent()->create($request->all());
        $message->toUsers()->sync($request->user_ids);

        session()->flash('success', 'Your message has been sent.');

        return redirect()->route('message.inbox');
    }

    public function delete(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'message_ids' => 'required',
        ]);

        $messages = Auth::user()->messagesReceived()->whereIn('messages.id', $request->message_ids)->get();
        foreach ($messages as $message)
        {
            $message->pivot->is_deleted = 1;
            $message->pivot->save();
        }

        session()->flash('success', 'The messages have been deleted.');

        return redirect()->route('message.inbox');
    }

    public function inbox(): View
    {
        $args = [];
        $args['messages'] = Auth::user()->messagesReceived()->wherePivot('is_deleted', 0)->get();
        $args['folders']  = Message::folders();
        $args['title']    = 'Message Inbox';

        return view('message.inbox', $args);
    }

    public function outbox(): View
    {
        $args = [];
        $args['messages'] = Auth::user()->messagesSent()->get();
        $args['folders']  = Message::folders();
        $args['title']    = 'Message Outbox';

        return view('message.inbox', $args);
    }

    public function show(string $uuid): View
    {
        $args = [];
        $args['folders'] = Message::folders();
        $args['message'] = Auth::user()->messagesReceived()->where('messages.id', $uuid)->first();

        if (!$args['message'])
        {
            $args['message'] = Auth::user()->messagesSent()->where('messages.id', $uuid)->first();

            if (!$args['message'])
            {
                abort(403, 'You do not have permission to view this message, or it doesn\'t exist.');
            }
        } else {
            $args['message']->markAsRead();
        }

        return view('message.show', $args);
    }
}
