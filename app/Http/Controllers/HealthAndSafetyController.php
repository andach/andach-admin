<?php

namespace App\Http\Controllers;

use App\Enums\HealthAndSafety\BodyPart;
use App\Enums\HealthAndSafety\KindOfAccident;
use App\Enums\HealthAndSafety\MainFactor;
use App\Enums\HealthAndSafety\WorkProcess;
use App\Events\WorkflightCreated;
use App\Models\HealthAndSafety\Incident;
use App\Models\HealthAndSafety\Injury;
use App\Models\Workflow\Workflight;
use App\Models\Chart;
use App\Models\Company;
use App\Models\RolePermission;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\View\View;

class HealthAndSafetyController extends Controller
{
    public function create(): View
    {
        $args = [];
        $args['companies']      = Incident::canAccessCompanies('create')->pluck('name', 'id');
        $args['locations']      = Incident::workflowLocationsForForm();
        $args['users']          = RolePermission::usersWithAccessTo(['health-and-safety-incident.read', 'health-and-safety-incident.mine', 'health-and-safety-incident.edit', 'health-and-safety-incident.admin'])->pluck('name', 'id');
        $args['udfs']           = Incident::udfsForCreatePage();
        $args['instantlyClose'] = Incident::canBeInstantlyClosed();
        $args['enumKind']       = KindOfAccident::toArray();
        $args['enumFactor']     = MainFactor::toArray();
        $args['enumProcess']    = WorkProcess::toArray();

        return view('health-and-safety-incident.create', $args);
    }

    public function createInjury(int $incidentID): View
    {
        $args = [];
        $args['incident']  = Incident::find($incidentID);
        $args['users']     = User::all()->pluck('name', 'id');
        $args['bodyParts'] = BodyPart::toArray();

        return view('health-and-safety-injury.create', $args);
    }

    public function createInjuryPost(Request $request, int $incidentID): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'user_id' => 'required_without:injured_person_name',
        ]);

        $data = $request->all();
        $data['incident_id']     = $incidentID;
        $data['enum_body_parts'] = implode(',', $request->body_part);

        $incident = Incident::find($incidentID);
        $incident->canAccessCompanyOrDie('edit');

        Injury::create($data);

        session()->flash('success', 'The injury was successfully logged.');
        return redirect()->route('health-and-safety-incident.edit', $incident->id);
    }

    public function createPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'company_id' => 'required',
            'enum_kind_of_accident' => 'required',
            'enum_main_factor' => 'required',
            'enum_work_process' => 'required',
        ]);

        $data = $request->all();
        $data['user_id'] = Auth::id();

        $incident = Incident::create($data);
        $incident->updateUDFs($data);
        event(new WorkflightCreated($incident));

        // We need to check if incidents can be closed instantly, and if so, check whether the user has instantly closed it.
        // And if so, then close it.
        if (isset($request->instantly_close))
        {
            $incident->instantlyClose();
            session()->flash('success', 'The incident was instantly created and closed.');
            return redirect()->route('health-and-safety-incident.index');
        }

        session()->flash('success', 'The Incident was created');
        $incident->assignWorkflightTo($request->user_id);

        return redirect()->route('health-and-safety-incident.edit', $incident->id);
    }

    public function dashboard(): View
    {
        $args = [];

        $chartData2 = array_count_values(Incident::isOpen()
            ->get()
            ->pluck('days_open')
            ->toArray());

        $chart = new Chart('open_complaints_by_age', 'line', ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']);
        $chart->addManualDataset(['data' => [3, 5, 4, 6, 7, 10, 6, 10, 11, 2, 7, 8], 'label' => 'Current Year']);
        $chart->addManualDataset(['data' => [1, 3, 4, 5, 3, 4, 2, 4, 3, 5, 1, 1, 2], 'label' => 'Previous Year']);
        $args['charts'][] = $chart;

        $chart2 = new Chart('open_complaints_by_customer', 'bar', array_keys($chartData2));
        $chart2->addDatasetByColumn(Incident::isOpen()->get(), 'Open Incidents', 'days_open');
        $chart2->setColorScheme('blackwhite');
        $args['charts'][] = $chart2;

        $chart2 = new Chart('all_complaints_by_month', 'bar', ['Aaron', 'Bob', 'Charles', 'Davina', 'Eddie', 'Frank', 'Gert', 'Harry', 'India', 'Juliet']);
        $chart2->addManualDataset(['data' => [3, 5, 4, 6, 7, 10, 6, 10, 11, 2], 'label' => 'Age of Open Incidents in Days']);
        $chart2->setColorScheme('andach');
        $args['charts'][] = $chart2;

        // dd($args['charts']);

        return view('health-and-safety-incident.dashboard', $args);
    }

    public function edit(int $id): View
    {
        $args = [];
        $args['incident'] = Incident::find($id);

        $args['incident']->canAccessCompanyOrDie('edit');

        $args['companies']      = Incident::canAccessCompanies('create')->pluck('name', 'id');
        $args['locations']      = Incident::workflowLocationsForForm();
        $args['users']          = RolePermission::usersWithAccessTo(['health-and-safety-incident.read', 'health-and-safety-incident.mine', 'health-and-safety-incident.edit', 'health-and-safety-incident.admin'])->pluck('name', 'id');
        $args['udfs']           = Incident::udfsForCreatePage();
        $args['instantlyClose'] = Incident::canBeInstantlyClosed();
        $args['enumKind']       = KindOfAccident::toArray();
        $args['enumFactor']     = MainFactor::toArray();
        $args['enumProcess']    = WorkProcess::toArray();
        $args['udfs']           = $args['incident']->udfsForEditPage();

        return view('health-and-safety-incident.edit', $args);
    }

    public function editInjury(int $incidentID, int $injuryID): View
    {
        $args = [];
        $args['injury']    = Injury::find($injuryID);
        $args['incident']  = Incident::find($incidentID);
        $args['users']     = User::all()->pluck('name', 'id');
        $args['bodyParts'] = BodyPart::toArray();

        return view('health-and-safety-injury.edit', $args);
    }

    public function editInjuryPost(Request $request, int $incidentID, int $injuryID): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'user_id' => 'required_without:injured_person_name',
        ]);

        $data = $request->all();
        $data['incident_id']     = $incidentID;
        $data['enum_body_parts'] = implode(',', $request->body_part);

        $incident = Incident::find($incidentID);
        $incident->canAccessCompanyOrDie('edit');

        $injury = Injury::find($injuryID);
        $injury->update($data);

        session()->flash('success', 'The injury was successfully logged.');
        return redirect()->route('health-and-safety-incident.edit', $incident->id);
    }

    public function editPost(Request $request, int $id): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);

        $incident = Incident::find($id);
        $incident->canAccessCompanyOrDie('edit');

        $incident->update([
            'name'                    => $request->name,
            'description'             => $request->description,
            'location_id'             => $request->location_id,
            'enum_kind_of_accident'   => $request->enum_kind_of_accident,
            'enum_main_factor'        => $request->enum_main_factor,
            'enum_work_process'       => $request->enum_work_process,
            'is_riddor_reportable'    => $request->is_riddor_reportable,
            'riddor_report_date'      => $request->riddor_report_date,
            'riddor_report_reference' => $request->riddor_report_reference,
        ]);
        $incident->updateUDFs($request->all());
        $incident->transitionToState($request->transition_id);

        $incident->addComment($request->workflight_comments, $request->workflight_file);

        session()->flash('success', 'The Incident as been updated');
        $incident->assignWorkflightTo($request->user_id);

        return redirect()->route('health-and-safety-incident.edit', $incident->id);
    }

    public function index(Request $request): View
    {
        $defaults = [];
        $defaults['id']          = $request->id;
        $defaults['name']        = $request->name;
        $defaults['description'] = $request->description;
        $defaults['open_closed'] = $request->open_closed;
        $defaults['states_ids']  = $request->states_ids;
        $defaults['users']       = $request->users;
        $defaults['comments']    = $request->comments;

        // Handle the search logic
        $incidents = Incident::whereIn('company_id', Incident::canAccessCompanies('show')->pluck('id')->toArray());

        $search = [
            'id'          => ['=', $request->id],
            'name'        => ['LIKE', '%'.$request->name.'%'],
            'description' => ['LIKE', '%'.$request->description.'%'],
        ];
        $incidents = Incident::search($incidents, $search);

        if ('open' === $request->open_closed)
        {
            $incidents = $incidents->where('is_closed', 0);
        }

        if ('closed' === $request->open_closed)
        {
            $incidents = $incidents->where('is_closed', 1);
        }

        if ($request->states_ids)
        {
            if ($request->states_ids[0] !== null)
            {
                $incidentIDs = Workflight::whereIn('state_id', $request->states_ids)
                    ->where('workflowable_type', 'App\Models\Complaint\Complaint')
                    ->pluck('workflowable_id')
                    ->toArray();
                $incidents = $incidents->whereIn('id', $incidentIDs);
            }
        }

        if ($request->users)
        {
            if ($request->users[0] !== null)
            {
                $incidentIDs = Workflight::whereIn('assigned_to_user_id', $request->users)
                    ->where('workflowable_type', 'App\Models\Complaint\Complaint')
                    ->pluck('workflowable_id')
                    ->toArray();
                $incidents = $incidents->whereIn('id', $incidentIDs);
            }
        }

        $workflow = Incident::workflow();

        $args = [];
        $args['defaults']  = $defaults;
        $args['incidents'] = $incidents->get();
        $args['states']    = $workflow->states()->pluck('name', 'id');
        $args['users']     = User::pluck('name', 'id');

        return view('health-and-safety-incident.index', $args);
    }

    public function mine(): View
    {
        $args = [];
        $args['incidents'] = Incident::whereHas('workflight', function ($query): void {
            $query->where('assigned_to_user_id', Auth::id());
        })->get();

        return view('health-and-safety-incident.mine', $args);
    }

    public function show(int $id): View
    {
        $args = [];
        $args['incident'] = Complaint::find($id);
        $args['udfs']   = $args['incident']->udfsForShowPage();

        $args['incident']->canAccessCompanyOrDie('show');

        return view('health-and-safety-incident.show', $args);
    }
}
