<?php

namespace App\Http\Controllers\CareHome;

use App\Enums\CareHome\Medical\Alcohol;
use App\Enums\CareHome\Medical\Cognition;
use App\Enums\CareHome\Medical\Communication;
use App\Enums\CareHome\Medical\Continence;
use App\Enums\CareHome\Medical\Hearing;
use App\Enums\CareHome\Medical\Mobility;
use App\Enums\CareHome\Medical\Sight;
use App\Enums\CareHome\Medical\Smoker;
use App\Enums\CareHome\Meal;
use App\Http\Controllers\Controller;
use App\Models\CareHome\Assessment\Fall;
use App\Models\CareHome\Resident;
use App\Models\Location\Location;
use App\Models\HealthAndSafety\Incident;
use App\Models\HealthAndSafety\Injury;
use App\Models\Reference\County;
use App\Models\Reference\EthnicOrigin;
use App\Models\Reference\Religion;
use App\Models\Reference\SexualOrientation;
use App\Models\Rental\Rental;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ResidentController extends Controller
{
    public function create(): View
    {
        $args = [];
        $args['counties']            = County::all()->pluck('name', 'id');
        $args['ethnicOrigins']       = EthnicOrigin::all()->pluck('name', 'id');
        $args['religions']           = Religion::all()->pluck('name', 'id');
        $args['sexualOrientations']  = SexualOrientation::all()->pluck('name', 'id');
        $args['rooms']               = Location::rentable()->get()->pluck('name_with_renter', 'id');
        // $args['enumAlcohol']       = Alcohol::toArray();
        // $args['enumCognition']     = Cognition::toArray();
        // $args['enumCommunication'] = Communication::toArray();
        // $args['enumContinence']    = Continence::toArray();
        // $args['enumHearing']       = Hearing::toArray();
        // $args['enumMobility']      = Mobility::toArray();
        // $args['enumSight']         = Sight::toArray();
        // $args['enumSmoker']        = Smoker::toArray();

        return view('care-home.resident.create', $args);
    }

    public function createPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'name' => 'required',
            'rented_thing_id' => 'required',
            'from_date' => 'required',
        ]);

        $user     = User::create($request->all());
        $resident = $user->resident()->create($request->all());
        $resident->save();

        $resident->updateBillingInformation($request->all());

        return redirect()->route('care-home.resident.index');
    }

    public function edit(string $id): View
    {
        $args = [];
        $args['resident']       = Resident::find($id);
        $args['users']          = User::isStaff()->get()->pluck('name', 'id');
        // TODO: This
        $args['falls']          = Incident::all();
        $args['fallQuestions']  = Fall::questions();
        $args['eolWishes']      = $args['resident']->currentEndOfLifeWishes();
        $args['page']           = request('page');
        $args['lastFallAssess'] = $args['resident']->lastFallAssessmentArray();

        $date                   = \Carbon\Carbon::today()->subDays(7);
        $args['foodWeek']       = $args['resident']->foodLog()->where('date_eaten', '>=', $date)->get()->groupBy('date_eaten');

        $args['enumMeals']         = Meal::toArray();
        $args['enumAlcohol']       = Alcohol::toArray();
        $args['enumCognition']     = Cognition::toArray();
        $args['enumCommunication'] = Communication::toArray();
        $args['enumContinence']    = Continence::toArray();
        $args['enumHearing']       = Hearing::toArray();
        $args['enumMobility']      = Mobility::toArray();
        $args['enumSight']         = Sight::toArray();
        $args['enumSmoker']        = Smoker::toArray();

        return view('care-home.resident.edit', $args);
    }

    public function editPost(Request $request, string $id): \Illuminate\Http\RedirectResponse
    {
        $resident = Resident::find($id);

        switch ($request->action)
        {
            case 'catheter':
                $request->validate([
                    'date_changed' => 'required',
                    'changed_by_user_id' => 'required',
                    'size' => 'required',
                    'reasons' => 'required',
                ]);
                break;

            case 'foodlog':
                $request->validate([
                    'date_eaten' => 'required',
                    'enum_meal' => 'required',
                    'food_and_drink' => 'required',
                ]);
                break;

            case 'hospital':
                $request->validate([
                    'date_from' => 'required',
                    'reason' => 'required',
                ]);
                break;

            case 'illness':
                $request->validate([
                    'date_from' => 'required',
                    'illness' => 'required',
                ]);
                break;

            case 'malnutrition':
                $request->validate([
                    'must_bmi' => 'required',
                    'must_weight_loss' => 'required',
                ]);
                break;

            case 'skin':
                $request->validate([
                    'braden_sensory_perception' => 'required',
                    'braden_moisture' => 'required',
                    'braden_activity' => 'required',
                    'braden_mobility' => 'required',
                    'braden_nutrition' => 'required',
                    'braden_friction' => 'required',
                ]);
                break;

            case 'weightlog':
                $request->validate([
                    'bmi' => 'required',
                    'weight_in_kg' => 'required_without_all:weight_in_st,weight_in_lbs',
                    'weight_in_st' => 'required_without_all:weight_in_kg,weight_in_lbs',
                    'weight_in_lbs' => 'required_without_all:weight_in_kg,weight_in_st',
                ]);
                break;
        }

        $functionName = 'update'.ucfirst($request->action);

        $resident->$functionName($request->all());

        session()->flash('success', 'The resident has been updated.');

        return redirect()->route('care-home.resident.edit', [$id, $request->action]);
    }

    public function fallsAssessment(int $id): View
    {
        $args                = [];
        $args['assessment']  = Fall::find($id);
        $args['answers']     = $args['assessment']->jsonAsArray();
        $args['quesWithCat'] = Fall::questions();

        return view('care-home.resident.falls', $args);
    }

    public function finance(string $id): View
    {
        $args = [];
        $args['page']           = request('page');
        $args['resident']       = Resident::find($id);
        $args['rooms']          = Location::rentable()->pluck('name', 'id');
        $args['currentCharges'] = $args['resident']->currentCharges();
        $args['users']          = User::pluck('name', 'id');

        // Just in case
        // $args['resident']->generateInvoices();

        return view('care-home.resident.finance', $args);
    }

    public function financePost(Request $request, string $id): \Illuminate\Http\RedirectResponse
    {
        $resident = Resident::find($id);

        switch ($request->action)
        {
            case 'stay':
                $request->validate([
                    'rented_thing_id' => 'required',
                    'from_date' => 'required',
                    'charge_per_week' => 'required',
                ]);

                if ($resident->updateBillingInformation($request->all()))
                {
                    session()->flash('success', 'Billing information has been updated');
                    return redirect()->route('care-home.resident.finance', [$id, $request->action]);
                }

                session()->flash('danger', 'There was an error updating billing information. Probably the total charge per week did not match the amount charged to individuals.');
                return redirect()->route('care-home.resident.finance', [$id, $request->action]);
                break;
        }
    }

    public function index(): View
    {
        $args              = [];
        $args['locations'] = Location::rentable()->get();
        foreach ($args['locations'] as $location)
        {
            $args['rentals'][$location->id] = [];
            foreach ($location->rentals()->current()->get() as $rental)
            {
                $args['rentals'][$location->id][] = $rental;
            }
        }
        $args['users']     = User::pluck('name', 'id')->toArray();

        return view('care-home.resident.index', $args);
    }

    public function historic(): View
    {
        $args              = [];
        $args['residents'] = Resident::all();

        return view('care-home.resident.historic', $args);
    }

    public function show(string $id): View
    {
        $args = [];
        $args['resident']   = Resident::find($id);
        $args['rental']     = $args['resident']->user->rentals()->current()->first();
        $args['eolWishes']  = $args['resident']->endOfLifeWishes()->orderBy('id', 'desc')->first();
        $args['fallAssess'] = $args['resident']->fallAssessments()->orderBy('id', 'desc')->first();

        return view('care-home.resident.show', $args);
    }
}
