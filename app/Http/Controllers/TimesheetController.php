<?php

namespace App\Http\Controllers;

use App\Models\HR\Appointment;
use App\Models\Time\Category;
use App\Models\Time\Timesheet;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\View\View;

class TimesheetController extends Controller
{
    public function authorise(): View
    {
        $args = [];
        $args['selectedAppointment'] = Appointment::find(session()->get('appointment_id'));
        $args['timesheets']          = $args['selectedAppointment']->timesheetsToAuthorise();

        return view('timesheet.authorise', $args);
    }

    public function authorisePost(Request $request): \Illuminate\Http\RedirectResponse
    {
        $appointment = Appointment::find(session()->get('appointment_id'));
        $timesheets  = $appointment->timesheetsToAuthorise();

        foreach ($request->approve_reject as $timesheetID => $approveOrReject)
        {
            $timesheets->find($timesheetID)->approveOrReject($approveOrReject, $appointment->id);
        }

        session()->flash('success', 'The selected timesheets have been authorised / rejected as requested.');

        return redirect()->route('timesheet.authorise');
    }

    public function create(?string $date = ''): View
    {
        $args = [];
        $args['selectedAppointment'] = Appointment::find(session()->get('appointment_id'));
        $args['categories']          = Category::pluck('name', 'id');

        if ($date)
        {
            $date = date('Y-m-d', strtotime($date));
        } else {
            $date = date('Y-m-d');
        }

        $args['date'] = $date;
        $args['todayTimesheets'] = Timesheet::where('appointment_id', session()->get('appointment_id'))
            ->where('date_of_timesheet', $date)
            ->get();

        $args['totalTimeLogged'] = time_string_from_mins($args['todayTimesheets']->sum('mins_logged'));

        return view('timesheet.create', $args);
    }

    public function createPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'category_id' => 'required',
            'date_of_timesheet' => 'required',
            'time_hours' => 'required_without:time_mins',
            'time_mins' => 'required_without:time_hours',
        ]);

        // $appointment = Appointment::find(session()->get('appointment_id'));
        // if (Auth::id() != $appointment->user_id)
        // {
        //     abort(403, 'You are not authorised to log time to this appointment.');
        // }

        $data = $request->all();
        $data['appointment_id'] = session()->get('appointment_id');
        $data['mins_logged'] = $request->time_hours * 60 + $request->time_mins;

        Timesheet::create($data);

        session()->flash('success', 'The Timesheet has been created');

        return redirect()->route('timesheet.create', $request->date_of_timesheet);
    }

    public function mine(int $id): View
    {
        $args = [];
        $args['timesheet'] = Appointment::where('user_id', Auth::id())
            ->where('id', $id)
            ->get();

        return view('timesheet.my', $args);
    }
}
