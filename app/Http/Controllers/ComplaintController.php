<?php

namespace App\Http\Controllers;

use App\Events\WorkflightCreated;
use App\Models\Complaint\Complaint;
use App\Models\Workflow\Workflight;
use App\Models\Company;
use App\Models\RolePermission;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ComplaintController extends Controller
{
    public function create(): View
    {
        // No need to do any sort of authentication check here. The only relevant check is the role can access complaint.create.
        $args = [];
        $args['customers']      = User::isCustomer()->get()->pluck('name', 'id');
        $args['companies']      = Complaint::canAccessCompanies('create')->pluck('name', 'id');
        $args['locations']      = Complaint::workflowLocationsForForm();
        $args['users']          = RolePermission::usersWithAccessTo(['complaint.read', 'complaint.mine', 'complaint.edit', 'complaint.admin'])->pluck('name', 'id');
        $args['udfs']           = Complaint::udfsForCreatePage();
        $args['instantlyClose'] = Complaint::canBeInstantlyClosed();

        return view('complaint.create', $args);
    }

    public function createPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'company_id' => 'required',
        ]);

        $data = $request->all();
        $data['user_id'] = Auth::id();

        $complaint = Complaint::create($data);
        $complaint->updateUDFs($data);
        event(new WorkflightCreated($complaint));

        // We need to check if complaints can be closed instantly, and if so, check whether the user has instantly closed it.
        // And if so, then close it.
        if (isset($request->instantly_close))
        {
            $complaint->instantlyClose();
            session()->flash('success', 'The complaint was instantly created and closed.');
            return redirect()->route('complaint.index');
        }

        session()->flash('success', 'The Complaint was created');
        $complaint->assignWorkflightTo($request->user_id);

        return redirect()->route('complaint.edit', $complaint->id);
    }

    public function edit(int $id): View
    {
        $args = [];
        $args['complaint'] = Complaint::find($id);

        // Need to check that the person can access this complaint (i.e. that they can see complaints for this company).
        $args['complaint']->canAccessCompanyOrDie('edit');

        $args['customers'] = User::isCustomer()->get()->pluck('name', 'id');
        $args['companies'] = Complaint::canAccessCompanies('edit')->pluck('name', 'id');
        $args['locations'] = Complaint::workflowLocationsForForm();
        $args['users']     = RolePermission::usersWithAccessTo(['complaint.read', 'complaint.mine', 'complaint.edit', 'complaint.admin'])->pluck('name', 'id');
        $args['udfs']      = $args['complaint']->udfsForEditPage();

        return view('complaint.edit', $args);
    }

    public function editPost(Request $request, int $id): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);

        $complaint = Complaint::find($id);
        $complaint->canAccessCompanyOrDie('edit');

        $complaint->update([
            'name' => $request->name,
            'description' => $request->description,
            'customer_id' => $request->customer_id,
            'location_id' => $request->location_id,
        ]);
        $complaint->updateUDFs($request->all());
        $complaint->transitionToState($request->transition_id);

        $complaint->addComment($request->workflight_comments, $request->workflight_file);

        session()->flash('success', 'The Complaint as been updated');
        $complaint->assignWorkflightTo($request->user_id);

        return redirect()->route('complaint.edit', $complaint->id);
    }

    public function index(Request $request): View
    {
        $defaults = [];
        $defaults['id']          = $request->id;
        $defaults['name']        = $request->name;
        $defaults['description'] = $request->description;
        $defaults['open_closed'] = $request->open_closed;
        $defaults['states_ids']  = $request->states_ids;
        $defaults['users']       = $request->users;
        $defaults['comments']    = $request->comments;

        // Handle the search logic
        $complaints = Complaint::whereIn('company_id', Complaint::canAccessCompanies('show')->pluck('id')->toArray());

        $search = [
            'id'          => ['=', $request->id],
            'name'        => ['LIKE', '%'.$request->name.'%'],
            'description' => ['LIKE', '%'.$request->description.'%'],
        ];
        $complaints = Complaint::search($complaints, $search);

        if ('open' === $request->open_closed)
        {
            $complaints = $complaints->where('is_closed', 0);
        }

        if ('closed' === $request->open_closed)
        {
            $complaints = $complaints->where('is_closed', 1);
        }

        if ($request->states_ids)
        {
            if ($request->states_ids[0] !== null)
            {
                $complaintIDs = Workflight::whereIn('state_id', $request->states_ids)
                    ->where('workflowable_type', 'App\Models\Complaint\Complaint')
                    ->pluck('workflowable_id')
                    ->toArray();
                $complaints = $complaints->whereIn('id', $complaintIDs);
            }
        }

        if ($request->users)
        {
            if ($request->users[0] !== null)
            {
                $complaintIDs = Workflight::whereIn('assigned_to_user_id', $request->users)
                    ->where('workflowable_type', 'App\Models\Complaint\Complaint')
                    ->pluck('workflowable_id')
                    ->toArray();
                $complaints = $complaints->whereIn('id', $complaintIDs);
            }
        }

        $workflow = Complaint::workflow();

        $args = [];
        $args['defaults'] = $defaults;
        $args['complaints']  = $complaints->get();
        $args['states']   = $workflow->states()->pluck('name', 'id');
        $args['users']    = User::pluck('name', 'id');

        // $chartData1 = array_count_values(Complaint::isOpen()
        //     ->pluck('created_at')
        //     ->transform(function ($item) {
        //         return date('M-Y', strtotime($item));
        //     })
        //     ->toArray());

        // $chartData2 = array_count_values(Complaint::with('customer')
        //     ->isOpen()
        //     ->get()
        //     ->pluck('customer.name')
        //     ->toArray());

        // $chartData3 = array_count_values(Complaint::get()
        //     ->pluck('created_at')
        //     ->transform(function ($item) {
        //         return date('M-Y', strtotime($item));
        //     })
        //     ->toArray());

        // $args['charts']   = [
        //     Complaint::getChart('open_complaints_by_age', 'bar', $chartData1),
        //     Complaint::getChart('open_complaints_by_customer', 'pie', $chartData2),
        //     Complaint::getChart('all_complaints_by_month', 'bar', $chartData3),
        // ];

        return view('complaint.index', $args);
    }

    public function mine(): View
    {
        $args = [];
        $args['complaints'] = Complaint::whereHas('workflight', function ($query): void {
            $query->where('assigned_to_user_id', Auth::id());
        })->get();

        return view('complaint.mine', $args);
    }

    public function show(int $id): View
    {
        $args = [];
        $args['complaint'] = Complaint::find($id);
        $args['udfs']   = $args['complaint']->udfsForShowPage();

        $args['complaint']->canAccessCompanyOrDie('show');

        return view('complaint.show', $args);
    }
}
