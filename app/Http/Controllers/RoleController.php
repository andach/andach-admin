<?php

namespace App\Http\Controllers;

use App\Models\Workflow\Transition;
use App\Models\Workflow\Workflow;
use App\Models\Company;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\View\View;

class RoleController extends Controller
{
    public function create(): VIew
    {
        $args = [];
        $args['menuConfig']  = config('menu.menu');
        $args['menuKeys']    = ['read', 'mine', 'create', 'edit', 'admin'];
        $args['transitions'] = Transition::all()->groupBy('workflow_id');
        $args['companies']   = Company::all();
        $args['workflows']   = Workflow::all()->pluck('name', 'id')->toArray();

        return view('role.create', $args);
    }

    public function createPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'name' => 'required',
        ]);

        $role = Role::create($request->all());
        $role->companies()->sync($request->companies);
        $role->transitions()->sync($request->transitions);
        $role->updateRolePermissions(array_keys($request->rolePermission ?? []));

        session()->flash('success', 'The role has been successfully created');

        return redirect()->route('role.edit', $role->id);
    }

    public function edit(int $id): View
    {
        $args = [];
        $args['role']        = Role::with('transitions', 'rolePermissions')->find($id);
        $args['accessTo']    = $args['role']->rolePermissions->pluck('access_to')->toArray();
        $args['menuConfig']  = config('menu.menu');
        $args['menuKeys']    = ['read', 'mine', 'create', 'edit', 'admin'];
        $args['transitions'] = Transition::all()->groupBy('workflow_id');
        $args['companies']   = Company::all();
        $args['workflows']   = Workflow::all()->pluck('name', 'id')->toArray();

        return view('role.edit', $args);
    }

    public function editPost(Request $request, int $id): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'name' => 'required',
        ]);

        $role = Role::find($id);
        $role->update($request->all());
        $role->companies()->sync($request->companies);
        $role->transitions()->sync($request->transitions);
        $role->updateRolePermissions(array_keys($request->rolePermission ?? []));

        session()->flash('success', 'The role has been successfully edited');

        return redirect()->route('role.edit', $id);
    }

    public function index(): View
    {
        $args = [];
        $args['roles'] = Role::all();

        return view('role.index', $args);
    }
}
