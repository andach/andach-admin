<?php

namespace App\Http\Controllers;

use App\Models\Reference\County;
use App\Models\Reference\EthnicOrigin;
use App\Models\Reference\Religion;
use App\Models\Reference\SexualOrientation;
use App\Models\Company;
use App\Models\CompanyUser;
use App\Models\Role;
use App\Models\User;
use Auth;
use Hash;
use Illuminate\Http\Request;
use Illuminate\View\View;

class UserController extends Controller
{
    public function create(): View
    {
        $args = [];
        $args['companies']           = Company::all()->pluck('name', 'id');
        $args['counties']            = County::all()->pluck('name', 'id');
        $args['ethnicOrigins']       = EthnicOrigin::all()->pluck('name', 'id');
        $args['religions']           = Religion::all()->pluck('name', 'id');
        $args['sexualOrientations']  = SexualOrientation::all()->pluck('name', 'id');
        $args['roles']               = Role::all();

        return view('user.create', $args);
    }

    public function createPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'company_id' => 'required',
            'name' => 'required',
            'password' => 'confirmed',
            'picture_path' => 'nullable|image',
        ]);

        $user = User::create($request->all());
        $user->password = Hash::make($request->password);
        $user->roles()->sync($request->roles);
        $user->save();

        $user->updatePicture($request->picture_path);

        session()->flash('success', 'The User has been created');

        return redirect()->route('user.create');
    }

    public function edit(int $id): View
    {
        $args = [];
        $args['companies']           = Company::all()->pluck('name', 'id');
        $args['counties']            = County::all()->pluck('name', 'id');
        $args['ethnicOrigins']       = EthnicOrigin::all()->pluck('name', 'id');
        $args['religions']           = Religion::all()->pluck('name', 'id');
        $args['sexualOrientations']  = SexualOrientation::all()->pluck('name', 'id');
        $args['user']                = User::find($id);
        $args['roles']               = Role::all();

        return view('user.edit', $args);
    }

    public function editPost(Request $request, int $id): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'company_id' => 'required',
            'name' => 'required',
            'password' => 'nullable|confirmed',
            'picture_path' => 'nullable|image',
        ]);

        $user = User::find($id);
        $user->update($request->all());
        $user->updatePassword($request->password);
        $user->roles()->sync($request->roles);
        $user->save();

        if (isset($request->company_pivot_id))
        {
            foreach ($request->company_pivot_id as $companyUserID => $companyID)
            {
                $companyUser = CompanyUser::find($companyUserID);

                $companyUser->update([
                    'company_id' => $companyID,
                    'model_name' => $request->company_model_name[$companyUserID],
                    'permission' => $request->company_permission[$companyUserID],
                ]);
            }
        }

        if ($request->company_new_model_name)
        {
            $user->companyUsers()->create([
                'company_id' => $request->company_new_pivot_id,
                'model_name' => $request->company_new_model_name,
                'permission' => $request->company_new_permission,
            ]);
        }

        if (isset($request->company_pivot_delete_id))
        {
            foreach ($request->company_pivot_delete_id as $deleteCompanyUserID)
            {
                CompanyUser::destroy($deleteCompanyUserID);
            }
        }

        $user->updatePicture($request->picture_path);

        session()->flash('success', 'The User has been edited');

        return redirect()->route('user.edit', $user->id);
    }

    public function index(): View
    {
        $args = [];
        $args['users'] = User::all();

        return view('user.index', $args);
    }

    public function myAccount(): View
    {
        $args = [];
        $args['user']                = Auth::user();
        $args['counties']            = County::all()->pluck('name', 'id');
        $args['ethnicOrigins']       = EthnicOrigin::all()->pluck('name', 'id');
        $args['religions']           = Religion::all()->pluck('name', 'id');
        $args['sexualOrientations']  = SexualOrientation::all()->pluck('name', 'id');

        return view('user.my-account', $args);
    }

    public function myAccountPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'name' => 'required',
            'password' => 'nullable|confirmed',
            'picture_path' => 'nullable|image',
        ]);

        // We want to be careful here. Just because something is on the $fillable array doesn't mean we want to allow to update.
        $data = [];
        $data['name']                  = $request->name;
        $data['preferred_name']        = $request->preferred_name;
        $data['email']                 = $request->email;
        $data['address_1']             = $request->address_1;
        $data['address_2']             = $request->address_2;
        $data['address_3']             = $request->address_3;
        $data['address_4']             = $request->address_4;
        $data['county_id']             = $request->county_id;
        $data['postcode']              = $request->postcode;
        $data['mobile_number']         = $request->mobile_number;
        $data['phone_number']          = $request->phone_number;
        $data['ethnic_origin_id']      = $request->ethnic_origin_id;
        $data['religion_id']           = $request->religion_id;
        $data['sexual_orientation_id'] = $request->sexual_orientation_id;

        $user = Auth::user();
        $user->update($data);
        $user->updatePassword($request->password);
        $user->updatePicture($request->picture_path);

        session()->flash('success', 'You have successfully updated your details.');

        return redirect()->route('user.my-account');
    }

    public function show(int $id): View
    {
        $args = [];
        $args['user'] = User::find($id);

        return view('user.show', $args);
    }
}
