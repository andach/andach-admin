<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Auth;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CompanyController extends Controller
{
    public function index(): View
    {
        $args = [];
        $args['companies'] = Company::all();

        return view('company.index', $args);
    }

    public function indexPost(Request $request): \Illuminate\Http\RedirectResponse
    {
        if (isset($request->name))
        {
            foreach ($request->name as $id => $name)
            {
                $company = Company::find($id);
                $company->name = $name;
                $company->parent_id = $request->parent_id[$id] ?? 0;
                $company->save();
            }
            session()->flash('success', 'The companies have been updated.');
        }

        if ($request->delete_ids)
        {
            if (Auth::user()->is_super_admin)
            {
                Company::whereIn('id', $request->delete_ids)->delete();
            } else {
                session()->flash('error', 'You do not have permission to delete a company. Please contact support to do so.');
            }
        }

        if ($request->new_name)
        {
            Company::create([
                'name' => $request->new_name,
                'parent_id' => $request->new_parent_id ?? 0,
            ]);
            session()->flash('success', 'The companies have been updated and new company created.');
        }

        return redirect()->route('company.index');
    }
}
