<?php

namespace App\Http\Middleware;

use Closure;
use App\Services\TenantManager;

class IdentifyTenant
{
    /**
    * @var App\Services\TenantManager
    */
    protected $tenantManager;
    
    public function __construct(TenantManager $tenantManager)
    {
        $this->tenantManager = $tenantManager;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $host = $request->getHost();
        
        if ($host === env('TENANT_DOMAIN'))
        {
            if (!defined('TENANT_ID'))
            {
                define('TENANT_ID', 0);
            }

            if (!defined('IS_LANDLORD'))
            {
                define('IS_LANDLORD', true);
            }

            return $next($request);
        }

        $pos = strpos($host, env('TENANT_DOMAIN'));
        
        $loadThis = $pos !== false ? substr($host, 0, $pos - 1) : $host;

        if ($this->tenantManager->loadTenant($loadThis, $pos !== false)) 
        {
            $id = $this->tenantManager->getTenant()->id;

            if (!defined('TENANT_ID'))
            {
                define('TENANT_ID', $id);
            }

            if (!defined('IS_LANDLORD'))
            {
                define('IS_LANDLORD', false);
            }

            config()->set('filesystems.disks.public.root', storage_path("app/public/tenant_{$id}" ));
            config()->set('filesystems.disks.public.url', "/storage/tenant_{$id}");

            return $next($request);
        }
        
        throw new NotFoundHttpException;
    }
}