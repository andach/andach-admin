<?php

namespace App\Http\Middleware;

use App\Models\CareHome\Resident;
use App\Models\Location\Location;
use App\Models\User;
use Auth;
use Closure;
use Illuminate\Http\Request;

class InitialSetupLock
{
    public function handle(Request $request, Closure $next)
    {
        // If we have no residents, one location, or only one user then we allow initial setup. 
        if ((User::isStaff()->count() == 1 || Location::count() == 1 || Resident::count() == 0) && Auth::id() == 1)
        {
            return $next($request);
        }

        abort(403);
    }
}
