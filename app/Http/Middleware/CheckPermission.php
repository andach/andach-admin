<?php

namespace App\Http\Middleware;

use App\Models\Role;
use Auth;
use Closure;

class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $requiredAccess)
    {
        if (!Auth::id())
        {
            return redirect()->route('login');
        }

        if (Auth::user()->is_super_admin) {
            return $next($request);
        }
        
        $userRoles = Auth::user()->allRoles();
        if (!Role::canAccess($userRoles, $requiredAccess))
        {
            abort(403, 'Your user does not have the correct access for this page.');
        }

        return $next($request);

        // foreach ($userRoles as $role)
        // {
        //     // dd($role->rolePermissions->pluck('access_to'));
        //     if ($role->rolePermissions->pluck('access_to')->contains($requiredAccess))
        //     {
        //         return $next($request);
        //     }
        // }
        
        // abort(403, 'Your user does not have the correct access for this page.');
    }
}
