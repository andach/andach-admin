<?php

namespace Tests\Unit;

use App\Models\Tenant;
use App\Models\Company;
use App\Models\User;
use Tests\TestCase;

class CompanyTest extends TestCase
{
    public function testAccess()
    {
        // A not logged in user should not have access to any page, with 302 redirects to the login page. 
        $page = $this->get('/company');
        $page->assertStatus(302);

        // The superadmin should be able to see everything. 
        $this->be(User::find(1));
        $page = $this->get('/company');
        $page->assertStatus(200);

        $this->be(User::find(2));
        $page = $this->get('/company');
        $page->assertStatus(403);
    }

    public function testIndex()
    {
        $this->be(User::find(1));
        $page = $this->get('/company');
        $page->assertSee('Setup Companies');
        $page->assertSee('Parent Company');
    }
    
    public function testIndexPost()
    {
        $this->be(User::find(1));
        $page = $this->followingRedirects()->post('/company', [
            'delete_ids' => [4],
            'new_name' => 'PHPUnit New Company Name',
            'new_parent_id' => 1,

            'name' => [
                1 => 'Parent Company',
                2 => 'TEST 2',
                3 => 'TEST 3',
                4 => 'TEST 4',
            ],

            'parent_id' => [
                1 => null,
                2 => 1,
                3 => 2,
                4 => 3,
            ],
        ]);
        $page->assertSee('The companies have been updated and new company created');

        $company = Company::find(1);
        $this->assertEquals($company->name, 'Parent Company');
        $this->assertEquals($company->parent_id, 0);

        $company = Company::find(2);
        $this->assertEquals($company->name, 'TEST 2');
        $this->assertEquals($company->parent_id, 1);

        $company = Company::find(3);
        $this->assertEquals($company->name, 'TEST 3');
        $this->assertEquals($company->parent_id, 2);

        $this->assertSoftDeleted('companies', ['id' => 4]);

        $company = Company::find(5);
        $this->assertEquals($company->name, 'PHPUnit New Company Name');
        $this->assertEquals($company->parent_id, 1);

        // Test that a non-super-admin can't delete companies. 
        $this->be(User::find(1));
        $page = $this->post('/company', [
            'delete_ids' => [4],
            'new_name' => 'PHPUnit New Company Name',
            'new_parent_id' => 1,

            'name' => [
                1 => 'Parent Company',
                2 => 'TEST 2',
                3 => 'TEST 3',
                4 => 'TEST 4',
            ],

            'parent_id' => [
                1 => null,
                2 => 1,
                3 => 2,
                4 => 3,
            ],
        ]);
        $page->assertSee('You do not have permission to delete a company. Please contact support to do so');
    }
}
