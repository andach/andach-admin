<?php

namespace Tests\Unit;

use App\Models\HealthAndSafety\Incident;
use App\Models\Message\Message;
use App\Models\Workflow\Workflow;
use App\Models\User;
use Tests\TestCase;

class HealthAndSafetyTest extends TestCase
{
    public function testCreate()
    {
        // Test that a guest user gets an immediate 302 redirect to the login page.
        $response = $this->get('/health-and-safety-incident/create');
        $response->assertStatus(302);

        // The superadmin sees everything.
        $this->be(User::find(1));
        $response = $this->get('/health-and-safety-incident/create');
        $response->assertSee('Parent Company');
        $response->assertSee('Trading Company');
        $response->assertSee('Property Company');
        $response->assertSee('Dormant Company');

        // Virtually everything else here is tested in tickets as this is a workflowable item. No need to recreate. 
    }

    public function testCreatePost()
    {
        // Test that a guest user gets an immediate 302 redirect to the login page.
        $response = $this->followingRedirects()->post('/health-and-safety-incident/create', ['a' => 1]);
        $response->assertSee('Login');

        $this->be(User::find(1));
        // Test that a correct response gives success
        $response = $this->followingRedirects()->post('/health-and-safety-incident/create', [
            'name' => 'PHPUnit Test Name',
            'description' => 'PHPUnit Test Description',
            'company_id' => 1,
            'enum_kind_of_accident' => 'OTHER',
            'enum_main_factor' => 'OTHER',
            'enum_work_process' => 'OTHER',
        ]);
        $response->assertSee('The Incident was created');
        
        $incident = Incident::orderBy('id', 'desc')->first();
        $this->assertEquals($incident->name, 'PHPUnit Test Name');
        $this->assertEquals($incident->description, 'PHPUnit Test Description');
        $this->assertEquals($incident->company_id, 1);
        $this->assertEquals($incident->enum_kind_of_accident, 'OTHER');
        $this->assertEquals($incident->enum_main_factor, 'OTHER');
        $this->assertEquals($incident->enum_work_process, 'OTHER');
    }

    public function testEdit()
    {
        // Test that a guest user gets an immediate 302 redirect to the login page.
        $response = $this->get('/health-and-safety-incident/1/edit');
        $response->assertStatus(302);
        
        // Virtually everything else here is tested in tickets as this is a workflowable item. No need to recreate. 
    }

    public function testEditPost()
    {
        // Test that invalid response gives errors
        $this->be(User::find(1));
        $response = $this->followingRedirects()->post('/health-and-safety-incident/1/edit', ['a' => 1]);
        $response->assertSee('The name field is required');
        $response->assertSee('The description field is required');
        
        // Virtually everything else here is tested in tickets as this is a workflowable item. No need to recreate. 
    }

    public function testIndex()
    {
        // Test that a guest user gets an immediate 302 redirect to the login page.
        $response = $this->followingRedirects()->get('/complaint');
        $response->assertSee('Login');

        $this->be(User::find(1));
        // Test that a correct response gives success
        $response = $this->followingRedirects()->get('/complaint');
        $response->assertSee('There are no results.');

        // Virtually everything else here is tested in tickets as this is a workflowable item. No need to recreate. 
    }

    public function testNoWorkflow()
    {
        $this->be(User::find(1));
        Workflow::truncate();
        
        $response = $this->get('/health-and-safety-incident/create');
        $response->assertStatus(500);
        $response->assertSee('Error - Workflow Not Found');
        
        $response = $this->get('/complaint');
        $response->assertStatus(500);
        $response->assertSee('Error - Workflow Not Found');
    }
}
