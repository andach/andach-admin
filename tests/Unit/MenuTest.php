<?php

namespace Tests\Unit;

use App\Models\User;
use Tests\TestCase;

class MenuTest extends TestCase
{
    public function testMenuGuest()
    {
        // Guest should just see the login page.
        $homepage = $this->get("/");
        $homepage->assertStatus(302);
    }

    public function testMenu1()
    {
        // Superadmin should have access to everything. 
        $this->be(User::find(1));
        $homepage = $this->get("/");
        $homepage->assertSee("<!-- Menu Header -->Companies", false);
        $homepage->assertSee("<!-- Menu Header -->Locations", false);
        $homepage->assertSee("<!-- Menu Header -->Complaints", false);
        $homepage->assertSee("<!-- Menu Header -->Customers", false);
        $homepage->assertSee("<!-- Menu Header -->H&amp;S Incidents", false);
        // $homepage->assertSee("<!-- Menu Header -->Messages", false);
        $homepage->assertSee("<!-- Menu Header -->Tickets", false);
        $homepage->assertSee("<!-- Menu Header -->Users", false);
        $homepage->assertSee("<!-- Menu Header -->UDFs", false);
        $homepage->assertSee("<!-- Menu Header -->Workflow", false);
        $homepage->assertSee("<!-- Menu Header -->Roles", false);
        $homepage->assertStatus(200);
    }

    public function testMenu2()
    {
        // Ticket Manager should have access to tickets, users and UDFs.
        $this->be(User::find(2));
        $homepage = $this->get("/");
        $homepage->assertStatus(200);
        $homepage->assertDontSee("<!-- Menu Header -->Companies", false);
        $homepage->assertDontSee("<!-- Menu Header -->Locations", false);
        $homepage->assertDontSee("<!-- Menu Header -->Complaints", false);
        $homepage->assertSee("<!-- Menu Header -->Customers", false);
        $homepage->assertDontSee("<!-- Menu Header -->H&amp;S Incidents", false);
        // $homepage->assertSee("<!-- Menu Header -->Messages", false);
        $homepage->assertSee("<!-- Menu Header -->Tickets", false);
        $homepage->assertSee("<!-- Menu Header -->Users", false);
        $homepage->assertSee("<!-- Menu Header -->UDFs", false);
        $homepage->assertSee("<!-- Menu Header -->Workflow", false);
        $homepage->assertDontSee("<!-- Menu Header -->Roles", false);
    }

    public function testMenu3()
    {
        // Complaint Manager should have access to complaints.
        $this->be(User::find(3));
        $homepage = $this->get("/");
        $homepage->assertDontSee("<!-- Menu Header -->Companies", false);
        $homepage->assertDontSee("<!-- Menu Header -->Locations", false);
        $homepage->assertSee("<!-- Menu Header -->Complaints", false);
        $homepage->assertSee("<!-- Menu Header -->Customers", false);
        $homepage->assertDontSee("<!-- Menu Header -->H&amp;S Incidents", false);
        // $homepage->assertSee("<!-- Menu Header -->Messages", false);
        $homepage->assertDontSee("<!-- Menu Header -->Tickets", false);
        $homepage->assertDontSee("<!-- Menu Header -->Users", false);
        $homepage->assertDontSee("<!-- Menu Header -->UDFs", false);
        $homepage->assertDontSee("<!-- Menu Header -->Workflow", false);
        $homepage->assertDontSee("<!-- Menu Header -->Roles", false);
        $homepage->assertStatus(200);
    }

    public function testMenu4()
    {
        // Complaint Employee should have access to complaints and tickets.
        $this->be(User::find(4));
        $homepage = $this->get("/");
        $homepage->assertStatus(200);
        $homepage->assertDontSee("<!-- Menu Header -->Companies", false);
        $homepage->assertDontSee("<!-- Menu Header -->Locations", false);
        $homepage->assertSee("<!-- Menu Header -->Complaints", false);
        $homepage->assertSee("<!-- Menu Header -->Customers", false);
        $homepage->assertDontSee("<!-- Menu Header -->H&amp;S Incidents", false);
        // $homepage->assertSee("<!-- Menu Header -->Messages", false);
        $homepage->assertSee("<!-- Menu Header -->Tickets", false);
        $homepage->assertDontSee("<!-- Menu Header -->Users", false);
        $homepage->assertDontSee("<!-- Menu Header -->UDFs", false);
        $homepage->assertDontSee("<!-- Menu Header -->Workflow", false);
        $homepage->assertDontSee("<!-- Menu Header -->Roles", false);
    }

    public function testMenu5()
    {
        // Health and safety employees should have this and tickets. 
        $this->be(User::find(5));
        $homepage = $this->get("/");
        $homepage->assertStatus(200);
        $homepage->assertDontSee("<!-- Menu Header -->Companies", false);
        $homepage->assertDontSee("<!-- Menu Header -->Locations", false);
        $homepage->assertDontSee("<!-- Menu Header -->Complaints", false);
        $homepage->assertSee("<!-- Menu Header -->Customers", false);
        $homepage->assertSee("<!-- Menu Header -->H&amp;S Incidents", false);
        // $homepage->assertSee("<!-- Menu Header -->Messages", false);
        $homepage->assertSee("<!-- Menu Header -->Tickets", false);
        $homepage->assertDontSee("<!-- Menu Header -->Users", false);
        $homepage->assertDontSee("<!-- Menu Header -->UDFs", false);
        $homepage->assertDontSee("<!-- Menu Header -->Workflow", false);
        $homepage->assertDontSee("<!-- Menu Header -->Roles", false);
    }

    public function testMenu9()
    {
        // Receptionist should have access to complaints, incidents, users and tickets.
        $this->be(User::find(9));
        $homepage = $this->get("/");
        $homepage->assertStatus(200);
        $homepage->assertDontSee("<!-- Menu Header -->Companies", false);
        $homepage->assertDontSee("<!-- Menu Header -->Locations", false);
        $homepage->assertSee("<!-- Menu Header -->Complaints", false);
        $homepage->assertSee("<!-- Menu Header -->Customers", false);
        $homepage->assertSee("<!-- Menu Header -->H&amp;S Incidents", false);
        // $homepage->assertSee("<!-- Menu Header -->Messages", false);
        $homepage->assertSee("<!-- Menu Header -->Tickets", false);
        $homepage->assertSee("<!-- Menu Header -->Users", false);
        $homepage->assertDontSee("<!-- Menu Header -->UDFs", false);
        $homepage->assertDontSee("<!-- Menu Header -->Workflow", false);
        $homepage->assertDontSee("<!-- Menu Header -->Roles", false);
    }

    public function testMenu10()
    {
        // Pleb should have access to view users only.
        $this->be(User::find(10));
        $homepage = $this->get("/");
        $homepage->assertStatus(200);
        $homepage->assertDontSee("<!-- Menu Header -->Companies", false);
        $homepage->assertDontSee("<!-- Menu Header -->Locations", false);
        $homepage->assertDontSee("<!-- Menu Header -->Complaints", false);
        $homepage->assertSee("<!-- Menu Header -->Customers", false);
        $homepage->assertDontSee("<!-- Menu Header -->H&amp;S Incidents", false);
        // $homepage->assertSee("<!-- Menu Header -->Messages", false);
        $homepage->assertSee("<!-- Menu Header -->Tickets", false);
        $homepage->assertSee("<!-- Menu Header -->Users", false);
        $homepage->assertDontSee("<!-- Menu Header -->UDFs", false);
        $homepage->assertDontSee("<!-- Menu Header -->Workflow", false);
        $homepage->assertDontSee("<!-- Menu Header -->Roles", false);
    }
}
