<?php

namespace Tests\Unit;

use App\Models\CareHome\Resident;
use App\Models\Location\Location;
use App\Models\User;
use Tests\TestCase;

class InitialSetupTest extends TestCase
{
    public function testAccess()
    {
        // A guest user should be redirected to login. 
        $page = $this->followingRedirects()->get('/initial-setup');
        $page->assertSee('Login');

        // An admin user on the default seeded database should be disallowed.
        $this->be(User::find(1));
        $page = $this->followingRedirects()->get('/initial-setup');
        $page->assertStatus(403);

        // As should any non-admin user. 
        $this->be(User::find(2));
        $page = $this->followingRedirects()->get('/initial-setup');
        $page->assertStatus(403);

        // Now we delete out everything and see if we can run initial setup. 
        User::where('id', '>', 1)->delete();
        Location::where('id', '>', 1)->delete();
        Resident::truncate();

        // NOW we should be able to see this. It should redirect to locations. 
        $this->be(User::find(1));
        $page = $this->followingRedirects()->get('/initial-setup');
        $page->assertStatus(200);
        $page->assertSee('<h1>Initial Setup - Locations</h1>', false);
    }

    public function testLocationPost()
    {
        User::where('id', '>', 1)->delete();
        Location::where('id', '>', 1)->delete();
        Resident::truncate();
        
        $this->be(User::find(1));
        $page = $this->followingRedirects()->post('/initial-setup/location', [
            'setupType' => 'numbers',
            'number_of_rooms' => 7,
        ]);
        // $page->assertSee('Success: 7 rooms have been set up');
        $this->assertEquals(8, Location::count());
        
        Location::where('id', '>', 1)->delete();
        $page = $this->followingRedirects()->post('/initial-setup/location', [
            'setupType' => 'manual',
            'manual_names' => 'A
            B
            C
            D',
        ]);
        // $page->assertSee('Success: Your rooms have been set up');
        $this->assertEquals(5, Location::count());
        
        Location::where('id', '>', 1)->delete();
        $page = $this->followingRedirects()->post('/initial-setup/location', [
            'setupType' => 'wings',
            'wing_names' => 'Red Wing,Room 1
            Red Wing,Room 2
            Red Wing,Room 3
            Blue Wing,Room 4
            Blue Wing,Room 4a
            Blue Wing,Room 5
            Blue Wing,Room 6',
        ]);
        // $page->assertSee('Success: Your wings/corridors and rooms have been set up');
        $this->assertEquals(10, Location::count());
        $this->assertEquals(2, Location::where('parent_id', 1)->count());
        
        $redWingID = Location::where('name', 'Red Wing')->first()->id;
        $this->assertEquals(3, Location::where('parent_id', $redWingID)->count());
        
        $blueWingID = Location::where('name', 'Blue Wing')->first()->id;
        $this->assertEquals(4, Location::where('parent_id', $blueWingID)->count());
    }
}
