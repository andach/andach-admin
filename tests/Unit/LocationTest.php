<?php

namespace Tests\Unit;

use App\Models\Location\Location;
use App\Models\User;
use Tests\TestCase;

class LocationTest extends TestCase
{
    public function testAccess()
    {
        // A not logged in user should not have access to any page, with 302 redirects to the login page. 
        $page = $this->get('/location');
        $page->assertStatus(302);

        // The superadmin should be able to see everything. 
        $this->be(User::find(1));
        $page = $this->get('/location');
        $page->assertStatus(200);

        $this->be(User::find(2));
        $page = $this->get('/location');
        $page->assertStatus(403);
    }
    
    public function testCreate()
    {
        $this->be(User::find(1));
        $page = $this->followingRedirects()->get('/location/ajax/create/0/AAA');
        $page->assertSee('yep that worked');

        $newLocation = Location::orderBy('id', 'desc')->first();
        $this->assertEquals($newLocation->parent_id, 0);
        $this->assertEquals($newLocation->name, 'AAA');
    }

    public function testIndex()
    {
        $this->be(User::find(1));
        $page = $this->get('/location');
        $page->assertSee('All Locations');
    }
}
