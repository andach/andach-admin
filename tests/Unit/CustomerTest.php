<?php

namespace Tests\Unit;

use App\Models\User;
use Hash;
use Tests\TestCase;

class CustomerTest extends TestCase
{
    public function testAccess()
    {
        // A not logged in user should not have access to any page, with 302 redirects to the login page. 
        $page = $this->get('/customer');
        $page->assertStatus(302);
        $page = $this->get('/customer/create');
        $page->assertStatus(302);
        $page = $this->get('/customer/1');
        $page->assertStatus(302);
        $page = $this->get('/customer/1/edit');
        $page->assertStatus(302);
        $page = $this->post('/customer/create', []);
        $page->assertStatus(302);
        $page = $this->post('/customer/1/edit', []);
        $page->assertStatus(302);

        // The superadmin should be able to see everything. 
        $this->be(User::find(1));
        $page = $this->get('/customer');
        $page->assertStatus(200);
        $page = $this->get('/customer/create');
        $page->assertStatus(200);
        $page = $this->get('/customer/1');
        $page->assertStatus(403);
        $page = $this->get('/customer/1/edit');
        $page->assertStatus(403);
        $page = $this->get('/customer/11');
        $page->assertStatus(200);
        $page = $this->get('/customer/11/edit');
        $page->assertStatus(200);

        // As should the rest of the staff. 
        $this->be(User::find(2));
        $page = $this->get('/customer');
        $page->assertStatus(200);
        $page = $this->get('/customer/create');
        $page->assertStatus(200);
        $page = $this->get('/customer/1');
        $page->assertStatus(403);
        $page = $this->get('/customer/1/edit');
        $page->assertStatus(403);
        $page = $this->get('/customer/11');
        $page->assertStatus(200);
        $page = $this->get('/customer/11/edit');
        $page->assertStatus(200);

        // User 11 does not have access to customers (because they're not a staff member). 
        $this->be(User::find(11));
        $page = $this->get('/customer');
        $page->assertStatus(403);
        $page = $this->get('/customer/create');
        $page->assertStatus(403);
        $page = $this->get('/customer/1');
        $page->assertStatus(403);
        $page = $this->get('/customer/1/edit');
        $page->assertStatus(403);
        $page = $this->get('/customer/11');
        $page->assertStatus(403);
        $page = $this->get('/customer/11/edit');
        $page->assertStatus(403);
    }

    public function testCreate()
    {
        $this->be(User::find(1));
        $page = $this->get('/customer/create');
        $page->assertSee('<div class="card-header">Customer Details</div>', false);
        
        $this->be(User::find(3));
        $page = $this->get('/customer/create');
        $page->assertSee('<div class="card-header">Customer Details</div>', false);
        
        $this->be(User::find(11));
        $page = $this->get('/customer/create');
        $page->assertStatus(403);
    }

    public function testCreatePost()
    {
        // Check that posting no data results in an error. 
        $this->be(User::find(1));
        $page = $this->followingRedirects()->post('/customer/create', ['a' => 1]);
        $page->assertSee('The name field is required');

        // Check that invalid data results in an error
        $page = $this->followingRedirects()->post('/customer/create', [
            'password' => '12345678',
        ]);
        $page->assertSee('The company id field is required');
        $page->assertSee('The name field is required');
        $page->assertSee('The password confirmation does not match');

        // Check that posting valid data results in a success response. 
        $page = $this->followingRedirects()->post('/customer/create', [
            'company_id' => 1,
            'name' => 'PHPUnitCreate Name',
            'preferred_name' => 'PHPUnitCreate Preferred Name',
            'email' => 'PHPUnitCreate Email',
            'password' => '12345678',
            'password_confirmation' => '12345678',

            'address_1' => 'PHPUnitCreate Address 1',
            'address_2' => 'PHPUnitCreate Address 2',
            'address_3' => 'PHPUnitCreate Address 3',
            'address_4' => 'PHPUnitCreate Address 4',
            'county_id' => 1,
            'phone_number' => 'PHPUnitCreate Phone Number',
            'mobile_number' => 'PHPUnitCreate Mobile Number',

            'ethnic_origin_id' => 1,
            'religion_id' => 1,
            'sexual_orientation_id' => 1,
        ]);
        $page->assertSee('The Customer has been created');

        // Check that all values have been entered exactly as above. 
        $user = User::orderBy('id', 'desc')->first();
        $this->assertEquals($user->company_id, 1);
        $this->assertEquals($user->name, 'PHPUnitCreate Name');
        $this->assertEquals($user->preferred_name, 'PHPUnitCreate Preferred Name');
        $this->assertEquals($user->email, 'PHPUnitCreate Email');
        $this->assertTrue(Hash::check('12345678', $user->password));
        
        $this->assertEquals($user->address_1, 'PHPUnitCreate Address 1');
        $this->assertEquals($user->address_2, 'PHPUnitCreate Address 2');
        $this->assertEquals($user->address_3, 'PHPUnitCreate Address 3');
        $this->assertEquals($user->address_4, 'PHPUnitCreate Address 4');
        $this->assertEquals($user->county_id, 1);
        $this->assertEquals($user->phone_number, 'PHPUnitCreate Phone Number');
        $this->assertEquals($user->mobile_number, 'PHPUnitCreate Mobile Number');
        
        $this->assertEquals($user->ethnic_origin_id, 1);
        $this->assertEquals($user->religion_id, 1);
        $this->assertEquals($user->sexual_orientation_id, 1);
        
        $this->assertEquals($user->is_customer, 1);
        $this->assertEquals($user->is_staff, 0);
        $this->assertEquals($user->is_supplier, 0);
    }

    public function testEdit()
    {
        $this->be(User::find(1));
        $page = $this->get('/customer/1/edit');
        $page->assertSee('This is not a customer');
        $page->assertStatus(403);
        
        $this->be(User::find(2));
        $page = $this->get('/customer/1/edit');
        $page->assertSee('This is not a customer');
        $page->assertStatus(403);
        
        $this->be(User::find(1));
        $page = $this->get('/customer/11/edit');
        $page->assertSee('<div class="card-header">Customer Details</div>', false);
        
        $this->be(User::find(2));
        $page = $this->get('/customer/11/edit');
        $page->assertSee('<div class="card-header">Customer Details</div>', false);
    }

    public function testEditPost()
    {
        // Check that posting no data results in an error. 
        $this->be(User::find(1));
        $page = $this->followingRedirects()->post('/customer/1/edit', ['a' => 1]);
        $page->assertSee('The company id field is required');
        $page->assertSee('The name field is required');

        // Check that invalid data results in an error
        $page = $this->followingRedirects()->post('/customer/11/edit', [
            'password' => '12345678',
        ]);
        $page->assertSee('The company id field is required');
        $page->assertSee('The name field is required');
        $page->assertSee('The password confirmation does not match');

        // Check that posting valid data to a non-customer results in failure. 
        $page = $this->followingRedirects()->post('/customer/1/edit', [
            'company_id' => 2,
            'name' => 'PHPUnitEdit Name',
            'preferred_name' => 'PHPUnitEdit Preferred Name',
            'email' => 'PHPUnitEdit Email',
            'password' => 'aaaaaaaa',
            'password_confirmation' => 'aaaaaaaa',

            'address_1' => 'PHPUnitEdit Address 1',
            'address_2' => 'PHPUnitEdit Address 2',
            'address_3' => 'PHPUnitEdit Address 3',
            'address_4' => 'PHPUnitEdit Address 4',
            'county_id' => 1,
            'phone_number' => 'PHPUnitEdit Phone Number',
            'mobile_number' => 'PHPUnitEdit Mobile Number',

            'ethnic_origin_id' => 1,
            'religion_id' => 1,
            'sexual_orientation_id' => 1,
        ]);
        $page->assertSee('This is not a customer');
        $page->assertStatus(403);

        // Check that posting valid data results in a success response. 
        $page = $this->followingRedirects()->post('/customer/11/edit', [
            'company_id' => 2,
            'name' => 'PHPUnitEdit Name',
            'preferred_name' => 'PHPUnitEdit Preferred Name',
            'email' => 'PHPUnitEdit Email',
            'password' => 'aaaaaaaa',
            'password_confirmation' => 'aaaaaaaa',

            'address_1' => 'PHPUnitEdit Address 1',
            'address_2' => 'PHPUnitEdit Address 2',
            'address_3' => 'PHPUnitEdit Address 3',
            'address_4' => 'PHPUnitEdit Address 4',
            'county_id' => 1,
            'phone_number' => 'PHPUnitEdit Phone Number',
            'mobile_number' => 'PHPUnitEdit Mobile Number',

            'ethnic_origin_id' => 1,
            'religion_id' => 1,
            'sexual_orientation_id' => 1,
        ]);
        $page->assertSee('The Customer has been edited');

        // Check that all values have been entered exactly as above. 
        $user = User::find(11);
        $this->assertEquals($user->company_id, 2);
        $this->assertEquals($user->name, 'PHPUnitEdit Name');
        $this->assertEquals($user->preferred_name, 'PHPUnitEdit Preferred Name');
        $this->assertEquals($user->email, 'PHPUnitEdit Email');
        $this->assertTrue(Hash::check('aaaaaaaa', $user->password));
        
        $this->assertEquals($user->address_1, 'PHPUnitEdit Address 1');
        $this->assertEquals($user->address_2, 'PHPUnitEdit Address 2');
        $this->assertEquals($user->address_3, 'PHPUnitEdit Address 3');
        $this->assertEquals($user->address_4, 'PHPUnitEdit Address 4');
        $this->assertEquals($user->county_id, 1);
        $this->assertEquals($user->phone_number, 'PHPUnitEdit Phone Number');
        $this->assertEquals($user->mobile_number, 'PHPUnitEdit Mobile Number');
        
        $this->assertEquals($user->ethnic_origin_id, 1);
        $this->assertEquals($user->religion_id, 1);
        $this->assertEquals($user->sexual_orientation_id, 1);
        
        $this->assertEquals($user->is_customer, 1);
        $this->assertEquals($user->is_staff, 0);
        $this->assertEquals($user->is_supplier, 0);
    }

    public function testIndex()
    {
        $this->be(User::find(1));
        $page = $this->get('/customer');
        $page->assertSee('<h1>All Customers</h1>', false);
    }

    public function testShow()
    {
        $this->be(User::find(1));
        $page = $this->get('/customer/1');
        $page->assertStatus(403);
        $page->assertSee('This is not a customer');
        
        $this->be(User::find(1));
        $page = $this->get('/customer/11');
        $page->assertSee('<h1>Show Customer - The Auditor</h1>', false);
    }
}
