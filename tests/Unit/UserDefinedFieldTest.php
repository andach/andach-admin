<?php

namespace Tests\Unit;

use App\Models\UDF\UDF;
use App\Models\User;
use Tests\TestCase;

class UserDefinedFieldTest extends TestCase
{
    public function testCreate()
    {
        // Test that a guest user gets an immediate 302 redirect to the login page. 
        $response = $this->get('/user-defined-field/create');
        $response->assertStatus(302);

        // User 1 and 2 can see the pages, but user 3 can't. 
        $this->be(User::find(1));
        $response = $this->get('/user-defined-field/create');
        $response->assertStatus(200);
        $response->assertSee('<h1>Create User Defined Field</h1>', false);
        
        $this->be(User::find(2));
        $response = $this->get('/user-defined-field/create');
        $response->assertStatus(200);
        $response->assertSee('<h1>Create User Defined Field</h1>', false);
        
        $this->be(User::find(3));
        $response = $this->get('/user-defined-field/create');
        $response->assertStatus(403);
    }

    public function testCreatePost()
    {
        // Test that incomplete data gives error. 
        $this->be(User::find(1));
        $response = $this->followingRedirects()->post('/user-defined-field/create', [
            'a' => 1,
        ]);
        $response->assertStatus(200);
        $response->assertSee('The name field is required');
        $response->assertSee('The display name field is required');
        $response->assertSee('The type field is required');
        $response->assertSee('The model name field is required');

        // Test that complete data works fine. 
        $this->be(User::find(1));
        $response = $this->followingRedirects()->post('/user-defined-field/create', [
            'name' => 'PHPUnit UDF Name',
            'display_name' => 'PHPUnit Display Name',
            'type' => 'list',
            'model_name' => 'App\Models\Ticket\Ticket',
            'description' => 'PHPUnit Description',
            'items' => 'one,two,three',
        ]);
        $response->assertStatus(200);
        $response->assertSee('The User Defined Field has been added');

        $udf = UDF::orderBy('id', 'desc')->first();
        $this->assertEquals($udf->name, 'PHPUnit UDF Name');
        $this->assertEquals($udf->display_name, 'PHPUnit Display Name');
        $this->assertEquals($udf->type, 'list');
        $this->assertEquals($udf->model_name, 'App\Models\Ticket\Ticket');
        $this->assertEquals($udf->description, 'PHPUnit Description');

        $response = $this->get('/ticket/create');
        $response->assertSee('PHPUnit UDF Name');

        $itemArr = [];
        foreach ($udf->items as $item)
        {
            $itemArr[] = $item->name;
        }
        $this->assertEquals($itemArr, ['one', 'two', 'three']);
    }

    public function testEdit()
    {
        // Test that a guest user gets an immediate 302 redirect to the login page. 
        $response = $this->get('/user-defined-field/1/edit');
        $response->assertStatus(302);

        // User 1 and 2 can see the pages, but user 3 can't. 
        $this->be(User::find(1));
        $response = $this->get('/user-defined-field/1/edit');
        $response->assertStatus(200);
        $response->assertSee('<h1>Edit User Defined Field</h1>', false);
        
        $this->be(User::find(2));
        $response = $this->get('/user-defined-field/1/edit');
        $response->assertStatus(200);
        $response->assertSee('<h1>Edit User Defined Field</h1>', false);
        
        $this->be(User::find(3));
        $response = $this->get('/user-defined-field/1/edit');
        $response->assertStatus(403);
    }

    public function testEditPost()
    {
        // Test that incomplete data gives error. 
        $this->be(User::find(1));
        $response = $this->followingRedirects()->post('/user-defined-field/1/edit', [
            'a' => 1,
        ]);
        $response->assertStatus(200);
        $response->assertSee('The name field is required');
        $response->assertSee('The display name field is required');

        // Test that complete data works. 
        $this->be(User::find(1));
        $response = $this->followingRedirects()->post('/user-defined-field/1/edit', [
            'name' => 'PHPUnit Name',
            'display_name' => 'PHPUnit Display Name',
            'delete_values' => [1, 2],
            'newItems' => 'apple,banana',
        ]);

        $response->assertStatus(200);
        $response->assertSee('The User Defined Field has been edited');

        $udf = UDF::find(1);
        $this->assertEquals($udf->name, 'PHPUnit Name');
        $this->assertEquals($udf->display_name, 'PHPUnit Display Name');
        
        $itemArr = [];
        foreach ($udf->items as $item)
        {
            $itemArr[] = $item->name;
        }
        $this->assertEquals($itemArr, ['High', 'Urgent', 'apple', 'banana']);

        // Test that posting just the bare minimum amount of data does nothing. 
        $this->be(User::find(1));
        $response = $this->followingRedirects()->post('/user-defined-field/1/edit', [
            'name' => 'PHPUnit Name',
            'display_name' => 'PHPUnit Display Name',
        ]);

        $response->assertStatus(200);
        $response->assertSee('The User Defined Field has been edited');

        $udf = UDF::find(1);
        $this->assertEquals($udf->name, 'PHPUnit Name');
        $this->assertEquals($udf->display_name, 'PHPUnit Display Name');
        
        $itemArr = [];
        foreach ($udf->items as $item)
        {
            $itemArr[] = $item->name;
        }
        $this->assertEquals($itemArr, ['High', 'Urgent', 'apple', 'banana']);
    }

    public function testIndex()
    {
        // Test that a guest user gets an immediate 302 redirect to the login page. 
        $response = $this->get('/user-defined-field');
        $response->assertStatus(302);

        // User 1 and 2 can see the pages, but user 3 can't. 
        $this->be(User::find(1));
        $response = $this->get('/user-defined-field');
        $response->assertStatus(200);
        $response->assertSee('<h1>All User Defined Fields</h1>', false);
        $response->assertSee('Severity');
        $response->assertSee('Phone Number');
        $response->assertSee('Date of Question');
        $response->assertSee('Number of Children');
        $response->assertSee('Likelihood of Harm');
        $response->assertSee('Current Address');
        
        $this->be(User::find(2));
        $response = $this->get('/user-defined-field');
        $response->assertStatus(200);
        $response->assertSee('<h1>All User Defined Fields</h1>', false);
        
        $this->be(User::find(3));
        $response = $this->get('/user-defined-field');
        $response->assertStatus(403);
    }
}
