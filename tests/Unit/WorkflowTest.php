<?php

namespace Tests\Unit;

use App\Models\Workflow\State;
use App\Models\Workflow\Transition;
use App\Models\Workflow\Workflow;
use App\Models\User;
use Tests\TestCase;

class WorkflowTest extends TestCase
{
    public function testCreate()
    {
        // Test that a guest user gets an immediate 302 redirect to the login page.
        $response = $this->get('/workflow/create');
        $response->assertStatus(302);

        // The superadmin sees everything.
        $this->be(User::find(1));
        $response = $this->get('/workflow/create');
        $response->assertSee('<h1>Create Workflow</h1>', false);

        // User 2 can see workflows.
        $this->be(User::find(2));
        $response = $this->get('/workflow/create');
        $response->assertSee('<h1>Create Workflow</h1>', false);

        // User 3 does not have access to workflows.
        $this->be(User::find(3));
        $response = $this->get('/workflow/create');
        $response->assertStatus(403);
    }

    public function testCreatePost()
    {
        $this->be(User::find(1));
        // Test that a correct response gives success.
        $response = $this->followingRedirects()->post('/workflow/create', [
            'name' => 'PHPUnit Test Name',
            'model_name' => 'App\Models\Test\Test',
            'max_location_level' => 3,
            'min_location_level' => 2,
        ]);
        $response->assertSee('The Workflow has been created');

        $workflow = Workflow::orderBy('id', 'desc')->first();
        $this->assertEquals($workflow->name, 'PHPUnit Test Name');
        $this->assertEquals($workflow->model_name, 'App\Models\Test\Test');
        $this->assertEquals($workflow->max_location_level, 3);
        $this->assertEquals($workflow->min_location_level, 2);

        // Test that invalid response gives an error.
        $response = $this->followingRedirects()->post('/workflow/create', [
            'a' => 1,
        ]);
        $response->assertSee('The name field is required', false);
        $response->assertSee('The model name field is required', false);
        $response->assertSee('The max location level field is required', false);
        $response->assertSee('The min location level field is required', false);
    }

    public function testEdit()
    {
        // Test that a guest user gets an immediate 302 redirect to the login page.
        $response = $this->get('/workflow/1/edit');
        $response->assertStatus(302);

        // The superadmin sees everything.
        $this->be(User::find(1));
        $response = $this->get('/workflow/1/edit');
        $response->assertSee('<h1>Edit Workflow</h1>', false);

        // User 2 can see workflows.
        $this->be(User::find(2));
        $response = $this->get('/workflow/1/edit');
        $response->assertSee('<h1>Edit Workflow</h1>', false);

        // User 3 does not have access to workflows.
        $this->be(User::find(3));
        $response = $this->get('/workflow/1/edit');
        $response->assertStatus(403);
    }

    public function testEditPost()
    {
        $this->be(User::find(1));
        // Test that a valid response gives success
        $response = $this->followingRedirects()->post('/workflow/1/edit', [
            'name' => 'PHPUnit Test Name',
            'model_name' => 'App\Models\Test\Test',
            'max_location_level' => 3,
            'min_location_level' => 2,

            'delete_states' => [],
            
            'transition_name' => [1 => 'Close EDIT'],
            'from_state' => [1 => 1],
            'to_state' => [1 => 2],

            'new_state' => 'Pending',
            'new_state_is_default' => 1,

            'new_transition_name' => 'ReOpen',
            'new_transition_state_from_id' => 2,
            'new_transition_state_to_id' => 1,
        ]);
        $response->assertSee('The Workflow has been edited');

        $workflow = Workflow::find(1);
        $this->assertEquals($workflow->name, 'PHPUnit Test Name');
        // This shouldn't be changed even though we supplied it above. 
        $this->assertEquals($workflow->model_name, 'App\Models\Complaint\Complaint');
        $this->assertEquals($workflow->max_location_level, 3);
        $this->assertEquals($workflow->min_location_level, 2);

        $transition = Transition::find(1);
        $this->assertEquals($transition->name, 'Close EDIT');
        $this->assertEquals($transition->workflow_state_from_id, 1);
        $this->assertEquals($transition->workflow_state_to_id, 2);
        $this->assertEquals($transition->is_default, 0);

        $state = State::orderBy('id', 'desc')->first();
        $this->assertEquals($state->workflow_id, 1);
        $this->assertEquals($state->name, 'Pending');
        $this->assertEquals($state->is_default, 1);
        $this->assertEquals($state->is_closed, 0);

        $transition = Transition::orderBy('id', 'desc')->first();
        $this->assertEquals($transition->name, 'ReOpen');
        $this->assertEquals($transition->workflow_state_from_id, 2);
        $this->assertEquals($transition->workflow_state_to_id, 1);
        
        $response = $this->followingRedirects()->post('/workflow/2/edit', [
            'name' => 'PHPUnit Test Name',
            'model_name' => 'App\Models\Test\Test',
            'max_location_level' => 3,
            'min_location_level' => 2,
            
            'transition_name' => [
                2 => 'Instantly Close',
                3 => 'Send to Manager Review',
                4 => 'More Investivation Required from Manager Review',
                5 => 'Child Safety Review Required',
                6 => 'Close from Manager Review',
                7 => 'More Investivation Required from Child Safety Review',
                8 => 'TEST NEW NAME',
            ],
            'from_state' => [
                2 => 3,
                3 => 3,
                4 => 4,
                5 => 4,
                6 => 4,
                7 => 5,
                8 => 2,
            ],
            'to_state' => [
                2 => 6,
                3 => 4,
                4 => 3,
                5 => 5,
                6 => 6,
                7 => 3,
                8 => 2,
            ],
        ]);
        $response->assertSee('The Workflow has been edited');

        $transition = Transition::find(8);
        $this->assertEquals($transition->name, 'TEST NEW NAME');
        $this->assertEquals($transition->workflow_state_from_id, 2);
        $this->assertEquals($transition->workflow_state_to_id, 2);
    }
}
