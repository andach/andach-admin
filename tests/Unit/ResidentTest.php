<?php

namespace Tests\Unit;

use App\Models\CareHome\Resident;
use App\Models\Role;
use App\Models\User;
use Tests\TestCase;

class ResidentTest extends TestCase
{
    public function testCreate(): void
    {
        $response = $this->get('/role/create');
        $response->assertStatus(302);

        $this->be(User::find(1));
        $response = $this->get('/role/create');
        $response->assertSee('<h1>Create Role</h1>', false);
        $response->assertStatus(200);
    }

    public function testCreatePost(): void
    {
        $this->be(User::find(1));
        $response = $this->followingRedirects()->post('/role/create', [
            'name' => 'PHPUnit Role Name',
            'companies' => [1, 3],
            'transitions' => [4, 5, 6],
            'rolePermission' => [
                'complaint.read' => 1,
                'health-and-safety-incident.mine' => 1,
            ],
        ]);
        $response->assertSee('The role has been successfully created');
        $response->assertStatus(200);

        $role = Role::orderBy('id', 'desc')->first();
        $this->assertEquals($role->name, 'PHPUnit Role Name');
        $this->assertEquals($role->companies->pluck('id')->toArray(), [1, 3]);
        $this->assertEquals($role->transitions->pluck('id')->toArray(), [4, 5, 6]);
        $this->assertEquals($role->rolePermissions->pluck('access_to')->toArray(), ['complaint.read', 'health-and-safety-incident.mine']);
    }

    public function testEdit(): void
    {
        $response = $this->get('/role/1/edit');
        $response->assertStatus(302);

        $this->be(User::find(1));
        $response = $this->get('/role/1/edit');
        $response->assertSee('<h1>Edit Role - Manage Complaints</h1>', false);
        $response->assertStatus(200);

        $this->be(User::find(2));
        $response = $this->followingRedirects()->get('/role/1/edit');
        $response->assertStatus(403);
    }

    public function testEditPost(): void
    {
        $this->be(User::find(1));
        $response = $this->followingRedirects()->post('role/1/edit', [
            'name' => 'PHPUnit Edit Name',
            'companies' => [1, 3],
            'transitions' => [4, 5, 6],
            'rolePermission' => [
                'complaint.read' => 1,
                'health-and-safety-incident.mine' => 1,
            ],
        ]);
        $response->assertSee('The role has been successfully edited');

        $role = Role::find(1);
        $this->assertEquals($role->name, 'PHPUnit Edit Name');
        $this->assertEquals($role->companies->pluck('id')->toArray(), [1, 3]);
        $this->assertEquals($role->transitions->pluck('id')->toArray(), [4, 5, 6]);
        $this->assertEquals($role->rolePermissions->pluck('access_to')->toArray(), ['complaint.read', 'health-and-safety-incident.mine']);
    }

    public function testIndex(): void
    {
        $response = $this->get('/role');
        $response->assertStatus(302);

        $this->be(User::find(1));
        $response = $this->get('/role');
        $response->assertSee('<h1>Roles</h1>', false);
        $response->assertStatus(200);

        $this->be(User::find(2));
        $response = $this->followingRedirects()->get('/role');
        $response->assertStatus(403);
    }

    public function testShow(): void
    {
        $this->be(User::find(1));
        $response = $this->get('/care-home/resident/aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee');
        $response->assertStatus(200);
        $response->assertSee('Resident Details');
    }
}
