<?php

namespace Tests\Unit;

use App\Models\Complaint\Complaint;
use App\Models\Message\Message;
use App\Models\Workflow\Workflow;
use App\Models\User;
use Tests\TestCase;

class ComplaintTest extends TestCase
{
    public function testCreate()
    {
        // Test that a guest user gets an immediate 302 redirect to the login page.
        $response = $this->get('/complaint/create');
        $response->assertStatus(302);

        // The superadmin sees everything.
        $this->be(User::find(1));
        $response = $this->get('/complaint/create');
        $response->assertSee('Parent Company');
        $response->assertSee('Trading Company');
        $response->assertSee('Property Company');
        $response->assertSee('Dormant Company');

        // Virtually everything else here is tested in tickets as this is a workflowable item. No need to recreate. 
    }

    public function testCreatePost()
    {
        // Test that a guest user gets an immediate 302 redirect to the login page.
        $response = $this->followingRedirects()->post('/complaint/create', ['a' => 1]);
        $response->assertSee('Login');

        $this->be(User::find(1));
        // Test that a correct response gives success
        $response = $this->followingRedirects()->post('/complaint/create', [
            'name' => 'PHPUnit Test Name',
            'description' => 'PHPUnit Test Description',
            'company_id' => 1,
            'customer_id' => 11,
            'location_id' => 3,
        ]);
        $response->assertSee('The Complaint was created');
        
        $complaint = Complaint::orderBy('id', 'desc')->first();
        $this->assertEquals($complaint->name, 'PHPUnit Test Name');
        $this->assertEquals($complaint->description, 'PHPUnit Test Description');
        $this->assertEquals($complaint->company_id, 1);
        $this->assertEquals($complaint->customer_id, 11);
        $this->assertEquals($complaint->location_id, 3);
        
        // Test that assigning to an invalid user succeeds, but does not assign.  
        $response = $this->followingRedirects()->post('/complaint/create', [
            'name' => 'PHPUnit Test Name',
            'description' => 'PHPUnit Test Description',
            'company_id' => 1,
            'customer_id' => 11,
            'location_id' => 3,
            'user_id' => 3,
        ]);
        $response->assertSee('The Complaint was created');
    }

    public function testEdit()
    {
        // Test that a guest user gets an immediate 302 redirect to the login page.
        $response = $this->get('/complaint/1/edit');
        $response->assertStatus(302);
        
        // Virtually everything else here is tested in tickets as this is a workflowable item. No need to recreate. 
    }

    public function testEditPost()
    {
        // Test that invalid response gives errors
        $this->be(User::find(1));
        $response = $this->followingRedirects()->post('/complaint/1/edit', ['a' => 1]);
        $response->assertSee('The name field is required');
        $response->assertSee('The description field is required');
        
        // Virtually everything else here is tested in tickets as this is a workflowable item. No need to recreate. 
    }

    public function testIndex()
    {
        // Test that a guest user gets an immediate 302 redirect to the login page.
        $response = $this->followingRedirects()->get('/complaint');
        $response->assertSee('Login');

        $this->be(User::find(1));
        // Test that a correct response gives success
        $response = $this->followingRedirects()->get('/complaint');
        $response->assertSee('There are no results.');

        // Virtually everything else here is tested in tickets as this is a workflowable item. No need to recreate. 
    }

    public function testNoWorkflow()
    {
        $this->be(User::find(1));
        Workflow::truncate();
        
        $response = $this->get('/complaint/create');
        $response->assertStatus(500);
        $response->assertSee('Error - Workflow Not Found');
        
        $response = $this->get('/complaint');
        $response->assertStatus(500);
        $response->assertSee('Error - Workflow Not Found');
    }
}
