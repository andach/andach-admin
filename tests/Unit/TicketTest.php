<?php

namespace Tests\Unit;

use App\Models\Location\Location;
use App\Models\Message\Message;
use App\Models\Ticket\Ticket;
use App\Models\Workflow\Workflow;
use App\Models\User;
use Tests\TestCase;

class TicketTest extends TestCase
{
    public function testCreate()
    {
        Location::fixTree();

        // Test that a guest user gets an immediate 302 redirect to the login page.
        $response = $this->get('/ticket/create');
        $response->assertStatus(302);

        // The superadmin sees everything.
        $this->be(User::find(1));
        $response = $this->get('/ticket/create');
        $response->assertSee('Parent Company');
        $response->assertSee('Trading Company');
        $response->assertSee('Property Company');
        $response->assertSee('Dormant Company');

        // Check that locations show correctly. Tickets are only for location level 1.
        $response->assertSee('Offices');
        $response->assertSee('Manufacturing');
        $response->assertSee('Warehouses');
        $response->assertDontSee('Ground Floor');
        $response->assertDontSee('Company - Base Location');

        // Finally check that User 1 can see the instant close. 
        $response->assertSee('Instantly Close');

        // User 3 does not have access to tickets.
        $this->be(User::find(3));
        $response = $this->get('/ticket/create');
        $response->assertStatus(403);

        // Test that User 6 (who has access to show and edit tickets) can't create them. 
        $this->be(User::find(6));
        $response = $this->get('/ticket/create');
        $response->assertStatus(403);
    }

    public function testCreatePost()
    {
        // Test that a guest user gets an immediate 302 redirect to the login page.
        $response = $this->followingRedirects()->post('/ticket/create', ['a' => 1]);
        $response->assertSee('Login');

        $this->be(User::find(1));
        // Test that a correct response gives success
        $response = $this->followingRedirects()->post('/ticket/create', [
            'name' => 'PHPUnit Test Name',
            'description' => 'PHPUnit Test Description',
            'company_id' => 1,
            'customer_id' => 11,
            'location_id' => 3,
        ]);
        $response->assertSee('The Ticket was created');
        
        $ticket = Ticket::orderBy('id', 'desc')->first();
        $this->assertEquals($ticket->name, 'PHPUnit Test Name');
        $this->assertEquals($ticket->description, 'PHPUnit Test Description');
        $this->assertEquals($ticket->company_id, 1);
        $this->assertEquals($ticket->customer_id, 11);
        $this->assertEquals($ticket->location_id, 3);
        
        // Test that assigning to an invalid user succeeds, but does not assign.  
        $response = $this->followingRedirects()->post('/ticket/create', [
            'name' => 'PHPUnit Test Name',
            'description' => 'PHPUnit Test Description',
            'company_id' => 1,
            'customer_id' => 11,
            'location_id' => 3,
            'user_id' => 3,
        ]);
        $response->assertSee('The Ticket was created');
        
        // Test that instantly closing works. 
        $response = $this->followingRedirects()->post('/ticket/create', [
            'name' => 'PHPUnit Test Name',
            'description' => 'PHPUnit Test Description',
            'company_id' => 1,
            'customer_id' => 11,
            'location_id' => 3,
            'user_id' => 3,
            'instantly_close' => 1,
        ]);
        $response->assertDontSee('The Ticket was created');
        $response->assertSee('The ticket was instantly created and closed');

        $ticket = Ticket::orderBy('id', 'desc')->first();
        $this->assertEquals($ticket->is_closed, 1);
    }

    public function testEdit()
    {
        // Test that a guest user gets an immediate 302 redirect to the login page.
        $response = $this->get('/ticket/1/edit');
        $response->assertStatus(302);

        $this->be(User::find(1));
        // Test that a correct response gives success
        $response = $this->followingRedirects()->get('/ticket/1/edit');
        $response->assertSee('<h1>Edit Ticket</h1>', false);
        $response->assertSee('Severity');
        $response->assertSee('Phone Number');
        $response->assertSee('Date of Question');
        $response->assertSee('Number of Children');
        $response->assertSee('Likelihood Of Harm');
        $response->assertSee('Current Address');
        $response->assertSee('Comments (Current State: Closed)');
        $response->assertSee('I gave the auditor a call.');
        $response->assertDontSee('Change State');
    }

    public function testEditPost()
    {
        // Test that invalid response gives errors
        $this->be(User::find(1));
        $response = $this->followingRedirects()->post('/ticket/1/edit', ['a' => 1]);
        $response->assertSee('The name field is required');
        $response->assertSee('The description field is required');

        // Test that correct response succeeds
        $this->be(User::find(2));
        $response = $this->followingRedirects()->post('/ticket/2/edit', [
            'name'        => 'PHPUnit Name',
            'description' => 'PHPUnit Description',
            'customer_id' => 2,
            'location_id' => 2,
            'user_id'     => 4,

            'ticket_severity'         => 1,
            'ticket_current_phone'    => '123456',
            'ticket_date_of_question' => '2020-01-01',
            'ticket_children'         => 999,
            'ticket_danger'           => .99,
            'ticket_current_address'  => 'PHPUnit Current Address',

            'transition_id' => 3,
        ]);
        $response->assertSee('The Ticket as been updated');
        $response->assertDontSee('The ticket could not be assigned to the given user');

        $ticket = Ticket::find(2);
        $this->assertEquals($ticket->name, 'PHPUnit Name');
        $this->assertEquals($ticket->description, 'PHPUnit Description');
        $this->assertEquals($ticket->customer_id, 2);
        $this->assertEquals($ticket->location_id, 2);
        $this->assertEquals($ticket->workflight->assigned_to_user_id, 4);
        $this->assertEquals($ticket->udfValuesArray(), [
            1 => 'Low',
            2 => '123456',
            3 => '2020-01-01',
            4 => '999',
            5 => '0.99',
            6 => 'PHPUnit Current Address',
        ]);
        $this->assertEquals($ticket->workflight->state_id, 4);
        $this->assertEquals($ticket->is_closed, 0);

        // Finally check that re-updating with a transition to closed state closes the ticket. 
        $response = $this->followingRedirects()->post('/ticket/2/edit', [
            'name'        => 'PHPUnit Name',
            'description' => 'PHPUnit Description',
            'customer_id' => 0,
            'location_id' => 2,

            'ticket_severity'             => 1,
            'ticket_current_phone'        => '123456',
            'ticket_date_of_question' => '2020-01-01',
            'ticket_children'             => 999,
            'ticket_danger'               => .99,
            'ticket_current_address'      => 'PHPUnit Current Address',

            'transition_id' => 6,
        ]);
        $response->assertSee('The Ticket as been updated');
        
        // Also check that removing the customer didn't break the show page. 
        $response = $this->get('/ticket/2');
        $response->assertStatus(200);
        $response->assertSee('PHPUnit Name');
        
        // Also check that removing the customer didn't break the edit page. 
        $response = $this->get('/ticket/2/edit');
        $response->assertStatus(200);
        $response->assertSee('PHPUnit Name');
        
        $ticket = Ticket::find(2);
        $this->assertEquals($ticket->workflight->state_id, 6);
        $this->assertEquals($ticket->is_closed, 1);
    }

    public function testIndex()
    {
        // Test that a guest user gets an immediate 302 redirect to the login page.
        $response = $this->followingRedirects()->get('/ticket');
        $response->assertSee('Login');

        $this->be(User::find(1));
        // Test that a correct response gives success
        $response = $this->followingRedirects()->get('/ticket');
        $response->assertSee('All Tickets');

        $response = $this->followingRedirects()->call('GET', '/ticket', ['id' => 1]);
        $response->assertSee('Query from the Auditor');
        $response->assertDontSee('Customer wants a call back for their object');
        $response->assertDontSee('Have we seen the customers watch');

        $response = $this->followingRedirects()->call('GET', '/ticket', ['name' => 'ustomer']);
        $response->assertDontSee('Query from the Auditor');
        $response->assertSee('Customer wants a call back for their object');
        $response->assertSee('Have we seen the customers watch');

        $response = $this->followingRedirects()->call('GET', '/ticket', ['description' => 'em']);
        $response->assertSee('Query from the Auditor');
        $response->assertSee('Customer wants a call back for their object');
        $response->assertDontSee('Have we seen the customers watch');

        $response = $this->followingRedirects()->call('GET', '/ticket', ['open_closed' => 'open']);
        $response->assertDontSee('Query from the Auditor');
        $response->assertSee('Customer wants a call back for their object');
        $response->assertSee('Have we seen the customers watch');

        $response = $this->followingRedirects()->call('GET', '/ticket', ['open_closed' => 'closed']);
        $response->assertSee('Query from the Auditor');
        $response->assertDontSee('Customer wants a call back for their object');
        $response->assertDontSee('Have we seen the customers watch');

        $response = $this->followingRedirects()->call('GET', '/ticket', ['users' => [1]]);
        $response->assertSee('Query from the Auditor');
        $response->assertSee('Customer wants a call back for their object');
        $response->assertDontSee('Have we seen the customers watch');

        $response = $this->followingRedirects()->call('GET', '/ticket', ['users' => [2]]);
        $response->assertDontSee('Query from the Auditor');
        $response->assertDontSee('Customer wants a call back for their object');
        $response->assertSee('Have we seen the customers watch');

        $response = $this->followingRedirects()->call('GET', '/ticket', ['states_ids' => [2]]);
        $response->assertSee('Query from the Auditor');
        $response->assertDontSee('Customer wants a call back for their object');
        $response->assertDontSee('Have we seen the customers watch');
    }

    public function testNoEmailWorks()
    {
        $user = User::find(2);
        $user->email = 'invalid';
        $user->save();

        $this->be(User::find(1));

        // Check that adding a comment, when it is assigned to a user with an invalid email, causes no error. 
        $response = $this->followingRedirects()->post('/ticket/3/edit', [
            'name'        => 'PHPUnit Name',
            'description' => 'PHPUnit Description',
            'customer_id' => 0,
            'location_id' => 2,

            'ticket_severity'             => 1,
            'ticket_current_phone'        => '123456',
            'ticket_date_of_question' => '2020-01-01',
            'ticket_children'             => 999,
            'ticket_danger'               => .99,
            'ticket_current_address'      => 'PHPUnit Current Address',

            'workflight_comments' => 'a',
        ]);
        $response->assertStatus(200);
        $response->assertSee('The Ticket as been updated');
    }

    public function testNoWorkflow()
    {
        $this->be(User::find(1));
        Workflow::truncate();
        
        $response = $this->get('/ticket/create');
        $response->assertStatus(500);
        $response->assertSee('Error - Workflow Not Found');
        
        $response = $this->get('/ticket');
        $response->assertStatus(500);
        $response->assertSee('Error - Workflow Not Found');
    }
}
