<?php

namespace Tests\Unit;

use App\Models\User;
use Hash;
use Tests\TestCase;

class UserTest extends TestCase
{
    public function testAccess()
    {
        // A not logged in user should not have access to any page, with 302 redirects to the login page. 
        $page = $this->get('/user');
        $page->assertStatus(302);
        $page = $this->get('/user/create');
        $page->assertStatus(302);
        $page = $this->get('/user/1');
        $page->assertStatus(302);
        $page = $this->get('/user/1/edit');
        $page->assertStatus(302);
        $page = $this->post('/user/create', []);
        $page->assertStatus(302);
        $page = $this->post('/user/1/edit', []);
        $page->assertStatus(302);

        // The superadmin should be able to see everything. 
        $this->be(User::find(1));
        $page = $this->get('/user');
        $page->assertStatus(200);
        $page = $this->get('/user/create');
        $page->assertStatus(200);
        $page = $this->get('/user/1');
        $page->assertStatus(200);
        $page = $this->get('/user/1/edit');
        $page->assertStatus(200);

        // User 3 does not have access to users. 
        $this->be(User::find(3));
        $page = $this->get('/user');
        $page->assertStatus(403);
        $page = $this->get('/user/create');
        $page->assertStatus(403);
        $page = $this->get('/user/1');
        $page->assertStatus(403);
        $page = $this->get('/user/1/edit');
        $page->assertStatus(403);
    }

    public function testCreate()
    {
        $this->be(User::find(1));
        $page = $this->get('/user/create');
        $page->assertSee('<div class="card-header">User Details</div>', false);
        
        $this->be(User::find(3));
        $page = $this->followingRedirects()->get('/user/create');
        $page->assertStatus(403);
    }

    public function testCreatePost()
    {
        // Check that posting no data results in an error. 
        $this->be(User::find(1));
        $page = $this->followingRedirects()->post('/user/create', ['a' => 1]);
        $page->assertSee('The name field is required');

        // Check that invalid data results in an error
        $page = $this->followingRedirects()->post('/user/create', [
            'password' => '12345678',
        ]);
        $page->assertSee('The company id field is required');
        $page->assertSee('The name field is required');
        $page->assertSee('The password confirmation does not match');

        // Check that posting valid data results in a success response. 
        $page = $this->followingRedirects()->post('/user/create', [
            'company_id' => 1,
            'name' => 'PHPUnitCreate Name',
            'preferred_name' => 'PHPUnitCreate Preferred Name',
            'email' => 'PHPUnitCreate Email',
            'password' => '12345678',
            'password_confirmation' => '12345678',

            'address_1' => 'PHPUnitCreate Address 1',
            'address_2' => 'PHPUnitCreate Address 2',
            'address_3' => 'PHPUnitCreate Address 3',
            'address_4' => 'PHPUnitCreate Address 4',
            'county_id' => 1,
            'phone_number' => 'PHPUnitCreate Phone Number',
            'mobile_number' => 'PHPUnitCreate Mobile Number',

            'ethnic_origin_id' => 1,
            'religion_id' => 1,
            'sexual_orientation_id' => 1,
        ]);
        $page->assertSee('The User has been created');

        // Check that all values have been entered exactly as above. 
        $user = User::orderBy('id', 'desc')->first();
        $this->assertEquals($user->company_id, 1);
        $this->assertEquals($user->name, 'PHPUnitCreate Name');
        $this->assertEquals($user->preferred_name, 'PHPUnitCreate Preferred Name');
        $this->assertEquals($user->email, 'PHPUnitCreate Email');
        $this->assertTrue(Hash::check('12345678', $user->password));
        
        $this->assertEquals($user->address_1, 'PHPUnitCreate Address 1');
        $this->assertEquals($user->address_2, 'PHPUnitCreate Address 2');
        $this->assertEquals($user->address_3, 'PHPUnitCreate Address 3');
        $this->assertEquals($user->address_4, 'PHPUnitCreate Address 4');
        $this->assertEquals($user->county_id, 1);
        $this->assertEquals($user->phone_number, 'PHPUnitCreate Phone Number');
        $this->assertEquals($user->mobile_number, 'PHPUnitCreate Mobile Number');
        
        $this->assertEquals($user->ethnic_origin_id, 1);
        $this->assertEquals($user->religion_id, 1);
        $this->assertEquals($user->sexual_orientation_id, 1);
    }

    public function testEdit()
    {
        $this->be(User::find(1));
        $page = $this->get('/user/1/edit');
        $page->assertSee('<div class="card-header">User Details</div>', false);
    }

    public function testEditPost()
    {
        // Check that posting no data results in an error. 
        $this->be(User::find(1));
        $page = $this->followingRedirects()->post('/user/2/edit', ['a' => 1]);
        $page->assertSee('The name field is required');

        // Check that invalid data results in an error
        $page = $this->followingRedirects()->post('/user/2/edit', [
            'password' => '12345678',
        ]);
        $page->assertSee('The company id field is required');
        $page->assertSee('The name field is required');
        $page->assertSee('The password confirmation does not match');

        // Check that posting valid data results in a success response. 
        $page = $this->followingRedirects()->post('/user/2/edit', [
            'company_id' => 2,
            'name' => 'PHPUnitEdit Name',
            'preferred_name' => 'PHPUnitEdit Preferred Name',
            'email' => 'PHPUnitEdit Email',
            'password' => 'aaaaaaaa',
            'password_confirmation' => 'aaaaaaaa',

            'address_1' => 'PHPUnitEdit Address 1',
            'address_2' => 'PHPUnitEdit Address 2',
            'address_3' => 'PHPUnitEdit Address 3',
            'address_4' => 'PHPUnitEdit Address 4',
            'county_id' => 1,
            'phone_number' => 'PHPUnitEdit Phone Number',
            'mobile_number' => 'PHPUnitEdit Mobile Number',

            'ethnic_origin_id' => 1,
            'religion_id' => 1,
            'sexual_orientation_id' => 1,
        ]);
        $page->assertSee('The User has been edited');

        // Check that all values have been entered exactly as above. 
        $user = User::find(2);
        $this->assertEquals($user->company_id, 2);
        $this->assertEquals($user->name, 'PHPUnitEdit Name');
        $this->assertEquals($user->preferred_name, 'PHPUnitEdit Preferred Name');
        $this->assertEquals($user->email, 'PHPUnitEdit Email');
        $this->assertTrue(Hash::check('aaaaaaaa', $user->password));
        
        $this->assertEquals($user->address_1, 'PHPUnitEdit Address 1');
        $this->assertEquals($user->address_2, 'PHPUnitEdit Address 2');
        $this->assertEquals($user->address_3, 'PHPUnitEdit Address 3');
        $this->assertEquals($user->address_4, 'PHPUnitEdit Address 4');
        $this->assertEquals($user->county_id, 1);
        $this->assertEquals($user->phone_number, 'PHPUnitEdit Phone Number');
        $this->assertEquals($user->mobile_number, 'PHPUnitEdit Mobile Number');
        
        $this->assertEquals($user->ethnic_origin_id, 1);
        $this->assertEquals($user->religion_id, 1);
        $this->assertEquals($user->sexual_orientation_id, 1);
    }

    public function testIndex()
    {
        $this->be(User::find(1));
        $page = $this->get('/user');
        $page->assertSee('<h1>All Users</h1>', false);
    }

    public function myAccount()
    {
        $this->be(User::find(1));
        $page = $this->get('/user/my-account');
        $response->assertStatus(200);

        $this->be(User::find(2));
        $page = $this->get('/user/my-account');
        $response->assertStatus(200);

        $this->be(User::find(10));
        $page = $this->get('/user/my-account');
        $response->assertStatus(200);

        $this->be(User::find(3));
        $page = $this->get('/user/my-account');
        $response->assertStatus(302);
    }

    public function myAccountPost()
    {
        $this->be(User::find(1));
        
        // Check that posting valid data results in a success response. 
        $page = $this->followingRedirects()->post('/user/my-account', [
            'company_id' => 2,
            'name' => 'PHPUnitEdit Name',
            'preferred_name' => 'PHPUnitEdit Preferred Name',
            'email' => 'PHPUnitEdit Email',
            'password' => 'zzzzzzzz',
            'password_confirmation' => 'zzzzzzzz',

            'address_1' => 'PHPUnitEdit Address 1',
            'address_2' => 'PHPUnitEdit Address 2',
            'address_3' => 'PHPUnitEdit Address 3',
            'address_4' => 'PHPUnitEdit Address 4',
            'county_id' => 1,
            'phone_number' => 'PHPUnitEdit Phone Number',
            'mobile_number' => 'PHPUnitEdit Mobile Number',

            'ethnic_origin_id' => 1,
            'religion_id' => 1,
            'sexual_orientation_id' => 1,
        ]);
        $page->assertSee('You have successfully updated your details');

        // Check that all values have been entered exactly as above, except from the company ID, which should be unchanged. 
        $user = User::find(1);
        $this->assertEquals($user->company_id, 1);
        $this->assertEquals($user->name, 'PHPUnitEdit Name');
        $this->assertEquals($user->preferred_name, 'PHPUnitEdit Preferred Name');
        $this->assertEquals($user->email, 'PHPUnitEdit Email');
        $this->assertTrue(Hash::check('aaaaaaaa', $user->password));
        
        $this->assertEquals($user->address_1, 'PHPUnitEdit Address 1');
        $this->assertEquals($user->address_2, 'PHPUnitEdit Address 2');
        $this->assertEquals($user->address_3, 'PHPUnitEdit Address 3');
        $this->assertEquals($user->address_4, 'PHPUnitEdit Address 4');
        $this->assertEquals($user->county_id, 1);
        $this->assertEquals($user->phone_number, 'PHPUnitEdit Phone Number');
        $this->assertEquals($user->mobile_number, 'PHPUnitEdit Mobile Number');
        
        $this->assertEquals($user->ethnic_origin_id, 1);
        $this->assertEquals($user->religion_id, 1);
        $this->assertEquals($user->sexual_orientation_id, 1);
    }

    public function testShow()
    {
        $this->be(User::find(1));
        $page = $this->get('/user/1');
        $page->assertSee('<h1>Show User - Super Admin</h1>', false);
    }
}
