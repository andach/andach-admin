<?php

namespace Tests\ToImplement;

use App\Models\Message\Message;
use App\Models\User;
use Tests\TestCase;

class MessageTest extends TestCase
{
    public function testBin(): void
    {
        $this->be(User::find(1));
        $response = $this->get('/message/bin');
        $response->assertSee('<h1>Bin</h1>', false);
        $response->assertDontSee('How are you?</a>', false);
        $response->assertDontSee('Hello Everybody</a>', false);
        $response->assertDontSee('I&#039;m going to be away from work tomorrow</a>', false);
        $response->assertSee('System Generated Test Message</a>', false);
        $response->assertDontSee('<p>There are no messages.</p>', false);
        
        $this->be(User::find(2));
        $response = $this->get('/message/bin');
        $response->assertSee('<h1>Bin</h1>', false);
        $response->assertDontSee('How are you?</a>', false);
        $response->assertDontSee('Hello Everybody</a>', false);
        $response->assertDontSee('I&#039;m going to be away from work tomorrow</a>', false);
        $response->assertDontSee('System Generated Test Message</a>', false);
        $response->assertSee('<p>There are no messages.</p>', false);
    }

    public function testCreate(): void
    {
        // Test that a guest user gets an immediate 302 redirect to the login page.
        $response = $this->get('/message/inbox');
        $response->assertStatus(302);

        // The superadmin sees everything.
        $this->be(User::find(1));
        $response = $this->get('/message/create');
        $response->assertSee('<h1>Create Message</h1>', false);
    }

    public function testCreatePost(): void
    {
        $this->be(User::find(2));
        $response = $this->followingRedirects()->post('/message/create', [
            'name' => 'PHPUnit Message Create Name',
            'description' => 'PHPUnit Message Create Description',
            'user_ids' => [2, 3],
        ]);
        $response->assertSee('Your message has been sent');

        $message = Message::where('name', 'PHPUnit Message Create Name')->first();
        $this->assertEquals($message->name, 'PHPUnit Message Create Name');
        $this->assertEquals($message->description, 'PHPUnit Message Create Description');
        $this->assertEquals($message->toUsers()->pluck('users.id')->toArray(), [2, 3]);

        $response = $this->get('/message/'.$message->id);
        $response->assertSee('PHPUnit Message Create Name');
        $response->assertStatus(200);
    }

    public function testDelete(): void
    {
        $this->be(User::find(1));
        $response = $this->followingRedirects()->post('/message/delete', [
            'message_ids' => ['2b7d6f81-e093-43eb-a88f-c2b93900f00b', '0d489d6d-b0d4-4012-b031-3794d295ab06'],
        ]);

        $messages = User::find(1)->messagesReceived()->whereIn('messages.id', ['2b7d6f81-e093-43eb-a88f-c2b93900f00b', '0d489d6d-b0d4-4012-b031-3794d295ab06'])->get();
        foreach ($messages as $message)
        {
            $this->assertEquals($message->pivot->is_deleted, 1);
        }
    }

    public function testInbox(): void
    {
        // Test that a guest user gets an immediate 302 redirect to the login page.
        $response = $this->get('/message');
        $response->assertStatus(302);
        
        $this->be(User::find(1));
        $response = $this->get('/message');
        $response->assertSee('<h1>Message Inbox</h1>', false);
        $response->assertSee('How are you?</a>', false);
        $response->assertSee('Hello Everybody</a>', false);
        $response->assertSee('I&#039;m going to be away from work tomorrow</a>', false);
        $response->assertDontSee('System Generated Test Message</a>', false);
        $response->assertDontSee('<p>There are no messages.</p>', false);
        
        $this->be(User::find(2));
        $response = $this->get('/message');
        $response->assertSee('<h1>Message Inbox</h1>', false);
        $response->assertDontSee('How are you?</a>', false);
        $response->assertSee('Hello Everybody</a>', false);
        $response->assertSee('I&#039;m going to be away from work tomorrow</a>', false);
        $response->assertDontSee('System Generated Test Message</a>', false);
        $response->assertDontSee('<p>There are no messages.</p>', false);
        
        $this->be(User::find(3));
        $response = $this->get('/message');
        $response->assertSee('<h1>Message Inbox</h1>', false);
        $response->assertDontSee('How are you?</a>', false);
        $response->assertSee('Hello Everybody</a>', false);
        $response->assertDontSee('I&#039;m going to be away from work tomorrow</a>', false);
        $response->assertDontSee('System Generated Test Message</a>', false);
        $response->assertDontSee('<p>There are no messages.</p>', false);
    }

    public function testOutbox(): void
    {
        $this->be(User::find(1));
        $response = $this->get('/message/outbox');
        $response->assertSee('<h1>Message Outbox</h1>', false);
        $response->assertDontSee('How are you?</a>', false);
        $response->assertDontSee('Hello Everybody</a>', false);
        $response->assertDontSee('I&#039;m going to be away from work tomorrow</a>', false);
        $response->assertDontSee('System Generated Test Message</a>', false);
        $response->assertSee('<p>There are no messages.</p>', false);
        
        $this->be(User::find(2));
        $response = $this->get('/message/outbox');
        $response->assertSee('<h1>Message Outbox</h1>', false);
        $response->assertSee('How are you?</a>', false);
        $response->assertDontSee('Hello Everybody</a>', false);
        $response->assertDontSee('I&#039;m going to be away from work tomorrow</a>', false);
        $response->assertDontSee('System Generated Test Message</a>', false);
        $response->assertDontSee('<p>There are no messages.</p>', false);
    }

    public function testShow(): void
    {
        $response = $this->get('/message/2b7d6f81-e093-43eb-a88f-c2b93900f00b');
        $response->assertStatus(302);

        $this->be(User::find(1));
        $response = $this->followingRedirects()->get('/message/2b7d6f81-e093-43eb-a88f-c2b93900f00b');
        $response->assertSee('Hello Everybody');

        $this->be(User::find(2));
        $response = $this->followingRedirects()->get('/message/2b7d6f81-e093-43eb-a88f-c2b93900f00b');
        $response->assertSee('Hello Everybody');

        $this->be(User::find(2));
        $response = $this->followingRedirects()->get('/message/0d489d6d-b0d4-4012-b031-3794d295ab06');
        $response->assertSee('How are you?');

        $this->be(User::find(6));
        $response = $this->followingRedirects()->get('/message/0d489d6d-b0d4-4012-b031-3794d295ab06');
        $response->assertDontSee('How are you?');
        $response->assertStatus(403);
        
        $this->assertTrue(Message::find('2b7d6f81-e093-43eb-a88f-c2b93900f00b')->isReadForUser(1));
        $this->assertTrue(Message::find('2b7d6f81-e093-43eb-a88f-c2b93900f00b')->isReadForUser(2));
        // This one is seeded as read anyway.
        $this->assertTrue(Message::find('0d489d6d-b0d4-4012-b031-3794d295ab06')->isReadForUser(1));
        // This one is seeded as unread.
        $this->assertFalse(Message::find('4e303233-f438-45a1-97eb-01eb7215924b')->isReadForUser(1));
    }
}
