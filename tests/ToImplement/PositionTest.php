<?php

namespace Tests\ToImplement;

use App\Models\HR\Position;
use App\Models\User;
use Tests\TestCase;

class PositionTest extends TestCase
{
    public function testCreatePost(): void
    {
        $this->be(User::find(1));
        $response = $this->followingRedirects()->post('/position/create', [
            'company_id'     => 1,
            'job_id'         => 1,
            'location_id'    => 1,
            'team_id'        => 1,
            'cost_code_id'   => 1,
            'from_date'      => '2020-01-01',
            'hours_per_week' => 999,
        ]);
        $response->assertSee('The Position has been created');
        $response->assertStatus(200);

        $position = Position::orderBy('id', 'desc')->first();
        $this->assertEquals($position->name, '999 h/w of Director for Directors');
        $this->assertEquals($position->company_id, 1);
        $this->assertEquals($position->job_id, 1);
        $this->assertEquals($position->location_id, 1);
        $this->assertEquals($position->cost_code_id, 1);
        $this->assertEquals($position->team_id, 1);
        $this->assertEquals($position->from_date, '2020-01-01');
        $this->assertEquals($position->to_date, null);
        $this->assertEquals($position->hours_per_week, 999);
    }

    public function testEdit(): void
    {
        $response = $this->get('/position/1/edit');
        $response->assertStatus(302);

        $this->be(User::find(1));
        $response = $this->get('/position/1/edit');
        $response->assertSee('<h1>Edit Position</h1>', false);
        $response->assertStatus(200);

        $this->be(User::find(2));
        $response = $this->followingRedirects()->get('/position/1/edit');
        $response->assertStatus(403);
    }

    public function testEditPost(): void
    {
        $this->be(User::find(1));
        $response = $this->followingRedirects()->post('position/1/edit', [
            'company_id'     => 2,
            'job_id'         => 2,
            'location_id'    => 2,
            'team_id'        => 2,
            'cost_code_id'   => 2,
            'from_date'      => '2020-02-01',
            'to_date'        => '2020-02-02',
            'hours_per_week' => 999,
        ]);
        $response->assertSee('The Position has been updated');

        $position = Position::find(1);
        $this->assertEquals($position->name, '999 h/w of Ticket Manager for Tickets');
        $this->assertEquals($position->company_id, 2);
        $this->assertEquals($position->job_id, 2);
        $this->assertEquals($position->location_id, 2);
        $this->assertEquals($position->cost_code_id, 2);
        $this->assertEquals($position->team_id, 2);
        $this->assertEquals($position->from_date, '2020-02-01');
        $this->assertEquals($position->to_date, '2020-02-02');
        $this->assertEquals($position->hours_per_week, 999);
    }

    public function testIndex(): void
    {
        $response = $this->get('/position');
        $response->assertStatus(302);

        $this->be(User::find(1));
        $response = $this->get('/position');
        $response->assertSee('<h1>All Positions</h1>', false);
        $response->assertStatus(200);

        $this->be(User::find(2));
        $response = $this->followingRedirects()->get('/position');
        $response->assertStatus(403);
    }
}
