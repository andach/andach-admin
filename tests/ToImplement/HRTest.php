<?php

namespace Tests\ToImplement;

use App\Models\HR\Job;
use App\Models\HR\Team;
use App\Models\HR\WorkingTimePattern;
use App\Models\Payslip\PayPeriod;
use App\Models\User;
use Tests\TestCase;

class HRTest extends TestCase
{
    public function testJob(): void
    {
        $page = $this->get('/hr/job/1');
        $page->assertStatus(302);

        $this->be(User::find(1));
        $page = $this->get('/hr/job/1');
        $page->assertSee('<h1>Edit Job</h1>', false);
        $page->assertStatus(200);

        $page = $this->followingRedirects()->post('/hr/job/1', [
            'name' => 'PHPUnit Test Name',
            'is_director' => 0,
            'description_of_work' => 'PHPUnit Test Description',
            'fte_hours' => 999,
            'roles' => [1, 2, 3],
        ]);
        $page->assertSee('The Job has been edited');

        $job = Job::find(1);
        $this->assertEquals($job->name, 'PHPUnit Test Name');
        $this->assertEquals($job->is_director, 0);
        $this->assertEquals($job->description_of_work, 'PHPUnit Test Description');
        $this->assertEquals($job->fte_hours, 999);
        $this->assertEquals($job->roles->pluck('id')->toArray(), [1, 2, 3]);
    }

    public function testJobs(): void
    {
        $page = $this->get('/hr/job');
        $page->assertStatus(302);

        $this->be(User::find(1));
        $page = $this->get('/hr/job');
        $page->assertSee('<h1>Setup Jobs</h1>', false);
        $page->assertStatus(200);

        $page = $this->followingRedirects()->post('/hr/job', [
            'name' => 'test',
        ]);
        $page->assertSee('The fte hours field is required when name is present');

        $page = $this->followingRedirects()->post('/hr/job', [
            'name' => 'PHPUnit Test Name',
            'is_director' => 0,
            'description_of_work' => 'PHPUnit Test Description',
            'fte_hours' => 999,
            'delete_ids' => [1, 2],
        ]);
        $page->assertSee('The Jobs have been updated');

        $this->assertSoftDeleted('jobs', ['id' => 1]);
        $this->assertSoftDeleted('jobs', ['id' => 2]);

        $job = Job::orderBy('id', 'desc')->first();
        $this->assertEquals($job->name, 'PHPUnit Test Name');
        $this->assertEquals($job->is_director, 0);
        $this->assertEquals($job->description_of_work, 'PHPUnit Test Description');
        $this->assertEquals($job->fte_hours, 999);
    }

    public function testPayPeriod(): void
    {
        $page = $this->get('/hr/pay-period/1');
        $page->assertStatus(302);

        $this->be(User::find(1));
        $page = $this->get('/hr/pay-period/1');
        $page->assertSee('<h1>Edit Pay Period</h1>', false);
        $page->assertStatus(200);

        $page = $this->followingRedirects()->post('/hr/pay-period/1', [
            'pay_frequency_id' => 10,
            'pay_year_id' => 10,
            'name' => 'PHPUnit Test Name',
            'from_date' => '2000-01-01',
            'to_date' => '2000-02-01',
        ]);
        $page->assertSee('The Pay Period has been edited');

        $payPeriod = PayPeriod::find(1);
        $this->assertEquals($payPeriod->pay_frequency_id, 10);
        $this->assertEquals($payPeriod->pay_year_id, 10);
        $this->assertEquals($payPeriod->name, 'PHPUnit Test Name');
        $this->assertEquals($payPeriod->from_date, '2000-01-01');
        $this->assertEquals($payPeriod->to_date, '2000-02-01');
    }

    public function testPayPeriods(): void
    {
        $page = $this->get('/hr/pay-period');
        $page->assertStatus(302);

        $this->be(User::find(1));
        $page = $this->get('/hr/pay-period');
        $page->assertSee('<h1>Setup Pay Periods</h1>', false);
        $page->assertStatus(200);

        $page = $this->followingRedirects()->post('/hr/pay-period', [
            'name' => 'test',
        ]);
        $page->assertSee('The pay frequency id field is required when name is present');
        $page->assertSee('The pay year id field is required when name is present');
        $page->assertSee('The from date field is required when name is present');
        $page->assertSee('The to date field is required when name is present');

        $page = $this->followingRedirects()->post('/hr/pay-period', [
            'pay_frequency_id' => 10,
            'pay_year_id' => 10,
            'name' => 'PHPUnit Test Name',
            'from_date' => '2000-01-01',
            'to_date' => '2000-02-01',
            'delete_ids' => [1],
        ]);
        $page->assertSee('The Pay Periods have been updated');

        $this->assertSoftDeleted('pay_periods', ['id' => 1]);

        $payPeriod = PayPeriod::orderBy('id', 'desc')->first();
        $this->assertEquals($payPeriod->pay_frequency_id, 10);
        $this->assertEquals($payPeriod->pay_year_id, 10);
        $this->assertEquals($payPeriod->name, 'PHPUnit Test Name');
        $this->assertEquals($payPeriod->from_date, '2000-01-01');
        $this->assertEquals($payPeriod->to_date, '2000-02-01');
    }

    public function testTeam(): void
    {
        $page = $this->get('/hr/team/1');
        $page->assertStatus(302);

        $this->be(User::find(1));
        $page = $this->get('/hr/team/1');
        $page->assertSee('<h1>Edit Team</h1>', false);
        $page->assertStatus(200);

        $page = $this->followingRedirects()->post('/hr/team/1', [
            'name' => 'PHPUnit Test Name',
            'parent_id' => 10,
            'manager_id' => 10,
        ]);
        $page->assertSee('The Team has been edited');

        $team = Team::find(1);
        $this->assertEquals($team->name, 'PHPUnit Test Name');
        $this->assertEquals($team->parent_id, 10);
        $this->assertEquals($team->manager_id, 10);
    }

    public function testTeams(): void
    {
        $page = $this->get('/hr/team');
        $page->assertStatus(302);

        $this->be(User::find(1));
        $page = $this->get('/hr/team');
        $page->assertSee('<h1>Setup Teams</h1>', false);
        $page->assertStatus(200);

        $page = $this->followingRedirects()->post('/hr/team', [
            'name' => 'PHPUnit Test Name',
            'parent_id' => 10,
            'manager_id' => 10,
            'delete_ids' => [1, 2],
        ]);
        $page->assertSee('The Teams have been updated');

        $this->assertSoftDeleted('teams', ['id' => 1]);
        $this->assertSoftDeleted('teams', ['id' => 2]);

        $team = Team::orderBy('id', 'desc')->first();
        $this->assertEquals($team->name, 'PHPUnit Test Name');
        $this->assertEquals($team->parent_id, 10);
        $this->assertEquals($team->manager_id, 10);
    }

    public function testWorkingTimePattern(): void
    {
        $page = $this->get('/hr/working-time-pattern/1');
        $page->assertStatus(302);

        $this->be(User::find(1));
        $page = $this->get('/hr/working-time-pattern/1');
        $page->assertSee('<h1>Edit Working Time Pattern</h1>', false);
        $page->assertStatus(200);

        $page = $this->followingRedirects()->post('/hr/working-time-pattern/1', [
            'name' => 'PHPUnit Test Name',
            'mon_mins' => 10,
            'tue_mins' => 20,
            'wed_mins' => 30,
            'thu_mins' => 40,
            'fri_mins' => 50,
            'sat_mins' => 60,
            'sun_mins' => 70,
        ]);
        $page->assertSee('The Working Time Pattern has been edited');

        $workingTimePattern = WorkingTimePattern::find(1);
        $this->assertEquals($workingTimePattern->name, 'PHPUnit Test Name');
        $this->assertEquals($workingTimePattern->mon_mins, 10);
        $this->assertEquals($workingTimePattern->tue_mins, 20);
        $this->assertEquals($workingTimePattern->wed_mins, 30);
        $this->assertEquals($workingTimePattern->thu_mins, 40);
        $this->assertEquals($workingTimePattern->fri_mins, 50);
        $this->assertEquals($workingTimePattern->sat_mins, 60);
        $this->assertEquals($workingTimePattern->sun_mins, 70);
    }

    public function testWorkingTimePatterns(): void
    {
        $page = $this->get('/hr/working-time-pattern');
        $page->assertStatus(302);

        $this->be(User::find(1));
        $page = $this->get('/hr/working-time-pattern');
        $page->assertSee('<h1>Setup Working Time Patterns</h1>', false);
        $page->assertStatus(200);

        $page = $this->followingRedirects()->post('/hr/working-time-pattern', [
            'name' => 'PHPUnit Test Name',
            'mon_mins' => 10,
            'tue_mins' => 20,
            'wed_mins' => 30,
            'thu_mins' => 40,
            'fri_mins' => 50,
            'sat_mins' => 60,
            'sun_mins' => 70,
            'delete_ids' => [1, 2],
        ]);
        $page->assertSee('The Working Time Patterns have been updated');

        $this->assertSoftDeleted('working_time_patterns', ['id' => 1]);
        $this->assertSoftDeleted('working_time_patterns', ['id' => 2]);

        $workingTimePattern = WorkingTimePattern::orderBy('id', 'desc')->first();
        $this->assertEquals($workingTimePattern->name, 'PHPUnit Test Name');
        $this->assertEquals($workingTimePattern->mon_mins, 10);
        $this->assertEquals($workingTimePattern->tue_mins, 20);
        $this->assertEquals($workingTimePattern->wed_mins, 30);
        $this->assertEquals($workingTimePattern->thu_mins, 40);
        $this->assertEquals($workingTimePattern->fri_mins, 50);
        $this->assertEquals($workingTimePattern->sat_mins, 60);
        $this->assertEquals($workingTimePattern->sun_mins, 70);
    }
}
