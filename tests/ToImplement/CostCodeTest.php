<?php

namespace Tests\ToImplement;

use App\Models\CostCode;
use App\Models\User;
use Hash;
use Tests\TestCase;

class CostCodeTest extends TestCase
{
    public function testAccess()
    {
        // A not logged in user should not have access to any page, with 302 redirects to the login page. 
        $page = $this->get('/cost-code');
        $page->assertStatus(302);
        $page = $this->get('/cost-code/create');
        $page->assertStatus(302);
        $page = $this->get('/cost-code/1');
        $page->assertStatus(302);
        $page = $this->get('/cost-code/1/edit');
        $page->assertStatus(302);
        $page = $this->post('/cost-code/create', []);
        $page->assertStatus(302);
        $page = $this->post('/cost-code/1/edit', []);
        $page->assertStatus(302);

        // The superadmin should be able to see everything. 
        $this->be(User::find(1));
        $page = $this->get('/cost-code');
        $page->assertStatus(200);
        $page = $this->get('/cost-code/create');
        $page->assertStatus(200);
        $page = $this->get('/cost-code/1');
        $page->assertStatus(200);
        $page = $this->get('/cost-code/1/edit');
        $page->assertStatus(200);

        $this->be(User::find(11));
        $page = $this->get('/cost-code');
        $page->assertStatus(403);
        $page = $this->get('/cost-code/create');
        $page->assertStatus(403);
        $page = $this->get('/cost-code/1');
        $page->assertStatus(403);
        $page = $this->get('/cost-code/1/edit');
        $page->assertStatus(403);
    }

    public function testCreate()
    {
        $this->be(User::find(1));
        $page = $this->get('/cost-code/create');
        $page->assertSee('<h1>Create Cost Code</h1>', false);
        
        $this->be(User::find(11));
        $page = $this->get('/cost-code/create');
        $page->assertStatus(403);
    }

    public function testCreatePost()
    {
        // Check that posting no data results in an error. 
        $this->be(User::find(1));
        $page = $this->followingRedirects()->post('/cost-code/create', ['a' => 1]);
        $page->assertSee('The name field is required');

        // Check that posting valid data results in a success response. 
        $page = $this->followingRedirects()->post('/cost-code/create', [
            'name' => 'PHPUnitCreate Name',
            'code' => 'PHPUnitCreate Code',
            'parent_id' => 1,
        ]);
        $page->assertSee('The Cost Code has been created');

        // Check that all values have been entered exactly as above. 
        $costCode = CostCode::orderBy('id', 'desc')->first();
        $this->assertEquals($costCode->name, 'PHPUnitCreate Name');
        $this->assertEquals($costCode->code, 'PHPUnitCreate Code');
        $this->assertEquals($costCode->parent_id, 1);
    }

    public function testEdit()
    {
        $this->be(User::find(1));
        $page = $this->get('/cost-code/1/edit');
        $page->assertSee('<h1>Edit Cost Code</h1>', false);
    }

    public function testEditPost()
    {
        // Check that posting no data results in an error. 
        $this->be(User::find(1));
        $page = $this->followingRedirects()->post('/cost-code/1/edit', ['a' => 1]);
        $page->assertSee('The name field is required');

        // Check that posting valid data results in a success response. 
        $page = $this->followingRedirects()->post('/cost-code/1/edit', [
            'name' => 'PHPUnitCreate Name',
            'code' => 'PHPUnitCreate Code',
            'parent_id' => 2,
        ]);
        $page->assertSee('The Cost Code has been updated');

        // Check that all values have been entered exactly as above. 
        $costCode = CostCode::find(1);
        $this->assertEquals($costCode->name, 'PHPUnitCreate Name');
        $this->assertEquals($costCode->code, 'PHPUnitCreate Code');
        $this->assertEquals($costCode->parent_id, 2);
    }

    public function testIndex()
    {
        $this->be(User::find(1));
        $page = $this->get('/cost-code');
        $page->assertSee('<h1>All Cost Codes</h1>', false);
    }

    public function testShow()
    {
        $this->be(User::find(1));
        $page = $this->get('/cost-code/1');
        $page->assertSee('<h1>Show Cost Code</h1>', false);
    }
}
