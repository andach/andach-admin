<?php

namespace Tests\Landlord;

use Tests\LandlordTestCase;

class LandlordTest extends LandlordTestCase
{
    public function testHomepage(): void
    {
        $page = $this->get('http://'.env('TENANT_DOMAIN').'/');
        $page->assertStatus(200);
        $page->assertSee('01332 854 960');
        $page->assertSee('https://gitlab.com/andach/andach-admin');
    }

    // public function testSignup(): void
    // {
    //     $page = $this->followingRedirects()->post('https://'.env('TENANT_DOMAIN').'/', ['a' => '']);
    //     $page->assertStatus(200);
    //     $page->assertSee('Error: The name field is required');
    //     $page->assertSee('Error: The email field is required');
    //     $page->assertSee('Error: The company field is required');
    //     $page->assertSee('Error: The password field is required');
    //     $page->assertSee('Error: The subdomain field is required');
        
    //     $page = $this->followingRedirects()->post('https://'.env('TENANT_DOMAIN').'/', [
    //         'email' => false,
    //         'password' => 'a',
    //         'subdomain' => '!',
    //     ]);
    //     dd($page);
    //     $page->assertStatus(200);
    //     $page->assertSee('Error: The email must be a valid email address');
    //     $page->assertSee('Error: The password confirmation does not match');
    //     $page->assertSee('Error: The subdomain may only contain letters and numbers');
        
    //     $page = $this->followingRedirects()->post('https://'.env('TENANT_DOMAIN').'/', [
    //         'name'                  => 'aaa',
    //         'email'                 => 'bbb@ccc.com',
    //         'company'               => 'ddd',
    //         'password'              => 'eee',
    //         'password_confirmation' => 'eee',
    //         'subdomain'             => 'ffffff',
    //     ]);
    //     dd($page);
    //     $page->assertStatus(200);
    //     $page->assertSee('https://ffffff.');
    //     $page->assertSee('mailto:bbb@ccc.com');
    // }
}
