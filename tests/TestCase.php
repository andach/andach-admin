<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    
    public function setUp(): void
    {
        parent::setUp();
        
        \Artisan::call('migrate:fresh');
        $this->migrator = app('migrator');
        $this->migrator->setConnection('testing_sql');
        $this->migrator->run(database_path('migrations/tenants'), []);
        \Artisan::call('db:seed');
    }
}
