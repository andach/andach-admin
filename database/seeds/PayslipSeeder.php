<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PayslipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $payArray = [
            [
                'pay_frequency_id' => 1,
                'pay_year_id' => 1,
                'name' => '2020 Jan',
                'from_date' => '2020-01-01',
                'to_date' => '2020-01-31',
            ],
            [
                'pay_frequency_id' => 1,
                'pay_year_id' => 1,
                'name' => '2020 Feb',
                'from_date' => '2020-02-01',
                'to_date' => '2020-02-29',
            ],
            [
                'pay_frequency_id' => 1,
                'pay_year_id' => 1,
                'name' => '2020 Mar',
                'from_date' => '2020-03-01',
                'to_date' => '2020-03-31',
            ],
            [
                'pay_frequency_id' => 1,
                'pay_year_id' => 1,
                'name' => '2020 Apr',
                'from_date' => '2020-04-01',
                'to_date' => '2020-04-30',
            ],
            [
                'pay_frequency_id' => 1,
                'pay_year_id' => 1,
                'name' => '2020 May',
                'from_date' => '2020-05-01',
                'to_date' => '2020-05-31',
            ],
            [
                'pay_frequency_id' => 1,
                'pay_year_id' => 1,
                'name' => '2020 Jun',
                'from_date' => '2020-06-01',
                'to_date' => '2020-06-30',
            ],
            [
                'pay_frequency_id' => 1,
                'pay_year_id' => 1,
                'name' => '2020 Jul',
                'from_date' => '2020-07-01',
                'to_date' => '2020-07-31',
            ],
            [
                'pay_frequency_id' => 1,
                'pay_year_id' => 1,
                'name' => '2020 Aug',
                'from_date' => '2020-08-01',
                'to_date' => '2020-08-31',
            ],
            [
                'pay_frequency_id' => 1,
                'pay_year_id' => 1,
                'name' => '2020 Sep',
                'from_date' => '2020-09-01',
                'to_date' => '2020-09-30',
            ],
            [
                'pay_frequency_id' => 1,
                'pay_year_id' => 1,
                'name' => '2020 Oct',
                'from_date' => '2020-10-01',
                'to_date' => '2020-10-31',
            ],
            [
                'pay_frequency_id' => 1,
                'pay_year_id' => 1,
                'name' => '2020 Nov',
                'from_date' => '2020-11-01',
                'to_date' => '2020-11-30',
            ],
            [
                'pay_frequency_id' => 1,
                'pay_year_id' => 1,
                'name' => '2020 Dec',
                'from_date' => '2020-12-01',
                'to_date' => '2020-12-31',
            ],
        ];

        for ($i = 0; $i < 26; $i++)
        {
            $numWeeks = $i * 2;

            $payArray[] = [
                'pay_frequency_id' => 2,
                'pay_year_id' => 1,
                'name' => '2020 Fortnight '.$i,
                'from_date' => date('Y-m-d', strtotime('+'.$numWeeks.' weeks', strtotime('2019-12-30'))),
                'to_date' => date('Y-m-d', strtotime('+'.$numWeeks.' weeks +13 days', strtotime('2019-12-30'))),
            ];
        }

        for ($i = 0; $i < 52; $i++)
        {
            $payArray[] = [
                'pay_frequency_id' => 3,
                'pay_year_id' => 1,
                'name' => '2020 Week '.$i,
                'from_date' => date('Y-m-d', strtotime('+'.$i.' weeks', strtotime('2019-12-30'))),
                'to_date' => date('Y-m-d', strtotime('+'.$i.' weeks +6 days', strtotime('2019-12-30'))),
            ];
        }

        $array = [
            'pay_periods' => $payArray,

            'pay_years' => [
                ['name' => '2020'],
            ],
        ];


        foreach ($array as $tablename => $lines) {
            foreach ($lines as $line) {
                DB::table($tablename)->insert($line);
            }
        }
    }
}
