<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [
            'companies' => [
                ['name' => 'Main Company'],
            ],

            'permissions' => [
                ['name' => 'complaint.create', 'show_in_menu' => 1, 'menu_category_name' => 'Complaints', 'menu_detail_name' => 'New Complaint', 'menu_fontawesome' => 'fas fa-arrow-right nav-icon'],
                ['name' => 'complaint.create-post'],
                ['name' => 'complaint.edit'],
                ['name' => 'complaint.edit-post'],
                ['name' => 'complaint.index', 'show_in_menu' => 1, 'menu_category_name' => 'Complaints', 'menu_detail_name' => 'All Complaints', 'menu_fontawesome' => 'fas fa-arrow-right nav-icon'],
                ['name' => 'complaint.show'],

                ['name' => 'health-and-safety-incident.create', 'show_in_menu' => 1, 'menu_category_name' => 'H&S Incidents', 'menu_detail_name' => 'New Incident', 'menu_fontawesome' => 'fas fa-arrow-right nav-icon'],
                ['name' => 'health-and-safety-incident.create-post'],
                ['name' => 'health-and-safety-incident.edit'],
                ['name' => 'health-and-safety-incident.edit-post'],
                ['name' => 'health-and-safety-incident.index', 'show_in_menu' => 1, 'menu_category_name' => 'H&S Incidents', 'menu_detail_name' => 'All Incidents', 'menu_fontawesome' => 'fas fa-arrow-right nav-icon'],
                ['name' => 'health-and-safety-incident.show'],

                ['name' => 'ticket.create', 'show_in_menu' => 1, 'menu_category_name' => 'Tickets', 'menu_detail_name' => 'New Ticket', 'menu_fontawesome' => 'fas fa-arrow-right nav-icon'],
                ['name' => 'ticket.create-post'],
                ['name' => 'ticket.edit'],
                ['name' => 'ticket.edit-post'],
                ['name' => 'ticket.index', 'show_in_menu' => 1, 'menu_category_name' => 'Tickets', 'menu_detail_name' => 'All Tickets', 'menu_fontawesome' => 'fas fa-arrow-right nav-icon'],
                ['name' => 'ticket.show'],

                ['name' => 'user.create', 'show_in_menu' => 1, 'menu_category_name' => 'Users', 'menu_detail_name' => 'New User', 'menu_fontawesome' => 'fas fa-arrow-right nav-icon'],
                ['name' => 'user.create-post'],
                ['name' => 'user.edit'],
                ['name' => 'user.edit-post'],
                ['name' => 'user.index', 'show_in_menu' => 1, 'menu_category_name' => 'Users', 'menu_detail_name' => 'All Users', 'menu_fontawesome' => 'fas fa-arrow-right nav-icon'],
                ['name' => 'user.show'],

                ['name' => 'user-defined-field.create', 'show_in_menu' => 1, 'menu_category_name' => 'UDFs', 'menu_detail_name' => 'Create UDF', 'menu_fontawesome' => 'fas fa-arrow-right nav-icon'],
                ['name' => 'user-defined-field.create-post'],
                ['name' => 'user-defined-field.edit'],
                ['name' => 'user-defined-field.edit-post'],
                ['name' => 'user-defined-field.index', 'show_in_menu' => 1, 'menu_category_name' => 'UDFs', 'menu_detail_name' => 'All UDF', 'menu_fontawesome' => 'fas fa-arrow-right nav-icon'],
                ['name' => 'user-defined-field.show'],

                ['name' => 'workflow.create', 'show_in_menu' => 1, 'menu_category_name' => 'Workflow', 'menu_detail_name' => 'Create Workflow', 'menu_fontawesome' => 'fas fa-arrow-right nav-icon'],
                ['name' => 'workflow.create-post'],
                ['name' => 'workflow.edit'],
                ['name' => 'workflow.edit-post'],
                ['name' => 'workflow.index', 'show_in_menu' => 1, 'menu_category_name' => 'Workflow', 'menu_detail_name' => 'All Workflows', 'menu_fontawesome' => 'fas fa-arrow-right nav-icon'],
                ['name' => 'workflow.show'],

                ['name' => 'user.my-account', 'show_in_menu' => 1, 'menu_category_name' => 'Users', 'menu_detail_name' => 'My Account', 'menu_fontawesome' => 'fas fa-arrow-right nav-icon'],
                ['name' => 'user.my-account-post'],

                ['name' => 'ticket.mine', 'show_in_menu' => 1, 'menu_category_name' => 'Tickets', 'menu_detail_name' => 'Assigned to Me', 'menu_fontawesome' => 'fas fa-arrow-right nav-icon'],

                ['name' => 'message.inbox', 'show_in_menu' => 1, 'menu_category_name' => 'Messages', 'menu_detail_name' => 'Inbox', 'menu_fontawesome' => 'fas fa-arrow-right nav-icon'],
                ['name' => 'message.create', 'show_in_menu' => 1, 'menu_category_name' => 'Messages', 'menu_detail_name' => 'New Message', 'menu_fontawesome' => 'fas fa-arrow-right nav-icon'],
                ['name' => 'message.outbox', 'show_in_menu' => 1, 'menu_category_name' => 'Messages', 'menu_detail_name' => 'Sent Messages', 'menu_fontawesome' => 'fas fa-arrow-right nav-icon'],
                ['name' => 'message.bin', 'show_in_menu' => 1, 'menu_category_name' => 'Messages', 'menu_detail_name' => 'Bin/Trash', 'menu_fontawesome' => 'fas fa-arrow-right nav-icon'],
                ['name' => 'permission.index', 'show_in_menu' => 1, 'menu_category_name' => 'Permissions/Roles', 'menu_detail_name' => 'Manage Permissions', 'menu_fontawesome' => 'fas fa-arrow-right nav-icon'],
                ['name' => 'role.index', 'show_in_menu' => 1, 'menu_category_name' => 'Permissions/Roles', 'menu_detail_name' => 'Manage Roles', 'menu_fontawesome' => 'fas fa-arrow-right nav-icon'],
                ['name' => 'permission.edit'],
                ['name' => 'role.edit'],
                ['name' => 'permission.index-post'],
                ['name' => 'role.index-post'],
                ['name' => 'permission.edit-post'],
                ['name' => 'role.edit-post'],
                ['name' => 'message.show'],
                ['name' => 'message.delete'],
                ['name' => 'message.create-post'],

                ['name' => 'complaint.mine', 'show_in_menu' => 1, 'menu_category_name' => 'Complaints', 'menu_detail_name' => 'Assigned to Me', 'menu_fontawesome' => 'fas fa-arrow-right nav-icon'],
                ['name' => 'health-and-safety-incident.mine', 'show_in_menu' => 1, 'menu_category_name' => 'H&S Incidents', 'menu_detail_name' => 'Assigned to Me', 'menu_fontawesome' => 'fas fa-arrow-right nav-icon'],
            ],

            'users' => [
                [
                    'id' => 1,
                    'company_id' => 1,
                    'name' => 'Super Admin',
                    'email' => 'superadmin@example.com',
                    'password' => bcrypt('secret'),
                    'is_staff' => 1,
                    'is_super_admin' => 1,
                ],
            ],
        ];


        foreach ($array as $tablename => $lines) {
            foreach ($lines as $line) {
                DB::table($tablename)->insert($line);
            }
        }
    }
}
