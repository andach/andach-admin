<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TimeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [
            'link_appointments_projects' => [
                ['appointment_id' => 1, 'project_id' => 1,],
            ],
            
            'link_processes_teams' => [
                ['process_id' => 1, 'team_id' => 2,],
                ['process_id' => 2, 'team_id' => 3,],
                ['process_id' => 3, 'team_id' => 3,],
                ['process_id' => 4, 'team_id' => 4,],
            ],
            
            'processes' => [
                ['name' => 'Investigating Tickets', 'description' => '',],
                ['name' => 'Investigating Incidents', 'description' => '',],
                ['name' => 'Making RIDDOR Reports', 'description' => '',],
                ['name' => 'Investigating Complaints', 'description' => '',],
            ],

            'projects' => [
                [
                    'parent_id' => 0,
                    'appointment_id' => 1,
                    'name' => 'Implement New Software System',
                    'description' => '',
                    'minutes_budgeted' => 1,
                ]
            ],

            'projects_tasks' => [
                [
                    'parent_id' => 0,
                    'project_id' => 1,
                    'name' => 'Implement New Software System',
                    'description' => '',
                    'minutes_budgeted' => 1,
                ]
            ],

            'timesheets' => [
                [
                    'appointment_id' => 13,
                    'job_id' => 11,
                    'position_id' => 11,
                    'team_id' => 3,
                    'category_id' => 1,
                    'mins_logged' => 60,
                    'date_of_timesheet' => '2020-04-29',
                    'description' => 'test',
                ],
            ],

            'timesheets_categories' => [
                ['name' => 'General Administration',],
                ['name' => 'IT Issues',],
                ['name' => 'Making Tea',],
            ],
        ];


        foreach ($array as $tablename => $lines) {
            foreach ($lines as $line) {
                DB::table($tablename)->insert($line);
            }
        }
    }
}
