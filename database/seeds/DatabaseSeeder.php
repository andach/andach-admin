<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CareHomeSeeder::class);
        $this->call(CostCodeSeeder::class);
        $this->call(DevSeeder::class);
        $this->call(HRSeeder::class);
        $this->call(LocationSeeder::class);
        $this->call(MessageSeeder::class);
        $this->call(PayslipSeeder::class);
        $this->call(TicketSeeder::class);
        $this->call(TimeSeeder::class);
        $this->call(UDFSeeder::class);
        $this->call(WorkflowSeeder::class);
    }
}
