<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [
            'locations' => [
                ['name' => 'Company - Base Location', 'parent_id' => 0],
                ['name' => 'Offices', 'parent_id' => 1],
                ['name' => 'Manufacturing', 'parent_id' => 1],
                ['name' => 'Warehouses', 'parent_id' => 1],
                ['name' => 'Ground Floor', 'parent_id' => 2],

                ['name' => 'First Floor', 'parent_id' => 2],
                ['name' => 'Second Floor', 'parent_id' => 2],
                ['name' => 'IT Office', 'parent_id' => 5],
                ['name' => 'Receptionists Office', 'parent_id' => 5],
                ['name' => 'Sky Garden', 'parent_id' => 7],

                ['name' => 'Milton Keynes Workshop', 'parent_id' => 4],
                ['name' => 'Bristol Workshop', 'parent_id' => 4],
                ['name' => 'Glasgow Workshop', 'parent_id' => 4],
                ['name' => 'Birmingham Workshop', 'parent_id' => 4],
                ['name' => 'Derby Workshop', 'parent_id' => 4],

                ['name' => 'Customer Area', 'parent_id' => 11],
                ['name' => 'Customer Area', 'parent_id' => 12],
                ['name' => 'Customer Area', 'parent_id' => 13],
                ['name' => 'Customer Area', 'parent_id' => 14],
                ['name' => 'Customer Area', 'parent_id' => 15],

                ['name' => 'Staff Area', 'parent_id' => 11],
                ['name' => 'Staff Area', 'parent_id' => 12],
                ['name' => 'Staff Area', 'parent_id' => 13],
                ['name' => 'Staff Area', 'parent_id' => 14],
                ['name' => 'Staff Area', 'parent_id' => 15],

                ['name' => 'Hazardours Storage Cupboard', 'parent_id' => 21],
                ['name' => 'Hazardours Storage Cupboard', 'parent_id' => 22],
                ['name' => 'Hazardours Storage Cupboard', 'parent_id' => 23],
                ['name' => 'Hazardours Storage Cupboard', 'parent_id' => 24],
                ['name' => 'Hazardours Storage Cupboard', 'parent_id' => 25],

                ['name' => 'Kitchen', 'parent_id' => 21],
                ['name' => 'Kitchen', 'parent_id' => 22],
                ['name' => 'Kitchen', 'parent_id' => 23],
                ['name' => 'Kitchen', 'parent_id' => 24],
                ['name' => 'Kitchen', 'parent_id' => 25],
            ],
        ];


        foreach ($array as $tablename => $lines) {
            foreach ($lines as $line) {
                DB::table($tablename)->insert($line);
            }
        }
    }
}
