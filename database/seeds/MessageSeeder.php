<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [
            'messages' => [
                [
                    'id' => '2b7d6f81-e093-43eb-a88f-c2b93900f00b',
                    'user_id' => 0,
                    'name' => 'Hello Everybody',
                    'description' => 'This is a message from the system administrator.',
                    'from_name' => 'System Administrator',
                ],
                [
                    'id' => '0d489d6d-b0d4-4012-b031-3794d295ab06',
                    'user_id' => 2,
                    'name' => 'How are you?',
                    'description' => 'How is everything?',
                    'from_name' => '',
                ],
                [
                    'id' => '4e303233-f438-45a1-97eb-01eb7215924b',
                    'user_id' => 3,
                    'name' => 'I\'m going to be away from work tomorrow',
                    'description' => 'Work sucks, and I dont want to come in, so I\'m not going to. I will be in New York if anyone needs me.',
                    'from_name' => '',
                ],
                [
                    'id' => '818f03df-f019-4aea-98c9-d63ec3346213',
                    'user_id' => 0,
                    'name' => 'System Generated Test Message',
                    'description' => 'This is a system generated text message. You should just delete it.',
                    'from_name' => 'The System',
                ],
            ],

            'link_messages_users' => [
                ['message_id' => '2b7d6f81-e093-43eb-a88f-c2b93900f00b', 'user_id' => 1],
                ['message_id' => '2b7d6f81-e093-43eb-a88f-c2b93900f00b', 'user_id' => 2],
                ['message_id' => '2b7d6f81-e093-43eb-a88f-c2b93900f00b', 'user_id' => 3],
                ['message_id' => '2b7d6f81-e093-43eb-a88f-c2b93900f00b', 'user_id' => 4],
                ['message_id' => '2b7d6f81-e093-43eb-a88f-c2b93900f00b', 'user_id' => 5],
                ['message_id' => '2b7d6f81-e093-43eb-a88f-c2b93900f00b', 'user_id' => 6],
                ['message_id' => '2b7d6f81-e093-43eb-a88f-c2b93900f00b', 'user_id' => 7],
                ['message_id' => '2b7d6f81-e093-43eb-a88f-c2b93900f00b', 'user_id' => 8],
                ['message_id' => '2b7d6f81-e093-43eb-a88f-c2b93900f00b', 'user_id' => 9],
                ['message_id' => '2b7d6f81-e093-43eb-a88f-c2b93900f00b', 'user_id' => 10],
                
                ['message_id' => '0d489d6d-b0d4-4012-b031-3794d295ab06', 'user_id' => 1, 'is_starred' => 1, 'is_read' => 1],
                
                ['message_id' => '4e303233-f438-45a1-97eb-01eb7215924b', 'user_id' => 1],
                ['message_id' => '4e303233-f438-45a1-97eb-01eb7215924b', 'user_id' => 2],
                
                ['message_id' => '818f03df-f019-4aea-98c9-d63ec3346213', 'user_id' => 1, 'is_deleted' => 1],
            ],
        ];

        foreach ($array as $tablename => $lines) {
            foreach ($lines as $line) {
                DB::table($tablename)->insert($line);
            }
        }
    }
}
