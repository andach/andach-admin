<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TicketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [
            'tickets' => [
                [
                    'customer_id' => 11,
                    'company_id' => 1,
                    'user_id' => 1,
                    'name' => 'Query from the Auditor',
                    'description' => 'Lorem ipsum',
                    'is_closed' => 1,
                ],
                [
                    'customer_id' => 12,
                    'company_id' => 2,
                    'user_id' => 1,
                    'name' => 'Customer wants a call back for their object',
                    'description' => 'The customer has called and has asked us to call them back. ',
                ],
                [
                    'customer_id' => 11,
                    'company_id' => 2,
                    'user_id' => 1,
                    'name' => 'Have we seen the customers watch?',
                    'description' => 'The customer thinks they may have left their watch in our shop. ',
                ],
            ],

            'workflights' => [
                [
                    'workflowable_id' => 1,
                    'workflowable_type' => 'App\Models\Ticket\Ticket',
                    'workflow_id' => 1,
                    'assigned_to_user_id' => 1,
                    'state_id' => 2,
                    'name' => 'workflow',
                ],
                [
                    'workflowable_id' => 2,
                    'workflowable_type' => 'App\Models\Ticket\Ticket',
                    'workflow_id' => 2,
                    'assigned_to_user_id' => 1,
                    'state_id' => 3,
                    'name' => 'workflow',
                ],
                [
                    'workflowable_id' => 3,
                    'workflowable_type' => 'App\Models\Ticket\Ticket',
                    'workflow_id' => 2,
                    'assigned_to_user_id' => 2,
                    'state_id' => 4,
                    'name' => 'workflow',
                ],
            ],

            'comments' => [
                [
                    'user_id' => 1,
                    'workflight_id' => 1,
                    'description' => 'I gave the auditor a call.',
                ],
                [
                    'user_id' => 2,
                    'workflight_id' => 2,
                    'description' => 'Tried to call the customer back but he wasnt there.',
                ],
                [
                    'user_id' => 2,
                    'workflight_id' => 3,
                    'description' => 'Had a thorough look but couldnt find this.',
                ],
            ],
        ];

        foreach ($array as $tablename => $lines) {
            foreach ($lines as $line) {
                DB::table($tablename)->insert($line);
            }
        }
    }
}
