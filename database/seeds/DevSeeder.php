<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DevSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [
            'companies' => [
                ['name' => 'Parent Company'],
                ['name' => 'Trading Company', 'parent_id' => 1],
                ['name' => 'Property Company', 'parent_id' => 1],
                ['name' => 'Dormant Company', 'parent_id' => 2],
            ],

            'link_companies_users' => [
                // Ticket manager has all permissions for tickets.
                ['company_id' => 1, 'user_id' => 2, 'model_name' => 'App\Models\Ticket\Ticket', 'permission' => 'create'],
                ['company_id' => 2, 'user_id' => 2, 'model_name' => 'App\Models\Ticket\Ticket', 'permission' => 'create'],
                ['company_id' => 3, 'user_id' => 2, 'model_name' => 'App\Models\Ticket\Ticket', 'permission' => 'create'],
                ['company_id' => 1, 'user_id' => 2, 'model_name' => 'App\Models\Ticket\Ticket', 'permission' => 'edit'],
                ['company_id' => 2, 'user_id' => 2, 'model_name' => 'App\Models\Ticket\Ticket', 'permission' => 'edit'],
                ['company_id' => 3, 'user_id' => 2, 'model_name' => 'App\Models\Ticket\Ticket', 'permission' => 'edit'],
                ['company_id' => 1, 'user_id' => 2, 'model_name' => 'App\Models\Ticket\Ticket', 'permission' => 'show'],
                ['company_id' => 2, 'user_id' => 2, 'model_name' => 'App\Models\Ticket\Ticket', 'permission' => 'show'],
                ['company_id' => 3, 'user_id' => 2, 'model_name' => 'App\Models\Ticket\Ticket', 'permission' => 'show'],

                // Complaint manager can access complaints for company 2 only.
                ['company_id' => 2, 'user_id' => 3, 'model_name' => 'App\Models\Complaint\Complaint', 'permission' => 'create'],
                ['company_id' => 2, 'user_id' => 3, 'model_name' => 'App\Models\Complaint\Complaint', 'permission' => 'edit'],
                ['company_id' => 2, 'user_id' => 3, 'model_name' => 'App\Models\Complaint\Complaint', 'permission' => 'show'],

                // Complaint employee can only create complaints and tickets for company 2. Can't view them or edit them.
                ['company_id' => 2, 'user_id' => 4, 'model_name' => 'App\Models\Complaint\Complaint', 'permission' => 'create'],
                ['company_id' => 2, 'user_id' => 4, 'model_name' => 'App\Models\Ticket\Ticket', 'permission' => 'create'],

                // Health and safety director has full access to incidents and tickets for companies 2 and 3.
                ['company_id' => 2, 'user_id' => 5, 'model_name' => 'App\Models\HealthAndSafety\Incident', 'permission' => 'create'],
                ['company_id' => 2, 'user_id' => 5, 'model_name' => 'App\Models\Ticket\Ticket', 'permission' => 'create'],
                ['company_id' => 3, 'user_id' => 5, 'model_name' => 'App\Models\HealthAndSafety\Incident', 'permission' => 'create'],
                ['company_id' => 3, 'user_id' => 5, 'model_name' => 'App\Models\Ticket\Ticket', 'permission' => 'create'],
                ['company_id' => 2, 'user_id' => 5, 'model_name' => 'App\Models\HealthAndSafety\Incident', 'permission' => 'edit'],
                ['company_id' => 2, 'user_id' => 5, 'model_name' => 'App\Models\Ticket\Ticket', 'permission' => 'edit'],
                ['company_id' => 3, 'user_id' => 5, 'model_name' => 'App\Models\HealthAndSafety\Incident', 'permission' => 'edit'],
                ['company_id' => 3, 'user_id' => 5, 'model_name' => 'App\Models\Ticket\Ticket', 'permission' => 'edit'],
                ['company_id' => 2, 'user_id' => 5, 'model_name' => 'App\Models\HealthAndSafety\Incident', 'permission' => 'show'],
                ['company_id' => 2, 'user_id' => 5, 'model_name' => 'App\Models\Ticket\Ticket', 'permission' => 'show'],
                ['company_id' => 3, 'user_id' => 5, 'model_name' => 'App\Models\HealthAndSafety\Incident', 'permission' => 'show'],
                ['company_id' => 3, 'user_id' => 5, 'model_name' => 'App\Models\Ticket\Ticket', 'permission' => 'show'],

                // Health and safety manager has full access to incidents, but can only show and edit tickets, not create them.
                ['company_id' => 2, 'user_id' => 6, 'model_name' => 'App\Models\HealthAndSafety\Incident', 'permission' => 'create'],
                ['company_id' => 3, 'user_id' => 6, 'model_name' => 'App\Models\HealthAndSafety\Incident', 'permission' => 'create'],
                ['company_id' => 2, 'user_id' => 6, 'model_name' => 'App\Models\HealthAndSafety\Incident', 'permission' => 'edit'],
                ['company_id' => 2, 'user_id' => 6, 'model_name' => 'App\Models\Ticket\Ticket', 'permission' => 'edit'],
                ['company_id' => 3, 'user_id' => 6, 'model_name' => 'App\Models\HealthAndSafety\Incident', 'permission' => 'edit'],
                ['company_id' => 3, 'user_id' => 6, 'model_name' => 'App\Models\Ticket\Ticket', 'permission' => 'edit'],
                ['company_id' => 2, 'user_id' => 6, 'model_name' => 'App\Models\HealthAndSafety\Incident', 'permission' => 'show'],
                ['company_id' => 2, 'user_id' => 6, 'model_name' => 'App\Models\Ticket\Ticket', 'permission' => 'show'],
                ['company_id' => 3, 'user_id' => 6, 'model_name' => 'App\Models\HealthAndSafety\Incident', 'permission' => 'show'],
                ['company_id' => 3, 'user_id' => 6, 'model_name' => 'App\Models\Ticket\Ticket', 'permission' => 'show'],

                // Health and safety property employee can create incidents and tickets for company 2 only.
                ['company_id' => 2, 'user_id' => 7, 'model_name' => 'App\Models\HealthAndSafety\Incident', 'permission' => 'create'],
                ['company_id' => 2, 'user_id' => 7, 'model_name' => 'App\Models\Ticket\Ticket', 'permission' => 'create'],

                // Health and safety trading employee can create incidents and tickets for company 3 only.
                ['company_id' => 3, 'user_id' => 8, 'model_name' => 'App\Models\HealthAndSafety\Incident', 'permission' => 'create'],
                ['company_id' => 3, 'user_id' => 8, 'model_name' => 'App\Models\Ticket\Ticket', 'permission' => 'create'],

                // Receptionist has full access for company 1 to complaints, incidents and tickets. No access to other companies.
                ['company_id' => 1, 'user_id' => 9, 'model_name' => 'App\Models\Complaint\Complaint', 'permission' => 'create'],
                ['company_id' => 2, 'user_id' => 9, 'model_name' => 'App\Models\HealthAndSafety\Incident', 'permission' => 'create'],
                ['company_id' => 3, 'user_id' => 9, 'model_name' => 'App\Models\Ticket\Ticket', 'permission' => 'create'],
                ['company_id' => 1, 'user_id' => 9, 'model_name' => 'App\Models\Complaint\Complaint', 'permission' => 'edit'],
                ['company_id' => 2, 'user_id' => 9, 'model_name' => 'App\Models\HealthAndSafety\Incident', 'permission' => 'edit'],
                ['company_id' => 3, 'user_id' => 9, 'model_name' => 'App\Models\Ticket\Ticket', 'permission' => 'edit'],
                ['company_id' => 1, 'user_id' => 9, 'model_name' => 'App\Models\Complaint\Complaint', 'permission' => 'show'],
                ['company_id' => 2, 'user_id' => 9, 'model_name' => 'App\Models\HealthAndSafety\Incident', 'permission' => 'show'],
                ['company_id' => 3, 'user_id' => 9, 'model_name' => 'App\Models\Ticket\Ticket', 'permission' => 'show'],
            ],

            'roles_permissions' => [
                ['access_to' => 'complaint.read', 'role_id' => 1],
                ['access_to' => 'complaint.create', 'role_id' => 1],
                ['access_to' => 'complaint.edit', 'role_id' => 1],
                ['access_to' => 'complaint.mine', 'role_id' => 1],
                ['access_to' => 'complaint.admin', 'role_id' => 1],

                ['access_to' => 'health-and-safety-incident.read', 'role_id' => 2],
                ['access_to' => 'health-and-safety-incident.create', 'role_id' => 2],
                ['access_to' => 'health-and-safety-incident.edit', 'role_id' => 2],
                ['access_to' => 'health-and-safety-incident.mine', 'role_id' => 2],
                ['access_to' => 'health-and-safety-incident.admin', 'role_id' => 2],

                ['access_to' => 'ticket.read', 'role_id' => 3],
                ['access_to' => 'ticket.create', 'role_id' => 3],
                ['access_to' => 'ticket.edit', 'role_id' => 3],
                ['access_to' => 'ticket.mine', 'role_id' => 3],
                ['access_to' => 'ticket.admin', 'role_id' => 3],

                ['access_to' => 'user.read', 'role_id' => 4],
                ['access_to' => 'user.create', 'role_id' => 4],
                ['access_to' => 'user.edit', 'role_id' => 4],
                ['access_to' => 'user.mine', 'role_id' => 4],
                ['access_to' => 'user.admin', 'role_id' => 4],

                ['access_to' => 'user-defined-field.create', 'role_id' => 5],
                ['access_to' => 'user-defined-field.edit', 'role_id' => 5],
                ['access_to' => 'user-defined-field.read', 'role_id' => 5],

                ['access_to' => 'workflow.admin', 'role_id' => 6],

                ['access_to' => 'user.view', 'role_id' => 7],

                ['access_to' => 'ticket.edit', 'role_id' => 8],
                ['access_to' => 'ticket.edit', 'role_id' => 9],
                ['access_to' => 'ticket.edit', 'role_id' => 10],

                ['access_to' => 'user.mine', 'role_id' => 11],
                ['access_to' => 'ticket.mine', 'role_id' => 12],

                // Permission to view messages for the staff role.
                ['access_to' => 'message.admin', 'role_id' => 13],
                ['access_to' => 'customer.create', 'role_id' => 13],
                ['access_to' => 'customer.edit', 'role_id' => 13],
                ['access_to' => 'customer.read', 'role_id' => 13],
                ['access_to' => 'customer.admin', 'role_id' => 13],
                ['access_to' => 'timesheet.create', 'role_id' => 13],
                ['access_to' => 'appointment.mine', 'role_id' => 13],

                ['access_to' => 'ticket.edit', 'role_id' => 14],
                ['access_to' => 'role.admin', 'role_id' => 15],

                ['access_to' => 'company.admin', 'role_id' => 16],
                ['access_to' => 'location.admin', 'role_id' => 16],
                ['access_to' => 'hr.admin', 'role_id' => 17],
                ['access_to' => 'timesheet.edit', 'role_id' => 18],
            ],

            'link_roles_workflows_transitions' => [
                // Anyone who can see tickets can send to manager review.
                ['role_id' => 3, 'workflow_transition_id' => 3],

                // Instantly Close
                ['role_id' => 8, 'workflow_transition_id' => 2],

                // Manager Review
                ['role_id' => 9, 'workflow_transition_id' => 3],
                ['role_id' => 9, 'workflow_transition_id' => 4],
                ['role_id' => 9, 'workflow_transition_id' => 6],

                // Child Safety Review
                ['role_id' => 10, 'workflow_transition_id' => 5],
                ['role_id' => 10, 'workflow_transition_id' => 7],
                ['role_id' => 10, 'workflow_transition_id' => 8],
            ],

            'link_roles_users' => [
                ['role_id' => 3, 'user_id' => 2],
                ['role_id' => 4, 'user_id' => 2],
                ['role_id' => 5, 'user_id' => 2],
                ['role_id' => 6, 'user_id' => 2],

                ['role_id' => 1, 'user_id' => 3],

                ['role_id' => 1, 'user_id' => 4],
                ['role_id' => 3, 'user_id' => 4],

                ['role_id' => 2, 'user_id' => 5],
                ['role_id' => 3, 'user_id' => 5],

                ['role_id' => 2, 'user_id' => 6],
                ['role_id' => 14, 'user_id' => 6],

                ['role_id' => 2, 'user_id' => 7],
                ['role_id' => 3, 'user_id' => 7],

                ['role_id' => 2, 'user_id' => 8],
                ['role_id' => 3, 'user_id' => 8],

                ['role_id' => 1, 'user_id' => 9],
                ['role_id' => 2, 'user_id' => 9],
                ['role_id' => 3, 'user_id' => 9],
                ['role_id' => 4, 'user_id' => 9],
                
                ['role_id' => 7, 'user_id' => 10],
                ['role_id' => 11, 'user_id' => 10],
                ['role_id' => 12, 'user_id' => 10],

                ['role_id' => 8, 'user_id' => 2],
                ['role_id' => 8, 'user_id' => 5],
                ['role_id' => 8, 'user_id' => 6],
                ['role_id' => 8, 'user_id' => 9],

                ['role_id' => 9, 'user_id' => 2],
                ['role_id' => 10, 'user_id' => 5],

                // Staff role.
                ['role_id' => 13, 'user_id' => 2],
                ['role_id' => 13, 'user_id' => 3],
                ['role_id' => 13, 'user_id' => 4],
                ['role_id' => 13, 'user_id' => 5],
                ['role_id' => 13, 'user_id' => 6],
                ['role_id' => 13, 'user_id' => 7],
                ['role_id' => 13, 'user_id' => 8],
                ['role_id' => 13, 'user_id' => 9],
                ['role_id' => 13, 'user_id' => 10],
                ['role_id' => 13, 'user_id' => 13],
            ],

            'roles' => [
                ['name' => 'Manage Complaints'],
                ['name' => 'Manage Health and Safety Incidents'],
                ['name' => 'Manage Tickets'],
                ['name' => 'Manage Users'],
                ['name' => 'Manage UDFs'],

                ['name' => 'Manage Workflows'],
                ['name' => 'View Users'],
                ['name' => 'Tickets (Close)'],
                ['name' => 'Tickets (Manager Review)'],
                ['name' => 'Tickets (Child Safety Review)'],
                // 10 above
                ['name' => 'Manage Own Account'],
                ['name' => 'Work on Tickets Assigned to Them'],
                ['name' => 'Staff'],
                ['name' => 'Edit/Show but not Create Tickets'],
                ['name' => 'Manage Permissions and Roles'],
                
                ['name' => 'Administration and Setup'],
                ['name' => 'HR Manager'],
                ['name' => 'Team Leader'],
            ],
            
            'users' => [
                [
                    'id' => 1,
                    'company_id' => 1,
                    'name' => 'Super Admin',
                    'email' => 'superadmin@example.com',
                    'password' => bcrypt('secret'),
                    'is_staff' => 1,
                    'is_super_admin' => 1,
                ],
                [
                    'id' => 2,
                    'company_id' => 1,
                    'name' => 'Ticket Manager',
                    'email' => 'ticketmanager@example.com',
                    'password' => bcrypt('secret'),
                    'is_staff' => 1,
                ],
                [
                    'id' => 3,
                    'company_id' => 2,
                    'name' => 'Complaint Manager',
                    'email' => 'complaintmanager@example.com',
                    'password' => bcrypt('secret'),
                    'is_staff' => 1,
                ],
                [
                    'id' => 4,
                    'company_id' => 2,
                    'name' => 'Complaint Employee',
                    'email' => 'complaintemployee@example.com',
                    'password' => bcrypt('secret'),
                    'is_staff' => 1,
                ],
                [
                    'id' => 5,
                    'company_id' => 2,
                    'name' => 'Health and Safety Director',
                    'email' => 'healthandsafetydirector@example.com',
                    'password' => bcrypt('secret'),
                    'is_staff' => 1,
                ],
                [
                    'id' => 6,
                    'company_id' => 2,
                    'name' => 'Health and Safety Manager',
                    'email' => 'healthandsafetymanager@example.com',
                    'password' => bcrypt('secret'),
                    'is_staff' => 1,
                ],
                [
                    'id' => 7,
                    'company_id' => 2,
                    'name' => 'Health and Safety Trading Employee',
                    'email' => 'healthandsafetytradingemployee@example.com',
                    'password' => bcrypt('secret'),
                    'is_staff' => 1,
                ],
                [
                    'id' => 8,
                    'company_id' => 2,
                    'name' => 'Health and Safety Property Employee',
                    'email' => 'healthandsafetypropertyemployee@example.com',
                    'password' => bcrypt('secret'),
                    'is_staff' => 1,
                ],
                [
                    'id' => 9,
                    'company_id' => 2,
                    'name' => 'Receptionist',
                    'email' => 'receptionist@example.com',
                    'password' => bcrypt('secret'),
                    'is_staff' => 1,
                ],
                [
                    'id' => 10,
                    'company_id' => 2,
                    'name' => 'Pleb',
                    'email' => 'pleb@example.com',
                    'password' => bcrypt('secret'),
                    'is_staff' => 1,
                ],
                [
                    'id' => 11,
                    'company_id' => 1,
                    'name' => 'The Auditor',
                    'email' => 'auditor@example.com',
                    'password' => bcrypt('secret'),
                    'is_customer' => 1,
                ],
                [
                    'id' => 12,
                    'company_id' => 1,
                    'name' => 'Customer Number Two',
                    'email' => 'customer2@example.com',
                    'password' => bcrypt('secret'),
                    'is_customer' => 1,
                ],
                [
                    'id' => 13,
                    'company_id' => 3,
                    'name' => 'Other Property Employee',
                    'email' => 'otherhealthandsafetypropertyemployee@example.com',
                    'password' => bcrypt('secret'),
                    'is_staff' => 1,
                ],
            ],
        ];


        foreach ($array as $tablename => $lines) {
            foreach ($lines as $line) {
                DB::table($tablename)->insert($line);
            }
        }
    }
}
