<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UDFSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [
            'user_defined_fields' => [
                [
                    'name' => 'ticket_severity',
                    'display_name' => 'Severity',
                    'description' => '',
                    'type' => 'list',
                    'model_name' => 'App\Models\Ticket\Ticket',
                ],
                [
                    'name' => 'ticket_current_phone',
                    'display_name' => 'Phone Number',
                    'description' => 'If the person raising the ticket is using a different phone from their usual contact number, enter it here. ',
                    'type' => 'string',
                    'model_name' => 'App\Models\Ticket\Ticket',
                ],
                [
                    'name' => 'ticket_date_of_question',
                    'display_name' => 'Date of Question',
                    'description' => '',
                    'type' => 'date',
                    'model_name' => 'App\Models\Ticket\Ticket',
                ],
                [
                    'name' => 'ticket_children',
                    'display_name' => 'Number of Children',
                    'description' => 'Does the questioner have any children?',
                    'type' => 'integer',
                    'model_name' => 'App\Models\Ticket\Ticket',
                ],
                [
                    'name' => 'ticket_danger',
                    'display_name' => 'Likelihood of Harm',
                    'description' => 'If this ticket is not fulfiled, estimate the likelihood of harm to the business as a percentage',
                    'type' => 'float',
                    'model_name' => 'App\Models\Ticket\Ticket',
                ],
                [
                    'name' => 'ticket_current_address',
                    'display_name' => 'Current Address',
                    'description' => 'If the person is at a different address from their usual one, enter it here. ',
                    'type' => 'text',
                    'model_name' => 'App\Models\Ticket\Ticket',
                ],
            ],

            // Note: only used for UDFs which are lists or <select> items.
            'user_defined_fields_items' => [
                [
                    'user_defined_field_id' => 1,
                    'name' => 'Low',
                ],
                [
                    'user_defined_field_id' => 1,
                    'name' => 'Medium',
                ],
                [
                    'user_defined_field_id' => 1,
                    'name' => 'High',
                ],
                [
                    'user_defined_field_id' => 1,
                    'name' => 'Urgent',
                ],
            ],

            'user_defined_fields_values' => [
                [
                    'fieldable_id' => 1,
                    'fieldable_type' => 'App\Models\Ticket\Ticket',
                    'user_defined_field_id' => 1,
                    'user_defined_field_item_id' => 4,
                ],
                [
                    'fieldable_id' => 1,
                    'fieldable_type' => 'App\Models\Ticket\Ticket',
                    'user_defined_field_id' => 2,
                    'value_string' => '07123 456789',
                ],
                [
                    'fieldable_id' => 1,
                    'fieldable_type' => 'App\Models\Ticket\Ticket',
                    'user_defined_field_id' => 3,
                    'value_date' => '2020-01-05 12:34:56',
                ],
                [
                    'fieldable_id' => 1,
                    'fieldable_type' => 'App\Models\Ticket\Ticket',
                    'user_defined_field_id' => 5,
                    'value_float' => .7,
                ],
                [
                    'fieldable_id' => 2,
                    'fieldable_type' => 'App\Models\Ticket\Ticket',
                    'user_defined_field_id' => 1,
                    'user_defined_field_item_id' => 2,
                ],
                [
                    'fieldable_id' => 3,
                    'fieldable_type' => 'App\Models\Ticket\Ticket',
                    'user_defined_field_id' => 1,
                    'user_defined_field_item_id' => 3,
                ],
                [
                    'fieldable_id' => 3,
                    'fieldable_type' => 'App\Models\Ticket\Ticket',
                    'user_defined_field_id' => 5,
                    'value_float' => .1,
                ],
                [
                    'fieldable_id' => 3,
                    'fieldable_type' => 'App\Models\Ticket\Ticket',
                    'user_defined_field_id' => 6,
                    'value_text' => 'The watch is black and gold',
                ],
            ],
        ];


        foreach ($array as $tablename => $lines) {
            foreach ($lines as $line) {
                DB::table($tablename)->insert($line);
            }
        }
    }
}
