<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WorkflowSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [
            'workflows' => [
                [
                    // Complaints are to have a simple workflow, just open and closed.
                    'name' => 'Complaints',
                    'model_name' => 'App\Models\Complaint\Complaint',
                    'min_location_level' => 1,
                    'max_location_level' => 1,
                ],
                [
                    // Tickets are to have a workflow that varies based on UDF values.
                    'name' => 'Tickets',
                    'model_name' => 'App\Models\Ticket\Ticket',
                    'min_location_level' => 1,
                    'max_location_level' => 1,
                ],
                [
                    // TODO: Decide on this workflow later.
                    'name' => 'Health and Safety Investigation',
                    'model_name' => 'App\Models\HealthAndSafety\Incident',
                    'min_location_level' => 2,
                    'max_location_level' => 4,
                ],
            ],

            'workflows_states' => [
                [
                    'workflow_id' => 1,
                    'name' => 'Opened',
                    'is_default' => 1,
                ],
                [
                    'workflow_id' => 1,
                    'name' => 'Closed',
                    'is_closed' => 1,
                ],
                [
                    'workflow_id' => 2,
                    'name' => 'Opened',
                    'is_default' => 1,
                ],
                [
                    'workflow_id' => 2,
                    'name' => 'Awaiting Manager Signoff',
                ],
                [
                    'workflow_id' => 2,
                    'name' => 'Awaiting Child Safeguarding Signoff',
                ],
                [
                    'workflow_id' => 2,
                    'name' => 'Closed',
                    'is_closed' => 1,
                    'default_instant_close_status' => 1,
                ],
            ],

            'workflows_transitions' => [
                [
                    'workflow_id' => 1,
                    'name' => 'Close',
                    'workflow_state_from_id' => 1,
                    'workflow_state_to_id' => 2,
                ],
                [
                    'workflow_id' => 2,
                    'name' => 'Instantly Close',
                    'workflow_state_from_id' => 3,
                    'workflow_state_to_id' => 6,
                ],
                [
                    'workflow_id' => 2,
                    'name' => 'Send to Manager Review',
                    'workflow_state_from_id' => 3,
                    'workflow_state_to_id' => 4,
                ],
                [
                    'workflow_id' => 2,
                    'name' => 'More Investivation Required from Manager Review',
                    'workflow_state_from_id' => 4,
                    'workflow_state_to_id' => 3,
                ],
                [
                    'workflow_id' => 2,
                    'name' => 'Child Safety Review Required',
                    'workflow_state_from_id' => 4,
                    'workflow_state_to_id' => 5,
                ],
                [
                    'workflow_id' => 2,
                    'name' => 'Close from Manager Review',
                    'workflow_state_from_id' => 4,
                    'workflow_state_to_id' => 6,
                ],
                [
                    'workflow_id' => 2,
                    'name' => 'More Investivation Required from Child Safety Review',
                    'workflow_state_from_id' => 5,
                    'workflow_state_to_id' => 3,
                ],
                [
                    'workflow_id' => 2,
                    'name' => 'Close from Child Safety Review',
                    'workflow_state_from_id' => 5,
                    'workflow_state_to_id' => 6,
                ],

            ],

            'link_udf_values_workflows_transitions' => [
                [
                    'udf_id' => 1, // ticket_severity
                    'workflow_transition_id' => 2,
                    'allow_or_deny' => false,
                    'operator' => '>',
                    'operator_value' => 2, // Deny the ability to close instantly if the ticket is High or Urgent severity.
                ],
                [
                    'udf_id' => 6, // ticket_children
                    'workflow_transition_id' => 2,
                    'allow_or_deny' => false,
                    'operator' => '>',
                    'operator_value' => 0, // Deny the ability to close instantly if there are children.
                ],
                [
                    'udf_id' => 4, // ticket_children
                    'workflow_transition_id' => 6,
                    'allow_or_deny' => false,
                    'operator' => '>',
                    'operator_value' => 0, // Deny the ability to close without child protection review if there are children.
                ],
            ]
        ];


        foreach ($array as $tablename => $lines) {
            foreach ($lines as $line) {
                DB::table($tablename)->insert($line);
            }
        }
    }
}
