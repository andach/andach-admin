<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CostCodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [
            'cost_codes' => [
                ['name' => 'Turnover', 'code' => '1000-000-000', 'parent_id' => 0],
                ['name' => 'Expenses', 'code' => '2000-000-000', 'parent_id' => 0],
                ['name' => 'Wages and Salaries', 'code' => '2000-001-000', 'parent_id' => 2],
                ['name' => 'Directors', 'code' => '2000-001-001', 'parent_id' => 3],
                ['name' => 'Staff', 'code' => '2000-002-000', 'parent_id' => 3],
                ['name' => 'Managers', 'code' => '2000-002-001', 'parent_id' => 5],
                ['name' => 'Employees', 'code' => '2000-002-002', 'parent_id' => 5],
                ['name' => 'Casual Employees', 'code' => '2000-002-003', 'parent_id' => 5],
            ],
        ];


        foreach ($array as $tablename => $lines) {
            foreach ($lines as $line) {
                DB::table($tablename)->insert($line);
            }
        }
    }
}
