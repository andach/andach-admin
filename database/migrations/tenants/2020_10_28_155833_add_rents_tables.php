<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRentsTables extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('rentals', function(Blueprint $table) {
      $table->increments('id');
      $table->integer('rented_thing_id');
      $table->string('rented_thing_type');
      $table->integer('user_id');
      $table->date('from_date');
      $table->date('to_date')->nullable();
      $table->timestamps();
      $table->softDeletes();
    });

    Schema::create('rentals_charge_periods', function(Blueprint $table) {
      $table->increments('id');
      $table->integer('rental_id');
      $table->date('from_date');
      $table->date('to_date');
      $table->decimal('price_per_time_unit', 8, 2);
      $table->string('enum_time_unit');
      $table->timestamps();
      $table->softDeletes();
    });

    Schema::create('rentals_responsibility', function(Blueprint $table) {
      $table->increments('id');
      $table->integer('rental_charge_period_id');
      $table->integer('user_id');
      $table->decimal('price', 8, 2);
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('rentals');  
    Schema::drop('rentals_charge_periods');    
  }
}
