<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWorkflightsTable extends Migration {

	public function up()
	{
		Schema::create('workflights', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('workflowable_id');
			$table->string('workflowable_type');
			$table->integer('workflow_id');
			$table->integer('assigned_to_user_id')->nullable();
			$table->integer('state_id');
			$table->string('name');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('workflights');
	}
}