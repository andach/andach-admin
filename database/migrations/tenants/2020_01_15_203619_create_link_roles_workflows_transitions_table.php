<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLinkRolesWorkflowsTransitionsTable extends Migration {

	public function up()
	{
		Schema::create('link_roles_workflows_transitions', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('role_id');
			$table->integer('workflow_transition_id');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('link_roles_workflows_transitions');
	}
}
