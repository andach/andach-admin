<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAppointmentsTable extends Migration {

	public function up()
	{
		Schema::create('appointments', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('job_id');
			$table->integer('position_id');
			$table->integer('team_id');
			$table->integer('user_id');
			$table->date('from_date');
			$table->date('to_date')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('appointments');
	}
}