<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserDefinedFieldsValuesTable extends Migration {

	public function up()
	{
		Schema::create('user_defined_fields_values', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('fieldable_id');
			$table->string('fieldable_type');
			$table->integer('user_defined_field_id');
			$table->integer('user_defined_field_item_id')->nullable();
			$table->string('value_string')->nullable();
			$table->date('value_date')->nullable();
			$table->integer('value_integer')->nullable();
			$table->float('value_float')->nullable();
			$table->text('value_text')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('user_defined_fields_values');
	}
}
