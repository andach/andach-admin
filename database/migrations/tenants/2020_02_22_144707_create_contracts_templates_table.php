<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContractsTemplatesTable extends Migration {

	public function up()
	{
		Schema::create('contracts_templates', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('pay_frequency_id');
			$table->string('name');
			$table->text('full_text');
			$table->float('days_holiday_per_year');
			$table->boolean('days_holiday_includes_bank_holidays');
			$table->float('overtime_at_multiplier');
			$table->float('pension_percent_1')->nullable();
			$table->float('pension_matched_to_percent_1')->nullable();
			$table->float('pension_percent_2')->nullable();
			$table->float('pension_matched_to_percent_2')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('contracts_templates');
	}
}