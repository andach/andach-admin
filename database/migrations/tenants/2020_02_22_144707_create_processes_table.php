<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProcessesTable extends Migration {

	public function up()
	{
		Schema::create('processes', function(Blueprint $table) {
			$table->increments('id');
            $table->string('name');
            $table->text('description');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('processes');
	}
}
