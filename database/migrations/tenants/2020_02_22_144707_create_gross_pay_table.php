<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGrossPayTable extends Migration {

	public function up()
	{
		Schema::create('gross_pay', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('appointment_id');
			$table->integer('pay_period_id');
			$table->decimal('gross_pay');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('gross_pay');
	}
}