<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLinkMessagesUsersTable extends Migration {

	public function up()
	{
		Schema::create('link_messages_users', function(Blueprint $table) {
			$table->increments('id');
			$table->uuid('message_id');
			$table->integer('user_id');
			$table->boolean('is_read')->default(0);
			$table->boolean('is_deleted')->default(0);
			$table->boolean('is_starred')->default(0);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('link_messages_users');
	}
}
