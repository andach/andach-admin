<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHealthAndSafetyInjuriesTable extends Migration {

	public function up()
	{
		Schema::create('health_and_safety_injuries', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('incident_id');
			$table->integer('user_id')->nullable();
			$table->string('enum_body_parts')->nullable();
			$table->string('injured_person_name')->nullable();
			$table->text('injured_person_details')->nullable();
			$table->boolean('was_incapacitated')->nullable();
			$table->boolean('was_medically_treated')->nullable();
			$table->boolean('was_taken_to_hospital')->nullable();
			$table->date('incapacitated_from_date')->nullable();
			$table->date('returned_to_light_duties_date')->nullable();
			$table->date('returned_fully_date')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('health_and_safety_injuries');
	}
}