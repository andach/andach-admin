<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLinkCompaniesUsersTable extends Migration {

	public function up()
	{
		Schema::create('link_companies_users', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('company_id');
			$table->integer('user_id');
			$table->string('model_name');
			$table->string('permission');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('link_companies_users');
	}
}
