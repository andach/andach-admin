<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEthnicOriginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ethnic_origins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
			$table->timestamps();
			$table->softDeletes();
        });

        DB::table('ethnic_origins')->insert([
            ['name' => 'White - English / Welsh / Scottish / Northern Irish / British',],
            ['name' => 'White - Irish'],
            ['name' => 'White - Gypsy or Irish Traveller'],
            ['name' => 'White - Any other White background'],

            ['name' => 'Mixed - White and Black Carribbean'],
            ['name' => 'Mixed - White and Black African'],
            ['name' => 'Mixed - White and Asian'],
            ['name' => 'Mixed - Any other Mixed background'],
            
            ['name' => 'Asian - Indian'],
            ['name' => 'Asian - Pakistani'],
            ['name' => 'Asian - Bangladeshi'],
            ['name' => 'Asian - Chinese'],
            ['name' => 'Asian - Any other Asian background'],

            ['name' => 'Black - African'],
            ['name' => 'Black - Carribbean'],
            ['name' => 'Black - Any other Black background'],

            ['name' => 'Other - Arab'],
            ['name' => 'Other - Any other Ethnic group'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ethnic_origins');
    }
}
