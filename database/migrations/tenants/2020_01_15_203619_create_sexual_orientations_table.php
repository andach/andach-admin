<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSexualOrientationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sexual_orientations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
			$table->timestamps();
			$table->softDeletes();
        });

        DB::table('sexual_orientations')->insert([
            ['name' => 'Heterosexual / Straight'],
            ['name' => 'Homosexual / Gay / Lesbian'],
            ['name' => 'Bisexual'],
            ['name' => 'Asexual'],
            ['name' => 'Other'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sexual_orientations');
    }
}
