<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTimesheetsCategoriesTable extends Migration {

	public function up()
	{
		Schema::create('timesheets_categories', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->timestamps();
			$table->softDeletes();
		});
		
		DB::table('timesheets_categories')->insert(['name' => 'Other']);
		DB::table('timesheets_categories')->insert(['name' => 'Travel']);
		DB::table('timesheets_categories')->insert(['name' => 'HR and Onboarding']);
		DB::table('timesheets_categories')->insert(['name' => 'Training']);
	}

	public function down()
	{
		Schema::drop('timesheets_categories');
	}
}
