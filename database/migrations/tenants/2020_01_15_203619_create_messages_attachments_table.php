<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMessagesAttachmentsTable extends Migration {

	public function up()
	{
		Schema::create('messages_attachments', function(Blueprint $table) {
			$table->uuid('id')->primary();
			$table->integer('message_id');
			$table->string('name');
			$table->string('path');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('messages_attachments');
	}
}
