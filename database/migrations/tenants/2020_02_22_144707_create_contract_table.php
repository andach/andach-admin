<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContractTable extends Migration {

	public function up()
	{
		Schema::create('contracts', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('appointment_id');
			$table->integer('contract_template_id')->nullable();
			$table->integer('working_time_pattern_id')->nullable();
			$table->boolean('is_draft')->default(0);
			$table->date('from_date');
			$table->date('to_date')->nullable();
			$table->text('full_text');
			$table->decimal('pay_per_hour', 8,2)->nullable();
			$table->decimal('salary', 8,2)->nullable();
			$table->decimal('salary_fte', 8,2)->nullable();
			$table->string('file_path')->nullable();
			$table->boolean('is_part_time')->default(0);
			$table->boolean('is_zero_hours')->default(0);
			$table->boolean('is_positive_pay')->default(0);
			$table->decimal('hours_per_week', 8,2)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('contracts');
	}
}
