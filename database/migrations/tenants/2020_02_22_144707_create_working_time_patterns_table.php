<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWorkingTimePatternsTable extends Migration {

	public function up()
	{
		Schema::create('working_time_patterns', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->integer('mon_mins')->default('0');
			$table->integer('tue_mins')->default('0');
			$table->integer('wed_mins')->default('0');
			$table->integer('thu_mins')->default('0');
			$table->integer('fri_mins')->default('0');
			$table->integer('sat_mins')->default('0');
			$table->integer('sun_mins')->default('0');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('working_time_patterns');
	}
}
