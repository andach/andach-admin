<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLinkPositionsCostCodesTable extends Migration {

	public function up()
	{
		Schema::create('link_positions_cost_codes', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('cost_code_id');
			$table->integer('position_id');
			$table->date('from_date');
			$table->date('to_date')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('link_positions_cost_codes');
	}
}