<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLinkJobsRolesTable extends Migration {

	public function up()
	{
		Schema::create('link_jobs_roles', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('job_id');
			$table->integer('role_id');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('link_jobs_roles');
	}
}