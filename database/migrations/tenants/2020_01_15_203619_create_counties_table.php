<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCountiesTable extends Migration {

	public function up()
	{
		Schema::create('counties', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->timestamps();
			$table->softDeletes();
		});

        DB::table('counties')->insert([
			['name' => 'Bedfordshire'],
			['name' => 'Buckinghamshire'], 
			['name' => 'Cambridgeshire'], 
			['name' => 'Cheshire'], 
			['name' => 'Cleveland'], 
			['name' => 'Cornwall'], 
			['name' => 'Cumbria'], 
			['name' => 'Derbyshire'], 
			['name' => 'Devon'], 
			['name' => 'Dorset'], 
			['name' => 'Durham'], 
			['name' => 'East Sussex'], 
			['name' => 'Essex'], 
			['name' => 'Gloucestershire'], 
			['name' => 'Greater London'], 
			['name' => 'Greater Manchester'], 
			['name' => 'Hampshire'], 
			['name' => 'Hertfordshire'], 
			['name' => 'Kent'], 
			['name' => 'Lancashire'], 
			['name' => 'Leicestershire'], 
			['name' => 'Lincolnshire'], 
			['name' => 'Merseyside'], 
			['name' => 'Norfolk'], 
			['name' => 'North Yorkshire'], 
			['name' => 'Northamptonshire'], 
			['name' => 'Northumberland'], 
			['name' => 'Nottinghamshire'], 
			['name' => 'Oxfordshire'], 
			['name' => 'Shropshire'], 
			['name' => 'Somerset'], 
			['name' => 'South Yorkshire'], 
			['name' => 'Staffordshire'], 
			['name' => 'Suffolk'], 
			['name' => 'Surrey'], 
			['name' => 'Tyne and Wear'], 
			['name' => 'Warwickshire'], 
			['name' => 'West Berkshire'], 
			['name' => 'West Midlands'], 
			['name' => 'West Sussex'], 
			['name' => 'West Yorkshire'], 
			['name' => 'Wiltshire'], 
			['name' => 'Worcestershire'], 
			['name' => 'Flintshire'], 
			['name' => 'Glamorgan'], 
			['name' => 'Merionethshire'], 
			['name' => 'Monmouthshire'], 
			['name' => 'Montgomeryshire'], 
			['name' => 'Pembrokeshire'], 
			['name' => 'Radnorshire'], 
			['name' => 'Anglesey'], 
			['name' => 'Breconshire'], 
			['name' => 'Caernarvonshire'], 
			['name' => 'Cardiganshire'], 
			['name' => 'Carmarthenshire'], 
			['name' => 'Denbighshire'], 
			['name' => 'Aberdeen City'], 
			['name' => 'Aberdeenshire'], 
			['name' => 'Angus'], 
			['name' => 'Argyll and Bute'], 
			['name' => 'City of Edinburgh'], 
			['name' => 'Clackmannanshire'], 
			['name' => 'Dumfries and Galloway'], 
			['name' => 'Dundee City'], 
			['name' => 'East Ayrshire'], 
			['name' => 'East Dunbartonshire'], 
			['name' => 'East Lothian'], 
			['name' => 'East Renfrewshire'], 
			['name' => 'Eilean Siar'], 
			['name' => 'Falkirk'], 
			['name' => 'Fife'], 
			['name' => 'Glasgow City'], 
			['name' => 'Highland'], 
			['name' => 'Inverclyde'], 
			['name' => 'Midlothian'], 
			['name' => 'Moray'], 
			['name' => 'North Ayrshire'], 
			['name' => 'North Lanarkshire'], 
			['name' => 'Orkney Islands'], 
			['name' => 'Perth and Kinross'], 
			['name' => 'Renfrewshire'], 
			['name' => 'Scottish Borders'], 
			['name' => 'Shetland Islands'], 
			['name' => 'South Ayrshire'], 
			['name' => 'South Lanarkshire'], 
			['name' => 'Stirling'], 
			['name' => 'West Dunbartonshire'], 
			['name' => 'West Lothian'], 
			['name' => 'Antrim'], 
			['name' => 'Armagh'], 
			['name' => 'Down'], 
			['name' => 'Fermanagh'], 
			['name' => 'Derry and Londonderry'], 
			['name' => 'Tyrone'], 			
        ]);
	}

	public function down()
	{
		Schema::drop('counties');
	}
}
