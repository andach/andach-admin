<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePayPeriodsTable extends Migration {

	public function up()
	{
		Schema::create('pay_periods', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('pay_frequency_id');
			$table->integer('pay_year_id');
			$table->string('name');
			$table->date('from_date');
			$table->date('to_date');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('pay_periods');
	}
}