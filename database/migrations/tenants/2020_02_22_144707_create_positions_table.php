<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePositionsTable extends Migration {

	public function up()
	{
		Schema::create('positions', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('company_id');
			$table->integer('job_id');
			$table->integer('location_id');
			$table->string('name')->default('');
			$table->date('from_date');
			$table->date('to_date')->nullable();
			$table->integer('team_id');
			$table->integer('cost_code_id');
			$table->decimal('hours_per_week', 8,2);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('positions');
	}
}
