<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->integer('company_id');
            $table->string('name');
            $table->string('first_name')->nullable();
            $table->string('middle_names')->nullable();
            $table->string('last_name')->nullable();
            $table->string('preferred_name')->nullable();
            $table->string('email')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
			$table->boolean('is_customer')->default('0');
			$table->boolean('is_staff')->default('0');
			$table->boolean('is_supplier')->default('0');
            $table->boolean('is_super_admin')->default('0');
            $table->string('address_1')->nullable();
            $table->string('address_2')->nullable();
            $table->string('address_3')->nullable();
            $table->string('address_4')->nullable();
            $table->string('county_id')->nullable();
            $table->string('postcode')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('mobile_number')->nullable();
            $table->string('enum_gender')->nullable();
            $table->integer('ethnic_origin_id')->nullable();
            $table->integer('religion_id')->nullable();
            $table->integer('sexual_orientation_id')->nullable();
            $table->string('picture_path')->nullable();
            $table->rememberToken();
			$table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
