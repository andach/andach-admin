<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSalesInvoiceTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_invoices', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('name')->nullable();
            $table->date('invoice_date');
            $table->date('supply_date');
            $table->text('terms_of_payment');
            $table->decimal('total_net');
            $table->decimal('total_vat');
            $table->decimal('total_gross');
            $table->boolean('is_finalised');
            $table->timestamps();
        });

        Schema::create('sales_invoices_lines', function (Blueprint $table) {
            $table->id();
            $table->text('description');
            $table->integer('thing_being_invoiced_id');
            $table->integer('thing_being_invoiced_type');
            $table->decimal('net');
            $table->decimal('vat');
            $table->decimal('gross');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_invoices');
        Schema::dropIfExists('sales_invoices_lines');
    }
}
