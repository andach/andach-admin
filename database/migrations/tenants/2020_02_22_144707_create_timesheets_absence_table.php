<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTimesheetsAbsenceTable extends Migration {

	public function up()
	{
		Schema::create('timesheets_absence', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->timestamps();
			$table->softDeletes();
		});
		
		DB::table('timesheets_absence')->insert(['name' => 'Stomach']);
		DB::table('timesheets_absence')->insert(['name' => 'Headache']);
		DB::table('timesheets_absence')->insert(['name' => 'Dental / Tooth Issue']);
		DB::table('timesheets_absence')->insert(['name' => 'Mental Health']);
	}

	public function down()
	{
		Schema::drop('timesheets_absence');
	}
}
