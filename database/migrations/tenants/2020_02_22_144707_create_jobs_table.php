<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJobsTable extends Migration {

	public function up()
	{
		Schema::create('jobs', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->text('description_of_work')->nullable();
			$table->boolean('is_director')->default(0);
			$table->boolean('is_positively_paid')->default(0);
			$table->decimal('fte_hours', 8,2);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('jobs');
	}
}