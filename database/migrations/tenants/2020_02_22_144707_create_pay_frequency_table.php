<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePayFrequencyTable extends Migration {

	public function up()
	{
		Schema::create('pay_frequency', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->timestamps();
			$table->softDeletes();
		});

        DB::table('pay_frequency')->insert(['name' => 'Monthly']);
        DB::table('pay_frequency')->insert(['name' => 'Fortnightly']);
        DB::table('pay_frequency')->insert(['name' => 'Weekly']);
	}

	public function down()
	{
		Schema::drop('pay_frequency');
	}
}