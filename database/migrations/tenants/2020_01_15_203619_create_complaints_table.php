<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateComplaintsTable extends Migration {

	public function up()
	{
		Schema::create('complaints', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('company_id');
			$table->integer('customer_id')->nullable();
			$table->integer('product_id')->nullable();
			$table->integer('supplier_id')->nullable();
			$table->integer('location_id')->nullable();
			$table->integer('user_id');
			$table->string('name');
			$table->text('description');
			$table->boolean('is_closed')->default(0);
			$table->boolean('response_required')->default(0);
			$table->date('response_sent_date')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('complaints');
	}
}
