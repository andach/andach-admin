<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLinkProcessesTeamsTable extends Migration {

	public function up()
	{
		Schema::create('link_processes_teams', function(Blueprint $table) {
			$table->increments('id');
            $table->integer('process_id');
            $table->integer('team_id');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('link_processes_teams');
	}
}
