<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWorkflightsAuditTable extends Migration {

	public function up()
	{
		Schema::create('workflights_audit', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('assigned_to_user_id')->nullable();
			$table->integer('instigating_user_id');
			$table->integer('state_id');
			$table->integer('workflight_id');
			$table->datetime('from_datetime');
			$table->datetime('to_datetime')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('workflights_audit');
	}
}