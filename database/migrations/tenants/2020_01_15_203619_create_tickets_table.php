<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTicketsTable extends Migration {

	public function up()
	{
		Schema::create('tickets', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('customer_id')->nullable();
			$table->integer('company_id');
			$table->integer('location_id')->nullable();
			$table->integer('user_id');
			$table->string('name');
			$table->text('description');
			$table->boolean('is_closed')->default(0);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('tickets');
	}
}
