<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWorkflowsTransitionsTable extends Migration {

	public function up()
	{
		Schema::create('workflows_transitions', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('workflow_id');
			$table->string('name');
			$table->integer('workflow_state_from_id');
			$table->integer('workflow_state_to_id');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('workflows_transitions');
	}
}