<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReligionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('religions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
			$table->timestamps();
			$table->softDeletes();
        });

        DB::table('religions')->insert([
            ['name' => 'Christian - Church of England'],
            ['name' => 'Christian - Catholic'],
            ['name' => 'Christian - No Denomination'],
            ['name' => 'Christian - Other'],
            
            ['name' => 'Islam - Sunni'],
            ['name' => 'Islam - Shia'],
            ['name' => 'Islam - Other'],

            ['name' => 'Hinduism'],
            ['name' => 'Sikhism'],
            ['name' => 'Judaism'],
            ['name' => 'Buddhist'],

            ['name' => 'Pagan'],
            ['name' => 'No Religion'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('religions');
    }
}
