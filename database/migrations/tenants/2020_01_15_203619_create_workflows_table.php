<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWorkflowsTable extends Migration {

	public function up()
	{
		Schema::create('workflows', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('model_name');
			$table->integer('min_location_level');
			$table->integer('max_location_level');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('workflows');
	}
}
