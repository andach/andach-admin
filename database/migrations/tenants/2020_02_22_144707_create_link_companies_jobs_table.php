<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLinkCompaniesJobsTable extends Migration {

	public function up()
	{
		Schema::create('link_companies_jobs', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('link_companies_jobs');
	}
}