<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTimesheetsTable extends Migration {

	public function up()
	{
		Schema::create('timesheets', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('appointment_id');
			$table->integer('job_id');
			$table->integer('payslip_id')->nullable();
			$table->integer('position_id');
			$table->integer('shift_id')->nullable();
			$table->integer('team_id');
			$table->integer('absence_id')->nullable();
			$table->integer('category_id')->nullable();
			$table->integer('process_id')->nullable();
			$table->integer('project_task_id')->nullable();
			$table->integer('mins_logged');
			$table->integer('mins_break')->nullable();
            $table->datetime('start_datetime')->nullable();
            $table->datetime('end_datetime')->nullable();
			$table->date('date_of_timesheet');
			$table->date('week_of_timesheet')->nullable();
			$table->text('description')->nullable();
			$table->boolean('is_approved')->default(0);
			$table->boolean('is_rejected')->default(0);
			$table->integer('approved_by_appointment_id')->nullable();
			$table->datetime('approved_datetime')->nullable();
			$table->text('approved_comment')->default('');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('timesheets');
	}
}
