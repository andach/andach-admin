<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShiftsTable extends Migration {

	public function up()
	{
		Schema::create('shifts', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('appointment_id');
			$table->integer('job_id');
			$table->integer('position_id');
            $table->integer('team_id');
            $table->datetime('start_datetime');
            $table->datetime('end_datetime');
            $table->integer('minutes_worked');
            $table->integer('minutes_break');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('shifts');
	}
}
