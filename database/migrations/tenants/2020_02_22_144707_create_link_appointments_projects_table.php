<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLinkAppointmentsProjectsTable extends Migration {

	public function up()
	{
		Schema::create('link_appointments_projects', function(Blueprint $table) {
			$table->increments('id');
            $table->integer('appointment_id');
            $table->integer('project_id');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('link_appointments_projects');
	}
}
