<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectsTable extends Migration {

	public function up()
	{
		Schema::create('projects', function(Blueprint $table) {
			$table->increments('id');
            $table->integer('appointment_id');
            $table->string('name');
            $table->integer('parent_id')->default(0);
            $table->text('description');
            $table->integer('minutes_budgeted');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('projects');
	}
}
