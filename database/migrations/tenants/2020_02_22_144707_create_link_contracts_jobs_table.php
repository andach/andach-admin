<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLinkContractsJobsTable extends Migration {

	public function up()
	{
		Schema::create('link_contracts_jobs', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('contract_id');
			$table->integer('job_id');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('link_contracts_jobs');
	}
}