<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHealthAndSafetyIncidentsTable extends Migration {

	public function up()
	{
		Schema::create('health_and_safety_incidents', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('company_id');
			$table->integer('location_id')->nullable();
			$table->integer('user_id');
			$table->string('name');
			$table->text('description');
			$table->string('enum_kind_of_accident');
			$table->string('enum_work_process');
			$table->string('enum_main_factor');
			$table->boolean('is_closed')->default(0);
			$table->boolean('is_riddor_reportable')->nullable();
			$table->date('riddor_report_date')->nullable();
			$table->string('riddor_report_reference')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('health_and_safety_incidents');
	}
}
