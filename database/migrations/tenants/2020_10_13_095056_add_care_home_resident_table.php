<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCareHomeResidentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('care_home_residents', function(Blueprint $table) {
            $table->uuid('id')->primary();
            $table->integer('user_id');
            $table->boolean('is_challenging')->nullable();
            $table->text('challenging_details')->default('');
            $table->boolean('is_safeguarding')->nullable();
            $table->text('safeguarding_details')->default('');
            $table->text('allergies')->default('');
            $table->text('gp_details')->default('');
            $table->text('next_of_kin')->default('');
            $table->text('power_of_attorney')->default('');
			$table->timestamps();
			$table->softDeletes();
        });
        
		Schema::create('care_home_records_catheters', function(Blueprint $table) {
            $table->increments('id');
            $table->string('resident_id');
            $table->date('date_of_assessment');
            $table->integer('completed_by_user_id');
            $table->date('date_changed');
            $table->text('reasons');
            $table->string('size');
            $table->integer('changed_by_user_id');
			$table->timestamps();
			$table->softDeletes();
		});
        
		Schema::create('care_home_records_end_of_life_wishes', function(Blueprint $table) {
            $table->increments('id');
            $table->string('resident_id');
            $table->date('date_of_assessment');
            $table->integer('completed_by_user_id');
            $table->text('what')->default('');
            $table->text('who')->default('');
            $table->text('where')->default('');
            $table->text('special_requests')->default('');
			$table->timestamps();
			$table->softDeletes();
		});
        
		Schema::create('care_home_records_falls_risk_assessments', function(Blueprint $table) {
            $table->increments('id');
            $table->string('resident_id');
            $table->date('date_of_assessment');
            $table->integer('completed_by_user_id');
            $table->text('json_response');
			$table->timestamps();
			$table->softDeletes();
		});
        
		Schema::create('care_home_records_food_log', function(Blueprint $table) {
            $table->increments('id');
            $table->string('resident_id');
            $table->integer('completed_by_user_id');
            $table->date('date_eaten');
            $table->string('enum_meal');
            $table->string('food_and_drink');
            $table->string('fortified_with')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
        
		Schema::create('care_home_records_hospital_admissions', function(Blueprint $table) {
            $table->increments('id');
            $table->string('resident_id');
            $table->date('date_of_assessment');
            $table->integer('completed_by_user_id');
            $table->date('date_from');
            $table->date('date_to')->nullable();
            $table->string('reason')->nullable();
            $table->string('outcome')->nullable();
            $table->text('extra_risk_assessment')->default('');
			$table->timestamps();
			$table->softDeletes();
		});
        
		Schema::create('care_home_records_illnesses', function(Blueprint $table) {
            $table->increments('id');
            $table->string('resident_id');
            $table->date('date_of_assessment');
            $table->integer('completed_by_user_id');
            $table->date('date_from');
            $table->date('date_to')->nullable();
            $table->string('illness')->nullable();
            $table->text('general_observations')->default('');
            $table->text('extra_risk_assessment')->default('');
			$table->timestamps();
			$table->softDeletes();
		});
        
		Schema::create('care_home_records_malnutrition_assessments', function(Blueprint $table) {
            $table->increments('id');
            $table->string('resident_id');
            $table->date('date_of_assessment');
            $table->integer('completed_by_user_id');
            $table->integer('must_bmi');
            $table->integer('must_weight_loss');
            $table->boolean('is_acutely_ill')->nullable();
            $table->text('additional_comments')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
        
		Schema::create('care_home_records_medical', function(Blueprint $table) {
            $table->increments('id');
            $table->string('resident_id');
            $table->date('date_of_assessment');
            $table->integer('completed_by_user_id');
            $table->string('enum_mobility')->nullable();
            $table->string('enum_continence')->nullable();
            $table->string('enum_cognition')->nullable();
            $table->string('enum_communication')->nullable();
            $table->string('enum_hearing')->nullable();
            $table->string('enum_sight')->nullable();
            $table->string('blood_pressure')->nullable();
            $table->integer('pulse')->nullable();
            $table->string('oxygen_sats')->nullable();
            $table->string('respiration_rate')->nullable();
            $table->string('enum_smoker')->nullable();
            $table->string('enum_alcohol')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
        
		Schema::create('care_home_records_skin_integrity_assessments', function(Blueprint $table) {
            $table->increments('id');
            $table->string('resident_id');
            $table->date('date_of_assessment');
            $table->integer('completed_by_user_id');
            $table->integer('braden_sensory_perception');
            $table->integer('braden_moisture');
            $table->integer('braden_activity');
            $table->integer('braden_mobility');
            $table->integer('braden_nutrition');
            $table->integer('braden_friction');
            $table->integer('braden_total');
            $table->text('general_observations')->default('');
            $table->boolean('is_broken_skin')->nullable();
            $table->boolean('is_infection_present')->nullable();
            $table->string('infection_treated_with')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
        
		Schema::create('care_home_records_weight_log', function(Blueprint $table) {
            $table->increments('id');
            $table->string('resident_id');
            $table->date('date_of_assessment');
            $table->integer('completed_by_user_id');
            $table->float('weight_in_kg')->nullable();
            $table->float('weight_in_lbs')->nullable();
            $table->float('bmi')->nullable();
			$table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('care_home_residents');
		Schema::drop('care_home_records_catheters');
		Schema::drop('care_home_residents_end_of_life_wishes');
		Schema::drop('care_home_records_falls_risk_assessments');
		Schema::drop('care_home_records_food_log');
		Schema::drop('care_home_records_hospital_admissions');
		Schema::drop('care_home_records_illnesses');
		Schema::drop('care_home_records_malnutrition_assessments');
		Schema::drop('care_home_records_medical');
		Schema::drop('care_home_records_skin_integrity_assessments');
		Schema::drop('care_home_records_weight_log');
    }
}
