<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHolidaysTable extends Migration {

	public function up()
	{
		Schema::create('holidays', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('appointment_id');
			$table->datetime('from_datetime');
			$table->datetime('to_datetime');
			$table->decimal('days_off', 8,2);
			$table->decimal('hours_off', 8,2);
			$table->boolean('is_approved')->default(0);
			$table->boolean('is_rejected')->default(0);
			$table->integer('approved_by_appointment_id')->nullable();
			$table->datetime('approved_datetime')->nullable();
			$table->timestamps();
			$table->softDeletes();
			
		});
	}

	public function down()
	{
		Schema::drop('holidays');
	}
}
