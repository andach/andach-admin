<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectsTasksTable extends Migration {

	public function up()
	{
		Schema::create('projects_tasks', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('parent_id')->default(0);
			$table->integer('project_id');
            $table->string('name');
            $table->text('description');
            $table->integer('minutes_budgeted');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('projects_tasks');
	}
}
