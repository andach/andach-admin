<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLinkUdfValuesWorkflowsTransitionsTable extends Migration {

	public function up()
	{
		Schema::create('link_udf_values_workflows_transitions', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('udf_id');
			$table->integer('workflow_transition_id');
            $table->boolean('allow_or_deny');
            $table->string('operator');
            $table->string('operator_value');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('link_udf_values_workflows_transitions');
	}
}
