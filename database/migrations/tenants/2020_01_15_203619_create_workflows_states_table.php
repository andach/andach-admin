<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWorkflowsStatesTable extends Migration {

	public function up()
	{
		Schema::create('workflows_states', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('workflow_id');
			$table->string('name');
			$table->boolean('is_default')->default(0);
			$table->boolean('is_closed')->default(0);
			$table->boolean('default_instant_close_status')->default(0);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('workflows_states');
	}
}
