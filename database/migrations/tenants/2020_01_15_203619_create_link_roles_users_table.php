<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLinkRolesUsersTable extends Migration {

	public function up()
	{
		Schema::create('link_roles_users', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('role_id');
			$table->integer('user_id');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('link_roles_users');
	}
}