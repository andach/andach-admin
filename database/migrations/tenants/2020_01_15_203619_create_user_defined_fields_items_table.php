<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserDefinedFieldsItemsTable extends Migration {

	public function up()
	{
		Schema::create('user_defined_fields_items', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_defined_field_id')->unsigned();
			$table->string('name');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('user_defined_fields_items');
	}
}
