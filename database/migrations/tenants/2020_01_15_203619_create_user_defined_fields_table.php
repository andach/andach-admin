<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserDefinedFieldsTable extends Migration {

	public function up()
	{
		Schema::create('user_defined_fields', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('display_name');
			$table->text('description')->nullable();
			$table->string('type');
			$table->string('model_name');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('user_defined_fields');
	}
}
