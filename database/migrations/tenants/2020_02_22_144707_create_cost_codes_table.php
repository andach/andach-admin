<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCostCodesTable extends Migration {

	public function up()
	{
		Schema::create('cost_codes', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('code')->nullable();
			$table->integer('parent_id')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('cost_codes');
	}
}