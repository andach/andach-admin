<?php

/*
 * This page contains information for the menu and permissions. Where an object has no menu item, it still needs an entry here, with an 
 * empty array. 
 */

return [
    'menu' => [
        'resident' => [
            'create' => [
                'text' => 'Create Resident',
                'route' => 'care-home.resident.create',
                'icon' => 'nav-icon fas fa-plus',
            ],
            'read' => [
                [
                    'text' => 'Current Residents',
                    'route' => 'care-home.resident.index',
                    'icon' => 'nav-icon fas fa-list',
                ],
                [
                    'text' => 'Historic Residents',
                    'route' => 'care-home.resident.historic',
                    'icon' => 'nav-icon fas fa-list',
                ],
            ],
            'edit' => [],
        ],
        
        'complaint' => [
            'create' => [
                'text' => 'Create Complaint',
                'route' => 'complaint.create',
                'icon' => 'nav-icon fas fa-plus',
            ],
            'read' => [
                'text' => 'All Complaints',
                'route' => 'complaint.index',
                'icon' => 'nav-icon fas fa-list',
            ],
            'mine' => [
                'text' => 'Assigned to Me',
                'route' => 'complaint.mine',
                'icon' => 'nav-icon fas fa-briefcase',
            ],
            'edit' => [],
        ],

        'health-and-safety-incident' => [
            'create' => [
                'text' => 'Create Incident',
                'route' => 'health-and-safety-incident.create',
                'icon' => 'nav-icon fas fa-plus',
            ],
            'read' => [
                'text' => 'All Incidents',
                'route' => 'health-and-safety-incident.index',
                'icon' => 'nav-icon fas fa-list',
            ],
            'mine' => [
                'text' => 'Assigned to Me',
                'route' => 'health-and-safety-incident.mine',
                'icon' => 'nav-icon fas fa-briefcase',
            ],
            'dashboard' => [
                'text' => 'Dashboard',
                'route' => 'health-and-safety-incident.dashboard',
                'icon' => 'nav-icon fas fa-chart-line',
            ],
            'edit' => [],
        ],
        
        'ticket' => [
            'create' => [
                'text' => 'Create Ticket',
                'route' => 'ticket.create',
                'icon' => 'nav-icon fas fa-plus',
            ],
            'read' => [
                'text' => 'All Tickets',
                'route' => 'ticket.index',
                'icon' => 'nav-icon fas fa-list',
            ],
            'mine' => [
                'text' => 'Assigned to Me',
                'route' => 'ticket.mine',
                'icon' => 'nav-icon fas fa-briefcase',
            ],
            'edit' => [],
        ],
        
        'user' => [
            'create' => [
                'text' => 'Create User',
                'route' => 'user.create',
                'icon' => 'nav-icon fas fa-plus',
            ],
            'read' => [
                'text' => 'All Users',
                'route' => 'user.index',
                'icon' => 'nav-icon fas fa-list',
            ],
            'mine' => [
                'text' => 'My Account',
                'route' => 'user.my-account',
                'icon' => 'nav-icon fas fa-briefcase',
            ],
            'edit' => [],
        ],

        'role' => [
            'admin' => [
                'text' => 'Manage Roles',
                'route' => 'role.index',
                'icon' => 'nav-icon fas fa-list',
            ],
        ],
        
        'user-defined-field' => [
            'create' => [
                'text' => 'Create UDF',
                'route' => 'user-defined-field.create',
                'icon' => 'nav-icon fas fa-plus',
            ],
            'read' => [
                'text' => 'All UDFs',
                'route' => 'user-defined-field.index',
                'icon' => 'nav-icon fas fa-list',
            ],
            'edit' => [],
        ],
        
        'company' => [
            'admin' => [
                'text' => 'Manage Companies',
                'route' => 'company.index',
                'icon' => 'nav-icon fas fa-list',
            ],
        ],
        
        'location' => [
            'admin' => [
                'text' => 'Manage Locations',
                'route' => 'location.index',
                'icon' => 'nav-icon fas fa-list',
            ],
        ],
        
        'customer' => [
            'create' => [
                'text' => 'Create Incident',
                'route' => 'customer.create',
                'icon' => 'nav-icon fas fa-plus',
            ],
            'read' => [
                'text' => 'All Incidents',
                'route' => 'customer.index',
                'icon' => 'nav-icon fas fa-list',
            ],
            'edit' => [],
        ],

        'workflow' => [
            'admin' => [
                [
                    'text' => 'Create Workflow',
                    'route' => 'workflow.create',
                    'icon' => 'nav-icon fas fa-plus',
                ],
                [
                    'text' => 'All Workflow',
                    'route' => 'workflow.index',
                    'icon' => 'nav-icon fas fa-list',
                ],
            ]
        ],

        'hr' => [
            'admin' => [
                [
                    'text' => 'Jobs',
                    'route' => 'hr.jobs',
                    'icon' => 'nav-icon fas fa-plus',
                ],
                [
                    'text' => 'Pay Periods',
                    'route' => 'hr.pay-periods',
                    'icon' => 'nav-icon fas fa-list',
                ],
                [
                    'text' => 'Pay Years',
                    'route' => 'hr.pay-years',
                    'icon' => 'nav-icon fas fa-list',
                ],
                [
                    'text' => 'Positions',
                    'route' => 'position.index',
                    'icon' => 'nav-icon fas fa-list',
                ],
                [
                    'text' => 'Working Time Patterns',
                    'route' => 'hr.working-time-patterns',
                    'icon' => 'nav-icon fas fa-list',
                ],
            ],
        ],

        'team' => [
            'edit' => [
                [
                    'text' => 'Jobs',
                    'route' => 'hr.jobs',
                    'icon' => 'nav-icon fas fa-plus',
                ],
                [
                    'text' => 'Pay Periods',
                    'route' => 'hr.pay-periods',
                    'icon' => 'nav-icon fas fa-list',
                ],
                [
                    'text' => 'Pay Years',
                    'route' => 'hr.pay-years',
                    'icon' => 'nav-icon fas fa-list',
                ],
                [
                    'text' => 'Positions',
                    'route' => 'position.index',
                    'icon' => 'nav-icon fas fa-list',
                ],
                [
                    'text' => 'Working Time Patterns',
                    'route' => 'hr.working-time-patterns',
                    'icon' => 'nav-icon fas fa-list',
                ],
            ],
        ],
    ],

    'menu_headers' => [
        'resident' => [
            'name' => 'Residents',
            'icon' => 'nav-icon fas fa-universal-access',
        ],
        'complaint' => [
            'name' => 'Complaints',
            'icon' => 'nav-icon fas fa-exclamation-triangle',
        ],
        'health-and-safety-incident' => [
            'name' => 'H&amp;S Incidents',
            'icon' => 'nav-icon fas fa-hard-hat',
        ],
        'ticket' => [
            'name' => 'Tickets',
            'icon' => 'nav-icon fas fa-clipboard-list',
        ],
        'user' => [
            'name' => 'Users',
            'icon' => 'nav-icon fas fa-users',
        ],
        'user-defined-field' => [
            'name' => 'UDFs',
            'icon' => 'nav-icon fas fa-list-ol',
        ],
        'company' => [
            'name' => 'Companies',
            'icon' => 'nav-icon fas fa-building',
        ],
        'location' => [
            'name' => 'Locations',
            'icon' => 'nav-icon fas fa-globe-europe',
        ],
        'role' => [
            'name' => 'Roles',
            'icon' => 'nav-icon fas fa-mask',
        ],
        'customer' => [
            'name' => 'Customers',
            'icon' => 'nav-icon fas fa-handshake',
        ],
        'workflow' => [
            'name' => 'Workflow',
            'icon' => 'nav-icon fas fa-network-wired',
        ],
        'hr' => [
            'name' => 'HR',
            'icon' => 'nav-icon fas fa-network-wired',
        ],
    ],
];