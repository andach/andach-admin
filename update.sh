#!/bin/bash

function pullstuff() {
        echo "Navigating to $1 to pull down from git"
        cd $1
        sudo -u www-data git config --global credential.helper store
        sudo -u www-data php artisan down
        sudo -u www-data git pull
        sudo -u www-data composer install --no-dev --no-interaction --prefer-dist --optimize-autoloader

        # Clear caches
        php artisan cache:clear

        # Clear expired password reset tokens
        php artisan auth:clear-resets

        # Clear and cache routes
        php artisan route:clear
        php artisan route:cache

        # Clear and cache config
        php artisan config:clear
        php artisan config:cache

        # Turn off maintenance mode
        php artisan up
}

echo Hello, World

# Use $1 to get the first argument:
if [ "$1" = "" ]
then
        echo Please specify the folder to update:
        read folder
else
        folder=$1
fi

echo We will update $folder
base="/var/www/html/"

directory="$base$folder"

if [[ -d "$directory" ]]
then
        echo "$directory exists on your filesystem."
        pullstuff $directory
else
        echo "Could not find the directory '$directory'. Script will stop execution"
fi
