# Andach Admin

This is a project to create a simple database to allow businesses to manage complaints, health and safety incidents and tickets, in a single centralised database. It will also allow for workflow for these objects to be managed and maintained. 

## Updating

php artisan db:seed --class=ProductionSeeder
composer install --no-dev && php artisan migrate

## Data Map

Note that model_name is referred to consistently throughout the script. By convention this will be the `protected $tableName` attribute on the model. 

### Workflows and Workflights

A "workflow" is a series of states and transitions. An instance of a workflow is called a "workflight". A workflow is linked to a model, which requies the trait App\Traits\CreatesWorkflight. Adding this trait to a model will automatically, on creation (using Laravel's onCreate event) create a workflight of the corresponding workflow.

Workflows can be different for different companies, reflecting that (for example), two companies in the same group might have different processes surrounding complaints. 

* Company A may require external evidence to be sent, so would have according states and transitions. 
* Company B might require two signoffs, from the team leader and the manager, so would have two states "Signed off by Team Leader" and "Signed off by Manager". 

Workflows are linked to genericised concepts of models by the `workflows_models` table. The trait will look for a workflow matching the model type and the companyID, and will 'fall back' to a workflow matching just the model type, with a null companyID. 

On creation, a workflight is immediately created, polymorphically linked to the instance of the model that has the CreatesWorkflight. Who is allowed to move a model from state to state is controlled by the link_roles_workflows_transitions table. It is anticipated that this will be set up once and very rarely changed. A user should be considered to be able to comment on a workflow object if:

* The user has the permission "model_name.comment". 
* The user has permission to move the model from its current state to at least one other state (from the "link_roles_workflows_transitions" table). 

## Workflowed Models
A user has permission to access a particular method on a workflowed model (e.g. an individual complaint or health and safety incident) if they have:

* Access to an appropriate permission "model_name.show", "model_name.edit", etc. 
* Access to the appropriate company via the `link_companies_users` table. 

## Permissions
Permissions are assignable individually to users. Eventually, a HR module will be built, where roles will be assigned to jobs, users will be appointed to jobs (etc), and more permission will be available this way. Permissions can also be assigned to users directly. 

* Permissions can be assigned directly to users. 
* Users can have a role directly assigned, which in turn gives various permissions. 
* Users can be appointed to a position, which is in turn linked to a job, multi-linked to various roles, with permissions. 

For example, if a user has access to the complaints.index permission, they can open the corresponding route. This role is checked with middleware, and verifies if the user has any access at all to see the complaint index function. 

To see whether a user can edit, see (etc.) the INDIVIDUAL model, this is taken care with by a policy. For index/search functions, the $user->canAccessCompaniesForModel('model_name') method should be included in the logic as a hard filter. 

## User Defined Fields and Categories
These are both available to be linked to arbitrary models. A user-defined field is a string, date, integer, float, text field or list. UDFs are linked (possibly limited by company ID) to certain models using the `user_defined_fields` and `user_defined_field_items` tables (the second being used only when the UDF is a list).

UDFs are limited to basic information, and cannot be used to create links between two models. A category is simply a user-defined field that is a list. 

## Dev Setup and Seeding
The dev setup is five users:

### Companies
1. Parent Company
2. Trading Company (under Parent)
3. Property Company (under Parent)
4. Dormant Company (under Trading)

Note we are assuming that parent and trading company have tickets, incidents and complaints. Property company has incidents and tickets, and dormant company has nothing. 

### Workflows
1. Single State Authorisation - a simple "enter and forget" process with only an open and closed state. This is linked to complaints, tickets and incidents for the parent company, for a situation whereby most of the activity goes on in the trading co, and receptionist is assumed to log the tiny number of incidents legally relating to the parent. 
2. Authorisation by Manager - Someone has responsibility for signing off items. This is used for both complaints and tickets in the trading company, and tickets in the property company. 
3. Health and Safety Investigation - this has an optional second manager signoff if relevant. Used for incidents in trading and property company. 

### Users
1. superadmin@example.com - Superadmin access to parent company. 
2. ticketmanager@example.com - Access to manage tickets (for all companies), workflows, users and UDFs (without ability to limit by company). 
3. complaintmanager@example.com - Access to enter & sign off complaints for company 2. 
4. complaintemployee@example.com - Access to enter complaints for company 2, and create tickets.  
5. healthandsafetydirector@example.com - Access to the second signoff for health and safety incidents throughout the group, to close tickets with child safety review. 
6. healthandsafetymanager@example.com - Access to enter and first signoff for all health and safety incidents for the group, and to close tickets. 
7. healthandsafetytradingemployee@example.com - Access to enter health and safety incidents for company 2, and create tickets (for company 2). 
8. healthandsafetypropertyemployee@example.com - Access to enter health and safety incidents for company 3, and create tickets (for company 3). 
9. receptionist@example.com - Full access for complaints, incidents and tickets for company 1, no access to other companies. Full access to users. Has access to Administration and Setup. 
10. pleb@example.com - Access to view users. No access to any workflow at all. 
11. customer1@example.com
12. customer2@example.com
13. otherhealthandsafetypropertyemployee@example.com - Access to enter health and safety incidents for company 3, and create tickets (for company 3). 

### Users (Allowed Ticket Transitions)
1. Everything
2. Open, Instantly Close, Manager Signoff
3. N/A
4. Open
5. Open, Instantly Close, Child Safety Signoff
6. Open, Instantly Close
7. Open
8. Open
9. Open, Instantly Close
10. N/A

## Checks
This needs to work for:
* A company with branches that needs to manage health and safety incidents. Managers need to sign off their own branches in a hierarchy, with area/regional managers able to sign off where a manager is available. An injury will result in a second signoff being needed. Second signoff has to be area manager or higher. 
* A company with only one single, simple workflow of open and closed complaints. 
* A company with complaints that vary on location (i.e. location in which products were manufactured). The production manager at the location and the central manager both need to sign off the complaint. 
* A company that can have tickets that require specialist knowledge, requiring them to be viewed by a separate team. 
* A company where the health and safety team leader for a location and the complaint team leader might be different people. 

This roughly means we need to:
* Limit transitions to locations. 
* Limit workflowables to companies. 
* Limit transitions based on UDFs. 
* Limit workflowables and transitions based on role. 

A workflowable will have a location (as a simple `location_id`), and each workflowable will be linked to a certian number of allowed locations or location levels (through the `link_locations_workflowables` table). Workflows will be linked to a max and min location level, which would usually be the same. Level 0 should be a dummy location. 

For example, complaints might be at a site level, whereas health and safety incidents might be at room level. A user's access to a location [for a workflowable, they might be different for each one] changes what he or she can assign the object to initially and also changes their access. 

Users will have access to {workflowables and companies} via the `link_companies_users` table. This is completely separate from the permissions and roles tables, which affect what view someone can see. A user will need BOTH an appropriate role AND permission to access the workflowable for the company to see it. 

To transit a workflowable to another status, we will have to look at:
* What transitions are available to this workflowable based on its UDFs. 
* What transitions the user has access to through their role. 

## HR and Employees
The system has a position-based (or chair-based) HR system designed to allow for appointing people easily to pre-existing positions and jobs. System supports:

* Full time employees on salary. 
* Part time employees on fixed contracts. 
* Part time employees on fixed number of hours per week, with or without overtime. 
* Zero hours contracts on floating number of hours per week. 

Supports pay frequency of:
* Weekly
* Two-weekly
* Monthly

## Users
1. superadmin@example.com - (Directors) Director Parent, FT, £100k, Contract-Director. Costed to 2000-001-0001 (Directors). 
2. ticketmanager@example.com - (Tickets) Trading, PT (22.5 hours, working time M/T/W 7.5 hours each), £18k FTE, Contract-Manager, 2000-002-0001.  
3. complaintmanager@example.com - (Complaints) Trading, FT, £50k, Contract-Manager, 2000-002-0003
4. complaintemployee@example.com - (Complaints) Trading, FT, £20k, Staff, 2000-002-0003
5. healthandsafetydirector@example.com - (H&S) Director Trading, FT, £80k, Contract-Director, 2000-002-0002. 
6. healthandsafetymanager@example.com - (H&S) Trading, FT, £60k, Contract-Manager, 2000-002-0002. Previously employed as the trading employee. 
7. healthandsafetytradingemployee@example.com - (H&S) Trading, PT (20 hours per week, overtime at time), £10/hour, Staff No Bank Holidays, 2000-002-0002. 
8. healthandsafetypropertyemployee@example.com - (H&S) Property, PT (20 hours per week, overtime at 1.5), £9/hour, Staff, 2000-002-0002. 
9. receptionist@example.com - (Directors) Parent, PT (30 hours, working time M/Tu/Th 8 hours, F 6 hours), £20k FTE, Staff, 2000-001-0001. 
10. pleb@example.com - (Tickets) Trading, ZH, £8/hour, Zero Hours (+ve pay), 2000-002-0001. Previously employed at £7.50/hour. 
11. customer
12. customer
13. otherhealthandsafetypropertyemployee@example.com - (H&S) Property, PT (20 hours per week, overtime at 1.5), £9/hour, Staff, 2000-002-0002. Plus a Zero-Hour Cleaning Contract. 

## Teams
1. Directors (Parent Team)
2. Tickets (Reports into Directors)
3. Health and Safety (Reports into Directors)
4. Complaints (Reports into Health and Safety)

## Contracts Templates
1. Directors (25+8, Pension doubled to 5% contributions, matched to 10%). Monthly. 
2. Managers (25+8, Pension matched to 5%). Monthly. 
3. Staff (20+8, Pension matched to 4%). Two-weekly. 
4. Staff No Bank Holidays (28, Pension matched to 4%). Two-weekly. 
5. Zero Hours (20+8, Pension matched to 4%). Weekly. 

## Tenancy Notes

To run updates on all tenant databases run "php artisan tenants:migrate". 

To create a tenant database run "php artisan tenants:create 1"

To seed a database run "php artisan tenants:seed 1"

php artisan make:migration create_posts_table ---path=database/migrations/tenants

To create a new tenant, add an entry to the landlords.tenants table, and run the commands to create, then seed their database. 