<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if (request()->getHost() === env('TENANT_DOMAIN'))
{
    Route::post('/', 'LandlordController@signUp')->name('landlord.sign-up');
    Route::get('/success', 'LandlordController@success')->name('landlord.success');

    Route::view('/docs/initial-setup', 'landlord.static.docs.initial-setup')->name('landlord.docs.initial-setup');
    Route::view('/docs/workflow', 'landlord.static.docs.workflow')->name('landlord.docs.workflow');
    Route::view('/docs', 'landlord.static.docs')->name('landlord.docs');

    Route::view('/', 'landlord.static.homepage')->name('landlord.static.homepage');
} else {
    Auth::routes();

    Route::group(['middleware' => ['auth']], function () 
    {
        // These routes need to be accessible in the initial setup and generally. 
        Route::get('/location/ajax/create/{parentID}/{title}', 'LocationController@ajaxCreate')->name('location.ajax-create')->middleware('check.permission:location.admin');
        Route::get('/location/ajax/delete/{id}', 'LocationController@ajaxDelete')->name('location.ajax-delete')->middleware('check.permission:location.admin');
        Route::get('/location/ajax/is-rentable/{id}/{trueOrFalse}', 'LocationController@ajaxIsRentable')->name('location.ajax-is-rentable')->middleware('check.permission:location.admin');
        Route::get('/location/ajax/update/{id}/{title}', 'LocationController@ajaxUpdate')->name('location.ajax-update')->middleware('check.permission:location.admin');
        Route::get('/location/json', 'LocationController@json')->name('location.json')->middleware('check.permission:location.admin');

        Route::group(['middleware' => ['initial.setup.lock']], function () 
        {
            Route::get('/initial-setup', 'InitialSetupController@start')->name('initial-setup');
            Route::get('/initial-setup/location', 'InitialSetupController@location')->name('initial-setup.location');
            Route::get('/initial-setup/resident', 'InitialSetupController@resident')->name('initial-setup.resident');
            Route::get('/initial-setup/staff', 'InitialSetupController@staff')->name('initial-setup.staff');
            Route::post('/initial-setup/location', 'InitialSetupController@locationPost')->name('initial-setup.location-post');
            Route::post('/initial-setup/resident', 'InitialSetupController@residentPost')->name('initial-setup.resident-post');
            Route::post('/initial-setup/staff', 'InitialSetupController@staffPost')->name('initial-setup.staff-post');
        });
        
        Route::group(['middleware' => ['initial.setup']], function () 
        {
            // Route::get('/session/appointment/{redirectRoute?}', 'SessionController@appointment')->name('session.appointment');
            // Route::post('/session/appointment', 'SessionController@appointmentPost')->name('session.appointment-post');
            // Route::get('/session/appointmentCheck/{redirectRoute?}', 'SessionController@appointmentCheck')->name('session.appointment-check');
            
            // Route::get('/appointment/mine/{id}', 'AppointmentController@mine')->name('appointment.mine');
            // Route::get('/appointment/mine', 'AppointmentController@mineAll')->name('appointment.mineall');
            // Route::get('/appointment/create', 'AppointmentController@create')->name('appointment.create');
            // Route::post('/appointment/create', 'AppointmentController@createPost')->name('appointment.create-post');
            // Route::get('/appointment/{id}/edit', 'AppointmentController@edit')->name('appointment.edit');
            // Route::get('/appointment/{id}', 'AppointmentController@show')->name('appointment.show');
            // Route::get('/appointment', 'AppointmentController@index')->name('appointment.index');

            Route::get('/care-home/resident/falls-assessment/{id}', 'CareHome\ResidentController@fallsAssessment')->name('care-home.resident.falls-assessment.view')->middleware('check.permission:resident.read');
            Route::get('/care-home/resident/create', 'CareHome\ResidentController@create')->name('care-home.resident.create')->middleware('check.permission:resident.create');
            Route::post('/care-home/resident/create', 'CareHome\ResidentController@createPost')->name('care-home.resident.create-post')->middleware('check.permission:resident.create');
            Route::get('/care-home/resident/{id}/edit/{page?}', 'CareHome\ResidentController@edit')->name('care-home.resident.edit')->middleware('check.permission:resident.edit');
            Route::post('/care-home/resident/{id}/edit', 'CareHome\ResidentController@editPost')->name('care-home.resident.edit-post')->middleware('check.permission:resident.edit');
            Route::get('/care-home/resident/{id}/finance/{page?}', 'CareHome\ResidentController@finance')->name('care-home.resident.finance')->middleware('check.permission:resident.edit');
            Route::post('/care-home/resident/{id}/finance', 'CareHome\ResidentController@financePost')->name('care-home.resident.finance-post')->middleware('check.permission:resident.edit');
            Route::get('/care-home/resident/{id}', 'CareHome\ResidentController@show')->name('care-home.resident.show')->middleware('check.permission:resident.read');
            Route::get('/care-home/resident/historic', 'CareHome\ResidentController@historic')->name('care-home.resident.historic')->middleware('check.permission:resident.read');
            Route::get('/care-home/resident', 'CareHome\ResidentController@index')->name('care-home.resident.index')->middleware('check.permission:resident.read');

            Route::get('/company', 'CompanyController@index')->name('company.index')->middleware('check.permission:company.admin');
            Route::post('/company', 'CompanyController@indexPost')->name('company.index-post')->middleware('check.permission:company.admin');

            // Route::get('/contact', 'ContactController@contact')->name('contact');

            Route::get('/complaint/create', 'ComplaintController@create')->name('complaint.create')->middleware('check.permission:complaint.create');
            Route::post('/complaint/create', 'ComplaintController@createPost')->name('complaint.create-post')->middleware('check.permission:complaint.create');
            Route::get('/complaint/mine', 'ComplaintController@mine')->name('complaint.mine')->middleware('check.permission:complaint.mine');
            Route::get('/complaint/{id}/edit', 'ComplaintController@edit')->name('complaint.edit')->middleware('check.permission:complaint.edit');
            Route::post('/complaint/{id}/edit', 'ComplaintController@editPost')->name('complaint.edit-post')->middleware('check.permission:complaint.edit');
            Route::get('/complaint/{id}', 'ComplaintController@show')->name('complaint.show')->middleware('check.permission:complaint.read');
            Route::get('/complaint', 'ComplaintController@index')->name('complaint.index')->middleware('check.permission:complaint.read');

            Route::get('/cost-code/create', 'CostCodeController@create')->name('cost-code.create');
            Route::post('/cost-code/create', 'CostCodeController@createPost')->name('cost-code.create-post');
            Route::get('/cost-code/{id}/edit', 'CostCodeController@edit')->name('cost-code.edit');
            Route::post('/cost-code/{id}/edit', 'CostCodeController@editPost')->name('cost-code.edit-post');
            Route::get('/cost-code/{id}', 'CostCodeController@show')->name('cost-code.show');
            Route::get('/cost-code', 'CostCodeController@index')->name('cost-code.index');

            Route::get('/customer/create', 'CustomerController@create')->name('customer.create')->middleware('check.permission:customer.create');
            Route::post('/customer/create', 'CustomerController@createPost')->name('customer.create-post')->middleware('check.permission:customer.create');
            Route::get('/customer/{id}/edit', 'CustomerController@edit')->name('customer.edit')->middleware('check.permission:customer.edit');
            Route::post('/customer/{id}/edit', 'CustomerController@editPost')->name('customer.edit-post')->middleware('check.permission:customer.edit');
            Route::get('/customer/{id}', 'CustomerController@show')->name('customer.show')->middleware('check.permission:customer.read');
            Route::get('/customer', 'CustomerController@index')->name('customer.index')->middleware('check.permission:customer.read');
            
            Route::post('/health-and-safety-incident/{id}/injury/{injuryID}/edit', 'HealthAndSafetyController@editInjuryPost')->name('health-and-safety-incident.edit-injury-post')->middleware('check.permission:health-and-safety-incident.edit-injury-post');
            Route::post('/health-and-safety-incident/{id}/injury/create', 'HealthAndSafetyController@createInjuryPost')->name('health-and-safety-incident.create-injury-post')->middleware('check.permission:health-and-safety-incident.create-injury-post');
            Route::get('/health-and-safety-incident/{id}/injury/{injuryID}/edit', 'HealthAndSafetyController@editInjury')->name('health-and-safety-incident.edit-injury')->middleware('check.permission:health-and-safety-incident.edit-injury');
            Route::get('/health-and-safety-incident/{id}/injury/create', 'HealthAndSafetyController@createInjury')->name('health-and-safety-incident.create-injury')->middleware('check.permission:health-and-safety-incident.create-injury');

            Route::get('/health-and-safety-incident/create', 'HealthAndSafetyController@create')->name('health-and-safety-incident.create')->middleware('check.permission:health-and-safety-incident.create');
            Route::post('/health-and-safety-incident/create', 'HealthAndSafetyController@createPost')->name('health-and-safety-incident.create-post')->middleware('check.permission:health-and-safety-incident.create');
            Route::get('/health-and-safety-incident/dashboard', 'HealthAndSafetyController@dashboard')->name('health-and-safety-incident.dashboard')->middleware('check.permission:health-and-safety-incident.dashboard');
            Route::get('/health-and-safety-incident/mine', 'HealthAndSafetyController@mine')->name('health-and-safety-incident.mine')->middleware('check.permission:health-and-safety-incident.mine');
            Route::get('/health-and-safety-incident/{id}/edit', 'HealthAndSafetyController@edit')->name('health-and-safety-incident.edit')->middleware('check.permission:health-and-safety-incident.edit');
            Route::post('/health-and-safety-incident/{id}/edit', 'HealthAndSafetyController@editPost')->name('health-and-safety-incident.edit-post')->middleware('check.permission:health-and-safety-incident.edit');
            Route::get('/health-and-safety-incident/{id}', 'HealthAndSafetyController@show')->name('health-and-safety-incident.show')->middleware('check.permission:health-and-safety-incident.read');
            Route::get('/health-and-safety-incident', 'HealthAndSafetyController@index')->name('health-and-safety-incident.index')->middleware('check.permission:health-and-safety-incident.read');

            Route::get('/hr/job/{id}', 'HRController@job')->name('hr.job');
            Route::post('/hr/job/{id}', 'HRController@jobPost')->name('hr.job-post');
            Route::get('/hr/job', 'HRController@jobs')->name('hr.jobs');
            Route::post('/hr/job', 'HRController@jobsPost')->name('hr.jobs-post');

            Route::get('/hr/pay-period/{id}', 'HRController@payPeriod')->name('hr.pay-period');
            Route::post('/hr/pay-period/{id}', 'HRController@payPeriodPost')->name('hr.pay-period-post');
            Route::get('/hr/pay-period', 'HRController@payPeriods')->name('hr.pay-periods');
            Route::post('/hr/pay-period', 'HRController@payPeriodsPost')->name('hr.pay-periods-post');
            
            Route::get('/hr/pay-year', 'HRController@payYears')->name('hr.pay-years');
            Route::post('/hr/pay-year', 'HRController@payYearsPost')->name('hr.pay-years-post');

            Route::get('/hr/team/{id}', 'HRController@team')->name('hr.team');
            Route::post('/hr/team/{id}', 'HRController@teamPost')->name('hr.team-post');
            Route::get('/hr/team', 'HRController@teams')->name('hr.teams');
            Route::post('/hr/team', 'HRController@teamsPost')->name('hr.teams-post');

            Route::get('/hr/working-time-pattern/{id}', 'HRController@workingTimePattern')->name('hr.working-time-pattern');
            Route::post('/hr/working-time-pattern/{id}', 'HRController@workingTimePatternPost')->name('hr.working-time-pattern-post');
            Route::get('/hr/working-time-pattern', 'HRController@workingTimePatterns')->name('hr.working-time-patterns');
            Route::post('/hr/working-time-pattern', 'HRController@workingTimePatternsPost')->name('hr.working-time-pattern-post');
            
            Route::get('/location', 'LocationController@index')->name('location.index')->middleware('check.permission:location.admin');
            Route::post('/location', 'LocationController@indexPost')->name('location.index-post')->middleware('check.permission:location.admin');
            
            // Route::get('/message/create', 'MessageController@create')->name('message.create');
            // Route::post('/message/create', 'MessageController@createPost')->name('message.create-post');
            // Route::post('/message/delete', 'MessageController@delete')->name('message.delete');
            // Route::get('/message/outbox', 'MessageController@outbox')->name('message.outbox');
            // Route::get('/message/bin', 'MessageController@bin')->name('message.bin');
            // Route::get('/message/{uuid}', 'MessageController@show')->name('message.show');
            // Route::get('/message', 'MessageController@inbox')->name('message.inbox');

            Route::post('/position/create', 'PositionController@createPost')->name('position.create-post');
            Route::get('/position/{id}/edit', 'PositionController@edit')->name('position.edit');
            Route::post('/position/{id}/edit', 'PositionController@editPost')->name('position.edit-post');
            Route::get('/position/{id}', 'PositionController@show')->name('position.show');
            Route::get('/position', 'PositionController@index')->name('position.index');
            
            Route::get('/role/create', 'RoleController@create')->name('role.create')->middleware('check.permission:role.admin');
            Route::post('/role/create', 'RoleController@createPost')->name('role.create-post')->middleware('check.permission:role.admin');
            Route::get('/role/{id}/edit', 'RoleController@edit')->name('role.edit')->middleware('check.permission:role.admin');
            Route::post('/role/{id}/edit', 'RoleController@editPost')->name('role.edit-post')->middleware('check.permission:role.admin');
            Route::get('/role', 'RoleController@index')->name('role.index')->middleware('check.permission:role.admin');

            Route::get('/ticket/create', 'TicketController@create')->name('ticket.create')->middleware('check.permission:ticket.create');
            Route::post('/ticket/create', 'TicketController@createPost')->name('ticket.create-post')->middleware('check.permission:ticket.create');
            Route::get('/ticket/mine', 'TicketController@mine')->name('ticket.mine')->middleware('check.permission:ticket.mine');
            Route::get('/ticket/{id}/edit', 'TicketController@edit')->name('ticket.edit')->middleware('check.permission:ticket.edit');
            Route::post('/ticket/{id}/edit', 'TicketController@editPost')->name('ticket.edit-post')->middleware('check.permission:ticket.edit');
            Route::get('/ticket/{id}', 'TicketController@show')->name('ticket.show')->middleware('check.permission:ticket.read');
            Route::get('/ticket', 'TicketController@index')->name('ticket.index')->middleware('check.permission:ticket.read');

            // Route::group(['middleware' => ['pick.appointment']], function () {
            //     Route::get('/timesheet/authorise', 'TimesheetController@authorise')->name('timesheet.authorise');
            //     Route::post('/timesheet/authorise', 'TimesheetController@authorisePost')->name('timesheet.authorise-post');
            //     Route::get('/timesheet/create/{date?}', 'TimesheetController@create')->name('timesheet.create');
            //     Route::post('/timesheet/create', 'TimesheetController@createPost')->name('timesheet.create-post');
            //     Route::get('/timesheet/mine', 'TimesheetController@mine')->name('timesheet.mine');
            // });

            Route::get('/user/create', 'UserController@create')->name('user.create')->middleware('check.permission:user.create');
            Route::post('/user/create', 'UserController@createPost')->name('user.create-post')->middleware('check.permission:user.create');
            Route::get('/user/my-account', 'UserController@myAccount')->name('user.my-account')->middleware('check.permission:user.mine');
            Route::post('/user/my-account', 'UserController@myAccountPost')->name('user.my-account-post')->middleware('check.permission:user.mine');
            Route::get('/user/{id}/edit', 'UserController@edit')->name('user.edit')->middleware('check.permission:user.edit');
            Route::post('/user/{id}/edit', 'UserController@editPost')->name('user.edit-post')->middleware('check.permission:user.edit');
            Route::get('/user/{id}', 'UserController@show')->name('user.show')->middleware('check.permission:user.read');
            Route::get('/user', 'UserController@index')->name('user.index')->middleware('check.permission:user.read');

            Route::get('/user-defined-field/create', 'UserDefinedFieldController@create')->name('user-defined-field.create')->middleware('check.permission:user-defined-field.create');
            Route::post('/user-defined-field/create', 'UserDefinedFieldController@createPost')->name('user-defined-field.create-post')->middleware('check.permission:user-defined-field.create');
            Route::get('/user-defined-field/{id}/edit', 'UserDefinedFieldController@edit')->name('user-defined-field.edit')->middleware('check.permission:user-defined-field.edit');
            Route::post('/user-defined-field/{id}/edit', 'UserDefinedFieldController@editPost')->name('user-defined-field.edit-post')->middleware('check.permission:user-defined-field.edit');
            Route::get('/user-defined-field/{id}', 'UserDefinedFieldController@show')->name('user-defined-field.show')->middleware('check.permission:user-defined-field.read');
            Route::get('/user-defined-field', 'UserDefinedFieldController@index')->name('user-defined-field.index')->middleware('check.permission:user-defined-field.read');

            Route::get('/workflow/create', 'WorkflowController@create')->name('workflow.create')->middleware('check.permission:workflow.admin');
            Route::post('/workflow/create', 'WorkflowController@createPost')->name('workflow.create-post')->middleware('check.permission:workflow.admin');
            Route::get('/workflow/{id}/edit', 'WorkflowController@edit')->name('workflow.edit')->middleware('check.permission:workflow.admin');
            Route::post('/workflow/{id}/edit', 'WorkflowController@editPost')->name('workflow.edit-post')->middleware('check.permission:workflow.admin');
            Route::get('/workflow/{id}', 'WorkflowController@show')->name('workflow.show')->middleware('check.permission:workflow.admin');
            Route::get('/workflow', 'WorkflowController@index')->name('workflow.index')->middleware('check.permission:workflow.admin');
            
            Route::get('/', 'HomeController@home')->name('home');
        });
    });
}

